﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CADLote
    {
        public List<CENDetLoteProxVencerProduct> obtenerLoteProxVencer(string codProducto)
        {
            List<CENDetLoteProxVencerProduct> datosDetLote = new List<CENDetLoteProxVencerProduct>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr;          //data reader
            CENDetLoteProxVencerProduct objDetLote = null;
            CADConexion CadCx = new CADConexion(); // Conexión

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_buscar_productoL_proxVencer", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@codProducto", SqlDbType.VarChar, 10).Value = codProducto.Trim();
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objDetLote = new CENDetLoteProxVencerProduct();
                    objDetLote.codLote = Convert.ToString(dr["codLote"]);
                    objDetLote.codProducto = Convert.ToString(dr["codProducto"]);
                    objDetLote.fechaVencimiento = Convert.ToDateTime(dr["fechaVencimiento"]).ToString("dd/MM/yyyy");
                    objDetLote.cantdayproximo = Convert.ToInt32(dr["cantdayproximo"]);
                    objDetLote.codProveedor = Convert.ToInt32(dr["codProveedor"]);
                    objDetLote.proveedor = Convert.ToString(dr["descripcion"]);
                    datosDetLote.Add(objDetLote);

                }
            }
            catch (Exception ex)
            {

                ex.StackTrace.ToString();
            }
            finally
            {
                con.Close();
            }

            return datosDetLote;


        }
       

    }
}
