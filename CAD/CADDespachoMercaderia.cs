﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
   public class CADDespachoMercaderia
    {
        public List<CENConceptos> ListarConcepto(int flag)
        {
            //DESCIPCIÓN: función para listar estados de la venta
            List<CENConceptos> listaConcepto = new List<CENConceptos>();
            CENConceptos objCENConceptos = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_datos_select_x_flag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@flag", SqlDbType.Int).Value = flag;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objCENConceptos = new CENConceptos();
                    objCENConceptos.correlativo = Convert.ToInt32(dr["correlativo"]);
                    objCENConceptos.descripcion = Convert.ToString(dr["descripcion"]);
                    listaConcepto.Add(objCENConceptos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return listaConcepto;
        }

        public List<CENDespachoCliente> BuscarCliente(string cliente)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            CENDespachoCliente CADobjCliente = null;
            List<CENDespachoCliente> lista = new List<CENDespachoCliente>();
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_cliente_venta", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_cadena", SqlDbType.VarChar, 50).Value = cliente.Trim();
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CADobjCliente = new CENDespachoCliente();
                    CADobjCliente.codCliente = Convert.ToInt32(dr["codCliente"]);
                    CADobjCliente.cliente = dr["cliente"].ToString();                   
                    lista.Add(CADobjCliente);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return lista;
        }
        public List<CENDespachoMercaderia> ListarVentaDespacho(CENDespachoDatos datos)
        {
            List<CENDespachoMercaderia> list_despacho = new List<CENDespachoMercaderia>();
            CENDespachoMercaderia CADobjListaVenta = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_despacho_filtros", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ntraVenta", SqlDbType.Int).Value = datos.ntraVent;
                cmd.Parameters.Add("@cliente", SqlDbType.Int).Value = datos.codCliente;
                cmd.Parameters.Add("@estado", SqlDbType.Int).Value = datos.estado;
                cmd.Parameters.Add("@codfechaEntregaI", SqlDbType.Char).Value = datos.codfechaEntregaI;
                cmd.Parameters.Add("@codfechaEntregaF", SqlDbType.Char).Value = datos.codfechaEntregaF;

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                   CADobjListaVenta = new CENDespachoMercaderia();
                   CADobjListaVenta.ntraVenta = Convert.ToInt32(dr["ntraVenta"]);
                   CADobjListaVenta.concatenado = Convert.ToString(dr["concatenado"]);
                   CADobjListaVenta.codCliente = Convert.ToInt32(dr["codCliente"]);
                   CADobjListaVenta.cliente = Convert.ToString(dr["cliente"]);
                   CADobjListaVenta.nroDocumento = Convert.ToString(dr["numDocumento"]);
                   CADobjListaVenta.puntoEntrega = Convert.ToInt32(dr["codPuntoEntrega"]);
                   CADobjListaVenta.direccion = Convert.ToString(dr["direccion"]);
                   CADobjListaVenta.fechaEntrega = Convert.ToDateTime(dr["fechaEntrega"]).ToString("dd/MM/yyyy");
                   CADobjListaVenta.horaEntrega = Convert.ToString(dr["horaEntrega"]);
                   CADobjListaVenta.codVendedor = Convert.ToInt32(dr["codVendedor"]);
                   CADobjListaVenta.vendedor = Convert.ToString(dr["vendedor"]);
                   CADobjListaVenta.codEstado = Convert.ToInt32(dr["estado"]);
                   CADobjListaVenta.Estado = Convert.ToString(dr["estadoV"]);            
                   list_despacho.Add(CADobjListaVenta);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return list_despacho;
        }

        public List<CENDespachoDetVenta> ListarDetalleVentaD( int codVenta)
        {
            List<CENDespachoDetVenta> CADListarDet = new List<CENDespachoDetVenta>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENDespachoDetVenta objDetVenta = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_det_venta", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@codVenta", SqlDbType.Int).Value = codVenta;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //Crear objeto Rutas
                    objDetVenta = new CENDespachoDetVenta();
                    objDetVenta.codVenta = Convert.ToInt32(dr["codVenta"]);
                    objDetVenta.codProducto = dr["codProducto"].ToString();
                    objDetVenta.descProduct = dr["descripcion"].ToString();
                    objDetVenta.cantP = Convert.ToInt32(dr["cantidadUnidadBase"]);
                    objDetVenta.codAlmacen = Convert.ToInt32(dr["codAlmacen"]);
                    objDetVenta.almacen = dr["almacen"].ToString();
                    CADListarDet.Add(objDetVenta);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return CADListarDet;

        }

        public int insertarMercaderia(CENRegistroDespachoM objRegistroDM)
        {
            int respuesta =0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registro_despacho_mercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_codVenta", objRegistroDM.codVenta);
                cmd.Parameters.AddWithValue("@p_codTransporte", objRegistroDM.codTransporte);
                cmd.Parameters.AddWithValue("@p_codAlmacen", objRegistroDM.codAlmacen);             
                cmd.Parameters.AddWithValue("@p_codProducto", objRegistroDM.codProduct);
                cmd.Parameters.AddWithValue("@p_codLote", objRegistroDM.codLote);
                cmd.Parameters.AddWithValue("@p_cantidad", objRegistroDM.cant);
                cmd.Parameters.AddWithValue("@p_usuario", objRegistroDM.usuario);           

                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                respuesta = Convert.ToInt32(cmd.Parameters["@resultado"].Value);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return respuesta;
        }
    }
}
