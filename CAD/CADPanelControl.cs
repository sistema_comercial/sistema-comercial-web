﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADPanelControl
    {





        public List<CENPanelControl> listadoCrecimiento(int flag)
        //DESCRIPCION:Retorna el crecimiento de las vetnas por dia,mes,año
        {

            List<CENPanelControl> list = new List<CENPanelControl>();
            CENPanelControl panel;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion cad = new CADConexion();

            try
            {
                con = new SqlConnection(cad.CxSQL());
                cmd = new SqlCommand("pa_obtener_crecimiento", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_flag", SqlDbType.Int).Value = flag;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    panel = new CENPanelControl();
                    panel.crecimiento = Convert.ToDouble(dr["crecimiento"].ToString());
                    panel.ventas = Convert.ToDouble(dr["ventas"].ToString());
                    panel.label = dr["fecha"].ToString();
                    list.Add(panel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();

            }

            return list;
        }

    }
}
