﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CAD
{
    public class CADEgresoMercaderia
    {

        public List<CENEgresoMercaderia> listadoEgresoMercaderia(string fechaEgreso,int codEgreso,int codMotivo) {
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            CENEgresoMercaderia cabecera = null;
            try {

                List<CENEgresoMercaderia> lista = new List<CENEgresoMercaderia>();

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_egresosMecaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_fechaEgreso", SqlDbType.VarChar).Value = fechaEgreso;
                cmd.Parameters.Add("@p_Egreso", SqlDbType.Int).Value = codEgreso;
                cmd.Parameters.Add("@p_motivo", SqlDbType.Int).Value = codMotivo;

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cabecera = new CENEgresoMercaderia();
                    cabecera.codEgreso = Convert.ToInt32(dr["codEgreso"]);
                    cabecera.fechaEgreso = Convert.ToDateTime(dr["fechaEgreso"]).ToString("dd/MM/yyyy");
                    cabecera.descMotivo = Convert.ToString(dr["descMotivo"]);
                    cabecera.motivo = Convert.ToInt32(dr["motivo"]);
                    cabecera.direccion = Convert.ToString(dr["direccion"]);
                    lista.Add(cabecera);
                }

                return lista;

            }
            catch (Exception ex) {
                throw ex;
            }
        }


        public List<CENDetalleEgreso> listEgresoDetalle(int codigo)
        {

            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            List<CENDetalleEgreso> lista = new List<CENDetalleEgreso>();
            CENDetalleEgreso detalle;
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_buscar_detalle_egresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_codEgreso", SqlDbType.Int).Value = codigo;

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    detalle = new CENDetalleEgreso();
                    detalle.ntraEgresoDetalle = Convert.ToInt32(dr["ntraDetalle"]);
                    detalle.ntraEgreso = Convert.ToInt32(dr["ntraEgreso"]);
                    detalle.codProducto = Convert.ToString(dr["codProducto"]);
                    detalle.descProducto = Convert.ToString(dr["descripcion"]);
                    detalle.lote = Convert.ToInt32(dr["lote"]);
                    detalle.cantidadProducto = Convert.ToInt32(dr["cantidad"]);
                    detalle.codAlmacen = Convert.ToInt32(dr["ntraAlmacen"]);
                    detalle.descAlmacen = Convert.ToString(dr["descAlmacen"]);
                    lista.Add(detalle);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lista;
        }

        public string editarEgreso(CENEgresoMercaderia cabecera, CENDetalleEgreso detallePedido)
        {
            string response = null;
            string xmlCabecera = ObjectToXMLGeneric<List<CENEgresoMercaderia>>(cabecera.listaEgreso);
            string xmlDetalle = ObjectToXMLGeneric<List<CENDetalleEgreso>>(detallePedido.listDetalleEgreso);
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_actualizar_egresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_cabecera", xmlCabecera);
                cmd.Parameters.AddWithValue("@p_detalle", xmlDetalle);
                cmd.Parameters.AddWithValue("@p_flag", CENConstante.g_const_2);

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;

        }

        public string registrarEgreso(CENEgresoMercaderia cabecera, CENDetalleEgreso detallePedido)
        {
            string response = null;
            string xmlCabecera = ObjectToXMLGeneric<List<CENEgresoMercaderia>>(cabecera.listaEgreso);
            string xmlDetalle = ObjectToXMLGeneric<List<CENDetalleEgreso>>(detallePedido.listDetalleEgreso);
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_actualizar_egresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_cabecera", xmlCabecera);
                cmd.Parameters.AddWithValue("@p_detalle", xmlDetalle);
                cmd.Parameters.AddWithValue("@p_flag", CENConstante.g_const_1);

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;

        }


        public string eliminarEgreso(int codIngreso)
        {
            string response = null;
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_eliminar_egresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_codEgreso", codIngreso);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;
        }


        public static String ObjectToXMLGeneric<T>(T filter)
        {
            //DESCRIPCION: CONVERTIR CLASS LIST EN CADENA XML
            string xml = null; // XML
            using (StringWriter sw = new StringWriter())
            {

                XmlSerializer xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, filter);
                try
                {
                    xml = sw.ToString();

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return xml;
        }
    }
}
