﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;

namespace CAD
{
    public class CADCuentaCobro
    {
        public CENCuentaCobro BuscarCuentaPorCobrar(int codVenta, int flag) {

            CENCuentaCobro objCxC = new CENCuentaCobro();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
           //    CENRutasAsignadasView objRutasAsignadas = null;

            CADConexion CadCx = new CADConexion();
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_busqueda_tipo_venta_pago", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_flagFiltro", SqlDbType.Int).Value = flag;
                cmd.Parameters.Add("@p_ntraVenta", SqlDbType.Int).Value = codVenta;
                con.Open();
                dr = cmd.ExecuteReader();

                //Crear objeto Cuentas por cobrar
                while (dr.Read())
                {
                    objCxC.ntra = Convert.ToInt32(dr["ntra"]);
                    objCxC.codOperacion = Convert.ToInt32(dr["codOperacion"]);
                    objCxC.importe = Convert.ToDecimal(dr["importe"]);
                    objCxC.fechaCobro = Convert.ToDateTime(dr["fechaCobro"].ToString());
                    objCxC.estado = Convert.ToInt16(dr["estado"]);
                    objCxC.tipoCambiov = Convert.ToDecimal(dr["tipoCambiov"]);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objCxC;

        }

        //-------------------------------------------------------------------------------------------
        public List<CENCuentas> ListarCuentasCobrar(int codCliente)
        {
            List<CENCuentas> listaCuentas = new List<CENCuentas>();
            CENCuentas objCuenta = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_cuentas_listar_pendiente", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@estado", SqlDbType.Int).Value = CENConstante.g_const_1;
                cmd.Parameters.Add("@codCliente", SqlDbType.Int).Value = codCliente;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objCuenta = new CENCuentas();
                    objCuenta.codOperacion = Convert.ToInt32(dr["codOperacion"]);
                    objCuenta.nombCli = Convert.ToString(dr["nombCli"]);
                    objCuenta.desModulo = Convert.ToString(dr["desModulo"]);
                    objCuenta.desPrefijo = Convert.ToString(dr["desPrefijo"]);
                    objCuenta.fechaTransaccion = Convert.ToDateTime(dr["fechaTransaccion"]).ToString("dd/MM/yyyy");
                    objCuenta.fechaCobro = Convert.ToDateTime(dr["fechaCobro"]).ToString("dd/MM/yyyy");
                    objCuenta.importe = Decimal.Round(Convert.ToDecimal(dr["importe"]), CENConstante.g_const_2);
                    objCuenta.nombVend = Convert.ToString(dr["nombVend"]);
                    objCuenta.nomRuta = Convert.ToString(dr["nomRuta"]);
                    objCuenta.direccion = Convert.ToString(dr["direccion"]);
                    objCuenta.desDocum = Convert.ToString(dr["desDocum"]);
                    objCuenta.desMoneda = Convert.ToString(dr["desMoneda"]);
                    objCuenta.tipoPersona = Convert.ToInt32(dr["tipoPersona"]);
                    objCuenta.identificacion = Convert.ToString(dr["identificacion"]);
                    objCuenta.codUbigeo = Convert.ToString(dr["codUbigeo"]);

                    objCuenta.plazo = 0;
                    objCuenta.nroCuotas = 0;
                    objCuenta.codPrestamo = 0;

                    listaCuentas.Add(objCuenta);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCuentas;
        }

        public List<CENCuentas> ListarLetrasCobrar(int codCliente)
        {
            List<CENCuentas> listaLetras = new List<CENCuentas>();
            CENCuentas objCuenta = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_cuentasLetras_listar_pendiente", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@estado", SqlDbType.Int).Value = CENConstante.g_const_1;
                cmd.Parameters.Add("@codCliente", SqlDbType.Int).Value = codCliente;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objCuenta = new CENCuentas();
                    objCuenta.codOperacion = Convert.ToInt32(dr["codVenta"]);
                    objCuenta.nombCli = Convert.ToString(dr["nombCli"]);
                    objCuenta.desModulo = "";
                    objCuenta.desPrefijo = "";
                    objCuenta.fechaTransaccion = Convert.ToDateTime(dr["fechaTransaccion"]).ToString("dd/MM/yyyy");
                    objCuenta.fechaCobro = "";
                    objCuenta.importe = Decimal.Round(Convert.ToDecimal(dr["importeTotal"]), CENConstante.g_const_2);
                    objCuenta.nombVend = Convert.ToString(dr["nombVend"]);
                    objCuenta.nomRuta = Convert.ToString(dr["nomRuta"]);
                    objCuenta.direccion = Convert.ToString(dr["direccion"]);
                    objCuenta.desDocum = Convert.ToString(dr["desDocum"]);
                    objCuenta.desMoneda = Convert.ToString(dr["desMoneda"]);
                    objCuenta.plazo = Convert.ToInt32(dr["plazo"]);
                    objCuenta.nroCuotas = Convert.ToInt32(dr["nroCuotas"]);
                    objCuenta.codPrestamo = Convert.ToInt32(dr["codPrestamo"]);
                    objCuenta.tipoPersona = Convert.ToInt32(dr["tipoPersona"]);
                    objCuenta.identificacion = Convert.ToString(dr["identificacion"]);
                    objCuenta.codUbigeo = Convert.ToString(dr["codUbigeo"]);

                    listaLetras.Add(objCuenta);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaLetras;
        }

        public List<CENCuotas> ListarCuotas(int codPrestamo)
        {
            List<CENCuotas> listaCuotas = new List<CENCuotas>();
            CENCuotas objCuota = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_cuentasLetras_detalle_cuotas", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@codPrestamo", SqlDbType.Int).Value = codPrestamo;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objCuota = new CENCuotas();
                    objCuota.codPrestamo = Convert.ToInt32(dr["codPrestamo"]);
                    objCuota.fechaPago = Convert.ToDateTime(dr["fechaPago"]).ToString("dd/MM/yyyy");
                    objCuota.nroCuota = Convert.ToInt32(dr["nroCuota"]);
                    objCuota.importe = Decimal.Round(Convert.ToDecimal(dr["importe"]), CENConstante.g_const_2);
                    objCuota.estCuota = Convert.ToString(dr["estCuota"]);
                    listaCuotas.Add(objCuota);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCuotas;
        }



    }
}
