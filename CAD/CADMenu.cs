﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADMenu
    {
        public List<CENMenu> cargarMantenedoresPorPerfil(int codUsuario, string modulo)
        //DESCRIPCION: lista los permisos del usuario 
        {

            CENMenu menu;
            List<CENMenu> dataMenu = new List<CENMenu>();
            CADConexion cnx = new CADConexion();
            SqlConnection con = null;
            SqlCommand cm;
            SqlDataReader dr;

            try
            {
                con = new SqlConnection(cnx.CxSQL());
                cm = new SqlCommand("pa_buscar_modulos", con);
                cm.CommandType = CommandType.StoredProcedure;
                cm.Parameters.Add("@p_codUser", SqlDbType.Int).Value = codUsuario;
                cm.Parameters.Add("@p_modulo", SqlDbType.VarChar).Value = modulo;
                cm.Parameters.Add("@p_flag", SqlDbType.Int).Value = CENConstante.g_const_3;
                con.Open();
                dr = cm.ExecuteReader();
                while (dr.Read())
                {
                    menu = new CENMenu();
                    menu.nomModulo = dr["nomMantenedor"].ToString();
                    menu.rutaSubMenu = dr["rutaMantenedor"].ToString();
                    dataMenu.Add(menu);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }



            return dataMenu;
        }

        public List<CENMenu> cargarMantenedoresPorPerfil(int codUsuario, int codmodulo)
        //DESCRIPCION: lista los permisos del usuario 
        {

            CENMenu menu;
            List<CENMenu> dataMenu = new List<CENMenu>();
            CADConexion cnx = new CADConexion();
            SqlConnection con = null;
            SqlCommand cm;
            SqlDataReader dr;

            try
            {
                con = new SqlConnection(cnx.CxSQL());
                cm = new SqlCommand("pa_buscar_modulos", con);
                cm.CommandType = CommandType.StoredProcedure;
                cm.Parameters.Add("@p_codUser", SqlDbType.Int).Value = codUsuario;
                cm.Parameters.Add("@p_modulo", SqlDbType.VarChar).Value = codmodulo;
                cm.Parameters.Add("@p_flag", SqlDbType.Int).Value = CENConstante.g_const_4;
                con.Open();
                dr = cm.ExecuteReader();
                while (dr.Read())
                {
                    menu = new CENMenu();
                    menu.nomSubMenu = dr["nomMantenedor"].ToString();
                    menu.rutaSubMenu = dr["rutaMantenedor"].ToString();
                    dataMenu.Add(menu);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }



            return dataMenu;
        }

        public List<CENModulo> cargarModulos(int codUsuario)
        //DESCRIPCION:Lista todos los modulos registrados del perfil asociado al usuario
        {
            CADConexion con = new CADConexion();
            SqlConnection sqlcon = null;
            SqlCommand cmd;
            SqlDataReader dr;
            List<CENModulo> modulos = new List<CENModulo>();
            CENModulo modulo;
            try
            {

                sqlcon = new SqlConnection(con.CxSQL());
                cmd = new SqlCommand("pa_buscar_modulos", sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_codUser", SqlDbType.Int).Value = codUsuario;
                cmd.Parameters.Add("@p_modulo", SqlDbType.VarChar).Value = "";
                cmd.Parameters.Add("@p_flag", SqlDbType.Int).Value = CENConstante.g_const_1;
                sqlcon.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    modulo = new CENModulo();
                    modulo.codModulo = Convert.ToInt16(dr["codModulo"].ToString());
                    modulo.descripcion = dr["nomModulo"].ToString();

                    modulos.Add(modulo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlcon.Close();
            }

            return modulos;
        }

    }
}
