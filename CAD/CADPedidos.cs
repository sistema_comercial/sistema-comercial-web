﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CAD
{
    public class CADPedidos
    {
        public List<CENCabeceraPedido> listaPedido(string fechaPedido, string fechaEntrega, string codProducto, int codProvedor,int comprobante) {

            List<CENCabeceraPedido> listaPedido = new List<CENCabeceraPedido>();

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENCabeceraPedido cabecera = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_pedidos", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_fechaPedido", SqlDbType.Char).Value = fechaPedido;
                cmd.Parameters.Add("@p_fechaEntrega", SqlDbType.Char).Value = fechaEntrega;
                cmd.Parameters.Add("@p_pedido", SqlDbType.NVarChar).Value = codProducto;
                cmd.Parameters.Add("@p_proveedor", SqlDbType.Int).Value = codProvedor;
                cmd.Parameters.Add("@p_comprobante", SqlDbType.Int).Value = comprobante;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cabecera = new CENCabeceraPedido();
                    cabecera.codPedido = Convert.ToInt32(dr["ntraPedido"]);
                    cabecera.fechaPedido = Convert.ToDateTime(dr["fechaPedido"]).ToString("dd/MM/yyyy");
                    cabecera.fechaEntrega = Convert.ToDateTime(dr["fechaEntrada"]).ToString("dd/MM/yyyy");
                    cabecera.codProveedor = Convert.ToInt32(dr["codProveedor"]);
                    cabecera.descProveedor = dr["proveedor"].ToString();
                    cabecera.tipoComprobante = Convert.ToInt32(dr["tipoComprobante"]);
                    cabecera.decrTipoComprobante = dr["descripcionConprobante"].ToString();
                    listaPedido.Add(cabecera);
                }
            }
            catch (Exception e)
            {
                e.StackTrace.ToString();
            }
            finally
            {
                con.Close();
            }

            return listaPedido;

        }


        public string guardarPedido(CENCabeceraPedido cabecera, CENDetallePedido detallePedido) {
            string response = null;
            string xmlCabecera = ObjectToXMLGeneric<List<CENCabeceraPedido>>(cabecera.listaPedido);
            string xmlDetalle = ObjectToXMLGeneric<List<CENDetallePedido>>(detallePedido.listDetallePedido);
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_actualizar_pedido", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_cabecera", xmlCabecera);
                cmd.Parameters.AddWithValue("@p_detalle", xmlDetalle);
                cmd.Parameters.AddWithValue("@p_flag", CENConstante.g_const_1);

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex) {
                throw ex;
            }
            finally {

                con.Close();
            }

            return response;

        }

        public string editarPedido(CENCabeceraPedido cabecera, CENDetallePedido detallePedido)
        {
            string response = null;
            string xmlCabecera = ObjectToXMLGeneric<List<CENCabeceraPedido>>(cabecera.listaPedido);
            string xmlDetalle = ObjectToXMLGeneric<List<CENDetallePedido>>(detallePedido.listDetallePedido);
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_actualizar_pedido", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_cabecera", xmlCabecera);
                cmd.Parameters.AddWithValue("@p_detalle", xmlDetalle);
                cmd.Parameters.AddWithValue("@p_flag", CENConstante.g_const_2);

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

            return response;

        }


        public List<CENDetalleAlmacen> listAlmacenes(int flag) {
            List<CENDetalleAlmacen> lista = new List<CENDetalleAlmacen>();
            CENDetalleAlmacen obj;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_datos_select_x_flag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    obj = new CENDetalleAlmacen();
                    obj.transaccion = Convert.ToInt32(dr["correlativo"]);
                    obj.descAlmacen = Convert.ToString(dr["descripcion"].ToString());
                    lista.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return lista;

        }

        public string eliminarPedido(int codPedido) {
            string response = null;
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_eliminar_pedido", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_codPedido", codPedido);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;
        }


        public static String ObjectToXMLGeneric<T>(T filter)
        {
            //DESCRIPCION: CONVERTIR CLASS LIST EN CADENA XML
            string xml = null; // XML
            using (StringWriter sw = new StringWriter())
            {

                XmlSerializer xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, filter);
                try
                {
                    xml = sw.ToString();

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return xml;
        }


        public List<CENDetallePedido> listaDetalle(int codigoPedido) {
            List<CENDetallePedido> lista = new List<CENDetallePedido>();
            CENDetallePedido obj;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CADConexion CadCx = new CADConexion();
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_buscar_detalle_pedido", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_codPedido", codigoPedido);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    obj = new CENDetallePedido();
                    obj.codDetalle = Convert.ToInt32(dr["ntraDetallePedido"]);
                    obj.codCabecera = Convert.ToInt32(dr["ntraPedido"]);
                    obj.codProducto = Convert.ToString(dr["codProducto"]);
                    obj.descProducto = Convert.ToString(dr["descripcion"]);
                    obj.cantidadProducto = Convert.ToInt32(dr["cantidadProducto"]);
                    obj.precioCompra = Convert.ToSingle(dr["precioCompra"]);
                    obj.almacenDestino = Convert.ToInt32(dr["ntraAlmacen"]);
                    obj.descAlmacen = Convert.ToString(dr["descripcionAlmacen"]);
                    lista.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return lista;

        }

    
    }


}
