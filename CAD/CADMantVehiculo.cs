﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CEN;
using System.IO;

namespace CAD
{
    public class CADMantVehiculo
    {
        public int RegistrarVehiculo(CENMantVehiculo objSO)
        {
            int response = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_vehiculo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@placa", objSO.idPlaca);
                cmd.Parameters.AddWithValue("@marca", objSO.idMarca);
                cmd.Parameters.AddWithValue("@modelo", objSO.idModelo);
                cmd.Parameters.AddWithValue("@peso", objSO.idPeso);
                cmd.Parameters.AddWithValue("@fabricacion", objSO.idFabricacion);
                cmd.Parameters.AddWithValue("@tipovehiculo", objSO.idTipoVehiculo);
                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                response = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<CENMantVehiculo> ObtenerVehiculo(CENMantVehiculo objtSOP)
        {
            List<CENMantVehiculo> ListaRA = new List<CENMantVehiculo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENMantVehiculo objMSO = null;

            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_vehiculos", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@placa", SqlDbType.VarChar, 10).Value = objtSOP.idPlaca;
                cmd.Parameters.Add("@marca", SqlDbType.VarChar, 30).Value = objtSOP.idMarca;
                cmd.Parameters.Add("@modelo", SqlDbType.VarChar, 30).Value = objtSOP.idModelo;
                cmd.Parameters.Add("@peso", SqlDbType.Int).Value = objtSOP.idPeso;
                cmd.Parameters.Add("@fabricacion", SqlDbType.Int).Value = objtSOP.idFabricacion;
                cmd.Parameters.Add("@tipovehiculo", SqlDbType.VarChar, 30).Value = objtSOP.idTipoVehiculo;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //Crear objeto Rutas
                    objMSO = new CENMantVehiculo();
                    objMSO.idVehiculo = Convert.ToInt32(dr["idvehiculo"]);
                    objMSO.idPlaca = dr["placa"].ToString();
                    objMSO.idMarca = dr["marca"].ToString();
                    objMSO.idModelo = dr["modelo"].ToString();
                    objMSO.idPeso = Convert.ToInt32(dr["peso"]);
                    objMSO.idFabricacion = Convert.ToInt32(dr["fabricacion"]);
                    objMSO.idTipoVehiculo = dr["tipovehiculo"].ToString();
                    ListaRA.Add(objMSO);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ListaRA;
        }

        public int EliminarVehiculo(string CODIGO)
        {
            int response = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_eliminar_vehiculo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cod", CODIGO);

                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                response = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public int ModificarVehiculo(CENMantVehiculo objSO)
        {
            int response = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_modificar_vehiculo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", objSO.idVehiculo);
                cmd.Parameters.AddWithValue("@placa", objSO.idPlaca);
                cmd.Parameters.AddWithValue("@marca", objSO.idMarca);
                cmd.Parameters.AddWithValue("@modelo", objSO.idModelo);
                cmd.Parameters.AddWithValue("@peso", objSO.idPeso);
                cmd.Parameters.AddWithValue("@fabricacion", objSO.idFabricacion);
                cmd.Parameters.AddWithValue("@tipovehiculo", objSO.idTipoVehiculo);
                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                response = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<CENMantConductorVehiculo> ListarConductores(int Jvehiculo)
        {
            List<CENMantConductorVehiculo> ListaRA = new List<CENMantConductorVehiculo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENMantConductorVehiculo objMSO = null;

            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_conductores", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@idVehiculo", SqlDbType.Int).Value = Jvehiculo;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //Crear objeto Rutas
                    objMSO = new CENMantConductorVehiculo();
                    objMSO.id = Convert.ToInt64(dr["id"]);
                    objMSO.idNombre = dr["nombrecompleto"].ToString();
                    ListaRA.Add(objMSO);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ListaRA;
        }
        
        public int RegistrarConductores(CENMantConductorVehiculo objSO)
        {
            int response = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_conductor_vehiculo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idvehiculo", objSO.id);
                cmd.Parameters.AddWithValue("@idpersona", objSO.idPers);
                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                response = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<CENMantConductorVehiculo> ListarConductoresAsignados(int Jvehiculo)
        {
            List<CENMantConductorVehiculo> ListaRA = new List<CENMantConductorVehiculo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENMantConductorVehiculo objMSO = null;

            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_conductores_asignados", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@idVehiculo", SqlDbType.Int).Value = Jvehiculo;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objMSO = new CENMantConductorVehiculo();
                    objMSO.id = Convert.ToInt64(dr["id"]);
                    objMSO.idNombre = dr["nombrecompleto"].ToString();
                    ListaRA.Add(objMSO);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ListaRA;
        }

        public int EliminarRegistro(string CODIGO)
        {
            int response = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_eliminar_vehiculo_asignado", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cod", CODIGO);

                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                response = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }
    }
}
