﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CEN;

namespace CAD
{
    public class CADTrabajador
    {
        public int registrarTrabajador(CENTrabajador data)
        {
            //DESCRIPCION: Registrar Trabajador
            int respuesta = 0;
            //int codPersona = 0;
            CADConexion CadCx = new CADConexion();
            SqlDataReader dr; //Data reader

            try
            {
                using (SqlConnection connection = new SqlConnection(CadCx.CxSQL()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("pa_registrar_trabajador", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@p_codigo", SqlDbType.Int).Value = data.codPersona;
                        command.Parameters.Add("@p_tipoPersona", SqlDbType.TinyInt).Value = data.tipoPersona;
                        command.Parameters.Add("@p_tipoDocumento", SqlDbType.TinyInt).Value = data.tipoDocumento;
                        command.Parameters.Add("@p_numDocumento", SqlDbType.VarChar, CENConstante.g_const_15).Value = data.numeroDocumento;
                        command.Parameters.Add("@p_ruc", SqlDbType.VarChar, CENConstante.g_const_15).Value = data.ruc;
                        command.Parameters.Add("@p_nombres", SqlDbType.VarChar, CENConstante.g_const_30).Value = data.nombres.ToUpper();
                        command.Parameters.Add("@p_apePaterno", SqlDbType.VarChar, CENConstante.g_const_20).Value = data.apellidoPaterno.ToUpper();
                        command.Parameters.Add("@p_apeMaterno", SqlDbType.VarChar, CENConstante.g_const_20).Value = data.apellidoMaterno.ToUpper();
                        command.Parameters.Add("@p_fechaNac", SqlDbType.Date).Value = data.fechaNacimiento;
                        command.Parameters.Add("@p_direccion", SqlDbType.VarChar, CENConstante.g_const_200).Value = data.direccion.ToUpper();
                        command.Parameters.Add("@p_correo", SqlDbType.VarChar, CENConstante.g_const_60).Value = data.correo.ToUpper();
                        command.Parameters.Add("@p_telefono", SqlDbType.VarChar, CENConstante.g_const_15).Value = data.telefono;
                        command.Parameters.Add("@p_celular", SqlDbType.Char, CENConstante.g_const_9).Value = data.celular;
                        command.Parameters.Add("@p_ubigeo", SqlDbType.Char, CENConstante.g_const_6).Value = data.codUbigeo;

                        command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CENConstante.g_const_20).Value = "";

                        command.Parameters.Add("@p_estadoCivil", SqlDbType.SmallInt).Value = data.estadoCivil;
                        command.Parameters.Add("@p_asignacionFamilia ", SqlDbType.SmallInt).Value = data.asignacionFamilia;
                        command.Parameters.Add("@p_area ", SqlDbType.SmallInt).Value = data.area;
                        command.Parameters.Add("@p_estadoTrabajador ", SqlDbType.SmallInt).Value = data.estadoTrabajador;
                        command.Parameters.Add("@p_tipoTrabajador ", SqlDbType.SmallInt).Value = data.tipoTrabajador;
                        command.Parameters.Add("@p_cargo ", SqlDbType.SmallInt).Value = data.cargo;
                        command.Parameters.Add("@p_formaPago ", SqlDbType.SmallInt).Value = data.formaPago;
                        command.Parameters.Add("@p_numeroCuenta ", SqlDbType.VarChar, CENConstante.g_const_16).Value = data.numeroCuenta;
                        command.Parameters.Add("@p_tipoRegimen ", SqlDbType.SmallInt).Value = data.tipoRegimen;
                        command.Parameters.Add("@p_regimenPensionario ", SqlDbType.SmallInt).Value = data.regimenPensionario;
                        command.Parameters.Add("@p_incioRegimen ", SqlDbType.Date).Value = data.inicioRegimen;
                        command.Parameters.Add("@p_bancoRemuneracion ", SqlDbType.SmallInt).Value = data.bancoRemuneracion;
                        command.Parameters.Add("@p_estadoPlanilla ", SqlDbType.SmallInt).Value = data.estadoPlanilla;
                        command.Parameters.Add("@p_modalidadContrato ", SqlDbType.SmallInt).Value = data.modalidadContrato;
                        command.Parameters.Add("@p_periodicidad ", SqlDbType.SmallInt).Value = data.periodicidad;
                        command.Parameters.Add("@p_inicioContrato ", SqlDbType.Date).Value = data.inicioContrato;
                        command.Parameters.Add("@p_finContrato ", SqlDbType.Date).Value = data.finContrato;
                        command.Parameters.Add("@p_fechaIngreso ", SqlDbType.Date).Value = data.fechaIngreso;
                        command.Parameters.Add("@p_sueldo ", SqlDbType.Money).Value = data.sueldo;

                        command.CommandTimeout = CENConstante.g_const_0;
                        dr = command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuesta = Convert.ToInt32(dr["mensaje"].ToString());
                            //codPersona = Convert.ToInt32(dr["codPersona"].ToString());
                        }
                    }
                    connection.Close();
                }
                return respuesta;
                //return codPersona;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENTrabajadorVIEW> ListarTrabajador(
            int codPersona, int codEstado, int codCargo)
        {
            List<CENTrabajadorVIEW> ListaTrabajador = new List<CENTrabajadorVIEW>();
            //int response = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENTrabajadorVIEW objTrabajador = null;
            CADConexion CadCx = new CADConexion();
            CAD_Consulta cad_consulta = new CAD_Consulta();

            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_trabajador", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@codPersona", SqlDbType.Int).Value = codPersona;
                cmd.Parameters.Add("@codEstado", SqlDbType.Int).Value = codEstado;
                cmd.Parameters.Add("@codCargo", SqlDbType.Int).Value = codCargo;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //Crear objeto trabajador
                    objTrabajador = new CENTrabajadorVIEW();
                    objTrabajador.codPersona = Convert.ToInt32(dr["codPersona"]);
                    objTrabajador.codTipoPersona = Convert.ToByte(dr["codTipoPersona"]);
                    objTrabajador.tipoPersona = dr["tipoPersona"].ToString();
                    objTrabajador.codTipoDocumento = Convert.ToByte(dr["codTipoDocumento"]);
                    objTrabajador.tipoDocumento = dr["tipoDocumento"].ToString();
                    objTrabajador.numeroDocumento = dr["numeroDocumento"].ToString();
                    objTrabajador.ruc = dr["ruc"].ToString();
                    //objTrabajador.razonSocial = dr["razonSocial"].ToString();
                    objTrabajador.nombres = dr["nombres"].ToString();
                    objTrabajador.apellidoPaterno = dr["apellidoPaterno"].ToString();
                    objTrabajador.apellidoMaterno = dr["apellidoMaterno"].ToString();
                    objTrabajador.fechaNacimiento = cad_consulta.ConvertFechaDateToString(DateTime.Parse(dr["fechaNacimiento"].ToString().Trim()));
                    objTrabajador.codEstadoCivil = Convert.ToInt16(dr["codEstadoCivil"]);
                    objTrabajador.estadoCivil = dr["estadoCivil"].ToString();
                    objTrabajador.codAsignacionFamiliar = Convert.ToInt16(dr["codAsignacionFamiliar"]);
                    objTrabajador.asignacionFamiliar = dr["asignacionFamiliar"].ToString();
                    objTrabajador.direccion = dr["direccion"].ToString();
                    objTrabajador.correo = dr["correo"].ToString();
                    objTrabajador.telefono = dr["telefono"].ToString();
                    objTrabajador.celular = dr["celular"].ToString();
                    objTrabajador.codUbigeo = dr["codUbigeo"].ToString();
                    objTrabajador.departamento = dr["departamento"].ToString();
                    objTrabajador.provincia = dr["provincia"].ToString();
                    objTrabajador.distrito = dr["distrito"].ToString();
                    objTrabajador.codArea = Convert.ToInt16(dr["codArea"]);
                    objTrabajador.area = dr["area"].ToString();
                    objTrabajador.codEstado = Convert.ToInt16(dr["codEstado"]);
                    objTrabajador.estado = dr["estado"].ToString();
                    objTrabajador.codTipoTrabajador = Convert.ToInt16(dr["codTipoTrabajador"]);
                    objTrabajador.tipoTrabajador = dr["tipoTrabajador"].ToString();
                    objTrabajador.codCargo = Convert.ToInt16(dr["codCargo"]);
                    objTrabajador.cargo = dr["cargo"].ToString();
                    objTrabajador.codFormaPago = Convert.ToInt16(dr["codFormaPago"]);
                    objTrabajador.formaPago = dr["formaPago"].ToString();
                    objTrabajador.numeroCuenta = dr["numeroCuenta"].ToString();
                    objTrabajador.codTipoRegimen = Convert.ToInt16(dr["codTipoRegimen"]);
                    objTrabajador.tipoRegimen = dr["tipoRegimen"].ToString();
                    objTrabajador.codRegimenPensionario = Convert.ToInt16(dr["codRegimenPensionario"]);
                    objTrabajador.regimenPensionario = dr["regimenPensionario"].ToString();
                    objTrabajador.inicioRegimen = cad_consulta.ConvertFechaDateToString(DateTime.Parse(dr["inicioRegimen"].ToString().Trim()));
                    objTrabajador.codBancoRemuneracion = Convert.ToInt16(dr["codBancoRemuneracion"]);
                    objTrabajador.bancoRemuneracion = dr["bancoRemuneracion"].ToString();
                    objTrabajador.codEstadoPlanilla = Convert.ToInt16(dr["codEstadoPlanilla"]);
                    objTrabajador.estadoPlanilla = dr["estadoPlanilla"].ToString();
                    objTrabajador.codModalidadContrato = Convert.ToInt16(dr["codModalidadContrato"]);
                    objTrabajador.modalidadContrato = dr["modalidadContrato"].ToString();
                    objTrabajador.codPeriodicidad = Convert.ToInt16(dr["codPeriodicidad"]);
                    objTrabajador.periodicidad = dr["periodicidad"].ToString();
                    objTrabajador.inicioContrato = cad_consulta.ConvertFechaDateToString(DateTime.Parse(dr["inicioContrato"].ToString().Trim()));
                    objTrabajador.finContrato = cad_consulta.ConvertFechaDateToString(DateTime.Parse(dr["finContrato"].ToString().Trim()));
                    objTrabajador.fechaIngreso = cad_consulta.ConvertFechaDateToString(DateTime.Parse(dr["fechaIngreso"].ToString().Trim()));
                    objTrabajador.sueldo = Convert.ToDecimal(dr["sueldo"]);

                    ListaTrabajador.Add(objTrabajador);
                }
            }

            catch (Exception e)
            {

                e.StackTrace.ToString();
            }
            finally
            {
                con.Close();
            }
            return ListaTrabajador;
        }
    }
}
