﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CAD
{
    public class CADIngresoMercaderia
    {

        public string guardarPedido(CENIngresoMercaderia cabecera, CENDetalleIngreso detallePedido)
        {
            string response = null;
            string xmlCabecera = ObjectToXMLGeneric<List<CENIngresoMercaderia>>(cabecera.listaIngreso);
            string xmlDetalle = ObjectToXMLGeneric<List<CENDetalleIngreso>>(detallePedido.listDetalleIngreso);
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_actualizar_ingresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_cabecera", xmlCabecera);
                cmd.Parameters.AddWithValue("@p_detalle", xmlDetalle);
                cmd.Parameters.AddWithValue("@p_flag", CENConstante.g_const_1);

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;

        }


        public string editarIngreso(CENIngresoMercaderia cabecera, CENDetalleIngreso detallePedido)
        {
            string response = null;
            string xmlCabecera = ObjectToXMLGeneric<List<CENIngresoMercaderia>>(cabecera.listaIngreso);
            string xmlDetalle = ObjectToXMLGeneric<List<CENDetalleIngreso>>(detallePedido.listDetalleIngreso);
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_registrar_actualizar_ingresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_cabecera", xmlCabecera);
                cmd.Parameters.AddWithValue("@p_detalle", xmlDetalle);
                cmd.Parameters.AddWithValue("@p_flag", CENConstante.g_const_2);

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;

        }



        public List<CENIngresoMercaderia> listaIngresos(string fechaIngreso, int ingreso, int proveedor, int motivo)
        {

            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            List<CENIngresoMercaderia> lista = new List<CENIngresoMercaderia>();
            CENIngresoMercaderia cabecera;
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_ingresos", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_fechaIngreso", SqlDbType.VarChar).Value = fechaIngreso;
                cmd.Parameters.Add("@p_ingreso", SqlDbType.Int).Value = ingreso;
                cmd.Parameters.Add("@p_proveedor", SqlDbType.Int).Value = proveedor;
                cmd.Parameters.Add("@p_motivo", SqlDbType.Int).Value = motivo;

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cabecera = new CENIngresoMercaderia();
                    cabecera.codIngreso = Convert.ToInt32(dr["ntraIngreso"]);
                    cabecera.fechaIngreso = Convert.ToDateTime(dr["fechaIngreso"]).ToString("dd/MM/yyyy");
                    cabecera.lote = Convert.ToInt32(dr["lote"]);
                    cabecera.descProveedor = Convert.ToString(dr["descProveedor"]);
                    cabecera.codProveedor = Convert.ToInt32(dr["proveedor"]);
                    cabecera.descMotivo = Convert.ToString(dr["descMotivo"]);
                    cabecera.motivo = Convert.ToInt32(dr["motivo"]);
                    lista.Add(cabecera);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lista;
        }


        public List<CENDetalleIngreso> listaIngresoDetalle(int codigo)
        {

            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();
            List<CENDetalleIngreso> lista = new List<CENDetalleIngreso>();
            CENDetalleIngreso detalle;
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_detalle_ingresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_codIngreso", SqlDbType.Int).Value = codigo;

                con.Open();

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    detalle = new CENDetalleIngreso();
                    detalle.codIngresoMercaderia = Convert.ToInt32(dr["ntraDetalle"]);
                    detalle.ntraIngreso = Convert.ToInt32(dr["codIngreso"]);
                    detalle.codProducto = Convert.ToString(dr["codProducto"]);
                    detalle.descProducto = Convert.ToString(dr["producto"]);
                    detalle.cantidadProducto = Convert.ToInt32(dr["cantidad"]);
                    detalle.fechaVencimiento = Convert.ToDateTime(dr["fechaVencimiento"]).ToString("dd/MM/yyyy");
                    detalle.codAlmacen = Convert.ToInt32(dr["codAlmacen"]);
                    detalle.descAlmacen = Convert.ToString(dr["almacen"]);
                    lista.Add(detalle);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lista;
        }



        public string eliminarIngreso(int codIngreso)
        {
            string response = null;
            SqlDataReader dr = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_eliminar_ingresoMercaderia", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_codIngreso", codIngreso);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToString(dr["mensaje"].ToString()) + "|" + Convert.ToString(dr["codigoError"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                con.Close();
            }

            return response;
        }


        public static String ObjectToXMLGeneric<T>(T filter)
        {
            //DESCRIPCION: CONVERTIR CLASS LIST EN CADENA XML
            string xml = null; // XML
            using (StringWriter sw = new StringWriter())
            {

                XmlSerializer xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, filter);
                try
                {
                    xml = sw.ToString();

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return xml;
        }

    }
}
