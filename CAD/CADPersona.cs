﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADPersona
    {
        public List<CENVendedor> listarVendedor(int sucursal)
        {
            List<CENVendedor> listaVendedor = new List<CENVendedor>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            CENVendedor objVendedor = null;
            CADConexion CadCx = new CADConexion();

            try
            {
                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_listar_vendedores", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@sucursal", SqlDbType.Int).Value = sucursal;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objVendedor = new CENVendedor();
                    objVendedor.codPersona = Convert.ToInt32(dr["ntraUsuario"]);
                    objVendedor.nombreCompleto = dr["vendedor"].ToString();
                    listaVendedor.Add(objVendedor);

                }
                return listaVendedor;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
