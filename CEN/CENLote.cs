﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENLote
    {
    }
    public class CENDetLoteProxVencerProduct
    {
        //DESCRIPCIÓN: atributos para la consulta de un producto en especifico proximo a vencer de acuerdo a un lote
        public string codLote { get; set; }
        public string codProducto { get; set; }
        public string fechaVencimiento { get; set; }
        public int cantdayproximo { get; set; }

        public int codProveedor { get; set; }
        public string proveedor { get; set; }
        public CENDetLoteProxVencerProduct()
        {

        }
        public CENDetLoteProxVencerProduct( string codProucto)
        {
            this.codProducto = codProducto;
        }
    }
    
}
