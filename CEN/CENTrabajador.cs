﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENTrabajador : CENPersona
    {
        public Int16 area { get; set; } 
        public Int16 estadoTrabajador { get; set; } 
        public Int16 tipoTrabajador { get; set; } 
        public Int16 cargo { get; set; }
        public Int16 formaPago { get; set; }
        public string numeroCuenta { get; set; }
        public Int16 tipoRegimen { get; set; }
        public Int32 regimenPensionario { get; set; } 
        public DateTime inicioRegimen { get; set; }
        public Int16 bancoRemuneracion { get; set; }
        public Int16 estadoPlanilla { get; set; }
        public Int16 modalidadContrato { get; set; }
        public Int16 periodicidad { get; set; }
        public DateTime inicioContrato { get; set; }
        public DateTime finContrato { get; set; }
        public DateTime fechaIngreso { get; set; }
        public float sueldo { get; set; }

    }

    public class CENTrabajadorVIEW
    {
        public Int32 codPersona { get; set; }
        public Byte codTipoPersona { get; set; }
        public string tipoPersona { get; set; }
        public Byte codTipoDocumento { get; set; }
        public string tipoDocumento { get; set; }
        public string numeroDocumento { get; set; }
        public string nombres { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string fechaNacimiento { get; set; }
        public string direccion { get; set; }
        public string correo { get; set; }
        public Int16 codEstadoCivil { get; set; }
        public string estadoCivil { get; set; }
        public string telefono { get; set; }
        public string celular { get; set; }
        public string ruc { get; set; }
        public Int16 codAsignacionFamiliar { get; set; }
        public string asignacionFamiliar { get; set; }
        public Int16 codArea { get; set; }
        public string area { get; set; }
        public Int16 codEstado { get; set; }
        public string estado { get; set;}
        public string razonSocial { get; set; }
        public Int16 codTipoTrabajador { get; set; }
        public string tipoTrabajador { get; set; }
        public Int16 codCargo { get; set; }
        public string cargo { get; set; }
        public string codUbigeo { get; set; }
        public string departamento { get; set; }
        public string provincia { get; set; }
        public string distrito { get; set; }
        public Int16 codFormaPago { get; set; }
        public string formaPago { get; set; }
        public string numeroCuenta { get; set; }
        public Int16 codTipoRegimen { get; set; }
        public string tipoRegimen { get; set; }
        public Int32 codRegimenPensionario { get; set; }
        public string regimenPensionario { get; set; }
        public string inicioRegimen { get; set; }
        public Int16 codBancoRemuneracion { get; set; }
        public string bancoRemuneracion { get; set; }
        public Int16 codEstadoPlanilla { get; set; }
        public string estadoPlanilla { get; set; }
        public Int16 codModalidadContrato { get; set; }
        public string modalidadContrato { get; set; }
        public Int16 codPeriodicidad { get; set; }
        public string periodicidad { get; set; }
        public string inicioContrato { get; set; }
        public string finContrato { get; set; }
        public string fechaIngreso { get; set; }
        public decimal sueldo { get; set; }
    }
}
