﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENDespachoMercaderia
    {
        public int ntraVenta { get; set; }
        public string concatenado { get; set; }
        public int codCliente { get; set; }
        public string cliente { get; set; }     
        public string nroDocumento { get; set; }
        public int puntoEntrega { get; set; }
        public string direccion { get; set; }
        public string fechaEntrega { get; set; }       
        public string horaEntrega { get; set; }
        public int codVendedor { get; set; }
        public string vendedor { get; set; }
        public int codEstado { get; set; }
        public string Estado { get; set; }

    }
    
    public class CENDespachoCliente
    {
        public int codCliente { get; set; }
        public string cliente { get; set; }
    }

    public class CENDespachoDatos
    {
        public int ntraVent { get; set; }
        public int codCliente { get; set; }
        public string codfechaEntregaI { get; set; }
        public string codfechaEntregaF { get; set; }
        public int estado { get; set; }

        public CENDespachoDatos()
        {

        }
        public CENDespachoDatos(int nroVenta, int codCliente, int estado, string codfechaEntregaI, string codFechaEntegaF )
        {
            this.ntraVent = nroVenta;
            this.codCliente = codCliente;
            this.estado = estado;
            this.codfechaEntregaI = codfechaEntregaI;
            this.codfechaEntregaF = codFechaEntegaF;
          
        }
    }

    public class CENDespachoDetVenta
    {
        public int codVenta { get; set; }
        public string codProducto { get; set; }
        public string descProduct { get; set; }
        public int cantP { get; set; }
        public int codAlmacen { get; set; }
        public string almacen { get; set; }
    }

    public class CENRegistroDespachoM
    {       
        public int codVenta { get; set;}
        public int codTransporte { get; set; }
        public int codAlmacen { get; set; }      
        public string codProduct { get; set; }
        public string codLote { get; set; }
        public int cant { get; set; }
        public string usuario { get; set; }
    
    }
}
