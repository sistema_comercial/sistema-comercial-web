﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENCabeceraPedido
    {
        //datos de cabecera
        public string fechaPedido { get; set; }
        public string fechaEntrega { get; set; }
        public int tipoComprobante { get; set; }
        public string decrTipoComprobante { get; set; }
        public int codPedido { get; set; }
        public int codProveedor { get; set; }
        public string descProveedor { get; set; }
        public string ip { get; set; }
        public string usuario { get; set; }
        public string mac { get; set; }

        public List<CENCabeceraPedido> listaPedido { get; set; }

        public CENCabeceraPedido() {
            listaPedido = new List<CENCabeceraPedido>();
        }


    }

    public class CENDetallePedido
    {

        //datos de detalle
        public int codCabecera { get; set; }
        public int codDetalle { get; set; }
        public string descProducto { get; set; }
        public string codProducto { get; set; }
        public int cantidadProducto { get; set; }
        public float precioCompra { get; set; }
        public int almacenDestino { get; set; }
        public string descAlmacen { get; set; }
        public int flagOperacion { get; set; }
        public string ip { get; set; }
        public string usuario { get; set; }
        public string mac { get; set; }


        public List<CENDetallePedido> listDetallePedido { set; get; }
    }
}
