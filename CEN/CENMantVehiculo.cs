﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENMantVehiculo
    {
        public Int32 idVehiculo { get; set; }
        public string idPlaca { get; set; }
        public string idMarca { get; set; }
        public string idModelo { get; set; }
        public Int32 idPeso { get; set; }
        public Int32 idFabricacion { get; set; }
        public string idTipoVehiculo { get; set; }
    }

    public class CENMantConductorVehiculo
    {
        public Int64 id { get; set; }
        public string idNombre { get; set; }
        public Int64 idPers { get; set; }
    }
}
