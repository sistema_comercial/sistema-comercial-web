﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENIngresoMercaderia
    {
        //datos de cabecera

        public string fechaIngreso { get; set; }
        public int codIngreso { get; set; }
        public int codProveedor { get; set; }
        public string descProveedor { get; set; }
        public int motivo { get; set; }
        public string descMotivo { get; set; }
        public int lote { get; set; }
        public string ip { get; set; }
        public string usuario { get; set; }
        public string mac { get; set; }

        public int flagOperacion { get; set; }

        public List<CENIngresoMercaderia> listaIngreso { get; set; }

        public CENIngresoMercaderia()
        {
            listaIngreso = new List<CENIngresoMercaderia>();
        }


    }

    public class CENDetalleIngreso
    {

        //datos de detalle
        public int ntraIngreso { get; set; }
        public int codIngresoMercaderia { get; set; }
        public string descProducto { get; set; }
        public string codProducto { get; set; }
        public int codAlmacen { get; set; }
        public string descAlmacen { get; set; }

        public string fechaVencimiento { get; set; }
        public int cantidadProducto { get; set; }
        public int flagOperacion { get; set; }
        public string ip { get; set; }
        public string usuario { get; set; }
        public string mac { get; set; }


        public List<CENDetalleIngreso> listDetalleIngreso { set; get; }

        public CENDetalleIngreso()
        {
            listDetalleIngreso = new List<CENDetalleIngreso>();
        }
    }
}
