﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    

    public class CENEgresoMercaderia { 
        //datos de cabecera

        public string fechaEgreso { get; set; }
        public int codEgreso { get; set; }
        public int motivo { get; set; }

        public string direccion { get; set; }
        public string descMotivo { get; set; }
        
        public string ip { get; set; }
        public string usuario { get; set; }
        public string mac { get; set; }

        public int flagOperacion { get; set; }

        public List<CENEgresoMercaderia> listaEgreso { get; set; }

        public CENEgresoMercaderia()
        {
            listaEgreso = new List<CENEgresoMercaderia>();
        }


    }

    public class CENDetalleEgreso
    {

        //datos de detalle
        public int ntraEgreso { get; set; }
        public int ntraEgresoDetalle { get; set; }
        public string descProducto { get; set; }
        public string codProducto { get; set; }
        public int codAlmacen { get; set; }
        public string descAlmacen { get; set; }
        public int lote { get; set; }
        public string fechaVencimiento { get; set; }
        public int cantidadProducto { get; set; }
        public int flagOperacion { get; set; }
        public string ip { get; set; }
        public string usuario { get; set; }
        public string mac { get; set; }


        public List<CENDetalleEgreso> listDetalleEgreso { set; get; }

        public CENDetalleEgreso()
        {
            listDetalleEgreso = new List<CENDetalleEgreso>();
        }
    }
}
