﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENCuentaCobro
    {
        public int ntra {get; set;}
        public int codOperacion { get; set; }
        public int codModulo {get; set;}
        public int prefijo  {get; set;}
        public int correlativo  {get; set;}
	    public decimal importe  {get; set;}
	    public DateTime fechaTransaccion {get; set;}
        public DateTime horaTransaccion {get; set;} 
        public DateTime fechaCobro {get; set;}        
        public Int16 estado {get; set;}
	    public string responsable {get; set;}
        public decimal tipoCambiov { get; set; }
       
    }

    //----------------------------------------------------------------
    public class CENCuentas
    {
        public int codOperacion { get; set; }
        public string nombCli { get; set; }
        public string desModulo { get; set; }
        public string desPrefijo { get; set; }
        public string fechaTransaccion { get; set; }
        public string fechaCobro { get; set; }
        public decimal importe { get; set; }
        public string nombVend { get; set; }
        public string nomRuta { get; set; }
        public string direccion { get; set; }
        public string desDocum { get; set; }
        public string desMoneda { get; set; }
        public int plazo { get; set; }
        public int nroCuotas { get; set; }
        public int codPrestamo { get; set; }
        public int tipoPersona { get; set; }
        public string identificacion { get; set; }
        public string codUbigeo { get; set; }
    }

    public class CENCuotas
    {
        public int codPrestamo { get; set; }
        public string fechaPago { get; set; }
        public int nroCuota { get; set; }
        public decimal importe { get; set; }
        public string estCuota { get; set; }
    }


}
