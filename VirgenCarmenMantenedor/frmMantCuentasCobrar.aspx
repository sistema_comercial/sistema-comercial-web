﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantCuentasCobrar.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantCuentasCobrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="Scripts/DataTable/datatables.min.css" />
    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="icon/style.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.min.css" />
    <link rel="stylesheet" href="css/mant_cuentaCobrar.css" type="text/css" media="screen, projection" />

    <script src="Scripts/DataTable/datatables.min.js"></script>
    <script src="Scripts/jqueryui/jquery-ui.min.js"></script>
    <script src="Scripts/sweetalert.min.js"></script>
    <script src="Scripts/cuentasCobrar_ajax.js"></script>

    <title>CUENTAS POR COBRAR</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid">
        <div class="box-header">
            <h3 class="box-title">MANTENEDOR DE CUENTAS POR COBRAR</h3>
        </div>
        <div class="form-group">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="pill" href="#menu1">CUENTAS POR COBRAR</a></li>
              <li><a data-toggle="pill" href="#menu2">LETRAS POR COBRAR</a></li>
            </ul>

            <div class="tab-content" >
              <div id="menu1" class="tab-pane fade active in">
                <div class="form-group">
                    <h4 class="box-filtros">LISTADO DE CUENTAS POR COBRAR</h4>
                </div>
                <div class="box-body table-responsive">
                    <table id="id_tblCuentas" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° OPE.</th>    <!-- 0 -->
                                <th>CLIENTE</th>
                                <th>MODULO</th>
                                <th>PREFIJO</th>
                                <th>FECHA PAGO</th>
                                <th>IMPORTE</th>    <!-- 5 -->
                                <th>PLAZO</th>
                                <th>RETRASO</th>
                                <th>ACCION</th>
                                <th>VENDEDOR</th>
                                <th>RUTA</th>    <!-- 10 -->
                                <th>PUNTO ENTREGA</th>
                                <th>DOCUMENTO</th>
                                <th>MONEDA</th>
                                <th>FECHA REG</th>
                            </tr>
                        </thead>
                        <tbody class="ui-sortable">
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>N° OPE.</th>    <!-- 0 -->
                                <th>CLIENTE</th>
                                <th>MODULO</th>
                                <th>PREFIJO</th>
                                <th>FECHA PAGO</th>
                                <th>IMPORTE</th>    <!-- 5 -->
                                <th>PLAZO</th>
                                <th>RETRASO</th>
                                <th>ACCION</th>
                                <th>VENDEDOR</th>
                                <th>RUTA</th>    <!-- 10 -->
                                <th>PUNTO ENTREGA</th>
                                <th>DOCUMENTO</th>
                                <th>MONEDA</th>
                                <th>FECHA REG</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
              </div>
              <div id="menu2" class="tab-pane fade">
                <div class="form-group">
                    <h4 class="box-filtros">LISTADO DE LETRAS POR COBRAR</h4>
                </div>
                <div class="box-body table-responsive">
                    <table id="id_tblLetras" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° OPE.</th>    <!-- 0 -->
                                <th>CLIENTE</th>
                                <th>FECHA REG.</th>
                                <th>IMPORTE</th>
                                <th>PLAZO</th>
                                <th>ACCION</th> <!-- 5 -->
                                <th>VENDEDOR</th>
                                <th>RUTA</th>
                                <th>PUNTO ENTREGA</th>
                                <th>DOCUMENTO</th>
                                <th>MONEDA</th> <!-- 10 -->
                                <th>CUOTAS</th>
                                <th>COD PRESTAMO</th>
                                <th>CONT</th>
                            </tr>
                        </thead>
                        <tbody class="ui-sortable">
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>N° OPE.</th>    <!-- 0 -->
                                <th>CLIENTE</th>
                                <th>FECHA REG.</th>
                                <th>IMPORTE</th>
                                <th>PLAZO</th>
                                <th>ACCION</th> <!-- 5 -->
                                <th>VENDEDOR</th>
                                <th>RUTA</th>
                                <th>PUNTO ENTREGA</th>
                                <th>DOCUMENTO</th>
                                <th>MONEDA</th> <!-- 10 -->
                                <th>CUOTAS</th>
                                <th>COD PRESTAMO</th>
                                <th>CONT</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>

    <!-- Ventana modal para ver detalle de cuentas por cobrar y letras por cobrar -->
    <div class="modal fade" id="modalCuenta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="id_tituloCuenta">DETALLE CUENTA POR COBRAR</h5>
                </div>
                <div class="modal-body">
                    <!-- Datos Generales -->
                    <div class="form-group">
                        <h4 class="box-filtros">Datos Generales</h4>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default col-xs-2" for="MVvendedor">Vendedor</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control " id="MVvendedor" readonly=""/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default col-xs-2" for="MVcliente">Cliente</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control " id="MVcliente" readonly=""/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default col-xs-2" for="MVruta">Ruta</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control " id="MVruta" readonly=""/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default col-xs-2" for="MVentrega">P. Entrega</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control " id="MVentrega" readonly=""/>
                        </div>
                    </div>
                    <!-- Datos de la cuenta -->
                    <div class="form-group">
                        <h4 class="box-filtros">Datos de la Cuenta</h4>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default  col-xs-2" for="MVventa">N° Operacion</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVventa" readonly=""/>
                        </div>
                        <label class="label label-default  col-xs-2" for="MVimporte">Importe</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVimporte" readonly=""/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default  col-xs-2" for="MVdocumento">Documento</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVdocumento" readonly=""/>
                        </div>
                        <label class="label label-default  col-xs-2" for="MVmoneda">moneda</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVmoneda" readonly=""/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default  col-xs-2" for="MVfechaReg">Fecha Reg.</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVfechaReg" readonly=""/>
                        </div>
                        <label class="label label-default  col-xs-2" for="MVfechaPag" id="label_fehc_cuota">Fecha Pago</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVfechaPag" readonly=""/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="label label-default  col-xs-2" for="MVplazo" id="label_plazo">Plazo</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVplazo" readonly=""/>
                        </div>
                        <div id="id_div_retraso">
                            <label class="label label-default  col-xs-2" for="MVretraso">Retraso</label>
                            <div class="col-xs-4">
                                <input type="text" class="form-control " id="MVretraso" readonly=""/>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group" id="id_div_modulo">
                        <label class="label label-default  col-xs-2" for="MVmodulo">Modulo</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVmodulo" readonly=""/>
                        </div>
                        <label class="label label-default  col-xs-2" for="MVprefijo">Prefijo</label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control " id="MVprefijo" readonly=""/>
                        </div>
                    </div>
                    <!-- Datos de las cuotas -->
                    <div class="form-group" id="id_div_cuota" style="margin-bottom:0">
                        <h4 class="box-filtros">Datos de las cuotas</h4>
                        <div class="box-body table-responsive">
                            <table id="id_tblCuota" class="table table-bordered table-hover" style="margin-bottom:0">
                                <thead>
                                    <tr><th>Cuota</th><th>Fecha Pago</th><th>Importe</th><th>Plazo Cuota</th><th>Retrazo</th><th>Estado</th></tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_ver">SALIR</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
