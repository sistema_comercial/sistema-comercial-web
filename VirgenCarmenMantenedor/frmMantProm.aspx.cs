﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEN;
using CLN;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantProm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                llenarCombos();
            }
        }


        public void llenarCombos()
        {

            CLNProveedor clnprov = new CLNProveedor();
            CLNPreventa clnpreven = new CLNPreventa();
            List<CENVendedor> ListVendedores = ListarVendedores();
            List<CENProveedor> ListaProveedor = clnprov.ListaProveedores(23);
            List<CENCamposPreventa> ListaClientes = clnpreven.ListarCampos(2);
            List<CENPromociones> ListaEstados = ListarEstadoPrmocion(28);
            List<CENConceptos> listaTipoVenta = listarTipoVenta();
            List<CENSeleccionar> listaProductos = ListarSeleccion(1);

            //vendedor.

            vendedor.Items.Clear();
            cbo_vendedor_reg.Items.Clear();

            vendedor.Items.Add(new ListItem("SELECCIONE VENDEDOR", "0"));
            cbo_vendedor_reg.Items.Add(new ListItem("SELECCIONE VENDEDOR", "0"));

            foreach (var itemVendedor in ListVendedores)
            {
                string item = itemVendedor.codPersona.ToString();
                var descripcion = itemVendedor.nombreCompleto;
                vendedor.Items.Add(new ListItem(descripcion, item));
                cbo_vendedor_reg.Items.Add(new ListItem(descripcion, item));
            }

            //producto

            producto_principal.Items.Clear();
            producto_principal.Items.Add(new ListItem("SELECCIONE PRODUCTO", "0"));

            foreach (var itemProducto in listaProductos)
            {
                string item = itemProducto.codigo;
                var descripcion = itemProducto.descripcion;
                producto_principal.Items.Add(new ListItem(descripcion, item));
            }

            //filtro_cliente

            filtro_cliente.Items.Clear();
            cbo_cliente_reg.Items.Clear();

            filtro_cliente.Items.Add(new ListItem("SELECCIONE CLIENTE", "0"));
            cbo_cliente_reg.Items.Add(new ListItem("SELECCIONE CLIENTE", "0"));


            foreach (var itemCliente in ListaClientes)
            {
                string item = itemCliente.codigo.ToString();
                var descripcion = itemCliente.descripcion;
                filtro_cliente.Items.Add(new ListItem(descripcion, item));
                cbo_cliente_reg.Items.Add(new ListItem(descripcion, item));

            }

            ////estado

            estado.Items.Clear();
            estado.Items.Add(new ListItem("SELECCIONE ESTADO", "0"));
            foreach (var itemEstado in ListaEstados)
            {
                string item = itemEstado.estado.ToString();
                var descripcion = itemEstado.desEstado;
                estado.Items.Add(new ListItem(descripcion, item));
            }

            ////id_proveedor

            id_proveedor.Items.Clear();
            id_proveedor.Items.Add(new ListItem("SELECCIONE PROVEEDOR", "0"));
            foreach (var itemProveedor in ListaProveedor)
            {
                string item = itemProveedor.codigoProveedor.ToString();
                var descripcion = itemProveedor.descproveedor;
                id_proveedor.Items.Add(new ListItem(descripcion, item));
            }

            ////tipoVenta
            tipoVenta.Items.Clear();
            tipoVenta.Items.Add(new ListItem("SELECCIONE TIPO VENTA", "0"));
            foreach (var itemTipo in listaTipoVenta)
            {
                string item = itemTipo.correlativo.ToString();
                var descripcion = itemTipo.descripcion;
                tipoVenta.Items.Add(new ListItem(descripcion, item));
            }
        }

        [WebMethod]
        public static List<CENVendedor> ListarVendedores()
        {
            List<CENVendedor> listVendedores = new List<CENVendedor>();
            try
            {
                listVendedores = new CLNPromociones().ListarVendedores(Convert.ToInt32(System.Web.HttpContext.Current.Session["sucursal"].ToString()));
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return listVendedores;
        }

        [WebMethod]
        public static List<CENPromociones> ListarEstadoPrmocion(int flag)
        {
            List<CENPromociones> listaCampos = null;
            CLNPromociones objCLNPromociones = null;
            try
            {
                objCLNPromociones = new CLNPromociones();
                listaCampos = objCLNPromociones.ListarEstadoPrmocion(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaCampos;
        }


        [WebMethod]
        public static List<CENPromocionesLista> ListarPromociones(string codfechaI, string codfechaF, int codProveedor, int codTipoVenta, string codProducto, int codVendedor, int codCliente, int codEstado)
        {
            List<CENPromocionesLista> lista_promociones = null;
            CENPromociones objCENPromocionesDatos = null;
            CLNPromociones objCLNPromociones = null;

            try
            {
                objCLNPromociones = new CLNPromociones();
                objCENPromocionesDatos = new CENPromociones(codfechaI, codfechaF, codProveedor, codTipoVenta, codProducto, codVendedor, codCliente, codEstado);
                lista_promociones = objCLNPromociones.ListarPromociones(objCENPromocionesDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lista_promociones;
        }


        [WebMethod]
        public static int EliminarPromocion(int codPromocion)
        {
            CLNPromociones objCLNPromocion = null;

            CENPromocionesLista objtPromociones = new CENPromocionesLista()
            {
                codPromocion = codPromocion,
            };
            try
            {
                objCLNPromocion = new CLNPromociones();
                int ok = objCLNPromocion.ElimiarPromocion(objtPromociones);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [WebMethod]
        public static List<CENDetallePromocion> ListarDetalle(int codPromocion)
        {
            List<CENDetallePromocion> listaDetalle = null;
            CLNPromociones objCLNPreventa = null;

            try
            {
                objCLNPreventa = new CLNPromociones();
                listaDetalle = objCLNPreventa.ListarDetalle(codPromocion);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return listaDetalle;
        }


        [WebMethod]

        public static string InsertarPromocion(List<CENPromocionesInsert> cabecera, List<CEN_Detalle_Flag_Promocion> detalle)
        {
            CLNPromociones objCLNProduct = new CLNPromociones();
            CEN_Detalle_Flag_Promocion objCENdetalle = new CEN_Detalle_Flag_Promocion();
            CENPromocionesInsert objProduc = new CENPromocionesInsert();
            objCENdetalle.listDetalle = detalle;
            objProduc.listProducto = cabecera;
            string resultado = null;

            try
            {
                resultado = objCLNProduct.InsertarPromociones(objProduc, objCENdetalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }







        [WebMethod]

        public static string ActualizarPromocion(List<CENPromocionesInsert> cabecera, List<CEN_Detalle_Flag_Promocion> detalle)
        {
            CLNPromociones objCLNProduct = new CLNPromociones();
            CEN_Detalle_Flag_Promocion objCENdetalle = new CEN_Detalle_Flag_Promocion();
            CENPromocionesInsert objProduc = new CENPromocionesInsert();
            objCENdetalle.listDetalle = detalle;
            objProduc.listProducto = cabecera;
            string resultado = null;

            try
            {
                resultado = objCLNProduct.actualizarPromocion(objProduc, objCENdetalle);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return resultado;
        }





        [WebMethod]
        public static int EliminarProductoActualizarPromocion(int cod, string codP)
        {

            CLNPromociones objCLNProductoEliminarDetalle = null;
            try
            {
                objCLNProductoEliminarDetalle = new CLNPromociones();
                int ok = objCLNProductoEliminarDetalle.EliminarProductoActualizarPromocion(cod, codP);
                return ok;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [WebMethod]
        public static List<CENProductolista> ListarProductosPromocion(string cadena)
        {
            List<CENProductolista> listProducto = null;
            CLNPromociones objCLNPromocion = null;
            try
            {
                objCLNPromocion = new CLNPromociones();
                listProducto = objCLNPromocion.ListarProductosPromocion(cadena);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return listProducto;
        }

        [WebMethod]
        public static List<CENSeleccionar> ListarSeleccion(int flag)
        {
            List<CENSeleccionar> lista = null;
            try
            {
                lista = new CLNConcepto().ListarSeleccion(flag);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return lista;
        }



        [WebMethod]
        List<CENConceptos> listarTipoVenta()
        {
            int flag = 21;
            List<CENConceptos> listaTipoVenta = null;
            CLNPromociones clnProm = new CLNPromociones();
            try
            {
                listaTipoVenta = clnProm.listarTipoVenta(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listaTipoVenta;
        }

    }
}