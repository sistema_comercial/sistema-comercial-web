﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEN;
using CLN;


namespace VirgenCarmenMantenedor
{
    public partial class frmDespachoMercaderia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<CENConceptos> ListarConcepto(int flag)
        {
            List<CENConceptos> listaConcepto = null;
            CLNDespachoMercaderia objCLNDespacho = null;
            try
            {
                objCLNDespacho = new CLNDespachoMercaderia();
                listaConcepto = objCLNDespacho.ListarConcepto(flag);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return listaConcepto;
        }
        [WebMethod]
        public static List<CENDespachoCliente> buscarCliente(string cliente)
        {
            List<CENDespachoCliente> listaCampos = null;
            CLNDespachoMercaderia objCLNDespacho = null;
            try
            {
                objCLNDespacho = new CLNDespachoMercaderia();
                listaCampos = objCLNDespacho.BuscarCliente(cliente);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return listaCampos;
        }

        [WebMethod]
        public static List<CENDespachoMercaderia> ListarVentaFiltro(string nroVenta, int codCliente, int estado, string codfechaEntregaI, string codFechaEntegaF)
        {
            List<CENDespachoMercaderia> ListVenta = null;
            CENDespachoDatos objCENDespacho = null;
            CLNDespachoMercaderia objCLNDespacho = null;
            int num = CENConstante.g_const_0;
            int Cventa = (int.TryParse(nroVenta, out num)) ? Int32.Parse(nroVenta) : num;


            try
            {
                objCLNDespacho = new CLNDespachoMercaderia();
                objCENDespacho = new CENDespachoDatos(Cventa, codCliente, estado, codfechaEntregaI, codFechaEntegaF);
                ListVenta = objCLNDespacho.ListarDatosPorFiltro(objCENDespacho);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListVenta;
        }

        [WebMethod]
        public static List<CENDespachoDetVenta> ListarDetVentaDespacho(int codVent)
        {
            List<CENDespachoDetVenta> listaDet = null;
            CLNDespachoMercaderia objCLNDet = null;

            try
            {
                objCLNDet = new CLNDespachoMercaderia();
                listaDet = objCLNDet.ListaDetVenta(codVent);
            }
            catch (Exception ex)
            {

                ex.StackTrace.ToString(); ;
            }
            return listaDet;
        }

        [WebMethod]
        public static List<CENDetLoteProxVencerProduct> ListaProductPorVencer(string codProducto)
        {
            List<CENDetLoteProxVencerProduct> listaDet = null;
            CLNLote objCLNDetLote = null;

            try
            {
                objCLNDetLote = new CLNLote();
                listaDet = objCLNDetLote.ListadoDetLoteProxVencer(codProducto);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return listaDet;
        }

        [WebMethod]
        public static int RegistroDespachoM(int codVent, int codTransp, int codAlmac, string codProduct, string codLote, int cant, string user)
        {
            CLNDespachoMercaderia objCLNDespachoM = null;
            CENRegistroDespachoM objDespacho = new CENRegistroDespachoM()
            {
                codVenta = Convert.ToInt32(codVent),
                codTransporte = Convert.ToInt32(codTransp),
                codAlmacen = Convert.ToInt32(codAlmac),              
                codProduct = codProduct,
                codLote = codLote,
                cant = Convert.ToInt32(cant),
                usuario = Convert.ToString(user)
               

            };
            try
            {
                objCLNDespachoM = new CLNDespachoMercaderia();
                int ok = objCLNDespachoM.RegistroDespachoMercaderia(objDespacho);
                return ok;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}