﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="VirgenCarmenMantenedor.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Login-Sistema de ventas</title>
        
        <%-- Validaciones --%>
        <script src="librerias/jquery-3.5.1.min.js"></script>
        <script src="librerias/jquery.validate.min.js"></script>
        <script src="librerias/additional-methods.min.js"></script>
        
        <%-- Alertas --%>   
        <link href="css/alertify.min.css" rel="stylesheet" />
        <script src="Scripts/alertify.min.js"></script>

        <%-- Estilos Pagina --%>
        <link href="css/login.css" rel="stylesheet" />
        
        <%-- Logica Pagina --%>
        <script src="Scripts/frmLogin.js"></script>
    </head>
    <body>
        <div id="id_bloquear" class="div_bloquear">Procesando ... espere por favor</div>            
        <div class="contener-formulario">
            <div class="izquierda">
                <div class="box-header">
                    <h3 class="box-title">SISTEMA COMERCIAL</h3>
                </div>
                <br />
                <br />
                <img src="imagenes/logo.jpg" class="imagen" alt="logo_virgen_del_carmen"/>   
            </div>
            <div class="derecha">
                <form class="formulario" id="form1" method="post" autocomplete="off">
                    <div>
                        <label class="label_form"  for="sucursal">Sucursal</label>
                        <br />
                        <select id="sucursal" class="cbo_form" runat="server">
                            <option value="0" runat="server">SELECCIONE SUCURSAL</option>
                        </select>
                    </div>
                    <div>
                        <label class="label_form"  for="exampleInputEmail1">Usuario</label>
                        <br />
                        <input type="text" class="input_form" id="exampleInputEmail1" placeholder="USUARIO" runat="server"/>
                    </div>
                    <div>
                        <label class="label_form"  for="exampleInputPassword1">Password</label>
                        <br />
                        <input type="password" class="input_form" id="exampleInputPassword1" placeholder="CONTRASEÑA" runat="server"/>
                    </div>
                    <button type="submit" class="btn_form" id="ingresar">Ingresar</button>
                    <br />
                </form>
            </div>
        </div>
    </body>
</html>
