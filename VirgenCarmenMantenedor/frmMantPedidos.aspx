﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantPedidos.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantCompras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Libreria Jquery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

     
    <link rel="stylesheet" href="icon/style.css" />
    
    <link rel="stylesheet" href="css/web_general.css" />
    <!-- Libreria Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
    <!-- librerias para fecha-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/moment@latest/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

     <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    

    <!-- Libreria Validaciones -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>

    <!-- Sweetalert -->
    <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="Scripts/frmMantPedidos.js"></script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container my-2">
        <h1 class="h3 text-center">Formulario de Pedidos</h1>
    </div>
    <div class="container my-4">
               
         <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="container">
                 <form class=" my-2 p-lg-3" id="form-busqueda">
                     <div class="form-row">                               
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="FechaEmision">Motivo de ingreso </label>
                                        <select class="form-control"  id="motivo" runat="server" ClientIDMode="Static"  name="motivo" ></select>
                                    </div>
                               </div>
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="FechaEntrega">Fecha Entrega</label>
                                        <input type="date" class="form-control" id="FechaEntrega" name="FechaEntrega" />
                                    </div>
                               </div>
                      </div>
                      <div class="form-row">
                           <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="codPedidoBusqueda">Codigo de pedido</label>
                                    <input type="text" class="form-control"  id="codPedidoBusqueda" name="codPedidoBusqueda" />
                                </div>
                           </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="proveedorBusqueda">Proveedor</label>
                                    <select class="form-control"  id="proveedorBusqueda" runat="server" ClientIDMode="Static"  name="proveedor" ></select>

                                </div>
                           </div>
                          <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="proveedorBusqueda">Tipo comprobante</label>
                                    <select class="form-control"  id="comprobante_busqueda" runat="server" ClientIDMode="Static"  name="comprobante_busqueda" ></select>

                                </div>
                           </div>
                        </div>
                  <div class="col-12 text-right">
                      <button type="button" class="btn rounded-0 shadow btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Crear nuevo Pedido</button>
                        <button type="button" class="btn rounded-0 shadow btn-outline-info" id="buscarPedido">Buscar pedido</button>
                  </div>
                </form>

             </div>
                <table class="table table-light text-center table-response" id="tblListado_pedidos">
                    <thead class="text-white bg-primary">
                        <tr>
                            <th>Código</th>
                            <th>Fecha Pedido</th>
                            <th>Fecha Entrega</th>
                            <th>codProveedor</th>
                            <th>Proveedor</th>
                            <th>codComprobante</th>
                            <th>Comprobante</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
          </div>

        <div class="modal fade shadow bd-example-modal-lg" tabindex="-1" data-backdrop="static"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="container ">
                 <div class="row mt-4">                     
                      <div class="col-sm-12  ">
                          <div class="card p-0 border-0">
                            <div class="card-body">
                                <form id="formularioGuardar">  
                                    <input type="hidden" id="codigoPedido"/>
                                     <div class="form-row">                                        
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label for="fechaPedido_save">Fecha Emision</label>
                                                <input type="date"  class="form-control" id="fechaPedido_save" name="fechaPedido_save" />
                                            </div>
                                        </div>
                                         <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label for="fechaEntrega_save">Fecha Entrega</label>
                                                <input type="date"  class="form-control" id="fechaEntrega_save" name="fechaEntrega_save" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label for="comprobante_save">Tipo de comprobante</label>
                                               <select class="form-control" id="comprobante_save" runat="server" name="comprobante_save" ClientIDMode="Static" ></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label for="proveedor_save" class="d-block"> Proveedor</label>
                                                  <select class="select2 "   id="proveedor_save" runat="server" name="proveedor_save" ClientIDMode="Static"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="col-sm-12 badge-info">
                                            <p class="text-center">Datos de producto</p>
                                        </div>
                                    <div class="form-group">
                                        <div class="col-12 p-0">
                                            <div class="form-group">
                                                <label for="producto_save">Producto

                                                </label>
                                               <select class="form-control select2"  id="producto_save" runat="server" name="producto_save" ClientIDMode="Static" ></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">                                        
                                        
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="precio_compra">Precio de compra</label>
                                                <input type="text" class="form-control" min="0" id="precio_compra" name="precio_compra"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="cantidad_save">cantidad</label>                
                                                <input type="text" class="form-control" min="0" id="cantidad_save" name="cantidad_save"/>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="form-row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="almacen_save">Almacen de destino
                                                </label>
                                               <select class="form-control select2" id="almacen_save" runat="server" name="almacen_save" ClientIDMode="Static" ></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn rounded-0 shadow btn-outline-primary" type="button" id="agregar">Agregar a detalle</button>
                                     </div>
                               </form> 
                            </div>
                         </div>
                      </div>
                     <div class="col-sm-12 ">
                         
                                      <table class="table table-light table-response" id="tablaPedidos">
                                        <thead class="text-white bg-primary">
                                            <tr>
                                                <th>codDetalle</th>
                                                <th>Código</th>
                                                <th>Producto</th>
                                                <th>Cantidad</th>
                                                <th>Precio Compra</th>
                                                <th>codAlmacen</th>
                                                <th>Almacen Destino</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tablaPedido">
                                        </tbody>
                                    </table>
                     </div>
                    <div class="col-12 text-right my-2">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         <input class="btn rounded-0 shadow btn-outline-success" id="guardar" type="submit" value="Guardar pedido">
                                    
                    </div>
                 </div>
             </div>
            </div>
          </div>
        </div>
    </div>
</asp:Content>
