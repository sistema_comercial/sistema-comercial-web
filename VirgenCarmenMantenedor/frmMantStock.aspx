﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantStock.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantStock1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>MANTENIMIENTO DE STOCK</title>

       <!-- Libreria Jquery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

     
    <link rel="stylesheet" href="icon/style.css" />
    
    <link rel="stylesheet" href="css/web_general.css" />
    <!-- Libreria Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
    <!-- librerias para fecha-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/moment@latest/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

     <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    

    <!-- Libreria Validaciones -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>

    <!-- Sweetalert -->
    <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


    <script src="Scripts/stock.js"></script>
    
    <script type="text/javascript">
        function HideMPEPopup() {
            $find("mdemodificardetalle").document;

        }
        function ValidateAndHideMPEPopup() {
            //  hide the Popup 
            if (Page_ClientValidate('AddNewFile')) {
                HideMPEPopup();
            }
        }

            function validarRefrescarPagina()
            {
                var cadenaAleatoria = generadorSecuenciaAleatoria();
                var campoOcultoCargaMasiva = document.getElementById("campoOcultoCargaMasiva"); 
                campoOcultoCargaMasiva.value = cadenaAleatoria;
            }   

            function generadorSecuenciaAleatoria() 
            { 
                var g = ""; 
                for(var i = 0; i < 32; i++) 
                g += Math.floor(Math.random() * 0xF).toString(0xF) 

                return g; 
            }

     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="container">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="container">
                 <form class="form p-lg-3" id="form-busqueda">
                     <div class="form-row my-2">
                         <div class="col-12">
                             <h1 class="text-center h3">Formulario de Stock</h1>
                         </div>
                     </div>
                     <div class="form-row">                               
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                       <label for="categoria_busqueda" >Categoria</label>
                                        <select class="form-control"  id="categoria_busqueda" ClientIDMode="Static"  runat="server"></select>
                                    </div>
                               </div>
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="subcategoria_busqueda">Sub categoria</label>
                                        <select class="form-control" id="subcategoria_busqueda"></select>
                                    </div>
                               </div>
                      </div>
                      <div class="form-row">
                           <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="fabricante_Busqueda">Fabicante</label>
                                    <select class="form-control"  id="fabricante_Busqueda" runat="server" ClientIDMode="Static"  name="motivoBusqueda" ></select>

                                </div>
                           </div>
                          <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="proveedor_busquead">Proveedor</label>
                                    <select class="form-control"  id="proveedor_busquead" runat="server" ClientIDMode="Static"  name="motivoBusqueda" ></select>

                                </div>
                           </div>
                        </div>
                     <div class="form-row">
                         <div class="col-12">
                             <div class="form-group">
                                 <label>Descripcion</label>
                                 <textarea class="form-control" maxlength="100" id="descripcion_busqueda"> </textarea>
                             </div>
                         </div>
                     </div>
                  <div class="col-12 text-right">
                      <button type="button" class="btn rounded-0 shadow btn-outline-info" id="buscarStock">Buscar stock</button>
             <button  type="button" class="btn rounded-0 shadow btn-outline-success" id="btnreporte"> Generar excel</button>

                  </div>
                </form>

             </div>
                <div class="col-12 text-right">
                       <h2 class="text-left ">Lista de precios</h2>
                </div>
            <div class="container mb-4">
                
                <table class="table table-light text-center table-response table-bordered" id="tblListado_stock" >
                    <thead class="text-white bg-primary">
                        <tr>
                            <th>codCategoria</th>
                            <th>Categoria</th>
                            <th>codSubCategoria</th>
                            <th>SubCategoria</th>
                            <th>codProveedor</th>
                            <th>Proveedor</th>
                            <th>codFabricante</th>
                            <th>Fabricante</th>
                            <th>codUnidadBase</th>
                            <th>UnidadBase</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
    </div>
  
         <div class="modal fade bd-example-modal-lg" tabindex="-1" data-backdrop="static"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="container ">
                     <div class="row mt-4">                     
                          <div class="col-sm-12  ">
                              <div class="card p-0 border-0">
                                <div class="card-body">
                                   <div class="container">
                                       <table class="table table-light text-center table-response" id="tblAlmacenes">
                                        <thead class="text-white bg-primary">
                                            <tr>
                                                <th>Transaccion</th>
                                                <th>Almacen</th>
                                                <th>Dirección</th>
                                                <th>codProducto</th>
                                                <th>Stock</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                  </div>
                                <button type="button" class="btn rounded-0 shadow btn-outline-secondary ml-auto" data-dismiss="modal">Close</button>
                             </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
</asp:Content>

