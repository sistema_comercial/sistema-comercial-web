﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantClientes.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantClientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="librerias/datatables.min.css" />
    <script src="librerias/datatables.min.js"></script>
    <script src="Scripts/frmMantClientes.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="title_frm">Mantenedor de clientes</div>
    <br />
    <div class="div_bloque">
        <div class="title_bloque">Filtros de Busqueda</div>
        <br />
        <label class="label_bloque" for="ndoc">N° Documento</label>
        <input type="text" class="input_bloque" id="ndoc" placeholder="DNI / RUC"/>
        <br />
        <label class="label_bloque" for="nomb">Nombres</label>
        <input type="text" class="input_bloque" id="nomb" placeholder="NOMBRES / APELLIDOS"/>
        <br />
        <label class="label_bloque" for="tper">Tipo Persona</label>
        <select class="cbo_bloque" id="tper" runat="server" ClientIDMode="Static" ><option>SELECCIONAR</option></select>
        <br />
        <button class="btn_bloque" id="btn_buscar">BUSCAR</button>
    </div>
    <br />

    <div class="div_table">
        <table id="tbl_clientes" class="table table-striped table-bordered" style='width: 100%;'>
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>T. PERSONA</th>
                    <th>N° DOCUMENTO</th>
                    <th>RUC</th>
                    <th>NOMBRES</th>
                    <th>RAZON SOCIAL</th>
                    <th>DIRECCION</th>
                    <th>CORREO</th>
                    <th>TELEFONO</th>
                    <th>CELULAR</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>
    </div>

    <!-- MODAL AGREGAR -->
    <button type="button" class="btn_general" id="btn_agregar" data-toggle="modal" data-target="#modalRegistrar" >AGREGAR</button>

    <div class="modal fade" id="modalRegistrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 750px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="div_bloque">
                        <div class="title_bloque">Registrar</div>
                        <br />
                        <label class="label_bloque">Tipo Persona</label>
                        <select class="cbo_bloque" id="tperR"><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">N° Documento</label>
                        <input type="text" class="input_bloque" id="ndocR"/>
                        <br />
                        <label class="label_bloque">Nombres</label>
                        <input type="text" class="input_bloque" id="nombR" />
                        <br />
                        <label class="label_bloque">Ap. Paterno</label>
                        <input type="text" class="input_bloque" id="apepR" />
                        <br />
                        <label class="label_bloque">Ap. Materno</label>
                        <input type="text" class="input_bloque" id="apemR" />
                        <br />
                        <label class="label_bloque">Razon Social</label>
                        <input type="text" class="input_bloque" id="razsR" />
                        <br />
                        <label class="label_bloque">Tipo Cliente</label>
                        <select class="cbo_bloque" id="tcliR"><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Departamento</label>
                        <select class="cbo_bloque" id="depaR" runat="server" ClienteIDMode="Static"><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Provincia</label>
                        <select class="cbo_bloque" id="provR" runat="server" ClienteIDMode="Static"><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Distrito</label>
                        <select class="cbo_bloque" id="distR" runat="server" ClienteIDMode="Static"><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Direccion</label>
                        <input type="text" class="input_bloque" id="direR" />
                        <br />
                        <label class="label_bloque">Correo</label>
                        <input type="text" class="input_bloque" id="corrR" />
                        <br />
                        <label class="label_bloque">Telefono</label>
                        <input type="text" class="input_bloque" id="telfR" />
                        <br />
                        <label class="label_bloque">Celular</label>
                        <input type="text" class="input_bloque" id="cellR" />
                        <br />
                        <div>
                            <div style="font-weight:bold">Direcciones de Entrega Adicionales</div>
                            <div class="div_table div_table_mas">
                                <!-- MODAL AGREGAR -->
                                <button type="button" class="btn_general btn_mas" id="btn_agregar_direccion" data-toggle="modal" data-target="#modalRegistrarDireccion" > Agregar </button>
                                <table id="tbl_direcciones_add" class="table table-striped table-bordered" style='width: 100%;'>
                                    <thead>
                                        <tr>
                                            <th>DEPARTAMENTO</th>
                                            <th>PROVINCIA</th>
                                            <th>DISTRITO</th>
                                            <th>DIRECCION</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br />
                        <button type="button" class="btn_bloque" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn_bloque" id="btnGuardarCliente">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalRegistrarDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 750px;">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="frmAgregarDireccion" class="div_bloque">
                        <div class="title_bloque">Agregar Direccion</div>
                        <br />
                        <label class="label_bloque">Departamento</label>
                        <select class="cbo_bloque" name='depaRD' id="depaRD" runat="server" ><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Provincia</label>
                        <select class="cbo_bloque" name="provRD" id="provRD" runat="server" ><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Distrito</label>
                        <select class="cbo_bloque" name="distRD" id="distRD" runat="server" ><option>SELECCIONAR</option></select>
                        <br />
                        <label class="label_bloque">Direccion</label>
                        <input type="text" class="input_bloque" name="direRD" id="direRD" />
                        <br />
                        <button type="button" class="btn_bloque" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn_bloque" id="btnGuardarDireccion">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
