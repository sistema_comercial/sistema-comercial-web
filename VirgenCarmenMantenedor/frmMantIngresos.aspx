﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantIngresos.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantIngresos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <!-- Libreria Jquery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

     
    <link rel="stylesheet" href="icon/style.css" />
    
    <link rel="stylesheet" href="css/web_general.css" />
    <!-- Libreria Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
    <!-- librerias para fecha-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/moment@latest/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

     <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    

    <!-- Libreria Validaciones -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>

    <!-- Sweetalert -->
    <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="Scripts/frmMantIngresos.js"></script>
    
    <style>
         .select2  {
            width:100% !important;
         }
         .select2-container .select2-selection--single.input-sm,.select2-container .select2-selection--single, .select2-container .select2-selection--single {
          border-radius: 3px;
          font-size: 16px;
          height: 38px;
          line-height: 1.5;
          padding: 5px 22px 5px 10px;
          /* 2 */
        }
        
     </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h2 class="text-center mt-2 text-uppercase">Ingreso de mercaderia</h2>
       
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="container">
                 <form class="form my-2 p-lg-3" id="form-busqueda">
                     <div class="form-row">                               
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="codigoIngresoBusqueda">Codigo de ingreso</label>
                                        <input type="text" class="form-control" id="codigoIngresoBusqueda" name="codigoIngresoBusqueda" />
                                    </div>
                               </div>
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="fechaIngreso">Fecha Ingreso</label>
                                        <input type="date" class="form-control" id="fechaIngreso" name="fechaIngreso" />
                                    </div>
                               </div>
                      </div>
                      <div class="form-row">
                           <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="motivoBusqueda">Motivo</label>
                                    <select class="form-control"  id="motivoBusqueda" runat="server" ClientIDMode="Static"  name="motivoBusqueda" ></select>

                                </div>
                           </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="proveedorBusqueda">Proveedor</label>
                                    <select class="form-control select2"  id="proveedorBusqueda" runat="server" ClientIDMode="Static"  name="proveedor" ></select>

                                </div>
                           </div>
                        </div>
                  <div class="col-12 text-right">
                      <button type="button" class="btn rounded-0 shadow btn-outline-primary" id="nuevoIngreso" data-toggle="modal" data-target=".bd-example-modal-lg">Nuevo ingreso</button>
                        <button type="button" class="btn rounded-0 shadow btn-outline-info" id="buscarIngreso">Buscar Ingreso</button>
                  </div>
                </form>

             </div>
                <table class="table table-light text-center table-response" id="tblListado_ingresos">
                    <thead class="text-white bg-primary">
                        <tr>
                            <th>Código</th>
                            <th>Fecha ingreso</th>
                            <th>Lote</th>
                            <th>codProveedor</th>
                            <th>Proveedor</th>
                            <th>codMotivo</th>
                            <th>Motivo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
          </div>

         <div class="modal fade bd-example-modal-lg" tabindex="-1" data-backdrop="static"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="container ">
                 <div class="row mt-4">                     
                      <div class="col-sm-12  ">
                          <div class="card p-0 border-0">
                            <div class="card-body">
                                <form class="card border-0">
                                    <input type="hidden" id="id_ingreso" />
                                    <div class="form-row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label for="motivosIngreso_id">MOTIVO DE INGRESO</label>
                                                <select class="form-control" id="motivosIngreso_id" runat="server" ClientIDMode="Static" name="motivoIngreso"></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label for="fechaProceso">Fecha de  proceso</label>
                                                <input type="date" class="form-control" id="fechaProceso" name="fechaProceso" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="motivoCompras">
                                        <div class="form-row">
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="codigoPedido">CÓDIGO DE PEDIDO</label>
                                                    <select id="codigoPedido" runat="server" ClientIDMode="Static" name="codigoPedido"></select>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="proveedor">Proveedor</label>
                                                    <input type="text" class="form-control" id="proveedor" name="nomProveedor" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="fechaPedido">FECHA DE PEDIDO</label>
                                                    <input type="text" class="custom-control-input" id="fechaPedido">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="fechaEntrega">FECHA DE ENTREGA</label>
                                                    <input type="text" class="custom-control-input" id="fechaEntrega">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="tipoDocumento">TIPO DE DOCUMENTO</label>
                                                    <input type="text" class="custom-control-input" id="tipoDocumento">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 p-lg-0">                         
                                            <table class="table table-light table-response" id="">
                                                <thead class="text-white bg-primary">
                                                    <tr>
                                                        <th>item</th>
                                                        <th>SKU</th>
                                                        <th>Producto</th>
                                                        <th>Cantidad</th>
                                                        <th>Fecha de vencimiento</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="">
                                                </tbody>
                                            </table>
                                     </div>
                                    </div>

                                    <div id="otroMotivo">
                                        <div class="form-row">
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="proveedor_id">Proveedor</label>
                                                    <select class="form-control select2" id="proveedor_id" runat="server" ClientIDMode="Static" name="proveedor"></select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="producto_id">Producto</label>
                                                    <select class="form-control select2" id="producto_id" runat="server" ClientIDMode="Static" name="producto"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-12 col-lg-2">
                                                <div class="form-group">
                                                    <label for="cantidad">CANTIDAD</label>
                                                    <input type="number" min="1" class="form-control" id="cantidad">
                                                </div>
                                            </div>                                            
                                            <div class="col-sm-12 col-lg-4">
                                                <div class="form-group">
                                                    <label for="fechaVencimiento">FECHA DE VENCIMIENTO</label>
                                                    <input type="date" class="form-control" id="fechaVencimiento">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="cantidad">Almacen destino</label>
                                                    <select class="form-control select2 "  id="almacen_id" runat="server" ClientIDMode="Static"  name="almacen" ></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row text-right my-2">
                                             <button type="button" id="btn_agregar" class="btn rounded-0 shadow btn-outline-dark ml-auto "> <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                  <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                </svg> Agregar</button>
                                        </div>
                                        <div class="col-sm-12 p-lg-0">                         
                                            <table class="table table-light table-response" id="tablaIngresos">
                                                <thead class="text-white bg-primary">
                                                    <tr>
                                                        <th>codDetalle</th>
                                                        <th>item</th>
                                                        <th>SKU</th>
                                                        <th>Producto</th>
                                                        <th>Cantidad</th>
                                                        <th>Almacen</th>
                                                        <th>Almacen destino</th>
                                                        <th>Fecha de vencimiento</th>
                                                        <th>Acciones</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaIngreso">
                                                </tbody>
                                            </table>
                                     </div>
                                    </div>
                                </form>
                            </div>
                         </div>
                      </div>
                     <div class="col-12" id="mensaje">
                        <div class="d-none">
                             <div class="text-center">
                                <div class="spinner-border" role="status">
                                <span class="sr-only">Guradando...</span>
                                </div>
                            </div>
                        </div>
                     </div>
                    <div class="col-12 text-right my-2">
                         <button type="button" class="btn rounded-0 shadow btn-outline-secondary" data-dismiss="modal">Close</button>
                         <input class="btn rounded-0 shadow btn-outline-success" id="btn_guardar" type="submit" value="Guardar">
                                    
                    </div>
                 </div>
             </div>
            </div>
          </div>
        </div>
    </div>
</asp:Content>
