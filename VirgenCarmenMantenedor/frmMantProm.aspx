﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantProm.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantProm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Mantenedor de Promociones</title>

    <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    
    <!-- Libreria Bootstrap -->
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="icon/style.css" />
    <link rel="stylesheet" href="css/web_general.css" />
    
    <script src="Scripts/RegistProm.js"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container-fluid">
         <!-- Formulario Horizontal-->
        <div class="row">
            <!-- ESTE SERA EL TITULO GENERAL DEL MANTENEDOR -->
            <div class="col-xs-12">
                <div class="box-header">
                    <h3 class="box-title" id="tituloID" style="font-size: 30px;font-weight: 600;color: #1b1818; text-transform: none;">Mantenedor de Promociones</h3>
                </div>
                <div class="div_bloque">
                    <div class="title_bloque">Filtros de Búsqueda</div>
                    <br />
                    <label for="idSelect2" class="label_bloque">Producto</label>
                    <select class="cbo_bloque" runat="server" ClientIDMode="Static" id="producto_principal" >
                        <option value="0">-SELECCIONAR-</option>                                   
                    </select>
                    <br />
                    <label for="idSelect2" class="label_bloque">Cliente</label>
                    <select class="cbo_bloque" runat="server" ClientIDMode="Static" id="filtro_cliente">
                        <option value="0">-SELECCIONAR-</option>                                   
                    </select>
                    <br />
                    <label for="idSelect2" class="label_bloque">Vendedor</label>
                    <select class="cbo_bloque" runat="server" ClientIDMode="Static" id="vendedor">
                        <option value="0">-SELECCIONAR-</option>               
                    </select>
                    <br />
                    <label for="idSelect2" class="label_bloque">Estado</label>
                    <select class="cbo_bloque" runat="server" ClientIDMode="Static" id="estado">
                        <option value="0">-SELECCIONAR-</option>
                    </select>
                    <br />
                    <label class="label_bloque" for="id_proveedor">Proveedor</label>
                    <select class="cbo_bloque" runat="server" ClientIDMode="Static" id="id_proveedor">
                        <option value = "0">-SELECCIONAR-</option>
                    </select>
                    <br />
                    <label for="idSelect2" class="label_bloque">Tipo Venta</label>
                    <select class="cbo_bloque" runat="server" ClientIDMode="Static" id="tipoVenta">
                        <option value="0">-SELECCIONAR-</option>                        
                    </select>
                    <br />
                    <label class="label_bloque" for="idSelect">Periodo </label>
                    <input type="date" id="min_date" class="cbo_bloque_mediun"/>
                    <input type="date" id="max_date" class="cbo_bloque_mediun"/>
                    <br />
                    <button type="button" class="btn_bloque" id="id_btnBuscar" >BUSCAR</button>
                </div>
            </div>
        </div>
        <br />

        <div class="row">
            <table id="tbl_promociones" class="table table-striped table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                        <th>CODIGO</th> <!-- 0-->

                        <th>CODIGO PRODUCTO</th> <!-- 1 -->
                        <th>PRODUCTO</th> <!-- 2-->
                        <th>FECHA INICIO</th><!-- 3-->
                        <th>FECHA FIN</th><!--4-->
                        
                        <th>CODIGO PROVEEDOR</th><!-- 5-->
                        <th>PROVEEDOR</th> <!-- 6-->          
                        
                        <th>ACCION</th><!-- 7-->

                        <th>promocion</th><!-- 8-->
                        <th>codhoraI</th><!-- 9-->
                        <th>codhoraF</th><!-- 10-->
                        <th>codEstado</th><!-- 11-->
                        <th>estado</th><!--12-->
                        <th>cantidadProd</th><!--13-->                           
                        <th>codUnidadBase</th><!--14-->
                        <th>desUnidadBase</th><!--15-->

                        <th>tipoProm</th><!--15-->
                        <th>detTipoProm</th><!--16-->
                        <th>codVendAplica</th><!--17-->
                        <th>desVendAplica</th><!--18-->
                        <th>codClienteAplica</th><!--19-->
                        <th>desClienetAplica</th><!--20-->
                        <th>vecesUsarProm</th><!--21-->
                        <th>vecesUsarPromXvend</th><!--22-->
                        <th>vecesUsarPromXcliente</th><!--23-->

                    </tr>
                </thead>
                
                <tbody id="tbl_body_table">
                    
                </tbody>
            </table>
        </div>
        <br />

        <!-- Button trigger modal -->
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 3%">
                <button type="button" class="btn btn-primary" id="buttonModal" data-toggle="modal" data-target="#exampleModal2"  >
                        AGREGAR
                </button>
            </div>
        </div>
    </div>

    <!-- Modal Detalle de Promocion -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalle de Promoción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" onsubmit="return false;">
                        
                    <div class="div_bloque" id="frm_detalle_promocion">
                        <div class="title_bloque">Promocion</div>
                        <br />
                        <table id="detalle_tb_agregar" class="table table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">codigoProducto</th>
                                    <th scope="col">PRODUCTO</th>
                                    <th scope="col">CANTIDAD</th>
                                    <th scope="col">cod.Pres</th>
                                    <th scope="col">Und. BASE</th>                       
                                    <th scope="col">IMPORTE</th>
                                    <th scope="col">ACCION</th>
                                </tr>
                            </thead>
                            <tbody id="">    
                            </tbody>
                        </table>
                       
                    </div>        
                    <br />

                 

                        <div class="form-group" style="text-align: right">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Regresar</button>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Registrar Promocion -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style=" width: 750px !important">
            <div class="modal-content" style="padding:15px;">
                <form class="div_bloque" id="frm_registrar_promo" >
                    <div class="title_bloque">Registrar Promoción</div>
                    <br />
                    <label for="Nombre" class="label_bloque">Nombre</label>
                    <input type="text" name="promocion" class="input_bloque" id="txt_nombre_promo_reg"  />
                    <input type="hidden" class="form-control horizontal" id="id_codPromocion"/>

                    <br />
                    <label class="label_bloque">Fechas</label>
                    <input type="date" id="txt_fech_inicio_reg" name="fechaInicial" class="input_bloque_mediun"/>
                    <input type="date" id="txt_fecha_final_reg" name="fechaFinal" class="input_bloque_mediun"/>
                        <div id="msj_txt_fecha" class="alert alert-danger"></div>

                    <br />
                    <label class="label_bloque">Horas</label>
                    <input type="time" id="txt_hora_inicio_reg" name="horaInicio" class="input_bloque_small"/>
                    <input type="time" id="txt_hora_fin_reg" name="horaFin" class="input_bloque_small"/>
                        <div id="msj_txt_hora" class="alert alert-danger"></div>                    
                    <br />
                    <label class="label_bloque">Activar Promoción</label>
                    <label class="radio_bloque"><input type="radio" name="activarProm"  id="radio_activar_si_reg" value="1" checked />SI</label>
                    <label class="radio_bloque"><input type="radio" name="activarProm" id="radio_activar_no_reg" value="2"  />NO</label>
                                       

                    <br />
                    <label class="label_bloque">Producto</label>
                    <input type="text" class="input_bloque_mediun" id="nonProdPrincipal" name="nonProdPrincipal" placeholder="Producto Principal" readonly="readonly" />
                    <button type="button" class="btn_bloque"  id="id_btn_seleccionar_prod" data-toggle="modal" data-target="#id_seleccionar"  >BUSCAR</button>
                    <input type="hidden" class="input_bloque" name="prodPrincipal" id="id_codProducto_reg"/>
                                        

                    <br />
                    <label class="label_bloque">por un total de</label>
                    <input type="text" id="txt-cant" name="txtCant" class="input_bloque_small"/>
                    <select name="unidadBase"  class="cbo_bloque_small" id="id_unidadBase">
                        <option value = "0"> SOLES </option>
                    </select>
                    <label class="label_bloque">o más</label>
                    <br />
                    <div class="div_bloque" id="frm_agregar_prod_promo">
                        <div class="title_bloque">Promocion</div>
                        <br />
                        <table id="tb_agregar" class="table table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">codigoProducto</th>
                                    <th scope="col">PRODUCTO</th>
                                    <th scope="col">CANTIDAD</th>
                                    <th scope="col">cod.Pres</th>
                                    <th scope="col">Und. BASE</th>                       
                                    <th scope="col">IMPORTE</th>
                                    <th scope="col">ACCION</th>
                                    <th scope="col">COD-FLAG</th>

                                </tr>
                            </thead>
                            <tbody id="">    
                            </tbody>
                        </table>
                        <br />
                        <button type="button" class="btn_bloque" id="btnAgregar" data-toggle="modal" data-target="#modal_prod_recibir_promo">Agregar</button>
                    </div>        
                    <br />

                    <div class="div_bloque">
                        <div class="title_bloque">Condiciones</div>
                        <br />
                        <label class="label_bloque">Tipo de Venta</label>
                        <label class="radio_bloque"><input type="radio" name="promocionAplicaReg" id="radio_contado_reg" value="1" checked />CONTADO</label>                            
                        <label class="radio_bloque"><input type="radio" name="promocionAplicaReg" id="radio_credito_reg" value="2" />CREDITO</label>
                        <br />
                        <label class="label_bloque">vendedor</label>
                        <select class="cbo_bloque" name ="desvendedorAplica" runat="server" ClientIDMode="Static" id="cbo_vendedor_reg">
                            <option value="0">-SELECCIONAR-</option>                                   
                        </select>
                        <br />
                        <label for="inputName" class="label_bloque">Cliente</label>
                        <select class="cbo_bloque" name="desClienteAplica" runat="server" ClientIDMode="static" id="cbo_cliente_reg">
                            <option value="0">-SELECCIONAR-</option>                                   
                        </select>
                        <br />
                        <label for="inputName" class="label_bloque_normal">Veces que puede usarse la Promocion</label>
                        <input type="text" id="txt_uso_promo_reg" name="cantMaxProm"  class="input_bloque_small" />
                        <br />
                        <div id="uso_vendedor">
                            <label for="inputName" class="label_bloque_normal">Veces que el vendedor puede usar la promocion</label>
                            <input type="text" id="txt_uso_promo_vendedor_reg" name="cantMaxVend" class="input_bloque_small" />
                            <div id="msj_txt_uso_vendedor"class="alert alert-danger"></div>                    
                        </div>
                        <div id="uso_cliente">
                            <label for="inputName" class="label_bloque_normal">Veces que el cliente puede usar la promocion</label>
                            <input type="text" id="txt_uso_promo_cliente_reg" name="cantMaxCliente" class="input_bloque_small" />
                        <div id="msj_txt_uso_cliente"class="alert alert-danger"></div>                    

                        </div>
                        </div>
                    <br />
                    <button type="button" class="btn_bloque" data-dismiss="modal" id="btnCancelaq">Cancelar</button>
                    <button type="button" class="btn_bloque" id="btnGuardar" >Guardar</button>
                    
                    <br />
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Agregar Producto Promocion -->
    <div class="modal fade modal_seleccionar" id="modal_prod_recibir_promo" >
        <div class="modal-dialog" role="document" style=" width: 750px !important">
            <div class="modal-content" style="padding:15px;">
                <form class="div_bloque" id="formProdPromocional">
                    <div class="title_bloque">Producto Promocion</div>
                    <br />
                    <label class="label_bloque">Recibirá</label>
                    <input type="text" class="input_bloque_mediun" id="id_Producto_promo" name="id_Producto_promo" placeholder="Producto Promocion" readonly="readonly"/>
                    <button type="button" class="btn_bloque" id="btn_buscar_prod_recibir" data-toggle="modal" data-target="#id_seleccionar"  >BUSCAR</button>
                    <input type="hidden" class="form-control horizontal" id="id_codProducto_promo"/>

                    <br />
                    <label class="label_bloque">Cant. a Recibir</label>
                    <input type="text" id="txt-cantidad" name="txt_cantidad" class="input_bloque_small"/>
                    <select class="cbo_bloque_mediun" id="id_unidadBase_promo" name="unidadBase_promo">
                        <option value = "0">-Seleccionar-</option>
                    </select>
                    <br />
                    <label for="inputName" class="label_bloque">a S./</label>
                    <input type="text" id="txt-dinero" name="txt_dinero" class="input_bloque_small"/>
                  <div class="alert alter-danger p-0" id="mensajeRegistrarProd"></div>                                                           
                    
                    <br />
                    <button type="button" class="btn_bloque" id="btn_agregar_prod_promo" >Agregar Producto</button>
                </form>
            </div>
        </div>
    </div>

 
    <!-- Modal Seleccionar -->
    <div class="modal fade modal_seleccionar" id="id_seleccionar" >
        <div class="modal-dialog" role="document" style=" width: 500px !important">
            <div class="modal-content" style="padding:15px;">
                <div class="div_bloque">
                    <div class="title_bloque">SELECCIONAR</div>
                    <br />
                    <table id="tb_seleccionar" class="table table-bordered table-hover" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>CODIGO</th>
                                <th>DESCRIPCION</th>
                                <th>ACCION</th>
                                <th>DATOS</th>
                            </tr>
                        </thead>
                        <tbody id="">    
                        </tbody>
                    </table>
                    <br />
                  <div class="container p-0" id="containermMensaje"></div>
                </div>
            </div>
        </div>
    </div>

   

</asp:Content>
