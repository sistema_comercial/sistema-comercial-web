﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantIngresos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarCombos();
        }

        private void cargarCombos() {

            CLNProveedor prov = new CLNProveedor();
            CLNConcepto concepto = new CLNConcepto();
            CLNPedido pedido = new CLNPedido();

            List<CENProveedor> listProv = prov.ListaProveedores(CENConstante.g_const_23);
            List<CENConcepto> lista = concepto.ListarConceptos(CENConstante.g_const_57);
            List<CENProductosInsert> listProducto = pedido.listProductos(CENConstante.g_const_7);
            List<CENDetalleAlmacen> listAlmacen = pedido.listAlmacenes(CENConstante.g_const_17);


            foreach (var item in lista)
            {
                string cod = item.correlativo.ToString();
                var descripcion = item.descripcion;
                motivosIngreso_id.Items.Add(new ListItem(descripcion, cod));
                motivoBusqueda.Items.Add(new ListItem(descripcion, cod));
            }

            proveedor_id.Items.Add(new ListItem("SELECCIONE PROVEEDOR", "0"));
            proveedorBusqueda.Items.Add(new ListItem("SELECCIONE PROVEEDOR", "0"));
            foreach (var item in listProv)
            {
                string cod = item.codigoProveedor.ToString();
                var descripcion = item.descproveedor;
                proveedor_id.Items.Add(new ListItem(descripcion, cod));
                proveedorBusqueda.Items.Add(new ListItem(descripcion, cod));
            }

            producto_id.Items.Add(new ListItem("SELECCIONE PRODUCTO", "0"));
            foreach (var item in listProducto)
            {
                string cod = item.codProducto;
                var descripcion = item.descProducto;
                producto_id.Items.Add(new ListItem(cod + "-" + descripcion, cod));
            }

            almacen_id.Items.Add(new ListItem("SELECCIONE ALMACEN", "0"));
            foreach (var item in listAlmacen)
            {
                string cod = item.transaccion.ToString();
                var descripcion = item.descAlmacen.ToString();
                almacen_id.Items.Add(new ListItem(descripcion, cod));
            }
        }

        [WebMethod]
        public static int obtenerLote() {
            try {
                int lote=0;
                CLNConcepto concepto = new CLNConcepto();
                List<CENConcepto> lista = concepto.ListarConceptos(58);
                foreach (var item in lista)
                {
                    lote = item.correlativo;
                   
                }
                return lote;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [WebMethod]
        public static string guardarIngreso(List<CENIngresoMercaderia> cabecera, List<CENDetalleIngreso> detalle) {
            try {
                CENIngresoMercaderia ingresoCabecera = new CENIngresoMercaderia();
                CENDetalleIngreso ingresoDetalle = new CENDetalleIngreso();

                ingresoCabecera.listaIngreso = cabecera;
                ingresoDetalle.listDetalleIngreso = detalle;
                CLNIngresoMercaderia cln = new CLNIngresoMercaderia();
                return cln.guardarIngreso(ingresoCabecera,ingresoDetalle);
            }
            catch (Exception ex) { 
                throw ex;           
            }
        }

        [WebMethod]
        public static string editarIngreso(List<CENIngresoMercaderia> cabecera, List<CENDetalleIngreso> detalle)
        {
            try
            {
                CENIngresoMercaderia ingresoCabecera = new CENIngresoMercaderia();
                CENDetalleIngreso ingresoDetalle = new CENDetalleIngreso();

                ingresoCabecera.listaIngreso = cabecera;
                ingresoDetalle.listDetalleIngreso = detalle;
                CLNIngresoMercaderia cln = new CLNIngresoMercaderia();
                return cln.editarIngreso(ingresoCabecera, ingresoDetalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [WebMethod]
        public static List<CENIngresoMercaderia> listaIngresos(string fechaIngreso, int ingreso, int proveedor, int motivo) {
            try {
                CLNIngresoMercaderia cln = new CLNIngresoMercaderia();
                return cln.listaIngresos(fechaIngreso,ingreso,proveedor,motivo);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [WebMethod]
        public static string eliminarPedido(int ingreso) {
            try {

                CLNIngresoMercaderia cln = new CLNIngresoMercaderia();
                return cln.eliminarIngreso(ingreso);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [WebMethod]
        public static List<CENDetalleIngreso> listaIngresoDetalle(int codigo)
        {
            try
            {
                CLNIngresoMercaderia cln = new CLNIngresoMercaderia();
                return cln.listaIngresoDetalle(codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static byte[] generarPdf(int codigoIngreso)
        {
            byte[] binaryData ;
            try
            {
                CLNIngresoMercaderia ingreso = new CLNIngresoMercaderia();
                binaryData = ingreso.generarPdf(codigoIngreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return binaryData;
        }
    }
}