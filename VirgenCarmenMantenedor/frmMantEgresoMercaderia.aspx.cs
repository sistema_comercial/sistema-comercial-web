﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantEgresoMercaderia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarCombos();
        }

        private void cargarCombos() {
            CLNProveedor prov = new CLNProveedor();
            CLNConcepto concepto = new CLNConcepto();
            CLNPedido pedido = new CLNPedido();

            List<CENProveedor> listProv = prov.ListaProveedores(CENConstante.g_const_23);
            List<CENConcepto> lista = concepto.ListarConceptos(CENConstante.g_const_59);
            List<CENProductosInsert> listProducto = pedido.listProductos(CENConstante.g_const_7);
            List<CENDetalleAlmacen> listAlmacen = pedido.listAlmacenes(CENConstante.g_const_17);

            foreach (var item in lista)
            {
                string cod = item.correlativo.ToString();
                var descripcion = item.descripcion;
                motivosEgreso_id.Items.Add(new ListItem(descripcion, cod));
                motivoBusqueda.Items.Add(new ListItem(descripcion, cod));
            }

            producto_id.Items.Add(new ListItem("SELECCIONE PRODUCTO", "0"));
            foreach (var item in listProducto)
            {
                string cod = item.codProducto;
                var descripcion = item.descProducto;
                producto_id.Items.Add(new ListItem(cod + "-" + descripcion, cod));
            }

            almacen_id.Items.Add(new ListItem("SELECCIONE ALMACEN", "0"));
            foreach (var item in listAlmacen)
            {
                string cod = item.transaccion.ToString();
                var descripcion = item.descAlmacen.ToString();
                almacen_id.Items.Add(new ListItem(descripcion, cod));
            }
        }

        [WebMethod]
        public static string guardarEgreso(List<CENEgresoMercaderia> cabecera, List<CENDetalleEgreso> detalle)
        {
            try
            {
                CENEgresoMercaderia egresoMercaderia = new CENEgresoMercaderia();
                CENDetalleEgreso egresoDetalle = new CENDetalleEgreso();

                egresoMercaderia.listaEgreso = cabecera;
                egresoDetalle.listDetalleEgreso = detalle;
                CLNEgresoMercaderia cln = new CLNEgresoMercaderia();
                return cln.guardarEgreso(egresoMercaderia, egresoDetalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static string editarEgreso(List<CENEgresoMercaderia> cabecera, List<CENDetalleEgreso> detalle)
        {
            try
            {
                CENEgresoMercaderia egresoMercaderia = new CENEgresoMercaderia();
                CENDetalleEgreso egresoDetalle = new CENDetalleEgreso();

                egresoMercaderia.listaEgreso = cabecera;
                egresoDetalle.listDetalleEgreso = detalle;
                CLNEgresoMercaderia cln = new CLNEgresoMercaderia();
                return cln.editarEgreso(egresoMercaderia, egresoDetalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [WebMethod]
        public static List<CENEgresoMercaderia> listaEgresos(string fechaIngreso, int codeEgreso, int motivo)
        {
            try
            {

                CLNEgresoMercaderia cln = new CLNEgresoMercaderia();
                return cln.listaEgresos(fechaIngreso, codeEgreso, motivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static string eliminarPedido(int codEgreso)
        {
            try
            {

                CLNEgresoMercaderia cln = new CLNEgresoMercaderia();
                return cln.eliminarEgreso(codEgreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static List<CENDetalleEgreso> listaIngresoDetalle(int codigo)
        {
            try
            {
                CLNEgresoMercaderia cln = new CLNEgresoMercaderia();
                return cln.listaEgresoDetalle(codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static byte[] generarPdf(int codEgreso)
        {
            byte[] binaryData;
            try
            {
                CLNEgresoMercaderia egreso = new CLNEgresoMercaderia();
                binaryData = egreso.generarPdf(codEgreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return binaryData;
        }
    }
}