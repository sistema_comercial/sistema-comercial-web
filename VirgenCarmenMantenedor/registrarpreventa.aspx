﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="registrarpreventa.aspx.cs" Inherits="VirgenCarmenMantenedor.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    
    <!-- Libreria Bootstrap -->
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="css/preventa.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="icon/style.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.min.css" />
    <script src="Scripts/Plantilla.js"></script>
    <script src="Scripts/jqueryui/jquery-ui.min.js"></script>
    <script src="Scripts/preventa_ajax.js"></script>
    <style>
      .ui-autocomplete-loading {
        background: white url("Imagenes/cargar.gif") right center no-repeat;
      }
      </style>

    <title>REGISTRAR PREVENTA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="id_bloquear" class="div_bloquear">Procesando ... espere por favor</div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                
                <!-- Titulo de la Pagina -->
                <div class="box-header">
                    <h3 class="box-title" id="tituloMant">REGISTRAR PREVENTA</h3>
                </div>

                <!-- Formulario para registrar -->
                <form class="form-horizontal" id="formulario" name="formulario">
                    
                    <!-- Datos del vendedor -->
                    <div class="div_bloque">
                        <div class="title_bloque">Datos del vendedor</div>
                        <br />
                        <label class="label_bloque" id="label_vendedor" for="id_vendedor">Vendedor</label>
                        <select class="cbo_bloque" id="id_vendedor">
                            <option value="0">Selecionar vendedor</option>
                        </select>
                        <br />
                        <label class="label_bloque" id="label_ruta" for="">Ruta</label>
                        <select class="cbo_bloque" id="id_desc_ruta"> </select>
                    </div>
                    <br />
                    
                    <!-- Datos de la venta -->
                    <div class="div_bloque">
                        <div class="title_bloque">Datos de Venta</div>
                        <br />

                        <label class="label_bloque" id="label_tipoventa" >T. Venta</label>
                        <label class="radio_bloque">
                            <input type="radio" name="radio_tipo_venta" id="id_contado" value="1" checked />
                            Contado
                        </label>
                        <label class="radio_bloque">
                            <input type="radio" name="radio_tipo_venta" id="id_credito" value="2" />
                            Credito
                        </label>
                        <br />

                        <label class="label_bloque" id="label_documento" >Documento</label>
                        <label class="radio_bloque">
                            <input type="radio" name="radio_doc" id="id_boleta" value="1" checked/>
                            Boleta
                        </label>
                        <label class="radio_bloque">
                            <input type="radio" name="radio_doc" id="id_factura" value="2" />
                            Factura
                        </label>
                        <br />

                        <label class="label_bloque" id="labelfecha" for="">F. Entrega</label>
                        <input type='text' class="input_bloque_small" id='id_fecha'/>
                        <br />

                        <label class="label_bloque" id="labelhora" for="">H. Entrega</label>
                        <input type="number" class="input_bloque_small" id="id_hora" value="06" />
                        <input type="number" class="input_bloque_small" id="id_minu" value="00"/>
                        <br />
                    </div>
                    <br />

                    <!-- Datos del Cliente -->
                    <div class="div_bloque">
                        <div class="title_bloque">Datos del Cliente</div>
                        <br />
                        <label class="label_bloque" for="id_nomCliente">Cliente</label>
                        <input type="text" class="input_bloque_mediun" id="id_documento" placeholder="Numero Documento" disabled/>
                        <input type="text" class="input_bloque" id="id_nomCliente" placeholder="Nombre / Numero Documento"/>
                        <input type="hidden" class="form-control horizontal" id="id_codCliente" />
                        <input type="hidden" class="form-control horizontal" id="id_tipoListaPrecio" />
                        <br />
                        <label  class="label_bloque" for="id_puntoEntrega">Direccion Entrega</label>
                        <select class="cbo_bloque" id="id_puntoEntrega">
                            <option value="0">Seleccionar Ruta</option>
                        </select>
                        <br />
                    </div>
                    <br />

                    <!-- Datos del producto -->
                    <div class="div_bloque">
                        <div class="title_bloque">Datos del Producto</div>
                        <br />
                        <label  class="label_bloque" for="id_desProducto">Producto</label>
                        <input type="text" class="input_bloque_mediun" id="id_codProducto" placeholder="Codigo" disabled/>
                        <input type="text" class="input_bloque" id="id_desProducto" placeholder="Descripcion / Codigo" disabled/>
                        <input type="hidden" class="form-control horizontal" id="id_tipoProducto" />
                        <br />
                        <label  class="label_bloque" id="lavelStock" for="">Stock:</label>
                        <input type="text" class="input_bloque_small" disabled="" id="id_stock" placeholder="0"/>
                        <input type="text" class="input_bloque_mediun" disabled="" id="id_unidBase" placeholder="Unidad Base"/>
                        <br />
                        <label  class="label_bloque" id="labelPrecio" for="">Precio:</label>
                        <input type="text" class="input_bloque_small" disabled="" id="id_precio" placeholder="0.0"/>
                        <br />
                        <label  class="label_bloque" id="labelAlmacen" for="id_almacen">Almacen:</label>
                        <select class="cbo_bloque" id="id_almacen">
                            <option value="0">Seleccionar</option>
                        </select>
                        <br />
                        <label  class="label_bloque" id="labelUnidad" for="">Unidad:</label>
                        <select class="cbo_bloque" id="id_unidad">
                            <option value="0">Seleccionar</option>
                        </select>
                        <br />
                        <label  class="label_bloque" id="labelCantidad" for="">Cantidad:</label>
                        <input type="number" class="input_bloque_small" id="id_cantidad" value ="1"/>
                        <br />
                        <!-- Promociones -->
                        <div class="div_bloque">
                            <div class="title_bloque">Promociones</div>
                            <div class="form-group col-xs-12" id="promos" ></div>
                        </div>
                        <br />
                        <!-- Descuentos -->
                        <div class="div_bloque">
                            <div class="title_bloque">Descuentos</div>
                            <div class="form-group col-xs-12" id="dsctos" ></div>
                        </div>
                        <br />
                        <button type="button" id="id_btnAgregar" class="btn_bloque">AGREGAR PRODUCTO</button>
                        <br />
                    </div>
                    <br />

                    <!-- Tabla -->
                    <div class="div_table">
                        <table id="id_carrito" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">ITEM</th>
                                        
                                    <th scope="col">CODIGO</th>
                                    <th scope="col">DESCRIPCION</th>

                                    <th scope="col">CANTIDAD</th>
                                    <th scope="col">CANTIDADUNIDADBASE</th>

                                    <th scope="col">PRECIO</th>
                                        
                                    <th scope="col">ALMACEN</th>    
                                    <th scope="col">CODALMACEN</th>    
                                        
                                    <th scope="col">UNIDAD</th>
                                    <th scope="col">CODPRESENTACION</th>
                                        
                                    <th scope="col">SUBTOTAL</th>
                                        
                                    <th scope="col">TIPO</th>
                                    <th scope="col">CODIGOTIPOPRODUCTO</th>

                                    <th scope="col">ACCION</th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body_table" class="ui-sortable">
                            </tbody>
                        </table>
                    </div>
                    <br />

                    <!-- Importes de la Preventa -->
                    <div class="div_bloque">
                        <div class="title_bloque">Importes</div>
                        <br />
                        <label  class="label_bloque" id="labelsubtotal" for="">SubTotal:</label>
                        <input type="text" class="input_bloque_small" id="id_subtotal" disabled="" placeholder="0.00" />
                        <br />
                        <label  class="label_bloque" id="labelrecargo" for="">Recargo:</label>
                        <input type="text" class="input_bloque_small" id="id_recargo" disabled="" placeholder="0.00"/>
                        <br />
                        <label  class="label_bloque" id="labeldesc" for="">Desc:</label>              
                        <input type="text" class="input_bloque_small" id="id_descuento" disabled="" placeholder="0.00"/>
                        <br />
                        <label  class="label_bloque" id="labeligv" for="">IGV:</label>                                
                        <input type="text" class="input_bloque_small" id="id_igv" disabled="" placeholder="0.00"/>
                        <br />
                        <label  class="label_bloque" id="labeltotal" for="">TOTAL:</label> 
                        <input type="text" class="input_bloque_small" id="id_total" disabled="" placeholder="0.00"/>
                        <br />
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="checkbox" class="horizontal col-sm-1 " id="id_flag_recargo" disabled="" hidden="hidden"/> 
                        <label hidden="hidden">Aplicar recargo por venta al credito</label> 
                    </div>

                    <div class="row text-right">
                         <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">CANCELAR</button>
                         <button type="button" class="btn btn-primary" id="btnGuardar">GUARDAR</button>
                    </div>
                    <br />

                </form>

            </div>
        </div>
    </div> <!-- Fin container -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="Mmensaje">
                        <p id="Mpmensaje"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="inputName">Producto</label>
                            <input type="text" class="form-control" id="id_codProM" disabled="disabled" />
                            <input type="text" class="form-control" id="id_desProM" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label for="inputName">Precio</label>
                            <input type="text" class="form-control" id="id_preProM" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label for="inputName">Stock</label>
                            <input type="text" class="form-control" id="id_stoProM" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label for="inputName">Almacen</label>
                            <select class="form-control" id="id_almProM">
                                <option value="">Seleccionar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Unidad</label>
                            <select class="form-control" id="id_uniProM">
                                <option value="">Seleccionar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Cantidad</label>
                            <input type="number" class="form-control" id="id_canProM"/>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelarM">Cancelar</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnGuardarM">Guardar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
