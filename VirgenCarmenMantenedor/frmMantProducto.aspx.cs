﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantProducto1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                llenarCombos();
            }

        }


        public void llenarCombos()
        {

            CLNCategoria categoria_cln = new CLNCategoria();
            CLNFabricante fabricante_cln = new CLNFabricante();
            CLNProveedor proveedor_cln = new CLNProveedor();
            CLNConcepto concepto = new CLNConcepto();

            List<CENProducto> ListaCategorias = categoria_cln.ListarCategorias(5);
            List<CENFabricante> ListFabr = fabricante_cln.ListaFabricante(27);
            List<CENProveedor> ListProv = proveedor_cln.ListaProveedores(23);
            List<CENConcepto> ListTipoProd = concepto.ListarConceptos(29);
            List<CENConcepto> listUnidadVenta = concepto.ListarConceptos(26);
            List<CENConcepto> listFlagVenta = concepto.ListarConceptos(53);

            //categoria

            categoria.Items.Clear();
            dllcategoria.Items.Clear();

            categoria.Items.Add(new ListItem("--SELECCIONE--", "0"));
            dllcategoria.Items.Add(new ListItem("--SELECCIONE--", "0"));

            foreach (var items in ListaCategorias)
            {
                string item = items.codCategoria.ToString();
                var descripcion = items.descCategoria;
                categoria.Items.Add(new ListItem(descripcion, item));
                dllcategoria.Items.Add(new ListItem(descripcion, item));
            }

            //fabricante

            fabricante.Items.Clear();
            dllfabricante.Items.Clear();
            dllfabricanteE.Items.Clear();

            fabricante.Items.Add(new ListItem("--SELECCIONE--", "0"));
            dllfabricante.Items.Add(new ListItem("--SELECCIONE--", "0"));
            dllfabricanteE.Items.Add(new ListItem("--SELECCIONE--", "0"));

            foreach (var items in ListFabr)
            {
                string item = items.codigoFabricante.ToString();
                var descripcion = items.descFabricante;
                fabricante.Items.Add(new ListItem(descripcion, item));
                dllfabricante.Items.Add(new ListItem(descripcion, item));
                dllfabricanteE.Items.Add(new ListItem(descripcion, item));
            }

            //proveedor

            proveedor.Items.Clear();
            dllproveedor.Items.Clear();
            dllproveedorE.Items.Clear();

            proveedor.Items.Add(new ListItem("--SELECCIONE--", "0"));
            dllproveedor.Items.Add(new ListItem("--SELECCIONE--", "0"));
            dllproveedorE.Items.Add(new ListItem("--SELECCIONE--", "0"));

            foreach (var items in ListProv)
            {
                string item = items.codigoProveedor.ToString();
                var descripcion = items.descproveedor;
                proveedor.Items.Add(new ListItem(descripcion, item));
                dllproveedor.Items.Add(new ListItem(descripcion, item));
                dllproveedorE.Items.Add(new ListItem(descripcion, item));
            }


            //unidad de venta

            unidadbase.Items.Clear();
            presentacion.Items.Clear();
            unidadbaseE.Items.Clear();
            presentacionE.Items.Clear();

            unidadbase.Items.Add(new ListItem("--SELECCIONE--", "0"));
            presentacion.Items.Add(new ListItem("--SELECCIONE--", "0"));
            unidadbaseE.Items.Add(new ListItem("--SELECCIONE--", "0"));
            presentacionE.Items.Add(new ListItem("--SELECCIONAR--", "0"));

            foreach (var items in listUnidadVenta)
            {
                string item = items.correlativo.ToString();
                var descripcion = items.descripcion;

                unidadbase.Items.Add(new ListItem(descripcion, item));
                unidadbaseE.Items.Add(new ListItem(descripcion, item));

                presentacion.Items.Add(new ListItem(descripcion, item));
                presentacionE.Items.Add(new ListItem(descripcion, item));
            }

            //Lista tipo de producto

            tipoProducto.Items.Clear();
            tipoProductoE.Items.Clear();

            tipoProducto.Items.Add(new ListItem("--SELECCIONE--", "0"));
            tipoProductoE.Items.Add(new ListItem("--SELECCIONE--", "0"));

            foreach (var items in ListTipoProd)
            {
                string item = items.correlativo.ToString();
                var descripcion = items.descripcion;
                tipoProducto.Items.Add(new ListItem(descripcion, item));
                tipoProductoE.Items.Add(new ListItem(descripcion, item));
            }

            //Lista flag de venta

            flagVenta.Items.Clear();
            flagVentaE.Items.Clear();

            flagVenta.Items.Add(new ListItem("--SELECCIONE--", "0"));
            flagVentaE.Items.Add(new ListItem("--SELECCIONE--", "0"));

            foreach (var items in listFlagVenta)
            {
                string item = items.correlativo.ToString();
                var descripcion = items.descripcion;
                flagVenta.Items.Add(new ListItem(descripcion, item));
                flagVentaE.Items.Add(new ListItem(descripcion, item));
            }
        }

        [WebMethod]
        public static List<CENSubcategoria> ListarSubCategoria(int codCat)
        {
            CLNSubCategoria subCategoria = null;
            List<CENSubcategoria> ListaSubCategoria = null;
            try
            {
                subCategoria = new CLNSubCategoria();
                ListaSubCategoria = subCategoria.ListarSubCategoriasPorCategoria(codCat);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListaSubCategoria;
        }

        [WebMethod]
        public static List<CENFabricante> ListaFabricante(int flag)
        {
            CLNFabricante fabricante = null;
            List<CENFabricante> ListFabr = null;

            try
            {
                fabricante = new CLNFabricante();
                ListFabr = fabricante.ListaFabricante(flag);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListFabr;
        }
        [WebMethod]
        public static List<CENProveedor> ListaProveedor(int flag)
        {
            CLNProveedor proveedor = null;
            List<CENProveedor> ListProv = null;
            try
            {
                proveedor = new CLNProveedor();
                ListProv = proveedor.ListaProveedores(flag);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListProv;
        }

        [WebMethod]
        public static List<CENConcepto> ListaCampos(int flag)
        {
            CLNConcepto concepto = null;
            List<CENConcepto> ListaCampos = null;

            try
            {
                concepto = new CLNConcepto();
                ListaCampos = concepto.ListarConceptos(flag);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListaCampos;
        }

        [WebMethod]
        public static List<CENProductoLista> ListaProductos(int codCategoria, int codSubcategoria, int codigoFabricante, int codigoProveedor, string descripcion)
        {
            List<CENProductoLista> ListaProduc = null;
            CENProducto objCENProducto = null;
            CLNProducto objCLNProduc = null;

            try
            {
                objCLNProduc = new CLNProducto();
                objCENProducto = new CENProducto(codCategoria, codSubcategoria, codigoProveedor, codigoFabricante, descripcion);
                ListaProduc = objCLNProduc.ListarProductos(objCENProducto);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListaProduc;
        }
        [WebMethod]
        public static List<CENProductoListaDetalle> ListaProductoByCod(string codProducto)
        {
            List<CENProductoListaDetalle> ListaProducCod = null;
            CLNProducto objCLNProducCod = null;

            try
            {
                objCLNProducCod = new CLNProducto();
                ListaProducCod = objCLNProducCod.ListaProductoCodCategoria(codProducto);
                //    Console.WriteLine("esto es la lista " + ListaProducCod);


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListaProducCod;

        }

        [WebMethod]
        public static int EliminarProducto(string Cod)
        {
            CLNProducto objCLNProducto = null;
            try
            {
                objCLNProducto = new CLNProducto();
                int ok = objCLNProducto.EliminarProducto(Cod);
                return ok;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [WebMethod]
        public static string InsertarProduct(List<CENProductosInsert> cabeceraProducto, List<CEN_Detalle_Presentacion_Product> detalleProducto = null)
        {

            CLNProducto objCLNProduct = null;
            CENProductosInsert cabecera = new CENProductosInsert();
            CEN_Detalle_Presentacion_Product detalle = new CEN_Detalle_Presentacion_Product();

            cabecera.listaProd = cabeceraProducto;
            detalle.detalleProducto = detalleProducto;

            try
            {
                objCLNProduct = new CLNProducto();
                return objCLNProduct.InsertarProduct(cabecera, detalle);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [WebMethod]
        public static string ActualizarProduct(List<CENProductosInsert> cabeceraProducto, List<CEN_Detalle_Presentacion_Product> detalleProducto = null)
        {

            CLNProducto objCLNProduct = null;
            CENProductosInsert cabecera = new CENProductosInsert();
            CEN_Detalle_Presentacion_Product detalle = new CEN_Detalle_Presentacion_Product();

            cabecera.listaProd = cabeceraProducto;
            detalle.detalleProducto = detalleProducto;

            try
            {
                objCLNProduct = new CLNProducto();
                return objCLNProduct.ActualizarProducto(cabecera, detalle);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [WebMethod]
        public static List<CEN_Detalle_Presentacion_Product> ListaDetallePresentacionProduct(string codProductos)
        {
            List<CEN_Detalle_Presentacion_Product> ListaProducCod = null;
            CLNProducto objCLNProducCod = null;

            try
            {
                objCLNProducCod = new CLNProducto();
                ListaProducCod = objCLNProducCod.ListarPresentacionProduc(codProductos);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListaProducCod;

        }

        [WebMethod]
        public static int EliminarPresentacionProductActualizar(string cod, int codP)
        {

            CLNProducto objCLNProductoEliminarDetalle = null;
            try
            {
                objCLNProductoEliminarDetalle = new CLNProducto();
                int ok = objCLNProductoEliminarDetalle.EliminarPresentacionProductActualizar(cod, codP);
                return ok;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [WebMethod]
        public static string ObtenerUser(int flag)
        {
            string duser = "";
            try
            {
                duser = System.Web.HttpContext.Current.Session["nombres"].ToString();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return duser;
        }


    }
}