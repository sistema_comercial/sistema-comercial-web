﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantIECaja : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static int RegistrarTransaccionCaja(CENTransaccionCaja objCENTransaccionCaja)
        {
            CLNCaja objCLNCaja = null;
            
            try
            {
                objCLNCaja = new CLNCaja();
                int ok = objCLNCaja.RegistrarTransaccionCaja(objCENTransaccionCaja);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [WebMethod]
        public static int ActualizarTransaccionCaja(CENTransaccionCaja objCENTransaccionCaja)
        {

            CLNCaja objCLNCaja = null;

            try
            {
                objCLNCaja = new CLNCaja();
                int ok = objCLNCaja.ActualizarTransaccionCaja(objCENTransaccionCaja);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static int EliminarTransaccionCaja(CENTransaccionCaja objCENTransaccionCaja)
        {
            CLNCaja objCLNCaja = null;     

            try
            {
                objCLNCaja = new CLNCaja();
                int ok = objCLNCaja.EliminarTransaccionCaja(objCENTransaccionCaja);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static List<CENCaja> ListarCajas(
            int ntraCaja, int estadoCaja, string fechaInicial, string fechaFinal)
        {
            CLNCaja objCLNCaja = null;
            List<CENCaja> listC = null;
            int ntraSucursal = Convert.ToInt32(System.Web.HttpContext.Current.Session["sucursal"].ToString());
            int ntraUsuario = Convert.ToInt32(System.Web.HttpContext.Current.Session["codUser"].ToString());
            try
            {
                objCLNCaja = new CLNCaja();
                listC = objCLNCaja.ListarCajas(ntraCaja, estadoCaja, ntraUsuario, ntraSucursal, fechaInicial, fechaFinal);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listC;
        }

        [WebMethod]
        public static List<CENAperturaCaja> ListarCajasAperturadas(int ntraCaja, int ntraUsuario, int flag, string fecha)
        {
            CLNCaja objCLNCaja = null;
            List<CENAperturaCaja> listCA = null;
            int ntraSucursal = Convert.ToInt32(System.Web.HttpContext.Current.Session["sucursal"].ToString());
            try
            {
                objCLNCaja = new CLNCaja();
                listCA = objCLNCaja.ListarCajasAperturadas(ntraSucursal, ntraCaja, ntraUsuario, flag, fecha);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listCA;
        }

        [WebMethod]
        public static List<CENCierreCaja> ListarCajasCerradas(int ntraCaja, int ntraUsuario, int flag, string fecha)
        {
            CLNCaja objCLNCaja = null;
            List<CENCierreCaja> listCC = null;
            int ntraSucursal = Convert.ToInt32(System.Web.HttpContext.Current.Session["sucursal"].ToString());

            try
            {
                objCLNCaja = new CLNCaja();
                listCC = objCLNCaja.ListarCajasCerradas(ntraSucursal, ntraUsuario, ntraCaja, flag, fecha);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listCC;
        }

        [WebMethod]
        public static List<CENTransaccionCaja> ListarTransaccionesCajas
            (int ntraCaja, int tipoTransaccion, int modoPago, int tipoRegistro,
            int tipoMoneda, string fechaTransaccion, int flag)
        {
            CLNCaja objCLNCaja = null;
            List<CENTransaccionCaja> listTC = null;
            int ntraSucursal = Convert.ToInt32(System.Web.HttpContext.Current.Session["sucursal"].ToString());
            try
            {
                objCLNCaja = new CLNCaja();
                listTC = objCLNCaja.ListarTransaccionesCajas
                    (ntraSucursal, ntraCaja, tipoTransaccion, modoPago, tipoRegistro,
                    tipoMoneda, fechaTransaccion, flag);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listTC;
        }
    }
}