﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantProducto.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantProducto1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Mantenedor Producto</title>

    <!-- Libreria Bootstrap -->
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    
    <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="Scripts/DataTable/Buttons-1.6.1/css/buttons.dataTables.min.css"/>
    <link rel="stylesheet" href="icon/style.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.min.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.structure.min.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.theme.min.css" />
    <script src="Scripts/DataTable/Buttons-1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/buttons.flash.min.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/jszip.min.js"></script>    
    <script src="Scripts/DataTable/Buttons-1.6.1/js/vfs_fonts.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/buttons.html5.min.js"></script> 
    <script src="Scripts/DataTable/Buttons-1.6.1/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="Scripts/jqueryui/jquery-ui.min.js"></script>
    <script src="Scripts/producto.js"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <!-- ESTE SERA EL TITULO GENERAL DEL MANTENEDOR -->
                <div class="box-header sub-titulo">
                    <h3 class="box-title ">Mantenedor Producto</h3>
                </div>
                <!-- ESTE SERA EL SUB TITULO GENERAL DE LO QUE SE QUIERE HACER -->
                <div class="text-center">
                    <h2 class="h2 ">Ingrese opción de Búsqueda</h2>
                </div>
            </div>
         </div>
          <div class="row justify-content-center">               
                <div class="col-sm-12 col-lg-8 ">
                     <form id="for_busqueda">
                    <div class="form-group">
                        <label for="categoria">Categoría</label>
                        <select id="categoria" class="form-control "  ClientIDMode="Static" runat="server">                     
                        </select>
                    </div>
                   
                    <div class="form-group">
                        <label for="ddlSubCategoria" class="input_bloque_mediun">SubCategoría</label>
                        <select id="subcategoria" class="form-control">
                            <option value="0">--Seleccionar--</option>                            
                        </select>
                    </div>

                     <div class="form-group ">
                        <label for="ddlFabricante" >Fabricante</label>
                        <select id="fabricante" class="form-control fabricante" ClientIDMode="Static" runat="server">                            
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="ddlProveedor">Proveedor</label>
                        <select id="proveedor" class="form-control proveedor" ClientIDMode="Static" runat="server">
                                                       
                        </select>
                    </div>

                    <div class="form-group">
                           <label for="txtProducto">Producto</label>
                           <input type="text" class="form-control" id="txtProducto" />
                    </div>     
                    <div class="form-group">
                            <button type="button" class="btn btn-primary" id="btnBuscar">Buscar</button>
                    </div>

                    <!--agregar nuevo producto --->
                     <div class="form-group col-xs-6">
                        <button type="button" class="btn btn-primary" id="buttonModal" data-toggle="modal" data-target="#productoModal">
                       Agregar
                        </button>

                    </div>                                           
                                                
               </form>
                </div>
            </div>
  </div>

     <div class="container">
        <div class="box-body table-responsive">
             <table id="id_tblProducto" class="table table-bordered table-hover">
                   <thead>
                        <tr>
                            <th>CÓDIGO</th>
                            <th>COD. CATEGORIA</th>
                            <th>CATEGORÍA</th>
                            <th>COD. SUBCATEGORIA</th>
                            <th>SUBCATEGORÍA</th>
                            <th>COD. PROVEEDOR</th>
                            <th>PROVEEDOR</th>
                            <th>COD. FABRICANTE</th>
                            <th>FABRICANTE</th>
                            <th>DESCRIPCIÓN</th>
                            <th>UNIDAD BASE</th> 
                            <th>COD. TIPO PRODUCT.</th> 
                            <th>TIPO PRODUCTO</th>  
                            <th>COD FLAG VENTA</th> 
                            <th>FLAG VENTA</th> 
                            <th>ACCIÓN</th>

                        </tr>
                    </thead>
                    <tbody id="tbl_body_table" class="ui-sortable">

                     </tbody>

                     <tfoot>
                         <tr>
                            <th>CÓDIGO</th>
                            <th>COD. CATEGORIA</th>
                            <th>CATEGORÍA</th>
                            <th>COD. SUBCATEGORIA</th>
                            <th>SUBCATEGORÍA</th>
                            <th>COD. PROVEEDOR</th>
                            <th>PROVEEDOR</th>
                            <th>COD. FABRICANTE</th>
                            <th>FABRICANTE</th>
                            <th>DESCRIPCIÓN</th>
                            <th>UNIDAD BASE</th> 
                            <th>COD. TIPO PRODUCT.</th> 
                            <th>TIPO PRODUCTO</th>  
                            <th>COD FLAG VENTA</th> 
                            <th>FLAG VENTA</th> 
                            <th>ACCIÓN</th>
                         </tr>
                     </tfoot>
                   
                </table>

        </div>
    </div>   

   
   <!-- Ventana modal REGISTRAR producto -->
    <div class="modal fade" id="productoModal" tabindex="-1" role="dialog" aria-labelledby="productoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="Mmensaje">
                        <p id="Mpmensaje"></p>
                    </div>
                    
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="Msku" disabled="disabled"/>                         
                           
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 label label-default"  for="inputEmail">Descripción</label>
                             <input type="text" class="form-control" id="Mdescripcion" maxlength="160"/>
                        </div>
                        <div class="row" >
                            <label class="col-sm-3 label label-default" for="inputName">Unidad de venta</label>
                            <div class="col-sm-3">
                                <select class="form-control" id="unidadbase" ClientIDMode="Static" runat="server" >
                            </select>
                            </div>
                            
                            <label class="col-sm-2 label label-default" for="categoria">Categoría</label>
                            <div class="col-sm-4">
                              <select class="form-control" id="dllcategoria" ClientIDMode="Static" runat="server" >
                                <option value="0" disabled>--Seleccionar--</option>
                             </select>
                            </div>
                        </div>
                          <div class="row">
                              <label class="col-sm-3 label label-default" for="inputEmail">Tipo Producto</label>
                              <div class="col-sm-3">
                                   <select class="form-control" id="tipoProducto" ClientIDMode="Static" runat="server">
                                 </select>
                                </div>

                            <label class="col-sm-2 label label-default" for="inputEmail">SubCategoría</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="dllsubcategoria" required>
                                    <option value="0" >--Seleccionar--</option>
                                 </select>
                             </div>
                            </div>
                  
                         <div class="row">
                            <label class="col-sm-3 label label-default" for="inputEmail">Fabricante</label>
                             <div class="col-sm-9">
                                  <select class="form-control fabricante" id="dllfabricante" ClientIDMode="Static" runat="server">
                                 </select>
                            </div>
                        </div>
                         <div class="row">
                            <label class="col-sm-3 label label-default" for="inputEmail">Proveedor</label>
                             <div class="col-sm-9">
                                  <select class="form-control proveedor" id="dllproveedor" ClientIDMode="Static" runat="server">
                                 </select>
                            </div>                        
                        </div>                   
                        <div class="row">
                            <label class="col-sm-3 label label-default" >Tipo Venta</label>
                             <div class="col-sm-9">
                                  <select class="form-control" id="flagVenta" ClientIDMode="Static" runat="server">                                 
                                 </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-4 label label-default" for="inputEmail">Presentación Producto</label>
                             <div class="col-sm-8">
                                  <select class="form-control" id="presentacion" ClientIDMode="Static" runat="server">
                                 </select>
                            </div>
                        </div>
                        <div class="row">
                             <label class="col-sm-3 label label-default" for="inputName">Cant. U. Base</label>
                                       
                            <div class="col-lg-3">
                                  <input type="text" class="form-control" id="Mcant" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="5"/>
                              
                            </div>    
                            
                            <div class="col-lg-6">
                                  <button type="submit" class="btn btn-primary" id="btnAgregar">Agregar</button>
                            </div>                             
                        </div>                 
                  
                       <div class="row">
                          <div class="box-body table-responsive">
                            <table id="idDateTablePresentacion" class="table table-bordered table-hover">
                               <thead>
                                    <tr>
                                        <th scope="col">CÓDIGO</th>    <!--Codigo del combo-->
                                        <th scope="col">PRESENTACIÓN</th>    <!--Valor del combo-->
                                        <th scope="col">CANTIDAD</th>       <!--Cantidad del combo-->
                                        <th scope="col">ACCIÓN</th>
                            
                                    </tr>
                                </thead>
                                <tbody id="idDateTableP" class="ui-sortable">                       
                        </tbody>        
                                <tfoot>
                                    <tr>
                                        <th scope="col">CÓDIGO</th>          <!--Codigo del combo-->
                                        <th scope="col">PRESENTACIÓN</th>    <!--Valor del combo-->
                                        <th scope="col">CANTIDAD</th>        <!--Cantidad del combo--> 
                                        <th scope="col">ACCIÓN</th>
                            
                                    </tr>
                                </tfoot>
                      </table>
                    </div>
                  </div>
                
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
                     
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Ventana modal EDITAR producto -->
     <div class="modal fade" id="productoEditarModal" tabindex="-1" role="dialog" aria-labelledby="productoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="MmensajeE">
                        <p id="MpmensajeE"></p>
                    </div>
                    
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">                                     
                            <input type="hidden" class="form-control" id="MskuE"/>                         
                           
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 label label-default"  for="inputEmail">Descripción</label>
                             <input type="text" class="form-control" id="MdescripcionE" maxlength="160"/>
                        </div>
                        <div class="row" >
                            <label class="col-sm-3 label label-default" for="inputName">Unidad de venta</label>
                            <div class="col-sm-3">
                                <select class="form-control" id="unidadbaseE" ClientIDMode="Static" runat="server" >
                                <option value="0" disabled>--Seleccionar--</option>
                            </select>
                            </div>
                            
                            <label class="col-sm-2 label label-default" for="categoria">Categoría</label>
                            <div class="col-sm-4">
                                 <input type="text" class="form-control" id="dllcategoriaE" disabled/>
                             <%-- <select class="form-control" id="dllcategoriaE">
                                <option value="0" disabled>--Seleccionar--</option>
                             </select>--%>
                            </div>
                        </div>
                          <div class="row">
                              <label class="col-sm-3 label label-default" for="inputEmail">Tipo Producto</label>
                              <div class="col-sm-3">
                                   <select class="form-control" id="tipoProductoE" ClientIDMode="Static" runat="server">
                                 </select>
                                </div>

                            <label class="col-sm-2 label label-default" for="inputEmail">SubCategoría</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="dllsubcategoriaE" required>
                                    <option value="0" disabled>--Seleccionar--</option>
                                 </select>
                             </div>
                            </div>
                  
                         <div class="row">
                            <label class="col-sm-3 label label-default" for="inputEmail">Fabricante</label>
                             <div class="col-sm-9">
                                  <select class="form-control fabricante" id="dllfabricanteE"  ClientIDMode="Static" runat="server">
                                 </select>
                            </div>
                        </div>
                         <div class="row">
                            <label class="col-sm-3 label label-default" for="inputEmail">Proveedor</label>
                             <div class="col-sm-9">
                                  <select class="form-control proveedor" id="dllproveedorE"  ClientIDMode="Static" runat="server">
                                 </select>
                            </div>                        
                        </div>                   
                        <div class="row">
                            <label class="col-sm-3 label label-default" >Tipo Venta</label>
                             <div class="col-sm-9">
                                  <select class="form-control" id="flagVentaE"  ClientIDMode="Static" runat="server" >                               
                                 </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-4 label label-default" for="inputEmail">Presentación Producto</label>
                             <div class="col-sm-8">
                                  <select class="form-control" id="presentacionE" ClientIDMode="Static" runat="server">
                                 </select>
                            </div>
                        </div>
                        <div class="row">
                             <label class="col-sm-3 label label-default" for="inputName">Cant. U. Base</label>
                                       
                            <div class="col-lg-3">
                                  <input type="text" class="form-control" id="McantE" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="5"/>
                            </div>    
                            
                            <div class="col-lg-6">
                                  <button type="submit" class="btn btn-primary" id="btnAgregarE">Agregar</button>
                            </div>                             
                        </div>                
                  
                        <!--DESDE AQUIiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii-->
                         <!-- Tabla para editar traeremos la data por medio de ajax -->
                        <div class="row">
                          <div class="box-body table-responsive">
                            <table id="idDatePresentacionEdit" class="table table-bordered table-hover">
                               <thead>
                                    <tr>
                                        <th scope="col">CÓDIGO</th>    <!--Codigo del combo-->
                                        <th scope="col">CODIGO2</th>    <!--Codigo del combo-->
                                        <th scope="col">PRESENTACIÓN</th>    <!--Valor del combo-->
                                        <th scope="col">CANTIDAD</th>       <!--Cantidad del combo-->
                                        <th scope="col">FLAG-OPERACION</th>       <!--OPERACION(REGISTRAR-ELIMINAR)-->
                                        <th scope="col">ACCIÓN</th>
                            
                                    </tr>
                                </thead>
                                <tbody id="idDateTablePE" class="ui-sortable">                                        
                                </tbody> 
                                 <!-- aqui traeremos la data por medio de ajax -->
                                <tfoot>
                                    <tr>
                                        <th scope="col">CÓDIGO</th>    <!--Codigo del combo-->
                                        <th scope="col">CODIGO2</th>    <!--Codigo del combo-->
                                        <th scope="col">PRESENTACIÓN</th>    <!--Valor del combo-->
                                        <th scope="col">CANTIDAD</th>       <!--Cantidad del combo-->
                                        <th scope="col">FLAG-OPERACION</th>       <!--OPERACION(REGISTRAR-ELIMINAR)-->
                                        <th scope="col">ACCIÓN</th>
                            
                                    </tr>
                                </tfoot>
                                        
                      </table>
                    </div>
                  </div>
        <!--HAST AQUIiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelarE">Cancelar</button>                            
                            <button type="submit" class="btn btn-primary" id="btnActualizar">Actualizar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
