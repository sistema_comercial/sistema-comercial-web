﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class panelControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<CENPanelControl> listarCrecimiento(int flag)
        {
            CLNPanelControl panel = new CLNPanelControl();
            try
            {
                return panel.listadoCrecimiento(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}