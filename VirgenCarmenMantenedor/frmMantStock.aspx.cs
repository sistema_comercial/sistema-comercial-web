﻿using CEN;
using CLN;
using CAD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using ExcelDataReader;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Web.Services;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantStock1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            llenarCombos();
            System.Web.HttpContext _httpContext = System.Web.HttpContext.Current;
            //VALIDAR SI SE PRESIONO EL BOTON REFRESCAR DEL NAVEGADOR - FIN

           
        }

        private void llenarCombos() {

            
            CLNFabricante clnFab = new CLNFabricante();
            CLNProveedor prov = new CLNProveedor();
            CLNCategoria clnCat = new CLNCategoria();

            List<CENProducto> listCat = clnCat.ListarCategorias(CENConstante.g_const_5);
            List<CENFabricante> listFab = clnFab.ListaFabricante(CENConstante.g_const_27);
            List<CENProveedor> listProv = prov.ListaProveedores(CENConstante.g_const_23);

            categoria_busqueda.Items.Add(new ListItem("SELECCIONE CATEGORIA","0"));
            foreach (var item in listCat)
            {
                string cod = item.codCategoria.ToString();
                var descripcion = item.descCategoria;
                categoria_busqueda.Items.Add(new ListItem(descripcion, cod));
            }

            fabricante_Busqueda.Items.Add(new ListItem("SELECCIONE FABRICANTE","0"));
            foreach (var item in listFab)
            {
                string cod = item.codigoFabricante.ToString();
                var descripcion = item.descFabricante;
                fabricante_Busqueda.Items.Add(new ListItem(descripcion, cod));
            }

            proveedor_busquead.Items.Add(new ListItem("SELECCIONE PROVEEDOR", "0"));
            foreach (var item in listProv)
            {
                string cod = item.codigoProveedor.ToString();
                var descripcion = item.descproveedor;
                proveedor_busquead.Items.Add(new ListItem(descripcion, cod));
            }
        }


        [WebMethod]
        public static List<CENProducto> listaProductos(int codCategoria, int subcategoria, int codProveedor,int codFabricante, string descProducto) {

            try
            {
                CLNProducto prod = new CLNProducto();
                CENDatosProducto response = new CENDatosProducto();
                response = prod.ListaDatosProducto(codCategoria, subcategoria, codProveedor, codFabricante, descProducto);
                return response.DatosProducto;
            }
            catch (Exception ex) {
                throw ex;
            }
            
        }
       
        [WebMethod]
        public static List<CENSubcategoria> listaSucabtegoria(int categoria) {
            try {
                CLNSubCategoria sub = new CLNSubCategoria();                
                return sub.ListarSubCategoriasPorCategoria(categoria);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [WebMethod]
        public static CENDatosDetalleAlmacen listaDetalle(string producto) {
            try {
                CLNDetalleAlmacen det = new CLNDetalleAlmacen();
                return det.Listadatosdetallealmacen(producto);
            }
            catch (Exception ex) { throw ex; }
        }
        
       
    }
}