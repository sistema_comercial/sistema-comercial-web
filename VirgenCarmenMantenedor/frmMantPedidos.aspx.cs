﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantCompras : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CLNProveedor prov = new CLNProveedor();
            CLNProducto prod = new CLNProducto();
            CLNPedido pedido = new CLNPedido();
            CLNConcepto conepto = new CLNConcepto();

            List<CENProveedor> listProv = prov.ListaProveedores(CENConstante.g_const_23);
            proveedor_save.Items.Add(new ListItem("SELECCIONE PROVEEDOR", "0"));
            proveedorBusqueda.Items.Add(new ListItem("SELECCIONE PROVEEDOR","0"));
            foreach (var item in listProv)
            {
                string cod = item.codigoProveedor.ToString();
                var descripcion = item.descproveedor;
                proveedor_save.Items.Add(new ListItem(descripcion, cod));
                proveedorBusqueda.Items.Add(new ListItem(descripcion, cod));
            }


            List<CENDetalleAlmacen> listAlmacen = pedido.listAlmacenes(CENConstante.g_const_17);
            almacen_save.Items.Add(new ListItem("SELECCIONE ALMACEN", "0"));

            foreach (var item in listAlmacen)
            {
                string cod = item.transaccion.ToString();
                var descripcion = item.descAlmacen.ToString();
                almacen_save.Items.Add(new ListItem(descripcion, cod));
            }


            List<CENProductosInsert> listProducto = pedido.listProductos(CENConstante.g_const_7);
            producto_save.Items.Add(new ListItem("SELECCIONE PRODUCTO", "0"));

            foreach (var item in listProducto)
            {
                string cod = item.codProducto;
                var descripcion = item.descProducto;
                producto_save.Items.Add(new ListItem(cod+"-"+descripcion, cod));
            }

            List<CENConcepto> listComprobante = conepto.ListarConceptos(CENConstante.g_const_22);
            comprobante_save.Items.Add(new ListItem("SELECCIONE COMPROBANTE", "0"));
            comprobante_busqueda.Items.Add(new ListItem("SELECCIONE COMPROBANTE","0"));
            foreach (var item in listComprobante)
            {
                string cod = item.correlativo.ToString();
                var descripcion = item.descripcion;
                comprobante_save.Items.Add(new ListItem(descripcion, cod));
                comprobante_busqueda.Items.Add(new ListItem(descripcion, cod));
            }
        }

        [WebMethod]
        public static string guardarPedido(List<CENCabeceraPedido> cabeceraPedido, List<CENDetallePedido> detallePedido)
            {

            string mensaje = null;
            CLNPedido cln = new CLNPedido();
            CENCabeceraPedido cabecera = new CENCabeceraPedido();
            CENDetallePedido detalle = new CENDetallePedido();
            cabecera.listaPedido = cabeceraPedido;
            detalle.listDetallePedido = detallePedido;
            try
            {
                
                return mensaje = cln.guadarPedido(cabecera,detalle);
            }
            catch (Exception ex)
            {

                throw ex;

            }

        }
       
        
        [WebMethod]
        public  static List<CENCabeceraPedido> listaPedido(string fechaPedido = null , string fechaEntrega = null, string codProducto = null, int codProvedor = 0,int comprobante = 0)
        {
            CLNPedido cln = new CLNPedido();
            List<CENCabeceraPedido> lista; 
            try {
               return lista = cln.listaPedido(fechaPedido,fechaEntrega,codProducto,codProvedor,comprobante);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [WebMethod]
        public static string eliminarPedido(int codigoPedido)
        {
            CLNPedido cln = new CLNPedido();
            try
            {
                return cln.eliminarPedido(codigoPedido);
            }
            catch (Exception ex)
            {
                throw ex;  
            }
        }
        
        [WebMethod]
        public static List<CENDetallePedido>  listarDetallePedido(int codigoPedido)
        {
            CLNPedido cln = new CLNPedido();
            List<CENDetallePedido> lista;
            try
            {
                return  lista = cln.buscarDetallePedido(codigoPedido);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static string editarPedido(List<CENCabeceraPedido> cabeceraPedido, List<CENDetallePedido> detallePedido) {
            try {
                CLNPedido cln = new CLNPedido();
                CENCabeceraPedido cabecera = new CENCabeceraPedido();
                CENDetallePedido detalle = new CENDetallePedido();
                cabecera.listaPedido = cabeceraPedido;
                detalle.listDetallePedido = detallePedido;

                return cln.editarPedido(cabecera, detalle);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [WebMethod]
        public static byte[] generarPdf( int codigoPedido) {
                byte[] binaryData = null;
            try {
                CLNPedido pedido = new CLNPedido();
                binaryData = pedido.generarPdf(codigoPedido);
            }
            catch (Exception ex) {
                throw ex;
            }
            return binaryData;
        }
    }
}