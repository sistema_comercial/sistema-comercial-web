﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmMantPagoFactura.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantPagoFactura" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="Scripts/DataTable/datatables.min.css" />
    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="icon/style.css" />
    <script src="Scripts/DataTable/datatables.min.js"></script>
    <script src="Scripts/sweetalert.min.js"></script>
    <script src="Scripts/mant_pagoFactura.js"></script>
    <title>Pago de Factura</title>
</head>
<body>
    <div class="container-fluid">
          
    </div>
        <!-- Modal -->
    <div class="modal fade" id="modalPago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Asignación de Rutas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="Mmensaje">
                        <p id="Mpmensaje"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="inputName">Vendedor</label>
                            <input type="hidden" class="form-control" id="McodOrden" disabled="disabled" />
                            <input type="hidden" class="form-control" id="McodVendedor" disabled="disabled" />
                            <input type="hidden" class="form-control" id="MRutaAnterior" disabled="disabled" />

                            <input type="text" class="form-control" id="Mvendedor" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Rutas</label>
                            <select class="form-control" id="rutas">
                                <option value="">Selecciona una opcion</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Abreviatura</label>
                            <input type="text" class="form-control" id="Mabreviatura"  />

                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Dias</label>
                            <select class="form-control" id="dias">
                                <option value="">Selecciona una opcion</option>

                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</body>

</html>
