﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmDespachoMercaderia.aspx.cs" Inherits="VirgenCarmenMantenedor.frmDespachoMercaderia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link rel="stylesheet" href="Scripts/DataTable/datatables.min.css" />
    <link rel="stylesheet" href="Scripts/DataTable/Buttons-1.6.1/css/buttons.dataTables.min.css"/>
    <link rel="stylesheet" href="icon/style.css" />
     <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.min.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.structure.min.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.theme.min.css" />
    <script src="Scripts/DataTable/datatables.min.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/buttons.flash.min.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/jszip.min.js"></script>  
    <script src="Scripts/DataTable/Buttons-1.6.1/js/vfs_fonts.js"></script>
    <script src="Scripts/DataTable/Buttons-1.6.1/js/buttons.html5.min.js"></script> 
    <script src="Scripts/DataTable/Buttons-1.6.1/js/buttons.print.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="Scripts/jqueryui/jquery-ui.min.js"></script>

    <script src="Scripts/sweetalert.min.js"></script>
    
    <script src="Scripts/DespachoMercaderiaL.js"></script>
    
    <link rel="stylesheet" href="CSS/modal_style.css" />
    <link rel="stylesheet" href="CSS/DataTable.css" />   

    <title>Despacho de Mercaderia</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="container-fluid">
         <div class="row">   
               <div class="col-xs-12">
                  <!-- ESTE SERA EL TITULO GENERAL DEL MANTENEDOR -->
                    <div class="box-header sub-titulo">
                        <h3 class="box-title ">Despacho Mercaderia</h3>
                     </div>
                <!-- ESTE SERA EL SUB TITULO GENERAL DE LO QUE SE QUIERE HACER -->
                      <div class="form-group">
                         <h4 class="box-filtros">Ingrese opción de Búsqueda</h4>
                     </div>            
                <!-- Zona de filtros -->                    
                   <div class="form-group row">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Cliente </label>
                        <div class="col-sm-8">
                          <input type="text" id="id_cliente" class="form-control" placeholder="Ingesar Nombre / Numero Documento" />
                          <input type="hidden" class="form-control horizontal" id="id_codCliente" value="0" />
                        </div>
                    </div>
                      <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">N° Venta </label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="txtVenta" />
                        </div>
                    </div>
                      <!-- div para mostrar mensaje de validacion de fecha de entrega-->
                        <div class="row form-group col-xs-12" >
                            <div class="col-xs-5 form-group " id="msjFechaE" hidden="">

                            </div>
                        </div>             
                     <div class="form-group col-sm-6">
                        <label class="col-sm-2 label label-default">Fechas </label>
                        <div class='col-sm-4'>
                            <input type="date" id="MSdiaMenor" class="form-control date-range-filter fEvalidar" min="2018-01-01" max="2100-01-01" required="required"/>
                        </div>
                        
                        <label class="col-sm-2 label label-default" style="text-align:center">- </label>
                        <div class='col-sm-4'>
                            <input type="date" id="MSdiaMayor" class="form-control date-range-filter fEvalidar" min="2018-01-01" max="2100-01-01" required="required"/>
                        </div>
                    </div>
                  
                     <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Estado </label>
                        <div class="col-sm-8">
                            <select id="MSestado" class="form-control">
                                <option value="0">--SELECCIONAR--</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Unidad Reparto </label>
                        <div class="col-sm-8">
                            <select id="MSreparto" class="form-control">
                                <option value="0">--SELECCIONAR--</option>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group col-sm-6"  style="text-align:right" >
                         <div class="col-sm-4"> </div>
                             <div class="col-sm-8">
                              <button type="submit" class="btn btn-primary" id="btnBuscar" >Buscar</button>
                        
                       </div>
                    </div>

                </div>
                <!--Fin: de filtros-->
          </div>  
       </div>          
       
          <div class="row">
        <div class="box-body table-responsive">
             <table id="id_tblDespacho" class="table table-bordered table-hover">
                   <thead>
                        <tr>
                            <th>N° VENTA</th>
                            <th>CONCATENADO</th>
                            <th>COD CLIENTE</th>
                            <th>CLIENTE</th>
                            <th>NRO DOCUMENTO</th>
                            <th>COD PUNTO ENTREGA</th>  
                            <th>PUNTO ENTREGA</th>                        
                            <th>FECHA ENTREGA</th>
                            <th>HORA ENTREGA</th>
                            <th>COD VENDEDOR</th>  
                            <th>VENDEDOR</th>   
                            <th>COD ESTADO</th>
                            <th>ESTADO</th>
                            <th>ACCIÓN</th>                           
                      </tr>
                    </thead>
                    <tbody id="tbl_body_table" class="ui-sortable">

                     </tbody>

                     <tfoot>
                         <tr>
                            <th>N° VENTA</th>
                            <th>CONCATENADO</th>
                            <th>COD CLIENTE</th>
                            <th>CLIENTE</th>
                            <th>NRO DOCUMENTO</th>
                            <th>COD PUNTO ENTREGA</th>  
                            <th>PUNTO ENTREGA</th>                        
                            <th>FECHA ENTREGA</th>
                            <th>HORA ENTREGA</th>
                            <th>COD VENDEDOR</th>  
                            <th>VENDEDOR</th>   
                            <th>COD ESTADO</th>
                            <th>ESTADO</th>
                            <th>ACCIÓN</th>            
                         </tr>
                     </tfoot>
                   
                </table>

        </div>
    </div>   
     </div>
      <!-- Ventana modal REGISTRAR DESPACHO -->
       <div class="modal fade" id="despachoModal" tabindex="-1" role="dialog" aria-labelledby="despachoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">                   
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="Mmensaje">
                        <p id="Mpmensaje"></p>
                    </div>                    
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">                 
                           <input type="hidden" class="form-control" id="McodDespacho" disabled="disabled"/>                     
                           
                        </div>
                        <div class="row">
                             <label  class="col-sm-3 label label-default" for="inputEmail">N° Venta</label>
                            <div class="col-sm-3">                                
                                 <input type="text" class="form-control" id="Mventa" disabled="disabled" />
                            </div>                                                      
                            
                             <label class="col-sm-3 label label-default" for="MfechaE">Fecha Entrega</label>
                                <div class="col-sm-3"> 
                                     <input type="text" class="form-control" id="MfechaE" disabled="disabled"/>
                                </div>                                
                           </div>

                         <div class="row ">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Punto Entrega</label>
                             <div class="col-sm-9">
                                  <input type="text" class="form-control" id="Mentrega" disabled="disabled" />
                             </div>                            
                         </div>

                        <div class="row">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Cliente</label>
                            <div class="col-sm-9">
                                 <input type="text" class="form-control" id="Mdescripcion" disabled="disabled"/>
                            </div>                            
                        </div>
                       

                        <div class="row ">                        
                          <label class="col-sm-3 label label-default" for="transporte">Transportista</label>
                            <div class="col-sm-9">
                              <select class="form-control" id="dlltransportista">
                                <option value="0">-Seleccionar-</option>
                             </select>
                            </div>
                        </div>
                                          
                       <div class="row">
                          <div class="box-body table-responsive">
                            <table id="idDateTableDespacho" class="table table-bordered table-hover">
                               <thead>
                                    <tr>
                                        <th scope="col">N° VENTA</th>    <!--Codigo de VENTA-->
                                        <th scope="col">CODIGO</th>        <!--Codigo del producto-->
                                        <th scope="col">DESCRIPCIÓN</th>    <!--Valor del combo-->
                                        <th scope="col">CANTIDAD</th>       <!--Cantidad del combo-->
                                      <%--  <th scope="col">LOTE</th>           <!--Valor del combo-->--%>
                                        <th scope="col">COD AlMACEN</th>       <!--COD DE ALMACEN-->                                        
                                        <th scope="col">ALMACEN</th>       <!--COD DE ALMACEN-->
                                        <th scope="col">ACCIÓN</th>                       
                                    </tr>
                                </thead>
                                <tbody id="idDateTableP" class="ui-sortable">                       
                        </tbody>        
                                <tfoot>
                                    <tr>
                                         <th scope="col">N° VENTA</th>    <!--Codigo de VENTA-->
                                        <th scope="col">CODIGO</th>        <!--Codigo del producto-->
                                        <th scope="col">DESCRIPCIÓN</th>    <!--Valor del combo-->
                                        <th scope="col">CANTIDAD</th>       <!--Cantidad del combo-->
                                        <%--<th scope="col">LOTE</th>           <!--Valor del combo-->--%>
                                        <th scope="col">COD AlMACEN</th>       <!--COD DE ALMACEN-->                                        
                                        <th scope="col">ALMACEN</th>       <!--COD DE ALMACEN-->
                                        <th scope="col">ACCIÓN</th>                       
                                      
                                    </tr>
                                </tfoot>
                      </table>
                    </div>
                  </div>
                
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>                       
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
        <!-- Ventana modal REGISTRO DETALLE DE DESPACHO -->
      <div class="modal fade" id="detalledespachoModal" tabindex="-1" role="dialog" aria-labelledby="despachoDetModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">    
                     <h5 class="modal-title" id="exampleModalLabel">Registro Detalle Lote</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                                   
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">                 
                           <input type="hidden" class="form-control" id="McodProducto" disabled="disabled"/>                     
                           
                        </div>

                         <div class="row ">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Producto</label>
                             <div class="col-sm-9">
                                  <input type="text" class="form-control" id="Mproducto" disabled="disabled" />
                             </div>                            
                         </div>    
                        <div class="row">
                             <label class="col-sm-3 label label-default" for="inputEmail">Cantidad</label>
                                <div class="col-sm-3"> 
                                     <input type="text" class="form-control" id="Mcant" disabled="disabled" />
                                </div>

                             <label  class="col-sm-3 label label-default" for="inputEmail">Lote</label>
                            <div class="col-sm-3">                                
                               <select class="form-control" id="Mlote">
                                <option value="0">-Seleccionar-</option>
                             </select>
                            </div>                                                    
                                              
                           </div>

                        <div class="row">
                             <label class="col-sm-3 label label-default" for="inputEmail">Cant. Saliente</label>
                                <div class="col-sm-3"> 
                                     <input type="text" class="form-control" id="McantSalida" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                </div>

                              <button type="submit" class="btn btn-primary" id="btnAgregar">Agregar</button>                                                     
                                              
                           </div>
               
                       <div class="row">
                          <div class="box-body table-responsive">
                            <table id="idDetTableDespacho" class="table table-bordered table-hover">
                               <thead>
                                    <tr>
                                        <th scope="col">LOTE</th>               <!--Codigo de lote-->
                                        <th scope="col">FECHA VENC.</th>        <!--fecha venc.-->
                                        <th scope="col">COD. PROVEEDOR</th>    <!--Valor del combo-->
                                        <th scope="col">PROVEEDOR</th>          <!--proveedor-->                                                                        
                                        <th scope="col">CANTIDAD</th>          <!--CANTIDAD-->
                                        <th scope="col">ACCIÓN</th>                       
                                    </tr>
                                </thead>
                                <tbody id="idDetDateTableP" class="ui-sortable">                       
                        </tbody>        
                                <tfoot>
                                    <tr>
                                         <th scope="col">LOTE</th>               <!--Codigo de lote-->
                                        <th scope="col">FECHA VENC.</th>        <!--fecha venc.-->
                                        <th scope="col">COD. PROVEEDOR</th>    <!--Valor del combo-->
                                        <th scope="col">PROVEEDOR</th>          <!--proveedor-->                                                                        
                                        <th scope="col">CANTIDAD</th>          <!--CANTIDAD-->
                                        <th scope="col">ACCIÓN</th>                       
                                      
                                    </tr>
                                </tfoot>
                      </table>
                    </div>
                  </div>
                
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnRegresar">Regresar</button>
                                             
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

       <!-- Ventana modal VER DETALLE DE DESPACHO -->
        
         <div class="modal fade" id="VerdespachoModal" tabindex="-1" role="dialog" aria-labelledby="despachoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">                   
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                                      
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">                 
                           <input type="hidden" class="form-control" id="McodDespachoV" disabled="disabled"/>                     
                           
                        </div>
                        <div class="row">
                            <label class="col-sm-3 label label-default"  for="inputEmail">N° Serie</label>
                            <div class="col-sm-9">
                                 <input type="text" class="form-control" id="MserieV" disabled="disabled"/>      
                            </div>                            
                        </div>
                      
                        <div class="row">
                             <label  class="col-sm-3 label label-default" for="inputEmail">N° Venta</label>
                            <div class="col-sm-3">                                
                                 <input type="text" class="form-control" id="MventaV" disabled="disabled" />
                            </div>                                                      
                            
                             <label class="col-sm-3 label label-default" for="inputEmail">Fecha Entrega</label>
                                <div class="col-sm-3"> 
                                     <input type="text" class="form-control" id="MfechaV" disabled="disabled" />
                                </div>                                
                           </div>
                        <div class="row">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Cliente</label>
                            <div class="col-sm-9">
                                 <input type="text" class="form-control" id="Mcliente" disabled="disabled"/>
                            </div>                            
                        </div>

                         <div class="row ">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Punto Entrega</label>
                             <div class="col-sm-9">
                                  <input type="text" class="form-control" id="MentregaV" disabled="disabled" />
                             </div>                            
                         </div>
                        <div class="row ">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Transporte</label>
                             <div class="col-sm-9">
                                  <input type="text" class="form-control" id="MtransporteV" disabled="disabled" />
                             </div>                            
                         </div>
                           <div class="row">
                             <label  class="col-sm-3 label label-default" for="inputEmail">Placa Vehicular</label>
                            <div class="col-sm-3">                                
                                 <input type="text" class="form-control" id="MplacaV" disabled="disabled" />
                            </div>                                                      
                            
                             <label class="col-sm-3 label label-default" for="inputEmail">Estado Reparto</label>
                                <div class="col-sm-3"> 
                                     <input type="text" class="form-control" id="MrepartoV" disabled="disabled" />
                                </div>                                
                           </div>
                         <div class="row ">
                            <label class="col-sm-3 label label-default"  for="inputEmail">Estibador</label>
                             <div class="col-sm-9">
                                  <input type="text" class="form-control" id="MestivadorV" disabled="disabled" />
                             </div>                            
                         </div>

                            
                       <div class="row">
                          <div class="box-body table-responsive">
                            <table id="idVerDateTableDespacho" class="table table-bordered table-hover">
                               <thead>
                                    <tr>
                                        <th scope="col">COD. PRODUCTO</th>    <!--Codigo de VENTA-->
                                        <th scope="col">PRODUCTO </th>        <!--Codigo del producto-->
                                        <th scope="col">CANTIDAD</th>    <!--Valor del combo-->
                                        <th scope="col">LOTE</th>       <!--Cantidad del combo-->
                                        <th scope="col">FECHA VENC.</th>           <!--Valor del combo-->
                                        <th scope="col">COD AlMACEN</th>       <!--COD DE ALMACEN-->                                        
                                        <th scope="col">ALMACEN</th>       <!--COD DE ALMACEN-->
                                        <th scope="col">ACCIÓN</th>                       
                                    </tr>
                                </thead>
                                <tbody id="idVerDateTableP" class="ui-sortable">                       
                        </tbody>        
                                <tfoot>
                                    <tr>
                                        <th scope="col">COD. PRODUCTO</th>    <!--Codigo de VENTA-->
                                        <th scope="col">PRODUCTO </th>        <!--Codigo del producto-->
                                        <th scope="col">CANTIDAD</th>    <!--Valor del combo-->
                                        <th scope="col">LOTE</th>       <!--Cantidad del combo-->
                                       <th scope="col">FECHA VENC.</th>           <!--Valor del combo-->
                                        <th scope="col">COD AlMACEN</th>       <!--COD DE ALMACEN-->                                        
                                        <th scope="col">ALMACEN</th>       <!--COD DE ALMACEN-->
                                        <th scope="col">ACCIÓN</th>                
                                      
                                    </tr>
                                </tfoot>
                      </table>
                    </div>
                  </div>
                
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancel">Regresar</button>                       
                            <button type="button" class="btn icon-file-pdf" id="btnDescargar">Descargar</button>
                       
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
