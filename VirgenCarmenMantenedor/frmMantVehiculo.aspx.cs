﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEN;
using CLN;
using Newtonsoft.Json;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantVehiculo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static int RegistrarVehiculo(string Jplaca, string Jmarca, string Jmodelo, int Jpeso, int Jfabricacion, string Jtipovehiculo)
        {
            CLNMantVehiculo objCLNSOV = null;
            CENMantVehiculo objtSOV = new CENMantVehiculo()
            {
                idPlaca = Convert.ToString(Jplaca),
                idMarca = Convert.ToString(Jmarca),
                idModelo = Convert.ToString(Jmodelo),
                idPeso = Convert.ToInt32(Jpeso),
                idFabricacion = Convert.ToInt32(Jfabricacion),
                idTipoVehiculo = Convert.ToString(Jtipovehiculo),
            };
            try
            {
                objCLNSOV = new CLNMantVehiculo();
                int ok = objCLNSOV.RegistrarVehiculo(objtSOV);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static List<CENMantVehiculo> ListarVehiculo(string Jplaca, string Jmarca, string Jmodelo, int Jpeso, int Jfabricacion, string Jtipovehiculo)
        {
            List<CENMantVehiculo> ListaC = null;
            CLNMantVehiculo objCiu = null;
            CENMantVehiculo objtSOV = new CENMantVehiculo()
            {
                idPlaca = Convert.ToString(Jplaca),
                idMarca = Convert.ToString(Jmarca),
                idModelo = Convert.ToString(Jmodelo),
                idPeso = Convert.ToInt32(Jpeso),
                idFabricacion = Convert.ToInt32(Jfabricacion),
                idTipoVehiculo = Convert.ToString(Jtipovehiculo),
            };
            try
            {
                objCiu = new CLNMantVehiculo();
                ListaC = objCiu.ObtenerVehiculo(objtSOV);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ListaC;
        }

        [WebMethod]
        public static int EliminarVehiculo(string CODIGO)
        {
            CLNMantVehiculo objCLNEliminar = null;
            try
            {
                objCLNEliminar = new CLNMantVehiculo();
                int ok = objCLNEliminar.EliminarVehiculo(CODIGO);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static int ModificarVehiculo(int Jid, string Jplaca, string Jmarca, string Jmodelo, int Jpeso, int Jfabricacion, string Jtipovehiculo)
        {
            CLNMantVehiculo objCLNSOV = null;
            CENMantVehiculo objtSOV = new CENMantVehiculo()
            {
                idVehiculo = Convert.ToInt32(Jid),
                idPlaca = Convert.ToString(Jplaca),
                idMarca = Convert.ToString(Jmarca),
                idModelo = Convert.ToString(Jmodelo),
                idPeso = Convert.ToInt32(Jpeso),
                idFabricacion = Convert.ToInt32(Jfabricacion),
                idTipoVehiculo = Convert.ToString(Jtipovehiculo),
            };
            try
            {
                objCLNSOV = new CLNMantVehiculo();
                int ok = objCLNSOV.ModificarVehiculo(objtSOV);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [WebMethod]
        public static List<CENMantConductorVehiculo> ListarConductores(int Jvehiculo)
        {
            List<CENMantConductorVehiculo> ListaC = null;
            CLNMantVehiculo objCiu = null;
            try
            {
                objCiu = new CLNMantVehiculo();
                ListaC = objCiu.ListarConductores(Jvehiculo);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ListaC;
        }

        [WebMethod]
        public static int RegistrarConductores(int Jconductor, int Jvehiculo)
        {
            CLNMantVehiculo objCLNSOV = null;
            CENMantConductorVehiculo objtSOV = new CENMantConductorVehiculo()
            {
                idPers = Convert.ToInt64(Jconductor),
                id = Convert.ToInt64(Jvehiculo),
            };
            try
            {
                objCLNSOV = new CLNMantVehiculo();
                int ok = objCLNSOV.RegistrarConductores(objtSOV);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static List<CENMantConductorVehiculo> ListarConductoresAsignados(int Jvehiculo)
        {
            List<CENMantConductorVehiculo> ListaC = null;
            CLNMantVehiculo objCiu = null;
            try
            {
                objCiu = new CLNMantVehiculo();
                ListaC = objCiu.ListarConductoresAsignados(Jvehiculo);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ListaC;
        }

        [WebMethod]
        public static int EliminarRegistro(string CODIGO)
        {
            CLNMantVehiculo objCLNEliminar = null;
            try
            {
                objCLNEliminar = new CLNMantVehiculo();
                int ok = objCLNEliminar.EliminarRegistro(CODIGO);
                return ok;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}