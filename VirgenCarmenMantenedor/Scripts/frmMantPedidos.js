﻿
$(document).ready(function () {
    
    $(".select2").select2();
    var btnEliminar = ' <button type="button" title="Anular" class="icon-bin btnDelete" data-toggle="modal" data-target="#modalAnular"></button>';
    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal" data-target=".bd-example-modal-lg" ></button>';
    var btndescargar = ' <button type="button" title="Descargar" class="icon-file-pdf btnPdf btndescargar"></button>';
    var cabeceraPedido = [];
    var detallePedido = [];
    var productosEliminados = []
    var ip;
    var user;
    

    $.getJSON('http://ip-api.com/json?callback=?', function (data) {
        ip = data.query;
    });

    function requestDatosSession() {
        $.ajax({
            type: "POST",
            url: "login.aspx/getSession",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                user = data.d[0].perfil;
            }
        });

    }

    requestDatosSession();

    function cambiarNombres() {
        $('#producto_save').attr('name','producto_save');   
        $('#proveedor_save').attr('name', 'proveedor_save');   
        $('#comprobante_save').attr('name', 'comprobante_save');   
        $('#almacen_save').attr('name', 'almacen_save');   
    }

    cambiarNombres();




    $("#tblListado_pedidos").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "10%", "targets": [0] },
            { "width": "15%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "30%", "targets": [4] },
            { "width": "10%", "targets": [6] },
            { "width": "20%", "targets": [7] },
            { "visible": false, "targets": [3, 5] }
        ]
    });

    var tablaPedidos = $("#tblListado_pedidos").DataTable();
    tablaPedidos.columns.adjust().draw();


    $("#tablaPedidos").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "10%", "targets": [1] },
            { "width": "25%", "targets": [2] },
            { "width": "15%", "targets": [3] },
            { "width": "15%", "targets": [4] },
            { "width": "10%", "targets": [6] },
            { "visible": false, "targets": [5,0] }
        ]
    });

    var tabDetalleGuardarPedido = $("#tablaPedidos").DataTable();
    tabDetalleGuardarPedido.columns.adjust().draw();

    /*********** LÓGICA PARA LISTADO DE PEDIDOS ***********/



    function listaInicial() {       

        $.ajax({
            type: "POST",
            url: "frmMantPedidos.aspx/listaPedido",
            data: JSON.stringify({ fechaPedido: null, fechaEntrega: null, codProducto: null, codProvedor: 0, comprobante:0}),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantPedidos.aspx/listaPedido :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                tablaPedidos.clear().draw();
                for (let i = 0; i < data.d.length; i++) {
                    tablaPedidos.row.add([
                        data.d[i].codPedido,
                        data.d[i].fechaPedido,
                        data.d[i].fechaEntrega,
                        data.d[i].codProveedor,
                        data.d[i].descProveedor,
                        data.d[i].tipoComprobante,
                        data.d[i].decrTipoComprobante,
                        btnEditar + btnEliminar + btndescargar]).draw();
                }

                tablaPedidos.search('').draw();
            },
        });

    }

    listaInicial();    



    /*** Funcion para eliminar un pedido ****/

    $('#tblListado_pedidos tbody').on('click', 'tr .btnDelete', function () {
        let tr = $(this).parent().parent();
        let id = tablaPedidos.row(tr).data()[0];

        $.ajax({
            type: "POST",
            url: "frmMantPedidos.aspx/eliminarPedido",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ codigoPedido: id }),
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                if (data.d.split('|')[1] == 0) {
                    swal({
                        icon: "success",
                        title: data.d.split('|')[0]
                    })
                    tablaPedidos.row(tr).remove().draw()
                } else {
                   
                    swal({
                        icon: "error",
                        title: data.d.split('|')[0]
                    })
                }
            }
        });


    })



    /*** Funcion para editar un pedido ****/

    $('#tblListado_pedidos tbody').on('click', 'tr .btnEditar', function () {
        let tr = $(this).parent().parent();
        let id = tablaPedidos.row(tr).data()[0];
        
        $.ajax({
            type: "POST",
            url: "frmMantPedidos.aspx/listarDetallePedido",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ codigoPedido: id }),
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {

                console.log(tablaPedidos.row(tr).data()[1])

                $('#codigoPedido').val(tablaPedidos.row(tr).data()[0])

                $("#comprobante_save ").val(tablaPedidos.row(tr).data()[5]);
                $("#proveedor_save ").val(tablaPedidos.row(tr).data()[3]);
                $("#proveedor_save").select2({
                    text: tablaPedidos.row(tr).data()[4]
                });

                //fecha Pedido
                let y = tablaPedidos.row(tr).data()[1].split('/')[2];
                let m = tablaPedidos.row(tr).data()[1].split('/')[1];
                let d = tablaPedidos.row(tr).data()[1].split('/')[0];
                let fecha = y + '-' + m + '-' + d;
                $("#fechaPedido_save").val(fecha);

                //fecha Entrega
                let y1 = tablaPedidos.row(tr).data()[2].split('/')[2];
                let m2 = tablaPedidos.row(tr).data()[2].split('/')[1];
                let d3 = tablaPedidos.row(tr).data()[2].split('/')[0];
                let fecha2 = y1 + '-' + m2 + '-' + d3;
               console.log(fecha2);
                $("#fechaEntrega_save").val(fecha2);  

                tabDetalleGuardarPedido.clear().draw();

                for (let i = 0; i < data.d.length; i++) {
                    tabDetalleGuardarPedido.row.add([
                        data.d[i].codDetalle,
                        data.d[i].codProducto,
                        data.d[i].descProducto,
                        data.d[i].precioCompra,
                        data.d[i].cantidadProducto,
                        data.d[i].almacenDestino,
                        data.d[i].descAlmacen,
                        btnEliminar]).draw();
                }

            }
        });


    })

    $('#tblListado_pedidos tbody').on('click', 'tr .btnPdf', function () {
        let tr = $(this).parent().parent();
        let id = tablaPedidos.row(tr).data()[0];
        var nomPdf = "Pedido_" + id + ".pdf";
        $.ajax({
            type: "POST",
            url: "frmMantPedidos.aspx/generarPdf",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ codigoPedido: id }),
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {

                var bytes = new Uint8Array(data.d);
                enviarpdf(nomPdf, bytes);
            }
        });


    })

    function enviarpdf(nomPdf, datos) {
        var blob = new Blob([datos]);
        var link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        var fileName = nomPdf;
        link.download = fileName;
        link.click();
    }

    var formularioRegistrar = $("#formularioGuardar").validate({

        rules: {
            fechaPedido_save: { required: true },
            fechaEntrega_save: { required: true},
            comprobante_save: { required: true },
            proveedor_save: { required: true },
        },
        messages: {
            fechaPedido_save: "<div class='d-block text-danger'>Ingrese la fecha del pedido</div>",
            fechaEntrega_save: "<div class='d-block text-danger'>Ingrese la fecha de entrega</div>",
            comprobante_save: "<div class='d-block text-danger'>Seleccione el comprobante </div>",
            proveedor_save: "<div class='d-block text-danger'>Seleccion el proveedor</div>",
         }
    })

    document.querySelector('#guardar').addEventListener('click', guardarPedido)
    document.querySelector('#formularioGuardar').addEventListener('submit', e => e.preventDefault())


    /*  llena la tabla de detalle*/
    $("#agregar").on('click', function () {

        let producto_save = document.querySelector('#producto_save').value;
        let producto_text = $('select[id="producto_save"] option:selected').text().split('-')[1];
        
        let precio_compra = document.querySelector('#precio_compra').value;
        let cantidad_save = document.querySelector('#cantidad_save').value;
        let almacen_text = $('select[id="almacen_save"] option:selected').text();
        let almacen_save = $('#almacen_save').val();

        if (producto_save != 0 && precio_compra != "" && cantidad_save != "" && almacen_save != 0) {

            // validar que el producto no este agregado en la tabla
            if (tabDetalleGuardarPedido.rows().count() > 0) {
                let contador = 0;
                for (let i = 0; i < tabDetalleGuardarPedido.rows().count(); i++) {                    
                    if (tabDetalleGuardarPedido.rows().data()[i][1] == producto_save) {
                        contador++;
                    } 

                }              

                if (contador == 0) {
                    tabDetalleGuardarPedido.row.add([
                        '',
                        producto_save,
                        producto_text,
                        cantidad_save,
                        precio_compra,
                        almacen_save,
                        almacen_text,
                        btnEliminar]).draw();
                } else {
                    swal({
                        icon: "info",
                        title: "El producto ya se encuentra agregado en la lista"
                    })
                }

            } else {
                tabDetalleGuardarPedido.row.add([
                    '',
                    producto_save,
                    producto_text,
                    precio_compra,
                    cantidad_save,
                    almacen_save,
                    almacen_text,
                    btnEliminar]).draw();
            }

            

           

            $('#producto_save').val(0);
            $('#producto_save').select2({
                text:'SELECCIONE PRODUCTO'    
            });
            $('#precio_compra').val("");
            $('#cantidad_save').val("");
            $('#almacen_save').val(0);
            $('#almacen_save').select2({
                text:'SELECCIONE ALMACEN'   
            });


        } else if (precio_compra <= 0){
            swal({
                icon: "info",
                title: "El precio de compra no puede ser menor a cero"
            })
        } else if (cantidad_save <= 0) {
            swal({
                icon: "info",
                title: "La cantidad a comprar debe ser mayor a cero"
            })
        } else {
            swal({
                icon: "info",
                title: "Debe completar todos los campos para poder agregar este producto al detalle"
            })
        }


         

        

        

    })


    /*Elimina un pedido de la tabla de detalle*/
    $('#tablaPedidos tbody').on('click', 'tr .btnDelete', function () {
        let tr = $(this).parent().parent();
        let data = tabDetalleGuardarPedido.row(tr).data();
        console.log(data[0])
        if (data[0] != "") {

            let detalle = {
                'codProducto': data[1],
                'cantidadProducto': data[3],
                'precioCompra': data[4],
                'almacenDestino': data[5],
                'codDetalle': data[0],
                'flagOperacion':0,
                'ip': ip,
                'user': user,
                'mac': ip
            }

            productosEliminados.push(detalle);

            tabDetalleGuardarPedido.row(tr).remove().draw()

        } else {

            tabDetalleGuardarPedido.row(tr).remove().draw()
        }

    })
 
    /** REGISTRAR PEDIDO */
    function guardarPedido() {

        let fechaPedido_save = document.querySelector('#fechaPedido_save').value;
        let fechaEntrega_save = document.querySelector('#fechaEntrega_save').value;
        let comprobante_save = document.querySelector('#comprobante_save').value;
        let proveedor_save = document.querySelector('#proveedor_save').value;


        if (formularioRegistrar.form()) {
            let cantidad = document.querySelector('#tablaPedido').rows.length;

            if (cantidad == 0) {
                swal({
                    icon: "info",
                    title: "Debe ingresar los productos que desea pedir"
                })
            } else if (fechaEntrega_save < fechaPedido_save) {
                swal({
                    icon: "info",
                    title: "La fecha de entrega debe ser mayor que la fecha de pedido"
                })
            } else if (comprobante_save == 0) {
                swal({
                    icon: "info",
                    title: "Seleccione el comprobante"
                })
            } else if (proveedor_save == 0) {
                swal({
                    icon: "info",
                    title: "Seleccione el proveedor"
                })
            } else {

                

               

                
                cabecera = {
                    'fechaPedido': fechaPedido_save,
                    'fechaEntrega': fechaEntrega_save,
                    'tipoComprobante': comprobante_save,
                    'codProveedor': proveedor_save,
                    'ip': ip,
                    'user': user,
                    'mac': ip
                }

                cabeceraPedido.push(cabecera)


                if (document.querySelector("#codigoPedido").value != "") {
                    
                    for (let i = 0; i < tabDetalleGuardarPedido.rows().count(); i++) {

                        let detalle = {
                            'codProducto': tabDetalleGuardarPedido.rows().data()[i][1],
                            'cantidadProducto': tabDetalleGuardarPedido.rows().data()[i][3],
                            'precioCompra': tabDetalleGuardarPedido.rows().data()[i][4],
                            'almacenDestino': tabDetalleGuardarPedido.rows().data()[i][5],
                            'flagOperacion': tabDetalleGuardarPedido.rows().data()[i][0] == ""  ? 1 : 3,
                            'codCabecera': document.querySelector("#codigoPedido").value,
                            'ip': ip,
                            'user': user,
                            'mac': ip
                        }
                        productosEliminados.push(detalle)
                    }

                    console.log(productosEliminados)

                    $.ajax({
                        type: "POST",
                        url: "frmMantPedidos.aspx/editarPedido",
                        data: JSON.stringify({ cabeceraPedido: cabeceraPedido, detallePedido: productosEliminados }),
                        contentType: 'application/json; charset=utf-8',
                        error: function (xhr, ajaxOtions, thrownError) {
                            console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                        },
                        success: function (data) {
                            if (data.d.split('|')[1] == 0) {
                                swal({
                                    icon: "success",
                                    title: data.d.split('|')[0]
                                })
                                $('#producto_save').val(0);
                                $('#precio_compra').val("");
                                $('#cantidad_save').val("");
                                $('#almacen_save').val(0);
                                $('#fechaPedido_save').val("");
                                $('#fechaEntrega_save').val("");
                                $('#comprobante_save').val(0);
                                $('#proveedor_save').val(0);

                                tabDetalleGuardarPedido.clear().draw();
                                productosEliminados.length = 0;
                            } else {
                                swal({
                                    icon: "warning",
                                    title: data.d.split('|')[0]
                                })
                            }
                        },
                    });

                } else {
                    for (let i = 0; i < tabDetalleGuardarPedido.rows().count(); i++) {

                        let detalle = {
                            'codProducto': tabDetalleGuardarPedido.rows().data()[i][1],
                            'cantidadProducto': tabDetalleGuardarPedido.rows().data()[i][3],
                            'precioCompra': tabDetalleGuardarPedido.rows().data()[i][4],
                            'almacenDestino': tabDetalleGuardarPedido.rows().data()[i][5],
                            'ip': ip,
                            'user': user,
                            'mac': ip
                        }
                        detallePedido.push(detalle)
                    }

                    $.ajax({
                        type: "POST",
                        url: "frmMantPedidos.aspx/guardarPedido",
                        data: JSON.stringify({ cabeceraPedido: cabeceraPedido, detallePedido: detallePedido }),
                        contentType: 'application/json; charset=utf-8',
                        error: function (xhr, ajaxOtions, thrownError) {
                            console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                        },
                        success: function (data) {
                            if (data.d.split('|')[1] == 0) {
                                swal({
                                    icon: "success",
                                    title: data.d.split('|')[0]
                                })
                                $('#producto_save').val(0);
                                $('#precio_compra').val("");
                                $('#cantidad_save').val("");
                                $('#almacen_save').val(0);
                                $('#fechaPedido_save').val("");
                                $('#fechaEntrega_save').val("");
                                $('#comprobante_save').val(0);
                                $('#proveedor_save').val(0);

                                tabDetalleGuardarPedido.clear().draw();
                            } else {
                                swal({
                                    icon: "warning",
                                    title: data.d.split('|')[0]
                                })
                            }
                        },
                    });
                }


                
            }

            


        } 

        
    }


/** funciones para busqueda **/
   

    document.querySelector("#form-busqueda").addEventListener('submit', e => e.preventDefault())
    document.querySelector("#buscarPedido").addEventListener('click', () => {

        let fechaEntrega = document.querySelector("#FechaEntrega").value == "" ? null : document.querySelector("#FechaEntrega").value ;
        let FechaEmision = document.querySelector("#FechaEmision").value == "" ? null : document.querySelector("#FechaEmision").value;
        let codPedidoBusqueda = document.querySelector("#codPedidoBusqueda").value == "" ? null : document.querySelector("#codPedidoBusqueda").value
        let proveedorBusqueda = document.querySelector("#proveedorBusqueda").value;
        let comprobante = document.querySelector("#comprobante_busqueda").value;

     


        if (fechaEntrega == null && FechaEmision == null && proveedorBusqueda == 0 && codPedidoBusqueda == null && comprobante==0 ){
            swal({
                icon: "warning",
                title: "No completo ningún filtro para su busqueda"
            })
        }  else {
       
            console.log(fechaEntrega);
            console.log(FechaEmision);
            console.log(proveedorBusqueda);
            console.log(codPedidoBusqueda);
            console.log(comprobante);



            $.ajax({
                type: "POST",
                url: "frmMantPedidos.aspx/listaPedido",
                data: JSON.stringify({ fechaPedido: FechaEmision, fechaEntrega: fechaEntrega, codProducto: codPedidoBusqueda, codProvedor: proveedorBusqueda, comprobante: comprobante }),
                contentType: 'application/json; charset=utf-8',
                error: function (xhr, ajaxOtions, thrownError) {
                    console.log("Error en frmMantPedidos.aspx/listaPedido :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                },
                success: function (data) {
                    tablaPedidos.clear().draw();
                    for (let i = 0; i < data.d.length; i++) {
                        tablaPedidos.row.add([
                            data.d[i].codPedido,
                            data.d[i].fechaPedido,
                            data.d[i].fechaEntrega,
                            data.d[i].codProveedor,
                            data.d[i].descProveedor,
                            data.d[i].tipoComprobante,
                            data.d[i].decrTipoComprobante,
                            btnEditar + btnEliminar + btndescargar]).draw();
                    }

                    tablaPedidos.search('').draw();
                },
            });

        }

    })

    const rgxNum = /^[0-9]+$/

    document.querySelector("#codPedidoBusqueda").addEventListener('', e => {
        if (e.target.value.trim().length > 0) {
            if (rgxNum.test(e.target.value.trim())) {
                swal({
                    icon: "warning",
                    title: "El código de pedido, solo debe contener número"
                })
                document.querySelector("#codPedidoBusqueda").focus;
            }

        } 
    })

})