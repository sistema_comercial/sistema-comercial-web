﻿addEventListener('load',() => {

    function llenarGrafico(data) {

        let values = Object.values(data)
        let array = []
        let label = []
        for (let i = 0; i < values.length; i++) {
            let item_array = values[i].crecimiento;
            let item_label = values[i].label;

            array.push(item_array)
            label.push(item_label)
        }


        console.log(array);

        let title = "ventas diarias"
        var ctx = document.getElementById('ventas').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: label,
                datasets: [{
                    label: title,
                    data: array,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(225, 119, 64, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 124, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 3
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }


    function cargarData(flag) {

        fetch('panelControl.aspx/listarCrecimiento', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; Scharset=utf-8'
            },
            body: JSON.stringify({ flag: flag })

        })
            .then(res => res.json())
            .then(data => { console.log(data); llenarGrafico(data.d) })
            .catch(e => console.log(e))

    }

    cargarData(1)
});

