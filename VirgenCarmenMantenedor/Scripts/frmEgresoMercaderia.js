﻿
$(document).ready(function () {

    var motivo = document.querySelector("#motivosEgreso_id");
    var motivoVentas = document.querySelector("#motivoVentas");
    var otroMotivo = document.querySelector("#otroMotivo");

    //varibales de ingreso


    var cantidad = document.querySelector("#cantidad");
    var fechaProceso = document.querySelector("#fechaProceso");
    var almacen = document.querySelector("#almacen_id");
    let direccion = document.querySelector("#direccion");
    var cabeceraPedido = [];
    var detalleIngreso = [];


    const OPERACION_INSERTAR = 1;
    const OPERACION_ELIMINAR = 0;
    const OPERACION_NEUTRO = 2;
    var id_egreso = document.querySelector("#id_egreso");

    var btnEliminar = ' <button type="button" title="Anular" class="icon-bin btnDelete" data-toggle="modal" data-target="#modalAnular"></button>';
    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal" data-target=".bd-example-modal-lg" ></button>';
    var btndescargar = ' <button type="button" title="Descargar" class="icon-file-pdf btnPdf btndescargar"></button>';
    var ip;
    var user;
    var lote;

    const G_CONST_1050 = 1050;

    $(".select2").select2();

    $.getJSON('http://ip-api.com/json?callback=?', function (data) {
        ip = data.query;
    });

    function requestDatosSession() {
        $.ajax({
            type: "POST",
            url: "login.aspx/getSession",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                user = data.d[0].perfil;
            }
        });

    }

    requestDatosSession();


    function init() {
        otroMotivo.classList.add('d-none');
        motivoVentas.classList.add('d-none');
        otroMotivo.classList.remove('d-block');
        motivoVentas.classList.remove('d-block');
        motivo.value == G_CONST_1050;
        var f = new Date();
        document.querySelector("#fechaProceso").value = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
    }

    init();



    motivo.addEventListener('change', function () {
        changeSelect();
    });

    function changeSelect() {
        motivo.value == '1051' ? motivoVentas.classList.replace('d-none', 'd-block') : motivoVentas.classList.replace('d-block', 'd-none');
        motivo.value == '1052' ? otroMotivo.classList.replace('d-none', 'd-block') : otroMotivo.classList.replace('d-block', 'd-none');
        motivo.value == G_CONST_1050 ? init() : '';
    }

    $("#tablaEgresos").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "20%", "targets": [3] },
            { "width": "30%", "targets": [4] },
            { "width": "10%", "targets": [6] },
            { "width": "10%", "targets": [7] },
            { "visible": false, "targets": [5, 0,8] }
        ]
    });

    var tablaEgresos = $("#tablaEgresos").DataTable();
    tablaEgresos.columns.adjust().draw();


    $("#tblListado_egresos").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [0] },
            { "width": "15%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "25%", "targets": [4] },
            { "width": "25%", "targets": [5] },
            { "visible": false, "targets": [3] }
        ]
    });

    var tblListado_egresos = $("#tblListado_egresos").DataTable();
    tblListado_egresos.columns.adjust().draw();


    /******* boton para agregar productos a la taba de detalle *****/

    document.querySelector("#btn_agregar").addEventListener('click', function () {

        var producto_id = document.querySelector("#producto_id");

        var producto_text = $('select[id="producto_id"] option:selected').text();
        var almacen_text = $('select[id="almacen_id"] option:selected').text();


        if (producto_id.value == 0) {
            swal({
                icon: "warning",
                title: "SELECCIONE EL PRODUCTO QUE DESEA AGREGAR"
            })
        } else if (almacen.value == 0) {
            swal({
                icon: "warning",
                title: "SELECCIONE EL ALMACEN"
            })
        } else if (cantidad.value == "") {
            swal({
                icon: "warning",
                title: "INGRESE A CANTIDAD DE PRODUCTO QUE DESEA AGREGAR"
            })
        } else if (cantidad.value == 0) {
            swal({
                icon: "warning",
                title: "LA  CANTIDAD DEL PRODUCTO QUE DESEA AGREGAR DEBE SER MAYOR A 0"
            })
        } else {
            let item = tablaEgresos.rows().count();

            item = item + 1;

            if (tablaEgresos.rows().count() > 0) {

                let contador = 0;

                for (let i = 0; i < tablaEgresos.rows().count(); i++) {
                    if (tablaEgresos.rows().data()[i][2] == producto_id.value) {
                        contador++;
                    }

                }

                if (contador == 0) {
                    tablaEgresos.row.add([
                        '',
                        item,
                        producto_id.value,
                        producto_text.split("-")[1],
                        parseInt(cantidad.value,10),
                        almacen.value,
                        almacen_text,
                        btnEliminar,
                        item
                    ]).draw();
                } else {
                    swal({
                        icon: "info",
                        title: "El producto ya se encuentra agregado en la lista"
                    })
                }

            } else {
                tablaEgresos.row.add([
                    '',
                    item,
                    producto_id.value,
                    producto_text,
                    parseInt(cantidad.value, 10),
                    almacen.value,
                    almacen_text,
                    btnEliminar,
                    item
                ]).draw();
            }

            tablaEgresos.search('').draw();
        }


        

    });

    /******* boton para eliminar un producto de la taba de detalle *****/

    $('#tablaEgresos tbody').on('click', 'tr .btnDelete', function () {
        let tr = $(this).parent().parent();
        if (tablaEgresos.row(tr).data()[0] == "") {

            tablaEgresos.row(tr).remove().draw()

        } else {

            let data = tablaEgresos.row(tr).data()

            let detalle = {
                'codProducto': data[2],
                'cantidadProducto': data[4],
                'codAlmacen': data[5],
                'flagOperacion': OPERACION_ELIMINAR,
                'ntraEgreso': id_egreso.value,
                'ntraEgresoDetalle': data[0],
                'ip': ip,
                'usuario': user,
                'mac': ip
            }
            detalleIngreso.push(detalle)
            tablaEgresos.row(tr).remove().draw()


        }

    });



    document.querySelector('#nuevoEgreso').addEventListener('click', () => {
        limpiarFormulario();
    });

    /****** LÓGICA PARA LISTAR *******/

    function list(fecha, egreso, motivo) {

        $.ajax({
            type: "POST",
            url: "frmMantEgresoMercaderia.aspx/listaEgresos",
            data: JSON.stringify({ fechaIngreso: fecha, codeEgreso: egreso, motivo: motivo }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantIngresos.aspx/listaEgresos :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                tblListado_egresos.clear().draw();
                if (data.d != null) {
                    for (let i = 0; i < data.d.length; i++) {
                        tblListado_egresos.row.add([
                            data.d[i].codEgreso,
                            data.d[i].fechaEgreso,
                            data.d[i].direccion,
                            data.d[i].motivo,
                            data.d[i].descMotivo,
                            btnEditar + btnEliminar + btndescargar
                        ]).draw();
                    }
                }
                tblListado_egresos.search('').draw();
            }
        });
    }

    list(null, 0, G_CONST_1050);

    /******* LÓGICA PARA GUARDAR UN INGRESO ******/

    document.querySelector("#btn_guardar").addEventListener('click', function () {

        document.querySelector("#mensaje").classList.replace('d-none', 'd-block');
      
        if (tablaEgresos.rows().count() == 0) {
            swal({
                icon: "warning",
                title: "DEBE AGREGAR LA LISTA DE PRODUCTOS QUE DESEA REGISTRAR"
            })
        } else if (fechaProceso.value == "") {
            swal({
                icon: "warning",
                title: "INGRESE LA FECHA DE EGRESO"
            })
        } else {

            if (id_egreso.value != "") {
                cabecera = {
                    'fechaEgreso': fechaProceso.value,
                    'direccion': direccion.value,
                    'motivo': motivo.value,
                    'codEgreso': id_egreso.value
                }

                cabeceraPedido.push(cabecera);

                for (let i = 0; i < tablaEgresos.rows().count(); i++) {
                    let detalle = {
                        'codProducto': tablaEgresos.rows().data()[i][2],
                        'cantidadProducto': tablaEgresos.rows().data()[i][4],

                        'lote': tablaEgresos.rows().data()[i][8],
                        'codAlmacen': tablaEgresos.rows().data()[i][5],
                        'flagOperacion': tablaEgresos.rows().data()[i][0] == "" ? OPERACION_INSERTAR : OPERACION_NEUTRO,
                        'ip': ip,
                        'usuario': user,
                        'mac': ip
                    }
                    detalleIngreso.push(detalle)
                }

                //editar ingreso

                $.ajax({
                    type: "POST",
                    url: "frmMantEgresoMercaderia.aspx/editarEgreso",
                    data: JSON.stringify({ cabecera: cabeceraPedido, detalle: detalleIngreso }),
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        if (data.d.split('|')[1] == 0) {
                            swal({
                                icon: "success",
                                title: data.d.split('|')[0]
                            });

                            limpiarFormulario();
                            detalleIngreso.length = 0;
                            list(null, 0, G_CONST_1050);
                        } else {
                            swal({
                                icon: "error",
                                title: "SE PRESENTO UN ERROR AL INTENTAR HACER LA ACTUALIZACION DEL REGISTRO"
                            })
                        }
                    },
                    complete: function (status) {
                        document.querySelector("#mensaje").classList.replace('d-block', 'd-none');
                    }
                });
            } else {

                cabecera = {
                    'fechaEgreso': fechaProceso.value,
                    'motivo': motivo.value,
                    'direccion': direccion.value,
                    'ip': ip,
                    'usuario': user,
                    'mac': ip
                }

                cabeceraPedido.push(cabecera);

                for (let i = 0; i < tablaEgresos.rows().count(); i++) {

                    let detalle = {
                        'codProducto': tablaEgresos.rows().data()[i][2],
                        'cantidadProducto': tablaEgresos.rows().data()[i][4],
                        'lote': tablaEgresos.rows().data()[i][8],
                        'codAlmacen': tablaEgresos.rows().data()[i][5],
                        'ip': ip,
                        'usuario': user,
                        'mac': ip
                    }
                    detalleIngreso.push(detalle)
                }

                $.ajax({
                    type: "POST",
                    url: "frmMantEgresoMercaderia.aspx/guardarEgreso",
                    data: JSON.stringify({ cabecera: cabeceraPedido, detalle: detalleIngreso }),
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        if (data.d.split('|')[1] == 0) {
                            swal({
                                icon: "success",
                                title: data.d.split('|')[0]
                            });


                            limpiarFormulario();
                            detalleIngreso.length = 0;
                            list(null, 0, G_CONST_1050);
                        } else if (data.d.split('|')[1] == 1) {
                            swal({
                                icon: "error",
                                title: data.d.split('|')[0]
                            })
                        } else {
                            swal({
                                icon: "error",
                                title: "SE PRESENTO UN PROBLEMA AL INTENTAR REGISTRAR EL EGRESO DE LA MERCADERIA"
                            })
                        }
                    },
                    complete: function (status) {
                        document.querySelector("#mensaje").classList.replace('d-block', 'd-none');
                    }
                });
            }

        }


    });


    /***** ELIMINAR INGRESO *****/

    $('#tblListado_egresos tbody').on('click', 'tr .btnDelete', function () {
        let tr = $(this).parent().parent();
        let id = tblListado_egresos.row(tr).data()[0];

        $.ajax({
            type: "POST",
            url: "frmMantEgresoMercaderia.aspx/eliminarPedido",
            data: JSON.stringify({ codEgreso: id }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantProm.aspx/eliminarPedido :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                if (data.d.split('|')[1] == 0) {
                    swal({
                        icon: "success",
                        title: data.d.split('|')[0]
                    })

                    tblListado_egresos.row(tr).remove().draw();
                } else {
                    swal({
                        icon: "error",
                        title: "No se pudo eliminar el ingreso seleccionado"
                    })
                }
            },
            complete: function (status) {
                document.querySelector("#mensaje").classList.replace('d-block', 'd-none');
            }
        });

    });


    /***** EDITAR INGRESO *****/

    $('#tblListado_egresos tbody').on('click', 'tr .btnEditar', function () {
        let tr = $(this).parent().parent();
        let id = tblListado_egresos.row(tr).data()[0];
        limpiarFormulario();
        $.ajax({
            type: "POST",
            url: "frmMantEgresoMercaderia.aspx/listaIngresoDetalle",
            data: JSON.stringify({ codigo: id }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantEgresoMercaderia.aspx/listaIngresoDetalle :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                console.log(data.d);
                document.querySelector("#id_egreso").value = id;

                let motivoTabla = tblListado_egresos.row(tr).data()[3];
                motivo.value = motivoTabla;

                let y = tblListado_egresos.row(tr).data()[1].split('/')[2];
                let m = tblListado_egresos.row(tr).data()[1].split('/')[1];
                let d = tblListado_egresos.row(tr).data()[1].split('/')[0];

                fechaProceso.value = y + "-" + m + "-" + d;
                direccion.value = tblListado_egresos.row(tr).data()[2];
                changeSelect();
                llenarFormularioEditar(data.d)
            }
        });

    });

    function llenarFormularioEditar(detalle) {


        for (let i = 0; i < detalle.length; i++) {
            tablaEgresos.row.add([
                detalle[i].ntraEgresoDetalle,
                i,
                detalle[i].codProducto,
                detalle[i].descProducto,
                detalle[i].cantidadProducto,
                detalle[i].codAlmacen,
                detalle[i].descAlmacen,
                btnEliminar,
                detalle[i].lote
            ]).draw();
        }

        tablaEgresos.search('').draw();
    }

    /*********** LÓGICA PARA BUSQUEDA ***********/
    document.querySelector("#buscarEgreso").addEventListener('click', function () {

        document.querySelector("#form-busqueda").addEventListener('submit', e => e.preventDefault());

        let motivoBusqueda = document.querySelector("#motivoBusqueda").value;
        let codigoIngresoBusqueda = document.querySelector("#codigoEgresoBusqueda").value == "" ? 0 : document.querySelector("#codigoEgresoBusqueda").value;
        let fechaIngreso = document.querySelector("#fechaEgreso").value == "" ? null : document.querySelector("#fechaEgreso").value;

        if (motivoBusqueda == G_CONST_1050 && codigoIngresoBusqueda == 0 && fechaIngreso == null ) {
            swal({
                icon: "error",
                title: "Para iniciar una  busqueda debe ingresar un parámetro"
            })
        } else {
            list(fechaIngreso, codigoIngresoBusqueda, motivoBusqueda);
        }


    });


    /**** LÓGICA PARA GENERAR PDF ***/


    $('#tblListado_egresos tbody').on('click', 'tr .btnPdf', function () {
        let tr = $(this).parent().parent();
        let id = tblListado_egresos.row(tr).data()[0];
        var nomPdf = "INGRESO_" + id + ".pdf";
        $.ajax({
            type: "POST",
            url: "frmMantEgresoMercaderia.aspx/generarPdf",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ codEgreso: id }),
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {

                var bytes = new Uint8Array(data.d);
                enviarpdf(nomPdf, bytes);
            }
        });


    })

    function enviarpdf(nomPdf, datos) {
        var blob = new Blob([datos]);
        var link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        var fileName = nomPdf;
        link.download = fileName;
        link.click();
    }


    /**** LÓGICA PARA LIMPIEZA DE  FORMULARIO DE REISTRO Y DE EDICION ****/

    function limpiarFormulario() {
        id_egreso.value = "";
        cantidad.value = "";
        fechaProceso.value = "";
        direccion.value = ""
        $('#almacen_id').val(0);
        $('#almacen_id').select2({
            text: "SELECCIONE ALMACEN"
        });

        $('#producto_id').val(0);
        $('#producto_id').select2({
            text: "SELECCIONE PRODUCTO"
        });

        tablaEgresos.clear().draw();

        init();
    }

})