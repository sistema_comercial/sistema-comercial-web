﻿$(document).ready(function () {

    //var mGuardar = $("#mGuardar");

    //var Mmensaje = $("#Mmensaje");
    //var Mpmensaje = $("#Mpmensaje");
    //var phtml = $("#phtml");
    var msjAlert = $("#msjAlert");

    //TITULOS
    var exampleModalLabel_editar = $("#exampleModalLabel_editar");
    var exampleModalLabel_detalle = $("#exampleModalLabel_detalle");
    var exampleModalLabel_agregar = $("#exampleModalLabel_agregar");

    //BUTTONS
    var buttonModal = $("#buttonModal");

    var btnActivar = $("#btnActivar");
    var btnBloquear = $("#btnBloquear");
    var btnCancelar = $("#btnCancelar");
    var btnGuardar = $("#btnGuardar");
    var btnActualizar = $("#btnActualizar");

    var btnGeneraPass = $("#btnGeneraPass");

    //CAMPOS MODAL
    var dniPer = $("#dniPer");
    var selectPerfil = $("#Perfiles");
    var nomUser = $("#nomUser");
    var pass = $("#pass");
    var selectSucursal = $("#id_sucursal");
    var selectEstado = $("#id_Estado");
    var nomPer = $("#nomPer");
    var telPer = $("#telPer");
    var celPer = $("#celPer")

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if (key < 48 || key > 57) {
            return false;
        } else {
            return true;
        }
    };

    dniPer.keypress(validateNumber);
    telPer.keypress(validateNumber);
    celPer.keypress(validateNumber);

    function limpiarCampos() {
        dniPer.val("");
        selectPerfil.prop('selectedIndex', "");
        nomUser.val("");
        pass.val("");
        selectSucursal.prop('selectedIndex', "");
        selectEstado.prop('selectedIndex', "");
        nomPer.val("");
        telPer.val("");
        celPer.val("");
    }

    buttonModal.click(function () {
        //Mmensaje.css('display', 'none');

        exampleModalLabel_editar.css('display', 'none');
        exampleModalLabel_detalle.css('display', 'none');
        exampleModalLabel_agregar.css('display', 'block');
        //var tableBodyTR = $("#tbl_body_table tr");

        //var optionSelected = vendedores.find("option:selected");
        //var optionSelectedText = optionSelected.text();
        //var optionSelectedVal = optionSelected.val();
        //var codOrden;


        //Mvendedor.val(optionSelectedText);
        //McodVendedor.val(optionSelectedVal);
        //if (isNaN(tableBodyTR.last()[0].cells[0].innerText)) {

        //codOrden = 1;
        //} else {
        //codOrden = parseInt(tableBodyTR.last()[0].cells[0].innerText) + 1;
        //}

        //McodOrden.val(codOrden);
        //perfiles.prop('selectedIndex', "");
        //id_sucursal.prop('selectedIndex', "");
        //Mabreviatura.val("");
        //selectDias.val("");

        limpiarCampos();
        dniPer.prop("disabled", false);

        btnActivar.css("display", "none");
        btnBloquear.css("display", "none");
        btnActualizar.css("display", "none");
        //btnCancelar.css("margin-right", "90px");
        btnCancelar.css("display", "block")
            .css("float", "right");

        btnGuardar.css("display", "block")
            .css("float", "right");
        //.css("margin-top", "-34px")
        //.css("margin-left", "460px");



    });

    //GENERACIÓN CONTRASEÑA - INICIO

    var tamanyo_password = 15;			// definimos el tamaño que tendrá nuestro password

    var caracteres_conseguidos = 0;			// contador de los caracteres que hemos conseguido
    var caracter_temporal = '';

    var array_caracteres = new Array();// array para guardar los caracteres de forma temporal

    for (var i = 0; i < tamanyo_password; i++) {		// inicializamos el array con el valor null
        array_caracteres[i] = null;
    }

    var password_definitivo = '';

    // función que genera un número aleatorio entre los límites superior e inferior pasados por parámetro
    function genera_aleatorio(i_numero_inferior, i_numero_superior) {
        var i_aleatorio = Math.floor((Math.random() * (i_numero_superior - i_numero_inferior + 1)) + i_numero_inferior);
        return i_aleatorio;
    }


    // función que genera un tipo de caracter en base al tipo que se le pasa por parámetro (mayúscula, minúscula, número, símbolo o aleatorio)
    function genera_caracter(tipo_de_caracter) {
        // hemos creado una lista de caracteres específica, que además no tiene algunos caracteres como la "i" mayúscula ni la "l" minúscula para evitar errores de transcripción
        var lista_de_caracteres = '$+=?@_23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz';
        var caracter_generado = '';
        var valor_inferior = 0;
        var valor_superior = 0;

        switch (tipo_de_caracter) {
            case 'minúscula':
                valor_inferior = 38;
                valor_superior = 61;
                break;
            case 'mayúscula':
                valor_inferior = 14;
                valor_superior = 37;
                break;
            case 'número':
                valor_inferior = 6;
                valor_superior = 13;
                break;
            case 'símbolo':
                valor_inferior = 0;
                valor_superior = 5;
                break;
            case 'aleatorio':
                valor_inferior = 0;
                valor_superior = 61;

        } // fin del switch

        caracter_generado = lista_de_caracteres.charAt(genera_aleatorio(valor_inferior, valor_superior));
        return caracter_generado;
    } // fin de la función genera_caracter()


    // función que guarda en una posición vacía aleatoria el caracter pasado por parámetro
    function guarda_caracter_en_posicion_aleatoria(caracter_pasado_por_parametro) {
        var guardado_en_posicion_vacia = false;
        var posicion_en_array = 0;

        while (guardado_en_posicion_vacia != true) {
            posicion_en_array = genera_aleatorio(0, tamanyo_password - 1);	// generamos un aleatorio en el rango del tamaño del password

            // el array ha sido inicializado con null en sus posiciones. Si es una posición vacía, guardamos el caracter
            if (array_caracteres[posicion_en_array] == null) {
                array_caracteres[posicion_en_array] = caracter_pasado_por_parametro;
                guardado_en_posicion_vacia = true;
            }
        }
    }


    // función que se inicia una vez que la página se ha cargado
    function generar_contrasenya() {

        caracteres_conseguidos = 0;			// contador de los caracteres que hemos conseguido
        caracter_temporal = '';

        array_caracteres = new Array();// array para guardar los caracteres de forma temporal

        for (var i = 0; i < tamanyo_password; i++) {		// inicializamos el array con el valor null
            array_caracteres[i] = null;
        }

        password_definitivo = '';

        // generamos los distintos tipos de caracteres y los metemos en un password_temporal
        caracter_temporal = genera_caracter('minúscula');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        caracteres_conseguidos++;

        caracter_temporal = genera_caracter('mayúscula');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        caracteres_conseguidos++;

        caracter_temporal = genera_caracter('número');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        caracteres_conseguidos++;

        caracter_temporal = genera_caracter('símbolo');
        guarda_caracter_en_posicion_aleatoria(caracter_temporal);
        caracteres_conseguidos++;

        // si no hemos generado todos los caracteres que necesitamos, de forma aleatoria añadimos los que nos falten
        // hasta llegar al tamaño de password que nos interesa
        while (caracteres_conseguidos < tamanyo_password) {
            caracter_temporal = genera_caracter('aleatorio');
            guarda_caracter_en_posicion_aleatoria(caracter_temporal);
            caracteres_conseguidos++;
        }

        // ahora pasamos el contenido del array a la variable password_definitivo
        for (var i = 0; i < array_caracteres.length; i++) {
            password_definitivo = password_definitivo + array_caracteres[i];
        }

        return password_definitivo;
    }

    //GENERACIÓN CONTRASEÑA - FIN

    btnGeneraPass.click(function () {
        $("#pass").val(generar_contrasenya());
    });

    btnGuardar.click(function () {
        var valuedniPer = dniPer.val();
        var optionSelectedPerfil = selectPerfil.find("option:selected");
        var valueSelectedPerfil = optionSelectedPerfil.val();
        var valuenomUser = nomUser.val();
        var valuePass = pass.val();
        var optionSelectedSucursal = selectSucursal.find("option:selected");
        var valueSelectedSucursal = optionSelectedSucursal.val();
        var optionSelectedEstado = selectEstado.find("option:selected");
        var valueSelectedEstado = optionSelectedEstado.val();
        var valuenomPer = nomPer.val();
        var valuetelPer = telPer.val();
        var valuecelPer = celPer.val();
        /*var json = JSON.stringify({
            usuario: {
                nomUsuario: nomUser,
                estado: valueSelectedEstado,
                desPerfil: ,
                desSucursal: ,
                codPerfil: valueSelectedPerfil,
                codSucursal: valueSelectedSucursal,
                codPersona: valuedniPer,
                telefono: valuetelPer,
                celular: valuecelPer
            }
            });*/

        if (valuedniPer != "" && valuedniPer.length == 8) {
            if (valueSelectedPerfil > 0) {
                if (valuenomUser != "") {
                    if (valuePass != "") {
                        if (valueSelectedSucursal > 0) {
                            if (valueSelectedEstado > 0) {
                                if (valuenomPer != "") {
                                    if (valuetelPer != "" && valuedniPer.length == 9) {
                                        if (valuecelPer != "" && valuedniPer.length == 9) {
                                            RegistrarUsuario(json);
                                        } else {
                                            celPer.attr("required", "required");
                                            msjAlert.removeAttr("hidden");
                                            msjAlert.html("<h5>Celular debe tener 9 dígitos</h5>");
                                            msjAlert.css('height', '30px');
                                            msjAlert.css("color", "#ffffff");
                                            msjAlert.css("background-color", "#EC340F")
                                                .css("border-color", "#2e6da4");
                                            setTimeout(function () {
                                                msjAlert.attr("hidden", true);
                                            }, 3000);
                                        }
                                    } else {
                                        telPer.attr("required", "required");
                                        msjAlert.removeAttr("hidden");
                                        msjAlert.html("<h5>Teléfono debe tener 9 dígitos</h5>");
                                        msjAlert.css('height', '30px');
                                        msjAlert.css("color", "#ffffff");
                                        msjAlert.css("background-color", "#EC340F")
                                            .css("border-color", "#2e6da4");
                                        setTimeout(function () {
                                            msjAlert.attr("hidden", true);
                                        }, 3000);
                                    }
                                } else {
                                    nomPer.attr("required", "required");
                                }
                            } else {
                                selectEstado.attr("required", "required");
                            }
                        } else {
                            selectSucursal.attr("required", "required");
                        }
                    } else {
                        msjAlert.removeAttr("hidden");
                        msjAlert.html("<h5>Debe dar clic en el botón generar contraseña aleatoria</h5>");
                        msjAlert.css('height', '50px');
                        msjAlert.css("color", "#ffffff");
                        msjAlert.css("background-color", "#EC340F")
                            .css("border-color", "#2e6da4");
                        setTimeout(function () {
                            msjAlert.attr("hidden", true);
                        }, 3000);
                    }
                } else {
                    nomUser.attr("required", "required");
                }
            } else {
                selectPerfil.attr("required", "required");
            }
        } else {
            dniPer.attr("required", "required");
            msjAlert.removeAttr("hidden");
            msjAlert.html("<h5>DNI debe tener 8 dígitos</h5>");
            msjAlert.css('height', '30px');
            msjAlert.css("color", "#ffffff");
            msjAlert.css("background-color", "#EC340F")
                .css("border-color", "#2e6da4");
            setTimeout(function () {
                msjAlert.attr("hidden", true);
            }, 3000);
            dniPer.focus();
        }

    });


});