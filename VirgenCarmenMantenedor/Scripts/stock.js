﻿
$(document).ready(function () {

    var btnVer = ' <button type="button" title="Ver" class="icon-eye btnEye btnVer" data-toggle="modal"  data-target=".bd-example-modal-lg" ></button>';

    var ip;
    var user;

    const G_CONST_0 = 0;

    $(".select2").select2();
     
    
    $("#tblListado_stock").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "15%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "20%", "targets": [3] },
            { "width": "20%", "targets": [5] },
            { "width": "17%", "targets": [7] },
            { "width": "10%", "targets": [9] },
            { "width": "3%", "targets": [10] },
            { "visible": false, "targets": [0,2,4,6,8] }
        ]
    });

    var tblListado_stock = $("#tblListado_stock").DataTable();
    tblListado_stock.columns.adjust().draw();


    $("#tblAlmacenes").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "15%", "targets": [0] },
            { "width": "15%", "targets": [1] },
            { "width": "20%", "targets": [2] },
            { "width": "20%", "targets": [3] },
            { "width": "20%", "targets": [4] },
        ]
    });

    var tblAlmacenes = $("#tblAlmacenes").DataTable();
    tblAlmacenes.columns.adjust().draw();

    function busqueda(codCategoria, subcategoria, codProveedor, codFabricante, descProducto) {
        $.ajax({
            type: "POST",
            url: "frmMantStock.aspx/listaProductos",
            data: JSON.stringify({ codCategoria: codCategoria, subcategoria: subcategoria, codProveedor: codProveedor, codFabricante: codFabricante, descProducto: descProducto }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                console.log(data.d);
                tblListado_stock.clear().draw();

                for (let i = 0; i < data.d.length; i++) {
                    tblListado_stock.row.add([
                        data.d[i].codCategoria,
                        data.d[i].descCategoria,
                        data.d[i].codSubCategoria,
                        data.d[i].descSubCategoria,
                        data.d[i].codProveedor,
                        data.d[i].descProveedor,
                        data.d[i].codFabricante,
                        data.d[i].descFabricante,
                        data.d[i].cantUnidBase,
                        data.d[i].descUndBase,
                        btnVer
                    ]).draw()
                }
            }
        });
    }

    busqueda(G_CONST_0, G_CONST_0, G_CONST_0, G_CONST_0, "");

    /*********** CHANGE DE LAS CATEGORIAS ******/
    document.querySelector("#categoria_busqueda").addEventListener('change', () => {
        let cat = document.querySelector("#categoria_busqueda").value;
        $.ajax({
            type: "POST",
            url: "frmMantStock.aspx/listaSucabtegoria",
            data: JSON.stringify({ categoria: cat }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                $("#subcategoria_busqueda").empty();
                for (let i = 0; i < data.d.length;i++) {
                    $("#subcategoria_busqueda").prepend("<option value=" + data.d[i].NtraSubcategoria + " >" + data.d[i].DescSubcategoria + "</option>");  // unidad base
                }
            }
        });
    })


    /************** BUSQUEDA ******************/
    document.querySelector("#buscarStock").addEventListener('click', () => {
        let codCategoria = document.querySelector("#categoria_busqueda").value;
        let subcategoria = document.querySelector("#subcategoria_busqueda").value;
        let codProveedor = document.querySelector("#proveedor_busquead").value;
        let codFabricante = document.querySelector("#fabricante_Busqueda").value;
        let descProducto = document.querySelector("#descripcion_busqueda").value;

        if (codCategoria == 0 && subcategoria == "", codProveedor == 0, codFabricante == 0, descProducto == "") {
            swal({
                icon: "error",
                title: "Ingrese un parámetro para iniciar la busqueda"
            })
        } else {
            subcategoria == "" ? subcategoria = G_CONST_0 : subcategoria = subcategoria
            busqueda(codCategoria, subcategoria, codProveedor, codFabricante, descProducto);
        }

    })


    /*******EXPORTAR EXCEL********************/

    document.querySelector('#btnreporte').addEventListener('click', () => {
        let date = new Date();
        let name = 'stock' + date
        exportTableToExcel('tblListado_stock',name);
    });

    function exportTableToExcel(tableID, filename = '') {
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

        // Specify file name
        filename = filename ? filename + '.xls' : 'excel_data.xls';

        // Create download link element
        downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }
    }

/******** BUSQUEDA DE DETALLE *********/

    $('#tblAlmacenes tbody').on('click', 'tr .btnVer', function () {

        let tr = $(this).parent().parent();
        let id = tblAlmacenes.row(tr).data()[0];

        $.ajax({
            type: "POST",
            url: "frmMantStock.aspx/listaDetalle",
            data: JSON.stringify({ producto: id }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                console.log(data);
                //tblListado_stock.clear().draw();

                //for (let i = 0; i < data.d.length; i++) {
                //    tblAlmacenes.row.add([
                //        data.d[i].codCategoria,
                //        data.d[i].descCategoria,
                //        data.d[i].codSubCategoria,
                //        data.d[i].descSubCategoria,
                //        data.d[i].codProveedor,
                //    ]).draw()
                //}
            }
        });


    });

});