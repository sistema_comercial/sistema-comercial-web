﻿$(document).ready(function () {

    var tbl_clientes = $("#tbl_clientes");
    var tbl_direcciones_add = $("#tbl_direcciones_add");

    var frmAgregarDireccion = $("#frmAgregarDireccion");
    var depaRD = $("#depaRD");
    var provRD = $("#provRD");
    var distRD = $("#distRD");
    var direRD = $("#direRD");
    var btnGuardarDireccion = $("#btnGuardarDireccion");

    depaRD.attr("name", "depaRD");
    provRD.attr("name", "provRD");
    distRD.attr("name", "distRD");


    tbl_clientes.DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        }
    });

    var tb_clientes = tbl_clientes.DataTable();
    tb_clientes.columns.adjust().draw();

    tbl_direcciones_add.DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        }
    });

    var tb_direcciones_add = tbl_direcciones_add.DataTable();
    tb_direcciones_add.columns.adjust().draw();

    function cargarProvinciasRD(data) {
        let listData = Object.values(data);
        for (let i = 0; i < listData.length; i++) {
            let option = document.createElement('option');
            option.value = listData[i].codProvincia;
            option.text = listData[i].nombre;
            provRD.append(option);
        }
    }
    
    depaRD.change(function () {
        provRD.find('option').remove().end().append('<option value="0">SELECCIONAR</option>').val(0);
        distRD.find('option').remove().end().append('<option value="0">SELECCIONAR</option>').val(0);
        var codDepRD = depaRD.val();
        $.ajax({
            type: "POST",
            url: "frmMantClientes.aspx/listaProvincia",
            data: "{'codDepartamento': '" + codDepRD + "'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("ListarProvincias: " + xhr + ajaxOtions + thrownError);
                alert("NO SE PUDO CARGAR PROVINCIAS");
            },
            success: function (data) {
                cargarProvinciasRD(data.d);
            },
            async: false
        });

    });

    function cargarDistritosRD(data) {
        let listData = Object.values(data);
        for (let i = 0; i < listData.length; i++) {
            let option = document.createElement('option');
            option.value = listData[i].codDistrito;
            option.text = listData[i].nombre;
            distRD.append(option);
        }
    }

    provRD.change(function () {
        distRD.find('option').remove().end().append('<option value="0">SELECCIONAR</option>').val(0);
        var codDepRD = depaRD.val();
        var codProvRD = provRD.val();
        $.ajax({
            type: "POST",
            url: "frmMantClientes.aspx/listaDistritos",
            data: "{'codDepartamento': '" + codDepRD + "', 'codProvincia': '" + codProvRD + "'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("ListarProvincias: " + xhr + ajaxOtions + thrownError);
                alert("NO SE PUDO CARGAR DISTRITOS");
            },
            success: function (data) {
                cargarDistritosRD(data.d);
            },
            async: false
        });

    });

    btnGuardarDireccion.click(function () {
        frmAgregarDireccion.validate({
            rules: {
                "depaRD": { min: 1 },
                "provRD": { min: 1 },
                "distRD": { min: 1 },
                "direRD": { required: true}
            },
            messages: {
                "depaRD": { min: "<div style='color:red; font-weight:normal;'>DEBE SELECCIONAR UNA OPCION</div>" },
                "provRD": { min: "<div style='color:red; font-weight:normal;'>DEBE SELECCIONAR UNA OPCION</div>" },
                "distRD": { min: "<div style='color:red; font-weight:normal;'>DEBE SELECCIONAR UNA OPCION</div>" },
                "direRD": { required: "<div style='color:red; font-weight:normal;'>DATO OBLIGATORIO</div>" }
            },
            submitHandler: function (form) {
                alert("todo correcto");
            }
        });
    });

});

$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});