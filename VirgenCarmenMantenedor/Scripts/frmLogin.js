﻿$(document).ready(function () {

    var BTN_INGRESAR = $("#ingresar");
    var cbn_sucursal = $("#sucursal");
    var txt_user = $("#exampleInputEmail1");
    var txt_pass = $("#exampleInputPassword1");
    var id_bloquear = $("#id_bloquear");

    document.getElementById('form1').addEventListener('submit', (e) => { e.preventDefault(); });

    function validaciones() {
        txt_user.val("");
        txt_pass.val("");
        BTN_INGRESAR.click(function () {
        $("#form1").validate({
            rules: {
                "exampleInputEmail1": { required: true },
                "exampleInputPassword1": { required: true },
                "sucursal" : { min: 1}
            },
            messages: {
                "exampleInputEmail1": { required: "<div style='color:red; font-weight:normal;'>Dato Obligatorio</div>" },
                "exampleInputPassword1": { required: "<div style='color:red; font-weight:normal;'>Dato Obligatorio</div>" },
                "sucursal": { min: "<div style='color:red; font-weight:normal;'>Debe elegir sucursal</div>"}
            },
            submitHandler: function (form) {
                BTN_INGRESAR.prop('disabled', true);
                id_bloquear.css('display', 'block');
                $.ajax({
                    type: "POST",
                    url: "login.aspx/validarDatos",
                    async: true,
                    data: "{'user':'" + txt_user.val() + "', 'pass':'" + txt_pass.val() + "' , 'codSucursal':'" + cbn_sucursal.val() + "'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        var response = data.d;
                        if (response) {
                            location.href = 'PanelControl.aspx';
                        } else {
                            BTN_INGRESAR.prop('disabled', false);
                            id_bloquear.css('display', 'none');
                            alertify.error('INICIO NO AUTORIZADO');
                        }
                    }
                });
            }
        });
        });
    }

    validaciones();
});

