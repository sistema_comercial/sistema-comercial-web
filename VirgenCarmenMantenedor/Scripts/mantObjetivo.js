﻿$(document).ready(function () {
    var tblObjetivo = $("#id_tblObjetivos");
    var TipoIndicador = $("#tindicador");    //variable para cargar el tipo de indicador
    var TipoindicadorM = $("#tindicadorM");
    var indicadorM = $("#indicadorM");
    var indicador = $("#indicador");
    var perfil = $("#perfil");
    var perfilM = $("#perfilM"); 
    var trabajador = $("#trabajador");
    var trabajadorM = $("#trabajadorM");
    var btnBuscar = $("#btnBuscar");
    var btnLimpiar = $("#btnLimpiar");
    var Mdescripcion = $("#Mdescripcion");
    var MVindicador = $("#vindicador");
    var MpmensajeE = $("#MpmensajeE");
    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal" data-target="#objetivoModal"></button>';
    var btnEliminar = ' <button type="button" title="Anular" class="icon-bin btnDelete" data-toggle="modal" data-target="#modalAnular"></button>';
    $("#min-date").datepicker(
        {
            changeMonth: true,
            changeYear: true,
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
            ],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            showOn: "button",
            buttonImage: "Imagenes/calendar.gif",
            buttonImageOnly: true,
            buttonText: "Select date",
            language: "es"
        }
    );

    $("#max-date").datepicker(
        {
            changeMonth: true,
            changeYear: true,
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
            ],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            showOn: "button",
            buttonImage: "Imagenes/calendar.gif",
            buttonImageOnly: true,
            buttonText: "Select date",
            language: "es"
        }
    );

    tblObjetivo.DataTable({
        ordering: false,
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            Paginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            {

                "width": "5%",
                "class": "text-center",
                "targets": [0]
            },
            {
                "width": "8%",
                "class": "text-center",
                "targets": [2]
            },
            {
                "width": "8%",
                "class": "text-center",
                "targets": [3]
            },
            {
                "width": "8%",
                "class": "text-center",
                "targets": [4]
            },
            {
                "width": "10%",
                "class": "text-center",
                "targets": [5]
            },
            {
                "width": "8%",
                "class": "text-center",
                targets: [7],
                render: function (data) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                "width": "15%",
                "class": "text-center",
                "targets": [8]
            },
            
            {
                "targets": [9, 10],
                "visible": false,
                "searchable": false
            }
        ]
    }) 

    var tabla = $("#id_tblObjetivos").dataTable(); //funcion jquery
    var table = $("#id_tblObjetivos").DataTable(); //funcion DataTable-libreria

    function agregarObjetivo(data) {
        for (var i = 0; i < data.length; i++) {
            tabla.fnAddData([
                data[i].codObjetivo,                            //0
                data[i].descripcion,                            //1
                data[i].descTipoIndicador,                      //2
                data[i].descIndicador,                          //3
                data[i].valorIndicador,                         //4
                data[i].descPerfil,                             //5
                data[i].descTrabajador,                         //6
                data[i].fechaRegistro,                          //7
                btnEditar + btnEliminar,                        //8
                data[i].codTipoIndicador,                       //9
                data[i].codIndicador,                           //10
                data[i].codPerfil,                              //11
                data[i].codTrabajador                           //12              
            ]);
        }

        $('.date-range-filter').change(function () {
            validar($('#min-date').val(), $('#max-date').val());
        });

        $("body").on('click', '.btnDelete', function () {

            //Obtengo los valores del producto seleccionado        
            var tr = $(this).parent().parent();
            var codProduct = table.row(tr).data()[0];
            console.log(codProduct);
            swal({
                title: "Se eliminara el registro",
                text: "¿Esta seguro que desea eliminar el registro?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                //Promesa que me trae el valor true al confirmar OK.
                .then((willDelete) => {
                    if (willDelete) {
                        EliminarProducto(codProduct)
                        swal("Se elimino Registro", {
                            icon: "success",
                        });
                        EnviarDatos();
                    } else {
                        swal("Se Cancelo la eliminación");

                    }
                });
        });

        $("body").on('click', '.btnEditar', function () {
            //MmensajeE.css('display', 'none');
            MpmensajeE.html("Editar Objetivo");
            var tr = $(this).parent().parent();

            Mdescripcion.val(table.row(tr).data()[1]);
            TipoindicadorM.val(table.row(tr).data()[9]);
            MVindicador.val(table.row(tr).data()[4]);
            perfilM.val(table.row(tr).data()[11]);
            let codigo = table.row(tr).data()[9];
            ListarIndicadorM(codigo);
            $("#indicadorM").val("2");
            //console.log(table.row(tr).data()[10]);
            //indicadorM.val(2);
            
            codigoProducto = table.row(tr).data()[0];
            //console.log(codigoProducto);         
        });
    }

    function ListarObjetivos(codTipoIndicador, codIndicador, codPerfil, codTrabajador, fechainicio, fechaFin) {
        MpmensajeE.html("Agregar Objetivo");
        var json = JSON.stringify({
            codTipoIndicador: codTipoIndicador,
            codIndicador: codIndicador,
            codPerfil: codPerfil,
            codTrabajador: codTrabajador,
            fechaInicio: fechainicio,
            fechaFin: fechaFin
        });
        $.ajax({
            type: "POST",
            url: "frmMantObjetivo.aspx/ListarObjetivos",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table.clear().draw();
                agregarObjetivo(data.d);
            }
        })
    }

    function validar(min, max) {
        var minr = min.split('/').join('-');
        var maxr = max.split('/').join('-');
        var minf = moment(minr, 'DD-MM-YYYY').format("YYYY-MM-DD");
        var maxf = moment(maxr, 'DD-MM-YYYY').format("YYYY-MM-DD");
        var startDate = moment(minf, "YYYY-MM-DD");
        var endDate = moment(maxf, "YYYY-MM-DD");
        if ((min != "" && max != "") && (endDate < startDate)) {
            $('#max-date').val("");
            swal("La fecha inicial debe ser mayor a la fecha final", {
                icon: "error"
            });
            rspta = false;
            return rspta;
        } else {
            rspta = true;
            return rspta;
        }
    }

    ListarObjetivos(0, 0, 0, 0, "", "")
    ListarTipoIndicador();
    ListarPerfil();  

    function addlistarTipoIndicador(data) {
        //función listar el tipo de indicador del objetivo
        for (var i = 0; i < data.length; i++) {
            TipoIndicador.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
            TipoindicadorM.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }
    function ListarTipoIndicador() {
        $.ajax({
            type: "POST",
            url: "frmMantObjetivo.aspx/ListaConceptos",
            data: "{'flag': '35'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                addlistarTipoIndicador(data.d);
            }
        })
    }

    function addlistarIndicadorM(data) {
        indicadorM.append("<option value='0'>-Seleccionar-</option>");
        for (var i = 0; i < data.length; i++) {
            indicadorM.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }
    function ListarIndicadorM(codigo) {
        indicadorM.children().remove();
        if (codigo == '0') {           
            indicadorM.append("<option value='0'>-Seleccionar-</option>");
        } else {
            if (codigo == '1') {
                $.ajax({
                    type: "POST",
                    url: "frmMantObjetivo.aspx/ListaConceptos",
                    data: "{'flag': '52'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        addlistarIndicadorM(data.d);
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmMantObjetivo.aspx/ListaConceptos",
                    data: "{'flag': '26'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        addlistarIndicadorM(data.d);
                       
                    }
                });
            }
        }        
    }

    function addlistarIndicador(data) {
        indicador.append("<option value='0'>-Seleccionar-</option>");
        for (var i = 0; i < data.length; i++) {
            indicador.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }
    function ListarIndicador(codigo) {
        indicador.children().remove();
        if (codigo == '0') {
            indicador.append("<option value='0'>-Seleccionar-</option>");
        } else {
            if (codigo == '1') {
                $.ajax({
                    type: "POST",
                    url: "frmMantObjetivo.aspx/ListaConceptos",
                    data: "{'flag': '52'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        addlistarIndicador(data.d);
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "frmMantObjetivo.aspx/ListaConceptos",
                    data: "{'flag': '26'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        addlistarIndicador(data.d);
                    }
                });
            }
        }
    }

    $("#tindicador").change(function () {
        let codigo = $("#tindicador").val();
        ListarIndicador(codigo);
    });
    $("#tindicadorM").change(function () {
        let codigo = $("#tindicadorM").val();
        ListarIndicadorM(codigo);
    });
    
    function addlistarPerfil(data) {
        //función listar el tipo de indicador del objetivo
        for (var i = 0; i < data.length; i++) {
            perfil.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
            perfilM.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }
    function ListarPerfil() {
        $.ajax({
            type: "POST",
            url: "frmMantObjetivo.aspx/ListaConcecptosPerfil",
            data: "{'flag': '32'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                addlistarPerfil(data.d);
            }
        })
    } 

    function addlistarTrabajadorM(data) {
        trabajadorM.append("<option value='0'>-Seleccionar-</option>");
        for (var i = 0; i < data.length; i++) {
            trabajadorM.append("<option value=" + data[i]["codPersona"] + ">" + data[i]["nombres"] + "</option>");
        }
    }
    function ListarTrabajadorM(codigo) {      
        trabajadorM.children().remove();
        if (codigo == '0') {           
            trabajadorM.append("<option value='0'>-Seleccionar-</option>");
        } else {
            $.ajax({
                type: "POST",
                url: "frmMantObjetivo.aspx/ListarTrabajador",
                data: "{'codPer': '" + codigo + "'}",
                contentType: 'application/json; charset=utf-8',
                error: function (xhr, ajaxOtions, thrownError) {
                    console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                },
                success: function (data) {
                    addlistarTrabajadorM(data.d);
                }
            });
        }
    }

    function addlistarTrabajador(data) {
        trabajador.append("<option value='0'>-Seleccionar-</option>");
        for (var i = 0; i < data.length; i++) {
            trabajador.append("<option value=" + data[i]["codPersona"] + ">" + data[i]["nombres"] + "</option>");
        }
    }
    function ListarTrabajador(codigo) {
        trabajador.children().remove();
        if (codigo == '0') {
            trabajador.append("<option value='0'>-Seleccionar-</option>");
        } else {
            $.ajax({
                type: "POST",
                url: "frmMantObjetivo.aspx/ListarTrabajador",
                data: "{'codPer': '" + codigo + "'}",
                contentType: 'application/json; charset=utf-8',
                error: function (xhr, ajaxOtions, thrownError) {
                    console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                },
                success: function (data) {
                    addlistarTrabajador(data.d);
                }
            });
        }
    }

    $("#perfil").change(function () {
        let codigo = $("#perfil").val();
        ListarTrabajador(codigo);
    });
    $("#perfilM").change(function () {
        let codigo = $("#perfilM").val();
        ListarTrabajadorM(codigo);
    });

    function InsertarObjetivo(data) {
        $.ajax({
            type: "POST",
            url: "frmMantObjetivo.aspx/InsertarObjetivo",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function () {
                swal("Registro Exitoso", {
                    icon: "success"
                });
                ListarObjetivos(0, 0, 0, 0, "", "")
            }

        });
    }

    function GuardarMeta() {
        $("#btnGuardar").click(function () {
            var descripcion = $("#Mdescripcion").val();
            var tindicador = $("#tindicadorM").val();
            var indicador = $("#indicadorM").val();
            var vaIndicador = $("#vindicador").val();
            var perfil = $("#perfilM").val();
            var trabajador = $("#trabajadorM").val();

            var json = JSON.stringify({
                descripcion: descripcion, tipoIndicador: tindicador, indicador: indicador,
                valorIndicador: vaIndicador, perfil: perfil, trabajador: trabajador});
            
            if (descripcion.length == 0) {
                swal("Debe Ingresar una Descripción", {
                    icon: "info"
                });
            }
            else {
                if (tindicador == 0) {
                    swal("Debe Seleccionar un Tipo de indicador", {
                        icon: "info"
                    });
                } else {
                    if (indicador == 0) {
                        swal("Debe Seleccionar un Indicador", {
                            icon: "info"
                        });
                    } else {
                        if (vaIndicador.length == 0) {
                            swal("Debe Ingresar el Valor del Indicador", {
                                icon: "info"
                            });
                        } else {
                            if (perfil == 0) {
                                swal("Debe Seleccionar un Perfil", {
                                    icon: "info"
                                });
                            } else {
                                if (trabajador == 0) {
                                    swal("Debe Seleccionar un Trabajador", {
                                        icon: "info"
                                    });
                                } else {
                                    InsertarObjetivo(json);
                                    limpiarcampos();
                                    //ListarMetas(0, 0, "", "")
                                }
                            }
                        }
                    }
                }            
            }
        })
    }

    GuardarMeta();

    function limpiarcampos() {
        $("#Mdescripcion").val("");
        let codigo = $("#tindicadorM").val();
        ListarIndicadorM(codigo);
        $("#vindicador").val("");
        $("#perfilM").val() == 0;
        let codigo2 = $("#perfilM").val();
        ListarTrabajadorM(codigo2);
        $("#objetivoModal").modal('hide');//ocultamos el modal
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del moda
    }

    btnBuscar.click(function () {
        var optionCodTipoIndicador = TipoIndicador.find("option:selected");
        var valueSelectedTipoIndicador = optionCodTipoIndicador.val();

        var optionCodIndicador = indicador.find("option:selected");
        var valueSelectedIndicador = optionCodIndicador.val();

        var optionCodperfil = perfil.find("option:selected");
        var valueSelectedperfil = optionCodperfil.val();

        var optionCodtrabajador = trabajador.find("option:selected");
        var valueSelectedtrabajador = optionCodtrabajador.val();

        if (valueSelectedTipoIndicador == undefined) {
            valueSelectedTipoIndicador = 0
        }

        if (valueSelectedIndicador == undefined) {
            valueSelectedIndicador = 0
        }

        if (valueSelectedperfil == undefined) {
            valueSelectedperfil = 0
        }

        if (valueSelectedtrabajador == undefined) {
            valueSelectedtrabajador = 0
        }

        var min = $('#min-date').val();
        var max = $('#max-date').val();

        if (min == "") {
            startDate = "";
        } else {
            var minr = min.split('/').join('-');
            var minf = moment(minr, 'DD-MM-YYYY').format("YYYY-MM-DD");
            var startDate = moment(minf, "YYYY-MM-DD");
        }

        if (max == "") {
            endDate = "";
        } else {
            var maxr = max.split('/').join('-');
            var maxf = moment(maxr, 'DD-MM-YYYY').format("YYYY-MM-DD");
            var endDate = moment(maxf, "YYYY-MM-DD");
        }

        if (valueSelectedTipoIndicador == 0 && valueSelectedIndicador == 0 && valueSelectedperfil == 0 && valueSelectedtrabajador == 0 && min == "" && max == "") {
            ListarObjetivos(0, 0, 0, 0, "", "")
        } else {
            if (min != "" && max == "") {
                swal("Debe Ingresar una fecha final o Borrar la fecha inicial", {
                    icon: "info"
                });
            } else {
                ListarObjetivos(valueSelectedTipoIndicador, valueSelectedIndicador, valueSelectedperfil, valueSelectedtrabajador, startDate, endDate)
            }
        }
    })

    btnLimpiar.click(function () {
        
        $("#tindicador").val('0');
        let codigo = $("#tindicador").val();
        ListarIndicador(codigo);
        $("#perfil").val('0');
        let codigo2 = $("#perfil").val();
        ListarTrabajador(codigo2);
        $("#min-date").val("");
        $("#max-date").val("");
        ListarObjetivos(0, 0, 0, 0, "", "")
    })


});