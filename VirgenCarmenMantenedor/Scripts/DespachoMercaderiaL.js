﻿$(document).ready(function () {
    //Variables de consulta
    var fechaEntregaI = $("#MSdiaMenor");
    var fechaEntregaF = $("#MSdiaMayor");

    //variables para agregar despacho
    var nroVenta = $("#Mventa");
    var cliente = $("#Mdescripcion");
    var transporte = $("#dlltransportista");
    var PuntoEntrega = $("#Mentrega");
    var fechaEntrega = $("#MfechaE");
    //botones
    var btnVer = ' <button type="button" title="Ver" class="icon-eye btnEye btnVer" data-toggle="modal"  data-target="#VerdespachoModal" ></button>';
    var btnAgregar = ' <button type="button" title="Agregar" class="icon-plus btnPlus btnAgregar" data-toggle="modal" data-target="#despachoModal"></button>';

    //Variables para agregar detalle Lote
    var btnAgregarDet = ' <button type="button" title="AgregarDetalle" class="icon-plus btnPlus btnAdd" data-toggle="modal" data-target="#detalledespachoModal"></button>';

    var descripcionProductoDet = $("#Mproducto");
    var cantDeProduct = $("#Mcant");
    //var lote = $("#Mlote");

    //VER detalle del modal Despacho Mercaderia icono (VER)
   
    


    //Select de Estado
    obetenerSelectEstado();

    $("#id_tblDespacho").DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            Paginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            {
                "width": "2%",
                "targets": [0]
            },
            {
                "width": "15%",
                "targets": [3,7, 8]
            },
            {
                "width": "25%",
                "targets": [12,13]
            },
            {
                "width": "35%",
                "targets": [6,10]
            },           
            {
                "targets": [1,2, 4, 5, 9,11],
                "visible": false,
                "searchable": false
            }

        ]
    })

    //creación de variables de que dataTable este creado en el DOM
    var tabla = $("#id_tblDespacho").dataTable(); //funcion jquery
    var table = $("#id_tblDespacho").DataTable(); //funcion DataTable-libreria

    function ListarTablaVenta(datos) {
        //DESCRIPCION : Funcion que me trae la lista de venta
        $.ajax({
            type: "POST",
            url: "frmDespachoMercaderia.aspx/ListarVentaFiltro",
            data: datos,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table.clear().draw();
                LlenarVentaDespacho(data.d);
            }
        });
    }
    function LlenarVentaDespacho(data) {
        //DESCRIPCIÓN: función de listar las ventas para despacho        
        var botones; 
        for (var i = 0; i < data.length; i++) {
            if (data[i].codEstado !== 1) {
                botones = btnVer;
            }
            else {
                botones = btnVer + btnAgregar;
            }

            tabla.fnAddData([
                data[i].ntraVenta,
                data[i].concatenado,
                data[i].codCliente,
                data[i].cliente,
                data[i].nroDocumento,
                data[i].puntoEntrega,
                data[i].direccion,
                data[i].fechaEntrega,
                data[i].horaEntrega,
                data[i].codVendedor,
                data[i].vendedor,
                data[i].codEstado,
                data[i].Estado,
                botones
            ]);

        }

        $("body").on('click', '.btnAgregar', function () {
            var tr = $(this).parent().parent();

            nroVenta.val(table.row(tr).data()[0]);
            cliente.val(table.row(tr).data()[3]);         
            PuntoEntrega.val(table.row(tr).data()[6]);

            //var fecha = table.row(tr).data()[7];
            fechaEntrega.val(table.row(tr).data()[7]);
            
            //Listar productos 
            codvt = table.row(tr).data()[0];
            ListarDetalleVent(codvt);
            console.log(codvt);

        });
        $("body").on('click', '.btnVer', function () {
            var tr = $(this).parent().parent();

            $("#MventaV").val(table.row(tr).data()[0]);
            $("#MfechaV").val(table.row(tr).data()[7]);
            $("#Mcliente").val(table.row(tr).data()[3]);
            $("#MentregaV").val(table.row(tr).data()[6]);
            //$("#MtransporteV").val(table.row(tr).data()[3]);
            //$("#MplacaV").val(table.row(tr).data()[3]);



        });
     
    }

    function EnviarDatos() {
        //DESCRIPCIÓN: función paa enviar los datos de los filtros
        $("#btnBuscar").on('click', function () {
            var codVenta = $("#txtVenta").val();
            var codCliente = $("#id_codCliente").val();
            var estado = $("#MSestado");
            var codEstado = estado.find("option:selected").val();
            var codfechaEntregaI = fechaEntregaI.val();
            var codfechaEntregaF = fechaEntregaF.val();    


            if (codfechaEntregaI > codfechaEntregaF) {
                swal("Fecha inicial debe ser menor a la final", {
                    icon: "info"
                });
                fechaInicial.focus();
            } else {

                var json = JSON.stringify({
                    nroVenta: codVenta, codCliente: codCliente, estado: codEstado,
                    codfechaEntregaI: codfechaEntregaI, codFechaEntegaF: codfechaEntregaF
                });
                ListarTablaVenta(json);
                limpiarSelectBusqueda();
            }
        });     
    }

    EnviarDatos();

    $("#id_cliente").keyup(function () {
       
        var cad = $(this).val();
        if (cad.length === 0) {
            $("#id_codCliente").val("0");
        } else {
            buscarClienteNombre(cad);
        }

    });
    function buscarClienteNombre(cadena) {
        //DESCRIPCION: Listas los cientes con autocomplete
        $("#id_cliente").autocomplete({
            minLength: 2,
            autoFocus: true,
            source:
                function (request, response) {
                    $.ajax({
                        type: "POST",
                        url: "frmDespachoMercaderia.aspx/buscarCliente",
                        data: JSON.stringify({ cliente: cadena }),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        error: function (xhr, ajaxOtions, thrownError) {
                            console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                        },
                        success: function (data) {
                            console.log(data.d);
                            response($.map(data.d, function (item) {
                                var obj = new Object();
                                obj.label = item.cliente;
                                obj.value = item.cliente;
                                obj.codCliente = item.codCliente;
                                return obj;
                            }));
                        }
                    });
                },
            select: function (event, ui) {
                $("#id_cliente").val(ui.item.label);
                $("#id_codCliente").val(ui.item.codCliente);
            }
            , change: function (event, ui) {
                if (!ui.item) {
                    $("#id_cliente").val('');
                    $("#id_codCliente").val('0');
                }
            }
        });
    }

    function limpiarSelectBusqueda() {
        //DESCRIPCIÓN: esta función es para limpiar los select después de la búsqueda realizada   
        $("#MSdiaMenor").val(0);
        $("#MSdiaMayor").val("");
        $("#id_cliente").val("");
        $("#txtVenta").val("");
        $("#MSestado").val(0);
    }

   //Inicio:REGISTRO DE DESPACHO --------------------------------------------

    $("#idDateTableDespacho").DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            Paginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            {

                "targets": [0,4],
                "visible": false,
                "searchable": false
            },
            {
                "width": "30%",
                "targets": [1, 2, 3,5,6]
            }

        ]
    })

    var tablaDetVenta = $("#idDateTableDespacho").dataTable(); //funcion jquery 
    var tableDetVenta = $("#idDateTableDespacho").DataTable(); //funcion DataTable-libreria
    tableDetVenta.columns.adjust().draw();

    function ListarDetalleVent(codVents) {
        //DESCRIPCIÓN: Función de listar el detalle de la venta, solo prducto, cant, almacen
        var json = JSON.stringify({ codVent: codVents });
        $.ajax({
            type: "POST",
            url: "frmDespachoMercaderia.aspx/ListarDetVentaDespacho",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                tableDetVenta.clear().draw();
                ObtenerListaDetalleVenta(data.d);
                console.log(data.d);
            }
        });
    }      

    function ObtenerListaDetalleVenta(dataS) {
        //DESCRIPCIÓN: lista el detalle de la venta    
      
        for (var i = 0; i < dataS.length; i++) {
            //aqui recorre la tabla para el llenado 
            tablaDetVenta.fnAddData([
                dataS[i].codVenta,
                dataS[i].codProducto,
                dataS[i].descProduct,
                dataS[i].cantP,
                dataS[i].codAlmacen,
                dataS[i].almacen,
                btnAgregarDet
            ]);
        }

        $("body").on('click', '.btnAdd', function () {
            var tr = $(this).parent().parent();

            descripcionProductoDet.val(tableDetVenta.row(tr).data()[2]);
            cantDeProduct.val(tableDetVenta.row(tr).data()[3]);

            var producto = tableDetVenta.row(tr).data()[1]
            obtenerSelectLote(producto);
            console.log("ver lote por codProducto: " + producto);
        });
    }

    $("#idDetTableDespacho").DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            Paginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            //{

            //    "targets": [0],
            //    "visible": false,
            //    "searchable": false
            //},
            {
                "width": "30%",
                "targets": [0,1, 2, 3,4,5]
            }

        ]
    })



    agregarModalDetalleLote();

    //FIN: REGISTRO DE DESPACHO ----------------------------------------------

    //OBTENER EL ID DEL USUARIO
    var ntraUsuario;
    ntraUsuario = '';
    function obtenerUsuario() {
        $.ajax({
            type: "POST",
            url: "login.aspx/getSession",
            data: "{}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                ntraUsuario = data.d[0].ntraUsuario;
                //ntraSucursal = data.d[0].sucursal;
                //nombreUsuario = data.d[0].nombre;
            }
        })
    }
    obtenerUsuario();
    cambiarDatosLote();



   
});



//variable global detalle del lote de acuerdo al producto
const arregloDetalleLoteProducto = [];
const arregloRegistroDetalleLote = [];
const arregloDetalleLoteConfirmado = [];




function listarSelectEstado(data) {
    //DESCRIPCION : Funcion para llenar la lista de opciones del select del estado.

    let estadoVenta = $("#MSestado");
    for (let i = 0; i < data.length; i++) {
        estadoVenta.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
    }
}

function obetenerSelectEstado() {
    $.ajax({
        type: "POST",
        url: "frmDespachoMercaderia.aspx/ListarConcepto",
        data: "{'flag': '30'}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOtions, thrownError) {
            console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            listarSelectEstado(data.d);
        }
    });
}

function listarSelectLote(data) {
    //DESCRIPCION : Funcion para llenar la lista de opciones del select del estado.
    let lote = $("#Mlote");

    $("#Mlote option").remove();
    lote.append("<option value='0'>-Seleccionar-</option>");
   
    for (let i = 0; i < data.length; i++) {
        lote.append("<option value=" + data[i]["codLote"] + ">" + data[i]["codLote"] +"</option>");
    }
}
function obtenerSelectLote(producto) {
    $.ajax({
        type: "POST",
        url: "frmDespachoMercaderia.aspx/ListaProductPorVencer",
        data: "{'codProducto': '" +producto + "'}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOtions, thrownError) {
            console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            listarSelectLote(data.d);
            console.log("vemos q trae " + JSON.stringify(data.d));
            const datita = data.d;
            for (let i = 0; i < datita.length; i++) {
                const objLote = new Object();
                objLote.codLote = datita[i]["codLote"];
                objLote.fechaVencimiento = datita[i]["fechaVencimiento"];
                objLote.codProveedor = datita[i]["codProveedor"];
                objLote.proveedor = datita[i]["proveedor"];
                arregloRegistroDetalleLote.push(objLote);
            }
        }
    });
}

function listarDatosDetalle() {
    //DESCRIPCIÓN: lista el detalle del producto (PARA AGREGAR NUEVO DETALLE)
    var tablaDetLote = $("#idDateTablePresentacion").dataTable(); //funcion jquery
    var tableDetLote = $("#idDateTablePresentacion").DataTable(); //funcion DataTable-libreria

    tableDetLote.clear().draw();

    for (let i = 0; i < arregloDetalleLoteProducto.length; i++) {
        //aqui recorre la tabla para el llenado 
        tablaDetLote.fnAddData([
            arregloDetalleLoteProducto[i].codLote,
            arregloDetalleLoteProducto[i].fechaVenc,
            arregloDetalleLoteProducto[i].idproveedor,
            arregloDetalleLoteProducto[i].proveedor,
            arregloDetalleLoteProducto[i].cant,
            '<button onclick="eliminarDetalle(' + i + ')" class="btn icon-bin btn-danger"></button>'
        ]);
    }
}

function eliminarDetalle(index) {
    //DESCRIPCIÓN: función para eliminar una presentación del producto (tabla temporal)
    swal({
        title: "Se eliminara el presentación de producto",
        text: "¿Esta seguro que desea eliminar el registro?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                arregloDetalleLoteProducto.splice(index, 1);
                listarDatosDetalle();

                swal("Se elimino el presentación", {
                    icon: "success",
                });

            } else {
                swal("Se Cancelo la eliminacion", {
                    icon: "info"
                });
            }

        });
}

function agregarModalDetalleLote() {

    $("#btnAgregar").click(function () {

        //Variable de la tabla del detalle de registro del lote
              
        let cantSaliente = parseInt($("#McantSalida").val());
        let cantidad = parseInt($("#Mcant").val());


        let tipLote = $("#Mlote").val();
        console.log("tipo de lote pe "+tipLote);
        const respuesta = arregloRegistroDetalleLote.find(obj => obj.codLote === tipLote);

        if (cantidad >= cantSaliente) {
            let resultadoCantidad = cantidad - cantSaliente;
            respuesta.cantidad = cantSaliente;
            arregloDetalleLoteConfirmado.push(respuesta);
            $("#Mcant").val(resultadoCantidad);
            respuesta = null;
        } else {
            swal('La cantidad ah pasado el limite', { icon: 'warning' });
        }

     
        // imprimir en la tabla
        
        listartArregloDetalleLoteConfirmado();

   
    });
}

function listartArregloDetalleLoteConfirmado() { 

    var tablaDetLote = $("#idDetTableDespacho").dataTable(); //funcion jquery 
    var tableDetLote = $("#idDetTableDespacho").DataTable(); //funcion DataTable-libreria
    tableDetLote.columns.adjust().draw();

    tableDetLote.clear().draw();
        for (var i = 0; i < arregloDetalleLoteConfirmado.length; i++) {
            tablaDetLote.fnAddData([
                arregloDetalleLoteConfirmado[i].codLote,
                arregloDetalleLoteConfirmado[i].fechaVencimiento,
                arregloDetalleLoteConfirmado[i].codProveedor,
                arregloDetalleLoteConfirmado[i].proveedor,
                arregloDetalleLoteConfirmado[i].cantidad,               
                '<button type="button" title="borrar" class="btn icon-bin btn-danger" onclick="eliminarArregloDetalleLoteConfirmado(' + i +','+ arregloDetalleLoteConfirmado[i].cantidad+')" ></button>'
            ]);

        }
}

function eliminarArregloDetalleLoteConfirmado(index, cantidad) {
    //pones tu alert de si desea confirmar la eliminacion
    let cantidadInicial = parseInt($("#Mcant").val());


    swal({
        title: "Se eliminará el registro del Lote",
        text: "¿Esta seguro que desea eliminar el registro?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                arregloDetalleLoteConfirmado.splice(index, 1);
                $("#Mcant").val(cantidadInicial+cantidad);
                listartArregloDetalleLoteConfirmado();

                swal("Eliminado correctamente", {
                    icon: "success",
                });

            } else {
                swal("Se Cancelo la eliminación", {
                    icon: "info"
                });
            }

        });



}
function cambiarDatosLote() {
    $("#Mlote").keyup(function () {
        alert("esto cambiara uwu");
        //mete tu ajax aqui y muestra los datos en el console.log



    });
}