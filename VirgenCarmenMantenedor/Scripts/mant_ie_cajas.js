﻿$(document).ready(function () {
    var btnEditar = '<button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal"  data-target="#modalAccion" ></button>'
    var btnEliminar = ' <button type="button" id="" class="icon-bin btnDelete" data-toggle="tooltip"  title="Eliminar" ></button>'
    var selectTipoTrans = $("#selectTipoTrans");
    var selectModoPago = $("#selectModoPago");
    var selectTipoRegistro = $("#selectTipoRegistro");
    var selectTipoMoneda = $("#selectTipoMoneda");
    var apeDate = $("#ape-date");
    var btnBuscar = $("#btnBuscar");
    var btnRegistrarMovManual = $("#btnRegistrarMovManual");
    var selectCaja = $("#selectCaja"); // CAJA ASIGNADA AL CAJERO
    var MmensajeCaja = $('#MmensajeCaja');
    var MpmensajeCaja = $('#MpmensajeCaja');
    var divDetalleApertura = $("#divDetalleApertura");
    var txtMST = $("#txtMST"); // Monto Soles Transferencia
    var txtMDT = $("#txtMDT"); // Monto Dolares Transferencia
    var txtMSE = $("#txtMSE"); // Monto Soles Efectivo
    var txtMDE = $("#txtMDE"); // Monto Dolares Efectivo

    // CAMPOS MODAL
    var lblTitle = $("#lblTitle");
    var Mpmensaje = $('#Mpmensaje');
    var Mmensaje = $('#Mmensaje');
    var txtNtraTransaccionCaja = $("#txtNtraTransaccionCaja");
    var dateMov = $("#date-mov");
    var selectTipoMov_Modal = $("#selectTipoMov_Modal");
    var txtModoPago = $("#txtModoPago");
    var selectTipoMoneda_Modal = $("#selectTipoMoneda_Modal");
    var txtImporte = $("#txtImporte");
    var btnGuardar = $("#btnGuardar");
    var btnRegistrar = $("#btnRegistrar");

    btnRegistrarMovManual.prop("disabled", true);

    apeDate.datepicker(
        {
            changeMonth: true,
            changeYear: true,
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
            ],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            showOn: "button",
            buttonImage: "Imagenes/calendar.gif",
            buttonImageOnly: true,
            buttonText: "Select date",
            language: "es",
            setDate: new Date()

        }
    );

    //TABLA MOVIMIENTOS DE CAJA
    $('#tb_movs_caja').DataTable({
        ordering: false,
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],

        //Para que el DataTable no me muestre ciertas Columnas.
        columnDefs: [
            {
                targets: [0, 3, 6],
                "visible": false,
                "searchable": false
            }

        ]

    });

    //creo variables despues de que el DataTable este creado en el DOM.
    var tabla = $("#tb_movs_caja").dataTable(); //funcion jquery
    var table = $("#tb_movs_caja").DataTable(); //funcion DataTable-libreria

    /////LENADO DE TABLA POR DEFECTO ////
    function addRowDT(data) {

        //DESCRIPCION : Funcion que me crea el listado de MOVIMIENTOS de caja en el body del DataTable


        for (var i = 0; i < data.length; i++) {

            var btns;

            if (data[i].codModoPago == 1) {
                if (data[i].codTipoMoneda == 1) {
                    MSE += data[i].importe;
                } else {
                    MDE += data[i].importe;
                }
            } else {
                if (data[i].codTipoMoneda == 1) {
                    MST += data[i].importe;
                } else {
                    MDT += data[i].importe;
                }
            }

            if (data[i].codTipoTransaccion == 2) {
                btns = btnEditar + btnEliminar
            } else {
                btns = ""
            }


            tabla.fnAddData([
                data[i].ntraTransaccionCaja,
                data[i].fechaTransaccion + ' ' + data[i].horaTransaccion,
                data[i].tipoRegistro,
                data[i].ntraTipoMovimiento,
                data[i].tipoMovimieno,
                parseFloat(data[i].importe).toFixed(2),
                data[i].codTipoMoneda,
                data[i].tipoMoneda,
                data[i].modoPago,
                data[i].tipoTransaccion,
                btns

            ]);
        }

        txtMSE.val("S/. " + parseFloat(MSE).toFixed(2))
        txtMST.val("S/. " + parseFloat(MST).toFixed(2))
        txtMDE.val("$ " + parseFloat(MDE).toFixed(2))
        txtMDT.val("$ " + parseFloat(MDT).toFixed(2))

        $("body").on('click', '.btnEditar', function () {
            lblTitle.text("Actualizar Transaccion de Caja");
            btnGuardar.css("display", "block");
            btnRegistrar.css("display", "none");
            Mmensaje.css('display', 'none');

            var tr = $(this).parent().parent();

            txtNtraTransaccionCaja.val(table.row(tr).data()[0]);
            dateMov.val(table.row(tr).data()[1].split(" ")[0]);
            selectTipoMov_Modal.val(table.row(tr).data()[3]);
            txtModoPago.val(table.row(tr).data()[8])
            selectTipoMoneda_Modal.val(table.row(tr).data()[6])
            txtImporte.val(table.row(tr).data()[5])

        });

        $("body").on('click', '.btnDelete', function () {
            //Obtengo los valores de mi tr seleccionado.
            var tr = $(this).parent().parent();
            var ntraTransaccionCaja = table.row(tr).data()[0];

                swal({
                    title: "Se eliminara el registro",
                    text: "¿Esta seguro que desea eliminar el registro?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    //Promesa que me trae el valor true al confirmar OK.
                    .then((willDelete) => {
                        if (willDelete) {
                            EliminarTransaccionCaja(ntraTransaccionCaja)
                            swal("Se eliminó el registro", {
                                icon: "success",
                            });

                        } else {
                            swal("Se canceló la eliminación");
                        }
                    });
        });

    }


    btnBuscar.click(function () {

        //SELECT CAJA
        var optionSelectCaja = selectCaja.find("option:selected");
        var valueSelectedCaja = optionSelectCaja.val();

        //FECHA
        var valueSelectedApeDate = apeDate.val();
        var dater = valueSelectedApeDate.split('/').join('-');
        var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
        var dateSelect = moment(datef, "YYYY-MM-DD");

        //CAMPOS FILTRO
        var optionSelectTipoTrans = selectTipoTrans.find("option:selected");
        var valueSelectedTipoTrans = optionSelectTipoTrans.val();

        var optionSelectModoPago = selectModoPago.find("option:selected");
        var valueSelectedModoPago = optionSelectModoPago.val();

        var optionSelectTipoRegistro = selectTipoRegistro.find("option:selected");
        var valueSelectedTipoRegistro = optionSelectTipoRegistro.val();

        var optionSelectTipoMoneda = selectTipoMoneda.find("option:selected");
        var valueSelectedTipoMoneda = optionSelectTipoMoneda.val();
        
        if (valueSelectedCaja > 0) {
            if (valueSelectedApeDate != "") {
                if (valueSelectedTipoTrans == 0 && valueSelectedModoPago == 0 && valueSelectedTipoRegistro == 0 && valueSelectedTipoMoneda == 0) {
                    ListarTransaccionesCaja(valueSelectedCaja, 0, 0, 0, 0, dateSelect, 0)
                } else {
                        ListarTransaccionesCaja(valueSelectedCaja, valueSelectedTipoTrans, valueSelectedModoPago, valueSelectedTipoRegistro, valueSelectedTipoMoneda, dateSelect, 0)

                }
            } else {
                swal({
                    title: "ALERTA",
                    text: "Debe seleccionar una fecha",
                    icon: "warning",
                    buttons: false,
                    dangerMode: true,
                })
            }
        } else {
            swal({
                title: "ALERTA",
                text: "Debe seleccionar una caja",
                icon: "warning",
                buttons: false,
                dangerMode: true,
            })
        }
        
    });

    function ListarTransaccionesCaja
        (ntraCaja, tipoTransaccion, modoPago, tipoRegistro, tipoMoneda,
            fechaTransaccion, flag) {
        //DESCRICION : Funcion que obtiene la lista de movimientos de caja 

        var json = JSON.stringify({
            ntraCaja: ntraCaja,
            tipoTransaccion: tipoTransaccion,
            modoPago: modoPago,
            tipoRegistro: tipoRegistro,
            tipoMoneda: tipoMoneda,
            fechaTransaccion: fechaTransaccion,
            flag: flag
        });

        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/ListarTransaccionesCajas",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                // console.log(data.d);
                table.clear().draw();
                addRowDT(data.d);
            }
        })
    }

    /////////////////////////////////////////////////////////////////////

    /////////FILTROS////

    function addSelectTiposTransaccion(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de estados de caja.
        for (var i = 0; i < data.length; i++) {
            selectTipoTrans.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    function ListarTiposTransaccion() {
        //DESCRIPCION : Funcion que trae la lista de registro
        $.ajax({
            type: "POST",
            url: "frmMaestroCajas.aspx/ListarxFlag",
            data: "{'flag': '54' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                addSelectTiposTransaccion(data.d);
            }
        })

    }
    ListarTiposTransaccion();

    function addSelectModosPago(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de estados de caja.
        for (var i = 0; i < data.length; i++) {
            selectModoPago.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    function ListarModosPago() {
        //DESCRIPCION : Funcion que trae la lista de registro
        $.ajax({
            type: "POST",
            url: "frmMaestroCajas.aspx/ListarxFlag",
            data: "{'flag': '31' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                addSelectModosPago(data.d);
            }
        })

    }
    ListarModosPago();

    function addSelectTiposRegistro(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de estados de caja.
        for (var i = 0; i < data.length; i++) {
            selectTipoRegistro.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    function ListarTiposRegistro() {
        //DESCRIPCION : Funcion que trae la lista de registro
        $.ajax({
            type: "POST",
            url: "frmMaestroCajas.aspx/ListarxFlag",
            data: "{'flag': '37' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                addSelectTiposRegistro(data.d);
            }
        })

    }
    ListarTiposRegistro();

    function addSelectTiposMoneda(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de estados de caja.
        for (var i = 0; i < data.length; i++) {
            selectTipoMoneda.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
            selectTipoMoneda_Modal.append("<option value=" + data[i]["correlativo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    function ListarTiposMoneda() {
        //DESCRIPCION : Funcion que trae la lista de registro
        $.ajax({
            type: "POST",
            url: "frmMaestroCajas.aspx/ListarxFlag",
            data: "{'flag': '37' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                addSelectTiposMoneda(data.d);
            }
        })

    }
    ListarTiposMoneda();

    //Listar Cajas Aperturadas - Usuario Logueado

    function addSelectCajas(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de estados de caja.
        for (var i = 0; i < data.length; i++) {
            selectCaja.append("<option value=" + data[i]["ntraCaja"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    var listaCajas = new Array();

    function ListarCajas(ntraCaja, estadoCaja, fechaInicial, fechaFinal) {
        //DESCRICION : Funcion que obtiene la lista de cajas aperturadas

        var json = JSON.stringify({
            ntraCaja: ntraCaja,
            estadoCaja: estadoCaja,
            fechaInicial: fechaInicial,
            fechaFinal: fechaFinal
        });

        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/ListarCajas",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                // console.log(data.d);
                listaCajas = data.d;
                addSelectCajas(listaCajas);
            }
        })
    }

    ListarCajas(0, 0, "","");

    function addSelectMovimientosCaja(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de estados de caja.
        for (var i = 0; i < data.length; i++) {
            selectTipoMov_Modal.append("<option value=" + data[i]["ntraTipoMovimiento"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    function ListarTiposMovimientosCaja(ntraCaja) {
        //DESCRICION : Funcion que obtiene la lista de movimientos de caja 
        var json = JSON.stringify({
            objCENCaja: {
                ntraCaja: ntraCaja
            }
        });

        $.ajax({
            type: "POST",
            url: "frmMaestroCajas.aspx/ListarTipos_Mov_Asig_Caja",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                // console.log(data.d);
                $('option', selectTipoMov_Modal).remove();
                selectTipoMov_Modal.append('<option value="0">SELECCIONE UN TIPO DE MOVIMIENTO</option>');
                addSelectMovimientosCaja(data.d.listaTipoMovimientos);

            }
        })
    }

    //Select CAJA - CHANGE

    selectCaja.val(new Date());

    var listaAperturas = new Array();
    var listaCierres = new Array();

    function obtenerDateTime(fecha) {
        //Dividimos la fecha primero utilizando el espacio para obtener solo la fecha y el tiempo por separado
        var splitDate = fecha.split(" ");
        var date = splitDate[0].split("/");
        var time = splitDate[1].split(":");

        // Obtenemos los campos individuales para todas las partes de la fecha
        var dd = date[0];
        var mm = date[1] - 1;
        var yyyy = date[2];
        var hh = time[0];
        var min = time[1];
        var ss = time[2];

        // Creamos la fecha con Javascript
        var fecha = new Date(yyyy, mm, dd, hh, min, ss);

        return fecha.getTime()

        // De esta manera se puede volver a convertir a un string
        //var formattedDate = ("0" + fecha.getDate()).slice(-2) + '-' + ("0" + (fecha.getMonth() + 1)).slice(-2) + '-' + fecha.getFullYear() + ' ' + ("0" + fecha.getHours()).slice(-2) + ':' + ("0" + fecha.getMinutes()).slice(-2) + ':' + ("0" + fecha.getSeconds()).slice(-2);
    }

    var MSE = MDE = MST = MDT = 0.0;

    selectCaja.change(function () {
        var optionSelectCaja = selectCaja.find("option:selected");
        var valueSelectedCaja = optionSelectCaja.val();
        var valueSelectedApeDate = apeDate.val();

        divDetalleApertura.html("");

        var contentDiv = ""

        listaAperturas = new Array();
        listaCierres = new Array();
        MSE = MDE = MST = MDT = 0.0;

        if (valueSelectedCaja > 0) {
            if (valueSelectedApeDate != "") {

                var dater = valueSelectedApeDate.split('/').join('-');
                var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
                var dateSelect = moment(datef, "YYYY-MM-DD");

                ListarTiposMovimientosCaja(valueSelectedCaja)
                ListarCajasAperturadas(valueSelectedCaja, 0, 1, dateSelect)
                ListarCajasCerradas(valueSelectedCaja, 0, 1, dateSelect)

                var ultimaApertura = null;

                if (listaAperturas.length > 0) {

                    contentDiv += '<hr/><div class="form-group sub-titulo">'+
                        '<h4 class="box-filtros">DATOS DE APERTURA</h4></div>'

                    $.each(listaAperturas, function (index, obj) {
                        contentDiv += '<label  class="label label-default col-sm-2" for="inputName">'+
                            'Fecha/Hora</label><div class="col-xs-3" >' +
                            '<input type="text" class="form-control" value="' + obj.fecha + ' ' + obj.hora + '" readonly="" /></div>'
                        contentDiv += '<label  class="label label-default col-sm-2" for="inputName">' +
                            'Saldo Inicial</label><div class="col-xs-2" >' +
                            '<input type="text" class="form-control" value="S/. ' + parseFloat(obj.saldoSoles).toFixed(2) + '" readonly="" /></div>' +
                            '<div class="col-xs-2" >' +
                            '<input type="text" class="form-control" value="$ ' + parseFloat(obj.saldoDolares).toFixed(2) + '" readonly="" /></div>'
                        

                        MSE += obj.saldoSoles;
                        MDE += obj.saldoDolares;
                    });

                    divDetalleApertura.html(contentDiv);


                    ultimaApertura = listaAperturas[listaAperturas.length - 1];
                }

                var ultimoCierre = null;

                if (listaCierres.length > 0) {
                    ultimoCierre = listaCierres[listaCierres.length - 1];
                }

                if (ultimaApertura != null) {
                    if (ultimoCierre != null) {
                        var timeUltimaApertura = obtenerDateTime(ultimaApertura.fecha + " " + ultimaApertura.hora)
                        var timeUltimoCierre = obtenerDateTime(ultimoCierre.fecha + " " + ultimoCierre.hora)

                        if (timeUltimoCierre > timeUltimaApertura) {
                            swal({
                                title: "ALERTA",
                                text: "Ya se realizó el cierre de la caja seleccionada en la fecha indicada",
                                icon: "warning",
                                buttons: false,
                                dangerMode: true,
                            })
                            btnRegistrarMovManual.prop("disabled", true);
                        } else {
                            btnRegistrarMovManual.prop("disabled", false);
                        }

                    } else {
                        btnRegistrarMovManual.prop("disabled", false);
                    }
                } else {
                    swal({
                        title: "ALERTA",
                        text: "La caja seleccionada no ha sido aperturada en la fecha indicada",
                        icon: "warning",
                        buttons: false,
                        dangerMode: true,
                    })
                    btnRegistrarMovManual.prop("disabled", true);
                }

                ListarTransaccionesCaja(valueSelectedCaja, 0, 0, 0, 0, dateSelect, 0)
            } else {
                MpmensajeCaja.css("color", "rgba(179, 10, 10, 0.69)");
                MmensajeCaja.css("background-color", "rgba(255, 0, 0, 0.51)")
                    .css("border-color", "rgb(203, 46, 46)");
                MpmensajeCaja.html("Debe seleccionar una fecha");
                MmensajeCaja.css("display", "block");
                setTimeout(function () {
                    MmensajeCaja.css('display', 'none');
                }, 2000);
                btnRegistrarMovManual.prop("disabled", true);
                table.clear().draw();
            }
        } else {
            MpmensajeCaja.css("color", "rgba(179, 10, 10, 0.69)");
            MmensajeCaja.css("background-color", "rgba(255, 0, 0, 0.51)")
                .css("border-color", "rgb(203, 46, 46)");
            MpmensajeCaja.html("Debe seleccionar una caja");
            MmensajeCaja.css("display", "block");
            setTimeout(function () {
                MmensajeCaja.css('display', 'none');
            }, 2000);
            btnRegistrarMovManual.prop("disabled", true);
            table.clear().draw();
        }
    });

    apeDate.change(function () {
        var optionSelectCaja = selectCaja.find("option:selected");
        var valueSelectedCaja = optionSelectCaja.val();
        var valueSelectedApeDate = $(this).val();

        divDetalleApertura.html("");

        var contentDiv = ""

        listaAperturas = new Array();
        listaCierres = new Array();
        MSE = MDE = MST = MDT = 0.0;

        if (valueSelectedCaja > 0) {
            if (valueSelectedApeDate != "") {
                var dater = valueSelectedApeDate.split('/').join('-');
                var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
                var dateSelect = moment(datef, "YYYY-MM-DD");

                ListarTiposMovimientosCaja(valueSelectedCaja)
                ListarCajasAperturadas(valueSelectedCaja, 0, 1, dateSelect)
                ListarCajasCerradas(valueSelectedCaja, 0, 1, dateSelect)

                var ultimaApertura = null;

                if (listaAperturas.length > 0) {

                    contentDiv += '<hr/><div class="form-group sub-titulo">' +
                        '<h4 class="box-filtros">DATOS DE APERTURA</h4></div>'

                    $.each(listaAperturas, function (index, obj) {
                        contentDiv += '<label  class="label label-default col-sm-1" for="inputName">' +
                            'Hora</label><div class="col-xs-2" >' +
                            '<input type="text" class="form-control" value="' + obj.hora + '" readonly="" /></div>'
                        contentDiv += '<label  class="label label-default col-sm-2" for="inputName">' +
                            'Saldo Inicial (S/.)</label><div class="col-xs-1" >' +
                            '<input type="text" class="form-control" value="' + obj.saldoSoles + '" readonly="" /></div>'
                        contentDiv += '<label  class="label label-default col-sm-2" for="inputName">' +
                            'Saldo Inicial ($)</label><div class="col-xs-1" >' +
                            '<input type="text" class="form-control" value="' + obj.saldoDolares + '" readonly="" /></div>'

                        MSE += obj.saldoSoles;
                        MDE += obj.saldoDolares;
                    });

                    divDetalleApertura.html(contentDiv);

                    ultimaApertura = listaAperturas[listaAperturas.length - 1];
                }

                var ultimoCierre = null;

                if (listaCierres.length > 0) {
                    ultimoCierre = listaCierres[listaCierres.length - 1];
                }

                if (ultimaApertura != null) {
                    if (ultimoCierre != null) {
                        var timeUltimaApertura = obtenerDateTime(ultimaApertura.fecha + " " + ultimaApertura.hora)
                        var timeUltimoCierre = obtenerDateTime(ultimoCierre.fecha + " " + ultimoCierre.hora)

                        if (timeUltimoCierre > timeUltimaApertura) {
                            swal({
                                title: "ALERTA",
                                text: "Ya se realizó el cierre de la caja seleccionada en la fecha indicada",
                                icon: "warning",
                                buttons: false,
                                dangerMode: true,
                            })
                            btnRegistrarMovManual.prop("disabled", true);
                        } else {
                            btnRegistrarMovManual.prop("disabled", false);
                        }

                    } else {
                        btnRegistrarMovManual.prop("disabled", false);
                    }
                } else {
                    swal({
                        title: "ALERTA",
                        text: "La caja seleccionada no ha sido aperturada en la fecha indicada",
                        icon: "warning",
                        buttons: false,
                        dangerMode: true,
                    })
                    btnRegistrarMovManual.prop("disabled", true);
                }

                ListarTransaccionesCaja(valueSelectedCaja, 0, 0, 0, 0, dateSelect, 0)
            } else {
                MpmensajeCaja.css("color", "rgba(179, 10, 10, 0.69)");
                MmensajeCaja.css("background-color", "rgba(255, 0, 0, 0.51)")
                    .css("border-color", "rgb(203, 46, 46)");
                MpmensajeCaja.html("Debe seleccionar una fecha");
                MmensajeCaja.css("display", "block");
                setTimeout(function () {
                    MmensajeCaja.css('display', 'none');
                }, 2000);
                btnRegistrarMovManual.prop("disabled", true);
                table.clear().draw();
            }
        } else {
            MpmensajeCaja.css("color", "rgba(179, 10, 10, 0.69)");
            MmensajeCaja.css("background-color", "rgba(255, 0, 0, 0.51)")
                .css("border-color", "rgb(203, 46, 46)");
            MpmensajeCaja.html("Debe seleccionar una caja");
            MmensajeCaja.css("display", "block");
            setTimeout(function () {
                MmensajeCaja.css('display', 'none');
            }, 2000);
            btnRegistrarMovManual.prop("disabled", true);
            table.clear().draw();
        }
    });

    function ListarCajasAperturadas(ntraCaja, ntraUsuario, flag, fecha) {
        //DESCRICION : Funcion que obtiene la lista de cajas aperturadas

        var json = JSON.stringify({
            ntraCaja: ntraCaja,
            ntraUsuario: ntraUsuario,
            flag: flag,
            fecha: fecha
        });

        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/ListarCajasAperturadas",
            data: json,
            async: false,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                //console.log(data.d);
                listaAperturas = data.d
            }
        })
    }

    function ListarCajasCerradas(ntraCaja, ntraUsuario, flag, fecha) {
        //DESCRICION : Funcion que obtiene la lista de cajas cerradas

        var json = JSON.stringify({
            ntraCaja: ntraCaja,
            ntraUsuario: ntraUsuario,
            flag: flag,
            fecha: fecha
        });

        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/ListarCajasCerradas",
            data: json,
            async: false,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                //console.log(data.d);
                listaCierres = data.d
            }
        })
    }

    function limpiarCampos() {
        dateMov.val("");
        selectTipoMov_Modal.prop('selectedIndex', 0);
        txtModoPago.val("EFECTIVO")
        selectTipoMoneda_Modal.prop('selectedIndex', 0);
        txtImporte.val("0.0");
    }

    btnRegistrarMovManual.click(function () {
        lblTitle.text("Registrar Transacción de Caja");
        btnGuardar.css("display", "none");
        btnRegistrar.css("display", "block");
        Mmensaje.css('display', 'none');
        limpiarCampos();

        var optionSelectCaja = selectCaja.find("option:selected");
        var valueSelectedCaja = optionSelectCaja.val();

        dateMov.val(apeDate.val())

    });

    btnRegistrar.click(function () {
        var date = dateMov.val();

        if (date == "") {
            selectDate = "";
        } else {
            var dater = date.split('/').join('-');
            var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
            var selectDate = moment(datef, "YYYY-MM-DD");
        }

        var cajaSelect = selectCaja.find("option:selected");
        var cajaValue = cajaSelect.val();

        var tipoMovSelect = selectTipoMov_Modal.find("option:selected");
        tipoMovValue = tipoMovSelect.val();
        var tipoMonedaSelect = selectTipoMoneda_Modal.find("option:selected");
        tipoMonedaValue = tipoMonedaSelect.val();
        var importeValue = txtImporte.val();

        if (selectDate != "") {
            if (tipoMovValue > 0) {
                if (tipoMonedaValue > 0) {
                    if (importeValue > 0) {
                        RegistrarTransaccionCaja(cajaValue, tipoMovValue, selectDate, 2, 1, tipoMonedaValue, importeValue)
                    } else {
                        txtImporte.attr("required", "required");
                        Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                        Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                            .css("border-color", "rgb(203, 46, 46)");
                        Mpmensaje.html("Debe ingresa un importe mayor a 0");
                        Mmensaje.css("display", "block");
                        setTimeout(function () {
                            Mmensaje.css('display', 'none');
                        }, 2000);
                    }
                } else {
                    selectTipoMoneda_Modal.attr("required", "required");
                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("Debe seleccionar un tipo de moneda");
                    Mmensaje.css("display", "block");
                    setTimeout(function () {
                        Mmensaje.css('display', 'none');
                    }, 2000);
                }
            } else {
                selectTipoMov_Modal.attr("required", "required");
                Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                    .css("border-color", "rgb(203, 46, 46)");
                Mpmensaje.html("Debe seleccionar un tipo de movimiento");
                Mmensaje.css("display", "block");
                setTimeout(function () {
                    Mmensaje.css('display', 'none');
                }, 2000);
            }
        } else {
            dateMov.attr("required", "required");
            Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
            Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                .css("border-color", "rgb(203, 46, 46)");
            Mpmensaje.html("Debe seleccionar una fecha");
            Mmensaje.css("display", "block");
            setTimeout(function () {
                Mmensaje.css('display', 'none');
            }, 2000);
        }


    });

    btnGuardar.click(function () {

        var date = dateMov.val();

        if (date == "") {
            selectDate = "";
        } else {
            var dater = date.split('/').join('-');
            var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
            var selectDate = moment(datef, "YYYY-MM-DD");
        }

        var transaccionCajaValue = txtNtraTransaccionCaja.val();

        var cajaSelect = selectCaja.find("option:selected");
        var cajaValue = cajaSelect.val();

        var tipoMovSelect = selectTipoMov_Modal.find("option:selected");
        tipoMovValue = tipoMovSelect.val();
        var tipoMonedaSelect = selectTipoMoneda_Modal.find("option:selected");
        tipoMonedaValue = tipoMonedaSelect.val();
        var importeValue = txtImporte.val();

        if (selectDate != "") {
            if (tipoMovValue > 0) {
                if (tipoMonedaValue > 0) {
                    if (importeValue > 0) {
                        ActualizarTransaccionCaja(transaccionCajaValue, cajaValue, tipoMovValue, selectDate, 2, 1, tipoMonedaValue, importeValue)
                    } else {
                        txtImporte.attr("required", "required");
                        Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                        Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                            .css("border-color", "rgb(203, 46, 46)");
                        Mpmensaje.html("Debe ingresa un importe mayor a 0");
                        Mmensaje.css("display", "block");
                        setTimeout(function () {
                            Mmensaje.css('display', 'none');
                        }, 2000);
                    }
                } else {
                    selectTipoMoneda_Modal.attr("required", "required");
                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("Debe seleccionar un tipo de moneda");
                    Mmensaje.css("display", "block");
                    setTimeout(function () {
                        Mmensaje.css('display', 'none');
                    }, 2000);
                }
            } else {
                selectTipoMov_Modal.attr("required", "required");
                Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                    .css("border-color", "rgb(203, 46, 46)");
                Mpmensaje.html("Debe seleccionar un tipo de movimiento");
                Mmensaje.css("display", "block");
                setTimeout(function () {
                    Mmensaje.css('display', 'none');
                }, 2000);
            }
        } else {
            dateMov.attr("required", "required");
            Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
            Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                .css("border-color", "rgb(203, 46, 46)");
            Mpmensaje.html("Debe seleccionar una fecha");
            Mmensaje.css("display", "block");
            setTimeout(function () {
                Mmensaje.css('display', 'none');
            }, 2000);
        }


    });

    function RegistrarTransaccionCaja
        (ntraCaja, ntraTipoMovimiento, fechaTransaccion, codTipoTransaccion,
            codModoPago, codTipoMoneda, importe) {

        var json = JSON.stringify({
            objCENTransaccionCaja: {
                ntraCaja: ntraCaja,
                ntraTipoMovimiento: ntraTipoMovimiento,
                fechaTransaccion: fechaTransaccion,
                codTipoTransaccion: codTipoTransaccion,
                codModoPago: codModoPago,
                codTipoMoneda: codTipoMoneda,
                importe: importe

            }
        });

        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/RegistrarTransaccionCaja",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {

                if (response.d === 0) {

                    Mpmensaje.html("El registro se realizó correctamente");
                    Mpmensaje.css("color", "#ffffff");
                    Mmensaje.css("background-color", "#337ab7")
                        .css("border-color", "#2e6da4");
                    Mmensaje.css("display", "block");

                    table.clear().draw();

                    var optionSelectCaja = selectCaja.find("option:selected");
                    var valueSelectedCaja = optionSelectCaja.val();

                    var valueSelectedApeDate = apeDate.val();
                    var dater = valueSelectedApeDate.split('/').join('-');
                    var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
                    var dateSelect = moment(datef, "YYYY-MM-DD");

                    ListarTransaccionesCaja(valueSelectedCaja, 0, 0, 0, 0, dateSelect, 0)

                    limpiarCampos()

                } else {

                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("No se pudo registrar la caja");
                    Mmensaje.css("display", "block");

                    limpiarCampos()

                }

            }
        });
    }

    function ActualizarTransaccionCaja
        (ntraTransaccionCaja, ntraCaja, ntraTipoMovimiento, fechaTransaccion,
            codTipoTransaccion, codModoPago, codTipoMoneda, importe)
    {

        var json = JSON.stringify({
            objCENTransaccionCaja: {
                ntraTransaccionCaja: ntraTransaccionCaja,
                ntraCaja: ntraCaja,
                ntraTipoMovimiento: ntraTipoMovimiento,
                fechaTransaccion: fechaTransaccion,
                codTipoTransaccion: codTipoTransaccion,
                codModoPago: codModoPago,
                codTipoMoneda: codTipoMoneda,
                importe: importe

            }
        });

        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/ActualizarTransaccionCaja",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {

                if (response.d === 0) {

                    Mpmensaje.html("El registro se actualizó correctamente");
                    Mpmensaje.css("color", "#ffffff");
                    Mmensaje.css("background-color", "#337ab7")
                        .css("border-color", "#2e6da4");
                    Mmensaje.css("display", "block");

                    table.clear().draw();
                    var optionSelectCaja = selectCaja.find("option:selected");
                    var valueSelectedCaja = optionSelectCaja.val();

                    var valueSelectedApeDate = apeDate.val();
                    var dater = valueSelectedApeDate.split('/').join('-');
                    var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
                    var dateSelect = moment(datef, "YYYY-MM-DD");

                    ListarTransaccionesCaja(valueSelectedCaja, 0, 0, 0, 0, dateSelect, 0)

                } else {

                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("No se pudo actualizar el registro");
                    Mmensaje.css("display", "block");

                }

            }
        });
    }

    // APERTURA DE CAJA

    $('.decimales').on('input', function () {
        this.value = this.value.replace(/[^0-9,.]/g, '').replace(/,/g, '.');
    });

    //Eliminar Registro
    function EliminarTransaccionCaja(ntraTransaccionCaja) {

        var json = JSON.stringify({ objCENTransaccionCaja: { ntraTransaccionCaja: ntraTransaccionCaja } });
        $.ajax({
            type: "POST",
            url: "frmMantIECaja.aspx/EliminarTransaccionCaja",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
            },
            success: function () {
                var optionSelectCaja = selectCaja.find("option:selected");
                var valueSelectedCaja = optionSelectCaja.val();

                var valueSelectedApeDate = apeDate.val();
                var dater = valueSelectedApeDate.split('/').join('-');
                var datef = moment(dater, 'DD-MM-YYYY').format("YYYY-MM-DD");
                var dateSelect = moment(datef, "YYYY-MM-DD");

                ListarTransaccionesCaja(valueSelectedCaja, 0, 0, 0, 0, dateSelect, 0)
            }
        })
    };

});