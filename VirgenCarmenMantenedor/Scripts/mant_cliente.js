﻿$(document).ready(function () {


    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal"  data-target="#modalRegistrar" ></button>';
    var btnEliminar = ' <button type="button"  class="icon-bin btnDelete" data-toggle="tooltip"  title="Eliminar" ></button>';
    var btnVer = ' <button type="button" title="Ver" class="icon-eye btnEye btnVer" data-toggle="modal"  data-target="#modalRegistrar" ></button>';

    var distritos = []
    var provincias = []
    var detallePuntosEntrega = []

    async function listarProvincias() {
        let prov = await fetch('frmMantCliente.aspx/listProvincia', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        });
        try {
            if (prov.ok) {
                let datos = await prov.json();
                provincias = datos.d;
                console.log(provincias)
            }
        } catch (error) {
            console.log(error)
        }

    }
    async function listarDistritos() {
        let dis = await fetch('frmMantCliente.aspx/listDistrito', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        });
        try {
            if (dis.ok) {
                let datos = await dis.json();

                distritos = datos.d;
                console.log(distritos)
            }
        } catch (error) {
            console.log(error)
        }

    }

    function cambioName() {
        console.log('init')
        $("#select_tipo_per_reg").attr("name", "select_tipo_per_reg");
        $("#select_tipo_doc_reg").attr("name", "select_tipo_doc_reg");
        $("#select_perfil_clinte").attr("name", "select_perfil_clinte");
        $("#select_clasificacion").attr("name", "select_clasificacion");
        $("#select_ruta").attr("name", "select_ruta");
        $("#select_lista_precio").attr("name", "select_lista_precio");
        $("#cbMrmDepartamento").attr("name", "cbMrmDepartamento");
        $("#cbMrmProvincia").attr("name", "cbMrmProvincia");
        $("#cbMrmDistrito").attr("name", "cbMrmDistrito");
    }

    cambioName();
    listarProvincias();
    listarDistritos();

    $("#tbl_clientes").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "10%", "targets": [2] },
            { "width": "10%", "targets": [4] },
            { "width": "5%", "targets": [5] },
            { "width": "5%", "targets": [6] },
            { "width": "10%", "targets": [7] },
            { "width": "10%", "targets": [8] },
            { "width": "10%", "targets": [12] },
            { "width": "10%", "targets": [13] },
            { "width": "10%", "targets": [14] },
            { "width": "10%", "targets": [15] },
            { "width": "10%", "targets": [16] },
            { "visible": false, "targets": [0, 1, 3, 9, 10, 11, 17, 18, 19, 20, 21, 22, 23, 24, 25] }
        ]
    });

    var tbl_cliente = $("#tbl_clientes").DataTable();
    tbl_cliente.columns.adjust().draw();

    $("#tbl_puntos_entrega").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "10%", "targets": [0] },
            { "width": "10%", "targets": [1] },
            { "width": "30%", "targets": [2] },
            { "width": "20%", "targets": [3] },
            { "width": "30%", "targets": [4] },
            { "visible": false, "targets": [5, 6, 7, 8, 9] }
        ]
    });

    var tbl_pe = $("#tbl_puntos_entrega").DataTable();
    tbl_pe.columns.adjust().draw();




    const showLoading = function () {
        swal({
            title: "Now loading",
            allowEscapeKey: false,
            allowOutsideClick: false,
            timer: 2000,
            onOpen: () => {
                swal.showLoading();
            }
        }).then(
            () => { },
            (dismiss) => {
                if (dismiss === "timer") {
                    console.log("closed by timer!!!!");
                    swal({
                        title: "Finished!",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
            }
        );
    };



    $("#txtMrmNumDocumento").on('keyup', function () {

        //showLoading();

        var txtMrmNumDocumento = document.querySelector("#txtMrmNumDocumento");
        var objcbMrmTipoDoc = document.getElementById("select_tipo_doc_reg");
        var objcbMrmTipoPersona = document.getElementById("select_tipo_per_reg");

        if (txtMrmNumDocumento.value.trim().length == 8 && objcbMrmTipoDoc.value == 1) {

            document.querySelector("#busquedaReniec").classList.replace("d-none", 'd-block');

            var numDocumento = "";
            var tipoDoc = objcbMrmTipoDoc.value;
            var tipoPer = objcbMrmTipoPersona.value;
            numDocumento = txtMrmNumDocumento.value.trim();
            ValidarExisteCliente(numDocumento, tipoDoc, tipoPer);

            //BuscarClienteReniec(valorNumdoc.val());
        }
    })

    function BuscarClienteReniec(numDocumento) {
        $('#mensaje_consulta_reniec').html('Consultando datos...');
        //$('<p>Consultando datos...</p>').appendTo('#mensaje_consulta_reniec');
        $("#mensaje_consulta_reniec").show();

        //DESCRIPCION : Funcion que me trae la lista de rutas.
        $.ajax({
            type: "POST",
            url: "frmMantCliente.aspx/BuscarClienteReniec",
            data: "{'numDocumento': '" + numDocumento + "'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                $("#mensaje_consulta_reniec").hide();
            },
            success: function (data) {
                console.log(data.d);
                $("#mensaje_consulta_reniec").hide();
                document.querySelector("#busquedaReniec").classList.replace("d-block", 'd-none');

                LlenarDatosReniec(data.d);


            }
        });
    }

    function LlenarDatosReniec(data) {
        var valNombreCliente = $("#txtMrmNombres");
        var valAppPatCliente = $("#txtMrmApellPaterno");
        var valAppMatCliente = $("#txtMrmApellMaterno");
        if (data.ErrorWebSer.CodigoErr == 2000) {
            valNombreCliente.val(data.respuestaWsReniec.nombres);
            valAppPatCliente.val(data.respuestaWsReniec.apellido_paterno);
            valAppMatCliente.val(data.respuestaWsReniec.apellido_materno);


        } else {
            console.log("Sin datos devueltos reniec");
        }

    }





    //sunat
    $("#txtMrmRUC").on('keyup', function () {
        if ($("#txtMrmRUC").val().trim().length == 11) {
            document.querySelector("#busquedaRuc").classList.replace("d-none", 'd-block');

            let tipoDoc = $("#select_tipo_doc_reg").val();
            let tipoPer = $("#select_tipo_per_reg").val();
            let numDocumento = $("#txtMrmRUC").val();

            ValidarExisteCliente(numDocumento, tipoDoc, tipoPer);

        }
    })

    function ValidarExisteCliente(numDocumento, tipoDoc, tipoPer) {
        //DESCRIPCION : Funcion que me trae la lista de rutas.
        var valorRuc = document.getElementById("txtMrmRUC");
        var txtMrmNumDocumento = document.getElementById("txtMrmNumDocumento");
        var estadoCli = true;
        var json = JSON.stringify({
            numDocumento: numDocumento,
            tipoDoc: tipoDoc, tipoPer: tipoPer
        });

        $.ajax({
            type: "POST",
            url: "frmMantCliente.aspx/ValidarExisteCliente",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                estadoCli = true;
                return estadoCli;
            },
            success: function (data) {
                //console.log("respuesta: " + data.d);
                estadoCli = data.d;
                if (tipoDoc == 1) {
                    if (data.d == false) {
                        mostrarMensajeError("Documento del cliente ya se encuentra registrado");
                        txtMrmNumDocumento.value = "";
                        document.querySelector("#busquedaReniec").classList.replace("d-block", 'd-none');

                    } else {
                        BuscarClienteReniec(numDocumento);
                    }
                }

                if (tipoDoc == 3 || tipoPer == 2) {
                    if (data.d == false) {
                        mostrarMensajeError("RUC del cliente ya se encuentra registrado");
                        valorRuc.value = "";
                        document.querySelector("#busquedaRuc").classList.replace("d-block", 'd-none');

                    } else {
                        BuscarClienteSunat(numDocumento);
                    }
                }

                return estadoCli;
                //var bytes = new Uint8Array(data.d);

            }
        });
        //return estadoCli;
    }

    function BuscarClienteSunat(numDocumento) {
        $('#mensaje_consulta_reniec').html('Consultando datos...');
        $("#mensaje_consulta_reniec").show();
        $.ajax({
            type: "POST",
            url: "frmMantCliente.aspx/BuscarClienteSunat",
            data: "{'numDocumento': '" + numDocumento + "'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                $("#mensaje_consulta_sunat").hide();
                document.querySelector("#busquedaRuc").classList.replace("d-block", 'd-none');

            },
            success: function (data) {
                console.log("sunat");
                console.log(data.d);
                $("#mensaje_consulta_sunat").hide();
                LlenarDatosSunat(data.d);
                document.querySelector("#busquedaRuc").classList.replace("d-block", 'd-none');


            }
        });
    }

    function LlenarDatosSunat(data) {
        var valRucCliente = $("#razon_social");
        if (data.ErrorWebSer.CodigoErr == 2000) {
            valRucCliente.val(data.respuestaWsSunat.razon_social);
        } else {
            swal("Atención", "No se encontraron datos del cliente", "warning");


        }

    }

    function mostrarMensajeError(mensaje) {
        swal("Atención", mensaje, "error");
    }





    //tipo de docuemnto

    $("#select_tipo_per_reg").on('change', function () {
        ($("#select_tipo_per_reg").val() == 1) ?
            $("#txtMrmRazonSocial").attr('disabled', true)
            :
            $("#txtMrmRazonSocial").removeAttr('disabled')

        if ($("#select_tipo_per_reg").val() == 2) {
            $("#select_tipo_doc_reg").attr("disabled", true)
        } else {

            $("#select_tipo_doc_reg").removeAttr("disabled")

        }
    })

    async function init() {
        let num = $("#numdoc").val();
        let tipo = 0;
        let data = await fetch('frmMantCliente.aspx/BusquedaCliente', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ auxtipoDocumento: tipo, auxNumDocumento: num, auxNombres: num })
        })

        try {

            if (data.ok) {
                let response = await data.json();
               
       
                llenarTablaCliente(response.d)
            }

        } catch (error) {
            console.log(error)
        }

    }

    function llenarTablaCliente(response) {
        for (var i = 0; i < response.length; i++) {
            tbl_cliente.row.add([
                response[i].codPersona,
                response[i].tipoPersona,
                response[i].descTipoPersona,
                response[i].tipoDocumento,
                response[i].descTipoDocumento,
                response[i].numeroDocumento,
                response[i].ruc,
                response[i].razonSocial,
                response[i].nombres + response[i].apellidoPaterno + response[i].apellidoMaterno,
                response[i].nombres,
                response[i].apellidoPaterno,
                response[i].apellidoMaterno,
                response[i].direccion,
                response[i].correo,
                response[i].telefono,
                response[i].celular,
                btnVer + btnEditar + btnEliminar,
                response[i].perfilCliente,
                response[i].clasificacionCliente,
                response[i].tipoListaPrecio,
                response[i].ordenAtencion,
                response[i].coordenadaX,
                response[i].coordenadaY,
                response[i].codUbigeo,
                response[i].frecuenciaCliente,
                response[i].codRuta

            ]).draw()
        }
        tbl_cliente.search('').draw()
    }

    init();





    /************** registrar cliente *************************/

    document.querySelector("#btn_mostrar_modal").addEventListener('click', function () {
        $("#select_tipo_per_reg").attr('disabled', false)
        $("#select_tipo_per_reg").val("0")
        $("#select_tipo_doc_reg").attr('disabled', false)
        $("#select_perfil_clinte").attr('disabled', false)
        $("#select_clasificacion").attr('disabled', false)
        $("#select_lista_precio").attr('disabled', false)
        $("#select_ruta").attr('disabled', false)
        $("#txtMrmOrdenAtencion").attr('disabled', false)
        $("#txtMrmNumDocumento").attr('disabled', false)
        $("#txtMrmRUC").attr('disabled', false)
        $("#txtMrmRazonSocial").attr('disabled', false)
        $("#txtMrmNombres").attr('disabled', false)
        $("#txtMrmApellPaterno").attr('disabled', false)
        $("#txtMrmApellMaterno").attr('disabled', false)
        $("#txtMrmDireccion").attr('disabled', false)
        $("#txtLatitudXa").attr('disabled', false)
        $("#txtLongitudYa").attr('disabled', false)
        $("#txtMrmCorreo").attr('disabled', false)
        $("#txtMrmTelefono").attr('disabled', false)
        $("#txtMrmCelular").attr('disabled', false)
        $("#cbMrmDepartamento").attr('disabled', false)
        $("#cbMrmProvincia").attr('disabled', false)
        $("#cbMrmDistrito").attr('disabled', false)
        $("#punto_entrega_form").show()

        document.querySelector("#btnAceptarMdRegMod").removeAttribute('disabled');

        limpiarFormulario()
    })

    var formRegistro = $("#pnlRegMod").validate({
        rules: {
            select_tipo_per_reg: { required: true },
            select_tipo_doc_reg: { required: true },
            select_perfil_clinte: { required: true },
            select_clasificacion: { required: true },
            select_ruta: { required: true },
            select_lista_precio: { required: true },
            cbMrmDepartamento: { required: true },
            cbMrmProvincia: { required: true },
            cbMrmDistrito: { required: true }
        },
        message: {
            select_tipo_per_reg: "<div style='color: red; font- weight: normal;'>Seleccione el tipo de persona</div>",
            select_tipo_doc_reg: "<div style='color: red; font- weight: normal;'>Seleccione el tipo de documento</div>",
            select_perfil_clinte: "<div style='color: red; font- weight: normal;'>Seleccione el perfil del cliente</div>",
            select_clasificacion: "<div style='color: red; font- weight: normal;'>Seleccione la clasificación del cliente</div>",
            select_ruta: "<div style='color: red; font- weight: normal;'>Seleccione la ruta</div>",
            select_lista_precio: "<div style='color: red; font- weight: normal;'>Seleccione la lista</div>"
        }
    })

    $("#form_registrar_cliente").on('submit', e => e.preventDefault())

    $('#btnAceptarMdRegMod').on('click', async function () {
        console.log('click')
        detallePuntosEntrega.length = 0
        if (formRegistro.form()) {

            document.querySelector("#preload").classList.replace("d-none", 'd-block');
            document.querySelector("#btnAceptarMdRegMod").setAttribute('disabled', true);

            let datosCliente = [{
                codPersona: $("#txtCodPersona").val() != "" ? $("#txtCodPersona").val() : 1,
                tipoPersona: $("#select_tipo_per_reg").val(),
                tipoDocumento: $("#select_tipo_doc_reg").val(),
                numeroDocumento: $("#txtMrmNumDocumento").val(),
                RUC: $("#txtMrmRUC").val(),
                razonSocial: $("#txtMrmRazonSocial").val(),
                nombres: $("#txtMrmNombres").val(),
                apellidoPaterno: $("#txtMrmApellPaterno").val(),
                apellidoMaterno: $("#txtMrmApellMaterno").val(),
                direccion: $("#txtMrmDireccion").val(),
                correo: $("#txtMrmCorreo").val(),
                telefono: $("#txtMrmTelefono").val() == "" ? $("#txtMrmTelefono").val() : 0,
                celular: $("#txtMrmCelular").val() == "" ? $("#txtMrmCelular").val() : 0,
                codUbigeo: $("#cbMrmDistrito").val(),
                ordenAtencion: 0,
                perfilCliente: 0,
                clasificacionCliente: $("#select_clasificacion").val(),
                frecuenciaCliente: 0,
                tipoListaPrecio: $("#select_lista_precio").val(),
                codRuta: $("#select_ruta").val(),
                coordenadaY: $("#txtLatitudXa").val(),
                coordenadaX: $("#txtLongitudYa").val()
            }]



            //llenado de los puntos de entrega
            for (let i = 0; i < tbl_pe.rows().count(); i++) {

                let puntoEntrega = {

                    'descDepartamento': tbl_pe.rows().data()[i][0],
                    'descProvincia': tbl_pe.rows().data()[i][1],
                    'descDistrito': tbl_pe.rows().data()[i][2],
                    'direccion': tbl_pe.rows().data()[i][3],
                    'referencia': tbl_pe.rows().data()[i][4],
                    'coordenadaX': tbl_pe.rows().data()[i][5],
                    'coordenadaY': tbl_pe.rows().data()[i][6],
                    'codUbigeo': tbl_pe.rows().data()[i][7],
                    'codPersona': tbl_pe.rows().data()[i][8] != "" ? tbl_pe.rows().data()[i][8] : 1,
                    'flag': tbl_pe.rows().data()[i][9] != "" ? tbl_pe.rows().data()[i][9] : 1
                }

                detallePuntosEntrega.push(puntoEntrega)
            }

            if ($("#txtCodPersona").val() == "") {

                let data = await fetch('frmMantCliente.aspx/registrarCliente', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: JSON.stringify({ cabecera: datosCliente, puntoEntrega: detallePuntosEntrega.length != 0 ? detallePuntosEntrega : null })
                });

                try {

                    if (data.ok) {

                        let response = await data.json();

                        if (response.d > 0) {

                            swal({
                                title: "",
                                text: "Cliente registrado de  manera exitosa",
                                icon: "success",
                                buttons: true,
                                dangerMode: true,
                            });



                        } else {
                            swal({
                                title: "",
                                text: "No se pudo registrar el cliente, intentelo nuevamente",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            });
                        }

                        document.querySelector("#preload").classList.replace("d-block", 'd-none');
                        document.querySelector("#btnAceptarMdRegMod").removeAttribute('disabled');
                        limpiarFormulario();

                    }
                } catch (error) {
                    swal({
                        icon: "Error",
                        title: "Se presento un error al intentar registrar al cliente"
                    })

                    console.log(error)
                }

            } else {

                let data = await fetch('frmMantCliente.aspx/editarCliente', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: JSON.stringify({ cabecera: datosCliente, puntoEntrega: detallePuntosEntrega })
                });

                try {

                    if (data.ok) {

                        let response = await data.json();

                        if (response.d > 0) {

                            swal({
                                title: "",
                                text: "Cliente modificado de  manera exitosa",
                                icon: "success",
                                buttons: true,
                                dangerMode: true,
                            });



                        } else {
                            swal({
                                title: "",
                                text: "No se pudo modificar el cliente, intentelo nuevamente",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            });
                        }

                        document.querySelector("#preload").classList.replace("d-block", 'd-none');
                        document.querySelector("#btnAceptarMdRegMod").removeAttribute('disabled');
                        limpiarFormulario();
                    }
                } catch (error) {
                    swal({
                        icon: "Error",
                        title: "Se presento un error al intentar modificar al cliente"
                    })

                    console.log(error)
                }

            }

        }
    });


    function limpiarFormulario() {
        $("#txtCodPersona").val("")
        $("#select_tipo_per_reg").val("")
        $("#select_tipo_doc_reg").val("")
        $("#select_perfil_clinte").val("")
        $("#select_clasificacion").val("")
        $("#select_lista_precio").val("")
        $("#select_ruta").val("")
        $("#txtMrmOrdenAtencion").val("")
        $("#txtMrmNumDocumento").val("")
        $("#txtMrmRUC").val("")
        $("#txtMrmRazonSocial").val("")
        $("#txtMrmNombres").val("")
        $("#txtMrmApellPaterno").val("")
        $("#txtMrmApellMaterno").val("")
        $("#txtMrmDireccion").val("")
        $("#txtLatitudXa").val("")
        $("#txtLongitudYa").val("")
        $("#txtMrmCorreo").val("")
        $("#txtMrmTelefono").val("")
        $("#txtMrmCelular").val("")
        tbl_pe.clear().draw();

    }


    $("#btnAgregarPE").on('click', pasarPuntosEntrega)

    //funcion para pasar los puntos de entrega a la tabla
    function pasarPuntosEntrega() {

        let direccion_pe = $("#txt_direccion_pe");
        let referencia_pe = $("#txt_referencia_pe");
        let longitud_pe = $("#longitud_pe");
        let latitud_pe = $("#latitud_pe");

        if ($('#select_provincia_pe').val() != "00" &&
            $('#select_distrito_pe').val() != "0000" &&
            direccion_pe.val() != "" &&
            direccion_pe.val() != "" && longitud_pe != ""
        ) {



            tbl_pe.row.add([
                $('select[id="select_departamento_pe"] option:selected').text(),
                $('select[id="select_provincia_pe"] option:selected').text(),
                $('select[id="select_distrito_pe"] option:selected').text(),
                direccion_pe.val(),
                referencia_pe.val(),
                longitud_pe.val(),
                latitud_pe.val(),
                $('#select_distrito_pe').val(),
                $("#CodPersona").val(),
                1,
                btnEliminar
            ]).draw()

            tbl_pe.search('').draw()

            direccion_pe.val("")
            referencia_pe.val("")
            longitud_pe.val("")
            latitud_pe.val("")

        } else {
            mostrarMensajeError("Complete todos los campos del punto de entrega")
        }



    }



    // funcion para eliminar punto de entrega 

    $('#tbl_puntos_entrega tbody ').on('click', 'tr .btnDelete', function () {

        let tr = $(this).parent().parent();

        let data = tbl_pe.row(tr).data();

        if (data[8] == "") {

            tbl_pe.row(tr).remove().draw()

        } else {

            let puntoEntregaEliminado = new Object();


            puntoEntregaEliminado.flag = 0
            puntoEntregaEliminado.codUbigeo = ""
            puntoEntregaEliminado.coordenadaX = ""
            puntoEntregaEliminado.coordenadaY = ""
            puntoEntregaEliminado.descDepartamento = ""
            puntoEntregaEliminado.descDistrito = ""
            puntoEntregaEliminado.descProvincia = 0
            puntoEntregaEliminado.descDistrito = 0
            puntoEntregaEliminado.direccion = 0
            puntoEntregaEliminado.ntraPuntoEntrega = data[8]
            puntoEntregaEliminado.ordenEntrega = 0
            puntoEntregaEliminado.referencia = 0

            detallePuntosEntrega.push(puntoEntregaEliminado)

            tbl_pe.row(tr).remove().draw()

        }
    })



    document.querySelector("#buttonModalsDireccion").addEventListener('click', function () {
        document.querySelector('#btnGuardarMap').classList.replace('d-none', 'd-block')
    })

    document.querySelector("#buttonModalpe").addEventListener('click', function () {
        document.querySelector('#btnGuardarMapPE').classList.replace('d-none', 'd-block')
    })

    // capturando las coordenadas del mapa guardadas en la sesion - datos cliente
    $('#btnGuardarMap').on('click', function () {
        $("#txtLongitudYa").val(window.sessionStorage.getItem('long'))
        $("#txtLatitudXa").val(window.sessionStorage.getItem('lat'))
        document.querySelector('#btnGuardarMap').classList.replace('d-block', 'd-none')


    })


    // capturando las coordenadas del mapa guardadas en la sesion - datos puntos de entrega
    $('#btnGuardarMapPE').on('click', function () {
        $("#longitud_pe").val(window.sessionStorage.getItem('long'))
        $("#latitud_pe").val(window.sessionStorage.getItem('lat'))
        document.querySelector('#btnGuardarMapPE').classList.replace('d-block', 'd-none')

    })





    //EVENTOS DE PARA FORMULARIO DE BUSQUEDA

    $('#rb_nom').change(function () {
        $("#nom").removeAttr('disabled')
        $("#nom").focus()

        $("#numdoc").attr('disabled', true)
        $("#select_tipo_doc").attr('disabled', true)
        $("#numdoc").val("");
        $("#select_tipo_doc").val(0)

    })

    $('#rb_tdoc').change(function () {

        $("#select_tipo_doc").removeAttr('disabled')
        $("#numdoc").removeAttr('disabled')
        $("#select_tipo_doc").focus()
        $("#nom").attr('disabled', true)
        $("#nom").val("")

    })


    //busqueda de cientes
    $("#formBusqueda").on('submit', e => e.preventDefault())

    $("#buscarCliente").on('click', async function () {

        if (!$('#rb_tdoc').is(':checked') && !$('#rb_nom').is(':checked')) {
            swal({
                icon: "info",
                title: "Debe seleccionar el tipo de busqueda "
            })
        } else {

            let tipodoc = $('#rb_tdoc').is(':checked') ? $('#rb_tdoc').val() : $('#rb_nom').val()
            let num = $("#numdoc").val()
            let nom = $("#nom").val()


            let data = await fetch('frmMantCliente.aspx/BusquedaCliente', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ auxtipoDocumento: tipodoc, auxNumDocumento: num, auxNombres: nom })
            })

            try {

                if (data.ok) {
                    let response = await data.json();

                    tbl_cliente.clear().draw();
                    llenarTablaCliente(response.d)


                }

            } catch (error) {
                console.log(error)
            }

        }
    })


    // button ver

    $('#tbl_clientes tbody ').on('click', 'tr .btnVer', function () {

        document.querySelector("#btnAceptarMdRegMod").setAttribute('disabled', true);


        let tr = $(this).parent().parent();
        let codigoPer = tbl_cliente.row(tr).data()[0];

        //desactivar campos

        $("#select_tipo_per_reg").attr('disabled', true)
        $("#select_tipo_doc_reg").attr('disabled', true)
        $("#select_perfil_clinte").attr('disabled', true)
        $("#select_clasificacion").attr('disabled', true)
        $("#select_lista_precio").attr('disabled', true)
        $("#select_ruta").attr('disabled', true)
        $("#txtMrmOrdenAtencion").attr('disabled', true)
        $("#txtMrmNumDocumento").attr('disabled', true)
        $("#txtMrmRUC").attr('disabled', true)
        $("#txtMrmRazonSocial").attr('disabled', true)
        $("#txtMrmNombres").attr('disabled', true)
        $("#txtMrmApellPaterno").attr('disabled', true)
        $("#txtMrmApellMaterno").attr('disabled', true)
        $("#txtMrmDireccion").attr('disabled', true)
        $("#txtLatitudXa").attr('disabled', true)
        $("#txtLongitudYa").attr('disabled', true)
        $("#txtMrmCorreo").attr('disabled', true)
        $("#txtMrmTelefono").attr('disabled', true)
        $("#txtMrmCelular").attr('disabled', true)
        $("#cbMrmDepartamento").attr('disabled', true)
        $("#cbMrmProvincia").attr('disabled', true)
        $("#cbMrmDistrito").attr('disabled', true)
        $("#punto_entrega_form").hide()
        //busqueda de detalle de promocion (productos promocionados)

        buscarPuntoEntrega(codigoPer)

        $("#txtCodPersona").val(tbl_cliente.row(tr).data()[0])
        $("#select_tipo_per_reg").val(tbl_cliente.row(tr).data()[1])
        $("#select_tipo_doc_reg").val(tbl_cliente.row(tr).data()[3])
        $("#select_perfil_clinte").val(tbl_cliente.row(tr).data()[17])
        $("#select_clasificacion").val(tbl_cliente.row(tr).data()[18])
        $("#select_lista_precio").val(tbl_cliente.row(tr).data()[19])
        $("#select_ruta").val(tbl_cliente.row(tr).data()[25])
        $("#txtMrmOrdenAtencion").val(tbl_cliente.row(tr).data()[20])
        $("#txtMrmNumDocumento").val(tbl_cliente.row(tr).data()[5])
        $("#txtMrmRUC").val(tbl_cliente.row(tr).data()[6])
        $("#txtMrmRazonSocial").val(tbl_cliente.row(tr).data()[7])
        $("#txtMrmNombres").val(tbl_cliente.row(tr).data()[9])
        $("#txtMrmApellPaterno").val(tbl_cliente.row(tr).data()[10])
        $("#txtMrmApellMaterno").val(tbl_cliente.row(tr).data()[11])
        $("#txtMrmDireccion").val(tbl_cliente.row(tr).data()[12])
        $("#txtLatitudXa").val(tbl_cliente.row(tr).data()[21])
        $("#txtLongitudYa").val(tbl_cliente.row(tr).data()[22])
        $("#txtMrmCorreo").val(tbl_cliente.row(tr).data()[13])
        $("#txtMrmTelefono").val(tbl_cliente.row(tr).data()[14])
        $("#txtMrmCelular").val(tbl_cliente.row(tr).data()[15])
        $("#cbMrmDepartamento").val(tbl_cliente.row(tr).data()[23].substring(0, 2))
        $("#cbMrmProvincia").val(tbl_cliente.row(tr).data()[23].substring(0, 4))
        $("#cbMrmDistrito").val(tbl_cliente.row(tr).data()[23])
    })


    //ELIMINAR CLIENTE 

    $('#tbl_clientes tbody ').on('click', 'tr .btnDelete', async function () {
        let tr = $(this).parent().parent();
        let row = tbl_cliente.row(tr).data();



        let data = await fetch('frmMantCliente.aspx/eliminarCliente', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({ codCliente: row[0] })
        })

        try {

            if (data.ok) {

                let response = await data.json()

                if (response.d != 0) {
                    swal({
                        icon: "error",
                        title: "Se produjo un error al intentar eliminar al cliente "
                    })
                } else {
                    tbl_cliente.row(tr).remove().draw()
                    tbl_cliente.search('').draw();
                }

            }


        } catch (e) {
            console.log(e)
        }

    })


    //EDITAR CLIENTES

    $('#tbl_clientes tbody ').on('click', 'tr .btnEditar', function () {

        $("#select_tipo_per_reg").attr('disabled', false)
        $("#select_tipo_doc_reg").attr('disabled', false)
        $("#select_perfil_clinte").attr('disabled', false)
        $("#select_clasificacion").attr('disabled', false)
        $("#select_lista_precio").attr('disabled', false)
        $("#select_ruta").attr('disabled', false)
        $("#txtMrmOrdenAtencion").attr('disabled', false)
        $("#txtMrmNumDocumento").attr('disabled', false)
        $("#txtMrmRUC").attr('disabled', false)
        $("#txtMrmRazonSocial").attr('disabled', false)
        $("#txtMrmNombres").attr('disabled', false)
        $("#txtMrmApellPaterno").attr('disabled', false)
        $("#txtMrmApellMaterno").attr('disabled', false)
        $("#txtMrmDireccion").attr('disabled', false)
        $("#txtLatitudXa").attr('disabled', false)
        $("#txtLongitudYa").attr('disabled', false)
        $("#txtMrmCorreo").attr('disabled', false)
        $("#txtMrmTelefono").attr('disabled', false)
        $("#txtMrmCelular").attr('disabled', false)
        $("#cbMrmDepartamento").attr('disabled', false)
        $("#cbMrmProvincia").attr('disabled', false)
        $("#cbMrmDistrito").attr('disabled', false)
        $("#punto_entrega_form").show()


        let tr = $(this).parent().parent();
        let codigoPer = tbl_cliente.row(tr).data()[0];

        //busqueda de detalle de promocion (productos promocionados)

        buscarPuntoEntrega(codigoPer)

        $("#txtCodPersona").val(tbl_cliente.row(tr).data()[0])
        $("#select_tipo_per_reg").val(tbl_cliente.row(tr).data()[1])
        $("#select_tipo_doc_reg").val(tbl_cliente.row(tr).data()[3])
        $("#select_perfil_clinte").val(tbl_cliente.row(tr).data()[17])
        $("#select_clasificacion").val(tbl_cliente.row(tr).data()[18])
        $("#select_lista_precio").val(tbl_cliente.row(tr).data()[19])
        $("#select_ruta").val(tbl_cliente.row(tr).data()[25])
        $("#txtMrmOrdenAtencion").val(tbl_cliente.row(tr).data()[20])
        $("#txtMrmNumDocumento").val(tbl_cliente.row(tr).data()[5])
        $("#txtMrmRUC").val(tbl_cliente.row(tr).data()[6])
        $("#txtMrmRazonSocial").val(tbl_cliente.row(tr).data()[7])
        $("#txtMrmNombres").val(tbl_cliente.row(tr).data()[9])
        $("#txtMrmApellPaterno").val(tbl_cliente.row(tr).data()[10])
        $("#txtMrmApellMaterno").val(tbl_cliente.row(tr).data()[11])
        $("#txtMrmDireccion").val(tbl_cliente.row(tr).data()[12])
        $("#txtLatitudXa").val(tbl_cliente.row(tr).data()[22])
        $("#txtLongitudYa").val(tbl_cliente.row(tr).data()[21])
        $("#txtMrmCorreo").val(tbl_cliente.row(tr).data()[13])
        $("#txtMrmTelefono").val(tbl_cliente.row(tr).data()[14])
        $("#txtMrmCelular").val(tbl_cliente.row(tr).data()[15])
        $("#cbMrmDepartamento").val(tbl_cliente.row(tr).data()[23].substring(0, 2))

        //buscar provincias

        let provinciaTabla = tbl_cliente.row(tr).data()[23].substring(0, 4);
        let listProv = Object.values(provincias).filter(el => (el.codDepartamento + el.codProvincia) == provinciaTabla)    

        $("#cbMrmProvincia").empty();

        for (provincia of listProv) {
            $("#cbMrmProvincia").append('<option value="' + provincia.codDepartamento + provincia.codProvincia + '">' + provincia.nombre + '</option>').val(0)
        }

        $("#cbMrmProvincia").val(tbl_cliente.row(tr).data()[23].substring(0, 4))


        //busca distrito

        let distritoTable = tbl_cliente.row(tr).data()[23];
        let listDist = Object.values(distritos).filter(el => (el.codDepartamento + el.codProvincia + el.codDistrito) == distritoTable) 

        $("#cbMrmDistrito").empty()
        for (dist of listDist) {
            $("#cbMrmDistrito").append('<option value="' + dist.codDepartamento + dist.codProvincia + dist.codProvincia + '">' + dist.nombre + '</option>').val(0)
        }

        $("#cbMrmDistrito").val(tbl_cliente.row(tr).data()[23])
    })


    async function buscarPuntoEntrega(codigo) {

        let datos = await fetch('frmMantCliente.aspx/BuscarPuntosEntrega', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({ codPersona: parseInt(codigo, 10) })
        });
        try {

            if (datos.ok) {

                let response = await datos.json()
                    
                llenaTablaPE(response.d)

            }


        } catch (e) {
            console.log(e)
        }

    }

    function llenaTablaPE(data) {
        tbl_pe.clear().draw();

        for (var i = 0; i < data.length; i++) {

            let distrito = distritos.filter(el => { ("" + el.codDepartamento + "" + el.codProvincia + "" + el.codDistrito + "") === (data[i].codUbigeo) })
            console.log(distrito);

            tbl_pe.row.add([
                data[i].codUbigeo,
                data[i].codUbigeo,
                data[i].codUbigeo,
                data[i].direccion,
                data[i].referencia,
                data[i].coordenadaY,
                data[i].coordenadaX,
                data[i].codUbigeo,
                data[i].ntraPuntoEntrega,
                2,
                btnEliminar
            ]).draw()

            tbl_pe.search('').draw()
        }
    }

    //Eventos para el llenado de provincias y distritos - orden de entrega

    $("#select_departamento_pe").on('change', function () {

        let dep = $("#select_departamento_pe").val();
        const listProv = Object.values(provincias).filter(el => el.codDepartamento == dep)

        $("#select_provincia_pe").empty();
        for (provincia of listProv) {
            $("#select_provincia_pe").append('<option value="' + provincia.codDepartamento + provincia.codProvincia + '">' + provincia.nombre + '</option>').val(0)
        }

    })

    $("#select_provincia_pe").on('change', function () {

        let provincia = $("#select_provincia_pe").val();
        const listDist = Object.values(distritos).filter(el => el.codDepartamento + el.codProvincia == provincia)

        $("#select_distrito_pe").empty()
        for (dist of listDist) {
            $("#select_distrito_pe").append('<option value="' + dist.codDepartamento + dist.codProvincia + dist.codProvincia + '">' + dist.nombre + '</option>').val(0)
        }

    })

    //Eventos para el llenado de provincias y distritos - domicilio fiscal

    $("#cbMrmDepartamento").on('change', function () {

        let dep = $("#cbMrmDepartamento").val();
        const listProv = Object.values(provincias).filter(el => el.codDepartamento == dep)

        $("#cbMrmProvincia").empty()
        for (provincia of listProv) {
            $("#cbMrmProvincia").append('<option value="' + provincia.codDepartamento + provincia.codProvincia + '">' + provincia.nombre + '</option>').val(0)
        }

    })

    $("#cbMrmProvincia").on('change', function () {

        let provincia = $("#cbMrmProvincia").val();
        const listDist = Object.values(distritos).filter(el => el.codDepartamento + el.codProvincia == provincia)

        $("#cbMrmDistrito").empty()
        for (dist of listDist) {
            $("#cbMrmDistrito").append('<option value="' + dist.codDepartamento + dist.codProvincia + dist.codProvincia + '">' + dist.nombre + '</option>').val(0)
        }

    })


})











