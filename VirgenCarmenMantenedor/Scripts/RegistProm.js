﻿



$(document).ready(function () {
    //Lista de Variables


    var mensaje = document.createElement('span')
    mensaje.classList.add('alert-danger')

    var containermMensaje = document.querySelector('#containermMensaje');

    var arregloDetalleProducto = [];


    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal"  data-target="#exampleModal2" ></button>';
    var btnEliminar = ' <button type="button"  class="icon-bin btnDelete" data-toggle="tooltip"  title="Eliminar" ></button>';
    var btnVer = ' <button type="button" title="Ver" class="icon-eye btnEye btnVer" data-toggle="modal"  data-target="#exampleModal" ></button>';
    var btn_agregar = $("#buttonModal");

    var btnEliminarDet = ' <button type="button" title="Anular" class="icon-bin btnDelete" data-toggle="modal" data-target="#modalAnular"></button>';




    //ocultar campos de mensajes--registrar
    $("#msj_txt_fecha").hide();
    $("#msj_txt_hora").hide();

    $("#msj_txt_uso_cliente").hide();
    $("#msj_txt_uso_vendedor").hide();

    $("#uso_vendedor").hide();
    $("#uso_cliente").hide();




    // campos del formulario de busqueda

    var estado = $('#estado');
    var proveedor = $('#id_proveedor');
    var tipoVenta = $('#tipoVenta');
    var fechaInicial = $('#min_date');
    var fechaFinal = $('#max_date');
    var cliente = $('#filtro_cliente');
    var vendedor = $('#vendedor');
    var producto_principal = $('#producto_principal');

    var btnBuscar = $("#id_btnBuscar");

    /* MODAL REGISTRAR */

    var txt_nombre_promo_reg = $("#txt_nombre_promo_reg");
    var txt_fech_inicio_reg = $("#txt_fech_inicio_reg");
    var txt_fecha_final_reg = $("#txt_fecha_final_reg");
    var txt_hora_inicio_reg = $("#txt_hora_inicio_reg");
    var txt_hora_fin_reg = $("#txt_hora_fin_reg");

    var radio_activar_si_reg = $("#radio_activar_si_reg");
    var radio_activar_no_reg = $("#radio_activar_no_reg");

    var id_codProducto_reg = $("#id_codProducto_reg");
    var id_Producto_reg = $("#nonProdPrincipal");
    var cantidadPrincipal = $("#txt-cant");
    var id_unidadBase = $("#id_unidadBase");
    var id_Producto_promo = $("#id_Producto_promo");
    var id_codProducto_promo = $("#id_codProducto_promo");
    var btn_buscar_prod_recibir = $("#btn_buscar_prod_recibir");
    var txt_cantidad = $("#txt-cantidad");
    var id_unidadBase_promo = $("#id_unidadBase_promo");
    var txt_dinero = $("#txt-dinero");

    var radio_contado_reg = $("#radio_contado_reg");
    var radio_credito_reg = $("#radio_credito_reg");
    var cbo_cliente_reg = $("#cbo_cliente_reg");
    var cbo_vendedor_reg = $("#cbo_vendedor_reg");

    var txt_uso_promo_reg = $("#txt_uso_promo_reg");
    var txt_uso_promo_vendedor_reg = $("#txt_uso_promo_vendedor_reg");
    var txt_uso_promo_cliente_reg = $("#txt_uso_promo_cliente_reg");


    document.querySelector('#frm_registrar_promo').addEventListener('submit', (e) => { e.preventDefault(); });

    var flag_seleccion = 0;

    //*** TABLA DE PROMOCIONES ***//

    $("#tbl_promociones").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [0] },
            { "width": "35%", "targets": [2] },
            { "width": "15%", "targets": [3] },
            { "width": "15%", "targets": [4] },
            { "width": "20%", "targets": [6] },
            { "width": "10%", "targets": [7] },
            { "visible": false, "targets": [1, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24] }
        ]
    });

    var tb_promo = $("#tbl_promociones").DataTable();
    tb_promo.columns.adjust().draw();

    /***** TABLA PARA AGREGAR PRODUCTOS DE PROMOCION *****/

    $("#tb_agregar").DataTable({
        "paging": false,
        "info": false,
        sDom: 'lrtip', // ocultar busquedad rapida
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "visible": false, "targets": [0, 3, 7] }
        ]
    });

    var tb_promo_agregar = $("#tb_agregar").DataTable();
    tb_promo_agregar.columns.adjust().draw();

    /******** TABLA PARA SELECCIONAR ********/

    $("#tb_seleccionar").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "10%", "targets": [0] },
            { "width": "80%", "targets": [1] },
            { "width": "10%", "targets": [2] },
            { "visible": false, "targets": [3] }
        ]
    });

    var tb_seleccionar = $("#tb_seleccionar").DataTable();
    tb_seleccionar.columns.adjust().draw();





    //*** TABLA DE PRODUCTOS PROMOCIONADOS - VER  ***//

    $("#detalle_tb_agregar").DataTable({
        "paging": false,
        "info": false,
        sDom: 'lrtip',
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "visible": false, "targets": [0, 3] }
        ]

    });

    var tb_dt_prom = $("#detalle_tb_agregar").DataTable();
    tb_dt_prom.columns.adjust().draw();






    //limpiar campos de modal 


    function limpiarCampos() {
        $("#txt_nombre_promo_reg").val("");
        $("#id_codPromocion").val("");
        $("#txt_fech_inicio_reg").val("");
        $("#txt_fecha_final_reg").val("");
        $("#txt_hora_inicio_reg").val("");
        $("#txt_hora_fin_reg").val("");
        $("#radio_activar_si_reg").attr("checked", true);
        $("#radio_activar_no_reg").attr("checked", false);
        $("#nonProdPrincipal").val("");
        $("#id_codProducto_reg").val("");
        $("#id_unidadBase").find('option').remove().end().append('<option value="0">Soles</option>').val(0);

        $("#txt-cant").val("");


        tb_promo_agregar.clear().draw();

        $("#radio_contado_reg").attr('checked', true)
        $("#radio_credito_reg").attr('checked', false)
        $("#cbo_vendedor_reg").val("0")
        $("#cbo_cliente_reg").val("0")

        $("#txt_uso_promo_reg").val("");
        $("#txt_uso_promo_vendedor_reg").val("");
        $("#txt_uso_promo_cliente_reg").val("");


        $("#uso_vendedor").hide();
        $("#uso_cliente").hide();


    }



    $("#buttonModal").click(function () {
        limpiarCampos()
    })






    //========== BUSCAR PROMOCIONES ==========//

    btnBuscar.click(EnviarDatos);

    function EnviarDatos() {
        //DESCRIPCION : Funcion para enviar los datos de los filtros.

        var codfechaInicial = fechaInicial.val();
        var codfechaFinal = fechaFinal.val();
        var codProveedor = proveedor.find("option:selected").val();
        var codTipoVenta = tipoVenta.find("option:selected").val();
        var codProducto = producto_principal.find("option:selected").val();
        var codVendedor = vendedor.find("option:selected").val();
        var codCliente = cliente.find("option:selected").val();
        let estado = document.querySelector('#estado').value

        console.log(codfechaInicial, codfechaFinal, codProveedor, codProducto, codTipoVenta, codVendedor, codCliente, estado)

        if (codfechaInicial > codfechaFinal) {
            swal({
                icon: "info",
                title: "Fecha inicial debe ser menor a la final"
            })
                .then(resultado => {
                    fechaInicial.focus();
                });
        }
        else if (codfechaInicial == "" && codfechaFinal == "" && codProveedor == 0 && codTipoVenta == 0 && codProducto == 0 && codVendedor == 0 && codCliente == 0 && estado == 0) {
            swal({
                icon: "info",
                title: "Para inicar una busqueda, debe seleccionar un filtro"
            })
        } else {
            var json = JSON.stringify({
                codfechaI: codfechaInicial,
                codfechaF: codfechaFinal,
                codProveedor: codProveedor,
                codTipoVenta: codTipoVenta,
                codProducto: codProducto != 0 ? codProducto : 0,
                codVendedor: codVendedor,
                codCliente: codCliente,
                codEstado: estado
            });
            console.log(json)
            ListarPromociones(json);
        }
    }


    function ListarPromociones(datos) {
        //DESCRIPCION : Funcion que me trae la lista de preventas
        $.ajax({
            type: "POST",
            url: "frmMantProm.aspx/ListarPromociones",
            data: datos,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                console.log(data.d.length)
                if (data.d.length == 0) {
                    swal({
                        icon: "info",
                        title: "No se encontro ninguna promoción con los parámetros seleccionados"
                    })
                        .then(resultado => {
                            fechaInicial.focus();
                        });
                } else {
                    tb_promo.clear().draw();
                    llenarTablaPromociones(data.d);
                }



            }
        });
    }





    //========= listar datos de promociones =============//

    async function init() {
        //DESCRIPCION : Funcion que me trae la lista de preventas

        var params = JSON.stringify({
            codfechaI: '',
            codfechaF: '',
            codProveedor: 0,
            codTipoVenta: 0,
            codProducto: 0,
            codVendedor: 0,
            codCliente: 0,
            codEstado: 0
        });
        console.log(params)

        let data = await fetch('frmMantProm.aspx/ListarPromociones', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: params
        })

        try {

            if (data.ok) {

                let response = await data.json()

                llenarTablaPromociones(response.d)
            }


        } catch (e) {
            console.log(e)
        }

    }

    function llenarTablaPromociones(data) {
        //DESCRIPCION : Funcion para llenar la tabla de promociones
        for (var i = 0; i < data.length; i++) {
            var botones;
            if (data[i].codEstado === 1 || data[i].codEstado === 2) {
                botones = btnVer + btnEditar + btnEliminar;
            } else {
                botones = btnVer;
            }

            tb_promo.row.add([
                data[i].codPromocion,//correcto
                data[i].codProducto,
                data[i].producto,
                data[i].codfechaI, //correcto
                data[i].codfechaF,//correcto
                data[i].codProveedor,
                data[i].proveedor,
                botones,
                data[i].promocion,
                data[i].codhoraI,
                data[i].codhoraF,
                data[i].codEstado,
                data[i].estado,
                data[i].cantidadProd,
                data[i].codUnidadBase,
                data[i].desUnidadBase,
                data[i].tipoProm,
                data[i].detTipoProm,
                data[i].codVendAplica,
                data[i].desVendAplica,
                data[i].codClienteAplica,
                data[i].desClienetAplica,
                data[i].vecesUsarProm,
                data[i].vecesUsarPromXvend,
                data[i].vecesUsarPromXcliente
            ]).draw();

            tb_promo.search('').draw();

            if (data[i].estado === 1) {

                var trcolor = $("#tbl_body_table tr")[i];
                trcolor.setAttribute('class', 'colortr');
                var trasig = $(".colortr");
                trasig.css('background', '#77c0ff');
            }
        }
    }

    init()



    // ================ eliminarPromociones ================ //

    $('#tbl_promociones tbody').on('click', 'tr .btnDelete', async function () {

        let tr = $(this).parent().parent();
        console.log("codigo de promocion" + tb_promo.row(tr).data()[0]);

        let estado = await eliminarPromocion(tb_promo.row(tr).data()[0])

        estado ? tb_promo.row(tr).remove().draw() : alertify.error('No se pudo eliminar la promocion, intentelo nuevamente');




    });

    async function eliminarPromocion(promocion) {

        let data = await fetch('frmMantProm.aspx/EliminarPromocion', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({ codPromocion: promocion })
        })

        try {

            if (data.ok) {

                let response = await data.json();
                console.log(response)
                return true;
            }


        } catch (e) {
            console.log(e)
        }


    }




    //========== REGISTRAR PROMOCION ==========//

    //eventos de para la validacion

    txt_nombre_promo_reg.click(function () {
        $("#msj_txt_nombre").hide();
    })

    txt_hora_fin_reg.click(function () {
        $("#msj_txt_hora").hide();
    })
    txt_fecha_final_reg.click(function () {
        $("#msj_txt_fecha").hide();
    })


    $("#frm_registrar_promo").on('submit', function (e) {
        e.preventDefault();
    })


    var formularioRegistrar = $("#frm_registrar_promo").validate({

        rules: {
            promocion: { required: true },
            fechaInicial: { required: true },
            fechaFinal: { required: true },
            horaInicio: { required: true },
            horaFin: { required: true },
            nonProdPrincipal: { required: true },
            txtCant: { required: true },
            cantMaxProm: { required: true }
        },
        messages: {
            promocion: "<div style='color: red; font- weight: normal;'>Ingrese el nombre de la promocion</div>",
            fechaInicial: "<div style='color: red; font- weight: normal;'>Ingrese la fecha del inicio para la promoción</div>",
            fechaFinal: "<div style='color: red; font- weight: normal;'>Debe ingresar la fecha de finalización de la promoción</div>",
            horaInicio: "<div style='color: red; font- weight: normal;'>Debe ingresar la hora de inicio de la promoción</div>",
            horaFin: "<div style='color: red; font- weight: normal;'>Debe ingresar la hora de fin de la promoción</div>",
            nonProdPrincipal: "<div style='color: red; font- weight: normal;'>Debe seleccionar el producto prinicpal para la promoción</div>",
            txtCant: "<div style='color: red; font- weight: normal;'>Debe ingresar la cantidad mínima para acceder a la promoción</div>",
            cantMaxProm: "<div style='color: red; font- weight: normal;'>Debe ingresar cuantas veces se puede usar la promoción</div>",
        }
    })


    //funcionpara guardar y editar las promciones 

    $('#btnGuardar').click(function () {

        if (formularioRegistrar.form()) {

            //validar formulario


            var radio_tipo_pago = $("input:radio[name=promocionAplicaReg]:checked");
            var activarProm = $("input:radio[name=activarProm]:checked");

            let detalle = []


            if (txt_fech_inicio_reg.val() > txt_fecha_final_reg.val()) {
                mensaje.innerHTML = "La fecha inical debe ser menor que la fecha de finalizacion de la promocion"
                mensaje.classList.add('text-warning')
                $("#msj_txt_fecha").append(mensaje)
                $("#msj_txt_fecha").show();

            } else if (txt_hora_inicio_reg.val() > txt_hora_fin_reg.val()) {
                mensaje.innerHTML = "La hora inical debe ser menor que la hora de finalizacion de la promocion"
                mensaje.classList.add('text-warning')
                $("#msj_txt_hora").append(mensaje)
                $("#msj_txt_hora").show();

            } else if (cbo_cliente_reg.val() > 0 && txt_uso_promo_cliente_reg.val() == "") {
                mensaje.innerHTML = "Debe ingresar la cantidad de veces que el cliente puede usar la promoción"
                mensaje.classList.add('text-warning')
                $("#msj_txt_uso_cliente").append(mensaje)
                $("#msj_txt_uso_cliente").show();

            } else if (cbo_vendedor_reg.val() > 0 && txt_uso_promo_vendedor_reg.val() == "") {
                mensaje.innerHTML = "Debe ingresar la cantidad de veces que el vendedor puede usar la promoción"
                mensaje.classList.add('text-warning')
                $("#msj_txt_uso_cliente").append(mensaje)
                $("#msj_txt_uso_cliente").show();

            } else if (tb_promo_agregar.rows().count() == 0) {

                swal({
                    title: "",
                    text: "Debe ingresar los productos que se ofertarán en esta promoción",
                    icon: "error",
                    buttons: true,
                    dangerMode: true,
                });

            } else {

                for (let i = 0; i < tb_promo_agregar.rows().count(); i++) {
                    seleccion = {
                        'descPrdoReg': tb_promo_agregar.rows().data()[i][1],
                        'cantProductoReg': tb_promo_agregar.rows().data()[i][2],
                        'costoProdReg': tb_promo_agregar.rows().data()[i][5],
                        'codProductoReg': tb_promo_agregar.rows().data()[i][0],
                        'idUnidBaseProdReg': tb_promo_agregar.rows().data()[i][3],
                        'codDetalleFlag': tb_promo_agregar.rows().data()[i][7],
                        'codPromocion': $("#id_codPromocion").val() == "" ? 0 : $("#id_codPromocion").val()

                    }
                    console.log(seleccion);
                    detalle.push(seleccion);
                }

                let objProduct = [
                    {

                        nomPromo: txt_nombre_promo_reg.val(),
                        fechaI: txt_fech_inicio_reg.val(),
                        fechaF: txt_fecha_final_reg.val(),
                        horaI: txt_hora_inicio_reg.val(),
                        horaF: txt_hora_fin_reg.val(),
                        activoInactivo: activarProm.val(),

                        decPrdPrin: $("#nonProdPrincipal").val(),
                        codProdPrin: id_codProducto_reg.val(),
                        desCantdadSoles: $('select[name="unidadBase"] option:selected').text(),
                        monto: $('#txt-cant').val(),
                        codCantdadSoles: id_unidadBase.val(),


                        desVendedorAplica: cbo_vendedor_reg.val() > 0 ? cbo_vendedor_reg.find('option:selected').text() : 'VENDEDOR',
                        codVendedorAplica: cbo_vendedor_reg.val() != 0 ? cbo_vendedor_reg.val() : 0,

                        desClienteAplica: cbo_cliente_reg.val() > 0 ? cbo_cliente_reg.find('option:selected').text() : 'CLIENTE',
                        codClienteAplica: cbo_cliente_reg.val() != 0 ? cbo_cliente_reg.val() : 0,

                        vecesUsarProm: txt_uso_promo_reg.val() ,

                        vecesUsarPromXvendedor: cbo_vendedor_reg.val() > 0 ? txt_uso_promo_vendedor_reg.val() : 0,

                        vecesUsarPromXcliente: cbo_cliente_reg.val() > 0 ? txt_uso_promo_cliente_reg.val() : 0,

                        desContadoCredito: radio_tipo_pago.val() == 1 ? 'CONTADO' : 'CREDITO',

                        codContadoCredito: radio_tipo_pago.val(),
                        codPromocion: $("#id_codPromocion").val() == "" ? 0 : $("#id_codPromocion").val()
                    }

                ]


                console.log(detalle)

                if ($("#id_codPromocion").val() == "") {

                    fetch('frmMantProm.aspx/InsertarPromocion', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                        body: JSON.stringify({ cabecera: objProduct, detalle: detalle })
                    })
                        .then(res => res.json())
                        .then(response => {

                            console.log("flag de error" + response.d)

                            if (response.d.split('|')[1] == 1) {

                                limpiarCampos()
                                arregloDetalleProducto = []
                                tb_promo_agregar.search('').draw();
                                swal({
                                    title: "",
                                    text: response.d.split('|')[0],
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                });

                            } else if (response.d.split('|')[1] == 0) {

                                limpiarCampos()
                                arregloDetalleProducto = []
                                tb_promo_agregar.search('').draw();
                                swal({
                                    title: "",
                                    text: response.d.split('|')[0],
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                });

                            } else {
                                swal({
                                    title: "",
                                    text: "Sucedio un error al intentar guardar la promocion",
                                    icon: "error",
                                    buttons: true,
                                    dangerMode: true,
                                });
                                ;
                            }


                        })
                        .catch(e => {
                            //arregloDetalleProducto = []
                            console.log(e)

                        }
                        )


                } else {

                    //actualizar la promocion                   

                    fetch('frmMantProm.aspx/ActualizarPromocion', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                        body: JSON.stringify({ cabecera: objProduct, detalle: detalle })
                    })
                        .then(res => res.json())
                        .then(response => {
                            console.log(response.d)
                            if (response.d.split('|')[1] == 0) {
                                limpiarCampos()
                                arregloDetalleProducto = []
                                tb_promo_agregar.search('').draw();
                                swal({
                                    title: "",
                                    text: "SE GUARDO LA PROMOCION DE MANERA EXITOSA",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                });

                               

                            } else {
                                swal({
                                    title: "",
                                    text: "SE PRODUJO UN ERROR AL INTENTAR ACTUALIZAR LA PROMOCIÓN",
                                    icon: "error",
                                    buttons: true,
                                    dangerMode: true,
                                });
                            }



                        })
                        .catch(e => {
                            arregloDetalleProducto.length = 0

                            console.log(e)

                        }
                        )
                }




            }


        }



    })


    //================= Ver detalle de promocion


    $('#tbl_promociones tbody ').on('click', 'tr .btnVer', function () {


        let tr = $(this).parent().parent();
        let codigoProm = tb_promo.row(tr).data()[0];
        tb_dt_prom.clear().draw();
        //busqueda de detalle de promocion (productos promocionados)

        buscarDetallePromocion(codigoProm, 2)
    })





    //============= EDITAR PROMOCION ========//

    $('#tbl_promociones tbody ').on('click', 'tr .btnEditar', function () {

        let tr = $(this).parent().parent();
        let codigoProm = tb_promo.row(tr).data()[0];

        //busqueda de detalle de promocion (productos promocionados)

        buscarDetallePromocion(codigoProm, 1)


        //// se agrega los valores de la promocion al formulario

        $("#id_codPromocion").val(tb_promo.row(tr).data()[0]);     //data[i].codPromocion,// codPromocion[0]
        id_codProducto_reg.val(tb_promo.row(tr).data()[1]); //data[i].codProducto,// codProducto[1]
        id_Producto_reg.val(tb_promo.row(tr).data()[2]); //data[i].producto,// producto[2]

        //fecha inicial
        let y = tb_promo.row(tr).data()[3].split('/')[2]
        let m = tb_promo.row(tr).data()[3].split('/')[1]
        let d = tb_promo.row(tr).data()[3].split('/')[0]

        txt_fech_inicio_reg.val(y + "-" + m + "-" + d);    //data[i].codfechaI, // codfechaI[3]       

        //fecha final

        y = tb_promo.row(tr).data()[4].split('/')[2]
        m = tb_promo.row(tr).data()[4].split('/')[1]
        d = tb_promo.row(tr).data()[4].split('/')[0]

        txt_fecha_final_reg.val(y + "-" + m + "-" + d);


        txt_nombre_promo_reg.val(tb_promo.row(tr).data()[8]);    // data[i].promocion,// promocion[8]
        txt_hora_inicio_reg.val(tb_promo.row(tr).data()[9]);    //data[i].codhoraI,// codhoraI[9]
        txt_hora_fin_reg.val(tb_promo.row(tr).data()[10]);  //data[i].codhoraF,// codhoraF[10]

        (tb_promo.row(tr).data()[11] == 1) ? radio_activar_si_reg.attr('checked', true) : radio_activar_no_reg.attr('checked', true); //data[i].codEstado,// codEstado[11]

        cantidadPrincipal.val(tb_promo.row(tr).data()[13]); //data[i].cantidadProd,// cantidadProd[13]


        id_unidadBase.prepend("<option value=" + tb_promo.row(tr).data()[14] + " >" + tb_promo.row(tr).data()[15] + "</option>");  // unidad base
        id_unidadBase.val(tb_promo.row(tr).data()[14]); // unidad base

        console.log(tb_promo.row(tr).data()[14]);
        (tb_promo.row(tr).data()[16] == 1) ? radio_contado_reg.attr('checked', true) : radio_contado_reg.removeAttr('checked');
        (tb_promo.row(tr).data()[16] == 2) ? radio_credito_reg.attr('checked', true) : radio_credito_reg.removeAttr('checked');

        txt_uso_promo_reg.val(tb_promo.row(tr).data()[22]);

        if (tb_promo.row(tr).data()[20] > 0) {

            $("#cbo_cliente_reg").val(tb_promo.row(tr).data()[20]);
            $('#uso_cliente').show()
            txt_uso_promo_cliente_reg.val(tb_promo.row(tr).data()[24]).prop('readonly', false);
        } else {

            $('#edit_usoCliente').hide()
            txt_uso_promo_cliente_reg.val("").prop('readonly', true);
        }

        if (tb_promo.row(tr).data()[18] > 0) {
            $("#cbo_vendedor_reg ").val(tb_promo.row(tr).data()[18]);
            $('#uso_vendedor').show()
            txt_uso_promo_vendedor_reg.val(tb_promo.row(tr).data()[23]).prop('readonly', false);
        } else {

            $('#uso_vendedor').hide()
            txt_uso_promo_vendedor_reg.val("").prop('readonly', true);
        }



    })

    //========================================


    //* detalle de promocion *//

    async function buscarDetallePromocion(codigoProm, operacion) {



        let data = await fetch('frmMantProm.aspx/ListarDetalle', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({ codPromocion: codigoProm })
        })

        try {

            if (data.ok) {

                let response = await data.json()
                tb_promo_agregar.clear().draw();
                tb_dt_prom.clear().draw();

                Object.values(response.d).map(({ codProductoProm, descripcionProd, cantidad, idPresentacion, presentacion, precio }) => {


                    if (operacion == 1) {
                        tb_promo_agregar.row.add([
                            codProductoProm,
                            descripcionProd,//correcto
                            cantidad,
                            idPresentacion,
                            presentacion,
                            precio,
                            btnEliminar,
                            '0'
                        ]).draw()

                        tb_promo_agregar.search('').draw()

                    } else {

                        tb_dt_prom.row.add([
                            codProductoProm,
                            descripcionProd,//correcto
                            cantidad,
                            idPresentacion,
                            presentacion,
                            precio,
                            btnEliminar,
                            '0'
                        ]).draw()
                        tb_dt_prom.search('').draw()

                    }
                })


            }


        } catch (e) {
            console.log(e)
        }

    }


    /* listado de productos */

    $("#id_btn_seleccionar_prod").click(function () {
        ajax_seleccionar(1);
    });

    btn_buscar_prod_recibir.click(function () {
        ajax_seleccionar(2);
    });

    function ajax_seleccionar(flag) {
        flag_seleccion = flag;
        $.ajax({
            type: "POST",
            url: "frmMantProm.aspx/ListarSeleccion",
            data: "{ 'flag' : '" + flag_seleccion + "' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                tb_seleccionar.clear().draw();
                llenarTablaSeleccionar(data.d);

            },
        });
    }



    function llenarTablaSeleccionar(data) {
        var botones = '<button class="btn_seleccionar" type="button" >Seleccionar</button>';
        for (var i = 0; i < data.length; i++) {
            tb_seleccionar.row.add([
                data[i].codigo,
                data[i].descripcion,
                botones,
                data[i].datos
            ]).draw();
        }
        tb_seleccionar.search('').draw();
    }

    $('#tb_seleccionar tbody').on('click', 'tr .btn_seleccionar', function () {

        var tr = $(this).parent();
        var data = tb_seleccionar.row(tr).data();
        console.log(arregloDetalleProducto)


        if (arregloDetalleProducto.length > 0) {

            const datos = arregloDetalleProducto.filter(el => el.codProductoReg == data[0])

            console.log(datos)

            if (datos.length > 0) {
                mensaje.innerHTML = "El producto ya se encuentra agregado"
                containermMensaje.classList.add("d-block");
                containermMensaje.append(mensaje)
            } else {
                llenar_datos(flag_seleccion, data);
                mensaje.innerHTML = ""
                containermMensaje.classList.add("d-none");
                $("#id_seleccionar").modal('hide');
            }



        } else {

            llenar_datos(flag_seleccion, data);
            $("#id_seleccionar").modal('hide');
        }


    });

    //elimnar producto promocional - registar

    $('#tb_agregar tbody').on('click', 'tr .btnDelete', async function () {
        var tr = $(this).parent();
        var data = tb_promo_agregar.row(tr).data();

        console.log(data)

        if (data[7] == "0") {
            let res = await deleteProductEditar($("#id_codPromocion").val(), data[0])
            console.log(res)

            if (res == 0) {
                tb_promo_agregar.row(tr).remove().draw()
                tb_promo_agregar.search('').draw();

            }

        } else {
            tb_promo_agregar.row(tr).remove().draw()
            tb_promo_agregar.search('').draw();

        }

        const datos = arregloDetalleProducto.filter(el => el.codProductoReg != data[0])
        arregloDetalleProducto = datos
        console.log(arregloDetalleProducto)
    });

    function llenar_datos(flag, data) {

        /* Split datos */
        var datos_adicionales = data[3].split('|');
        console.log(data)
        /* Buscar producto principal de la promocion */
        if (flag === 1) {
            /* Limpiar Datos */
            id_Producto_reg.val("");
            id_codProducto_reg.val("");
            id_unidadBase.find('option').remove().end().append('<option value="0">Soles</option>').val(0);

            /* Actualizar Datos */
            id_Producto_reg.val(data[1]);
            id_codProducto_reg.val(data[0]);
            id_unidadBase.append("<option value='" + datos_adicionales[0] + "' >" + datos_adicionales[1] + "</option>");
        }

        /* Buacar producto que recibira */
        if (flag === 2) {
            /* Limpiar Datos */
            id_Producto_promo.val("");
            id_codProducto_promo.val("");
            id_unidadBase_promo.find('option').remove().end().append('<option value="0">-Seleccionar-</option>').val(0);

            /* Actualizar Datos */
            id_Producto_promo.val(data[1]);
            id_codProducto_promo.val(data[0]);
            id_unidadBase_promo.append("<option value='" + datos_adicionales[0] + "' >" + datos_adicionales[1] + "</option>");
        }

        console.log(id_codProducto_reg.val())

    }


    /* Eventos de Combo Vendedores - Registrar */

    $('#cbo_vendedor_reg').change(function () {

        if ($('#cbo_vendedor_reg').val() > 0) {
            txt_uso_promo_vendedor_reg.prop('readonly', false);
            $("#uso_vendedor").show(500);
        } else {
            txt_uso_promo_vendedor_reg.prop('readonly', true);
            $("#uso_vendedor").hide(500);
        }
    });

    /* Eventos de Combo Clientes - Registrar */

    $('#cbo_cliente_reg').change(function () {
        if ($('#cbo_cliente_reg').val() > 0) {
            txt_uso_promo_cliente_reg.val("").prop('readonly', false);

            $("#uso_cliente").show(500);
        } else {
            txt_uso_promo_cliente_reg.val("").prop('readonly', true);
            $("#uso_cliente").hide(500);
        }
    });

    /* pasa productos promocionados a la tabla */

    var formularioProductoRegistrar = $("#formProdPromocional").validate({

        rules: {
            txt_cantidad: { required: true },
            txt_dinero: { required: true },
            unidadBase_promo: { required: true }
        },
        messages: {
            txt_cantidad: "<div style='color: red; font-weight: normal;'>Ingrese la cantidad a recibir</div>",
            txt_dinero: "<div style='color: red; font-weight: normal;'>Ingrese el costo del producto</div>",
            unidadBase_promo: "<div style='color: red; font-weight: normal;'>Debe selecciona la unidad base</div>"
        }
    })


    document.querySelector('#btn_agregar_prod_promo').addEventListener('click', pasarDatosTabla)

    function pasarDatosTabla() {

        if (formularioProductoRegistrar.form()) {

            if ($("#id_codProducto_promo").val() == "") {
                mensaje.innerHTML = "Debe buscar un producto para la promoción"
                $("#mensajeRegistrarProd").append(mensaje)
            } else {

                tb_promo_agregar.row.add([
                    id_codProducto_promo.val(),//correcto
                    id_Producto_promo.val(),
                    txt_cantidad.val(),
                    id_unidadBase_promo.val(),
                    $('select[name="unidadBase_promo"] option:selected').text(),
                    txt_dinero.val(),
                    btnEliminar,
                    '1'
                ]).draw()


                let objdetalle = new Object()

                objdetalle.descPrdoReg = id_Producto_promo.val()
                objdetalle.codProductoReg = id_codProducto_promo.val()
                objdetalle.cantProductoReg = txt_cantidad.val()
                objdetalle.costoProdReg = txt_dinero.val()
                objdetalle.idUnidBaseProdReg = id_unidadBase_promo.val()

                arregloDetalleProducto.push(objdetalle)

                id_unidadBase_promo.find('option').remove().end().append('<option value="0">-Seleccionar-</option>').val(0);

                id_Producto_promo.val("")
                txt_cantidad.val("")
                txt_dinero.val("")
            }

        }




    }



    //elimnar producto promocional - editar

    async function deleteProductEditar(cod, codProd) {

        let data = await fetch('frmMantProm.aspx/EliminarProductoActualizarPromocion', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({ cod: cod, codP: codProd })
        })

        try {

            if (data.ok) {

                let response = await data.json()

                return response.d



                const datos = arregloDetalleProducto.filter(el => el.codProductoReg != codProd)
                arregloDetalleProducto = datos
                console.log(arregloDetalleProducto)
            }


        } catch (e) {
            console.log(e)
        }


    }





});


$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});

