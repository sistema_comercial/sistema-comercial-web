﻿$(document).ready(function () {
    
        function cerrarSesionInactividad() {
            fetch('login.aspx/CerrarSesionInactividad', {
                method: 'POST',
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            })
            .then(response => response.json())
                .then(data => {
                    var res = data.d;
                    if (res == 2000) {
                        localStorage.clear();
                        swal({
                            title: "ADVERTENCIA",
                            text: "La sesion a caducado, vuelva a iniciar sesión",
                            icon: "warning"
                        }).then(resultado => {
                            location.href = 'login.aspx';
                        });
                    }
                });
        }

        function credenciales() {
            $.ajax({
                type: "POST",
                url: "login.aspx/getSession",
                contentType: 'application/json; charset=utf-8',
                error: function (xhr, ajaxOtions, thrownError) {
                    console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                },
                success: function (data) {
                    if (data.d === null) {
                        console.log('error en credenciales');
                        localStorage.setItem('error',1);
                        cerrarSesionInactividad();
                    } else {
                        let p = document.getElementById('nombreUsuario');
                        p.innerHTML = data.d[0].nombre + " - " + data.d[0].perfil + " / " + '<a href="#" class="navbar - link" id="cerrarSesion">Cerrar sesión  <span class="glyphicon glyphicon - log - out"> </span></a>';
                        document.getElementById('cerrarSesion').addEventListener('click', cerrarSesionInactividad);
                        localStorage.setItem('error', 0);
                        //obtenerModulo(data.d[0].ntraUsuario);
                    }
                }
            });
        }
        
        credenciales();
    }
);

