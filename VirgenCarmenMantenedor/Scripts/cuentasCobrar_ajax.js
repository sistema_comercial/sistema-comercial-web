﻿$(document).ready(function () {
    //Lista de Variables para modal ver
    var MVvendedor = $("#MVvendedor");
    var MVcliente = $("#MVcliente");
    var MVruta = $("#MVruta");
    var MVentrega = $("#MVentrega");
    var MVventa = $("#MVventa");
    var MVimporte = $("#MVimporte");
    var MVdocumento = $("#MVdocumento");
    var MVmoneda = $("#MVmoneda");
    var MVfechaReg = $("#MVfechaReg");
    var MVfechaPag = $("#MVfechaPag");
    var MVplazo = $("#MVplazo");
    var MVretraso = $("#MVretraso");
    var MVmodulo = $("#MVmodulo");
    var MVprefijo = $("#MVprefijo");
    //Variables para tablas
    var id_tblCuentas = $("#id_tblCuentas");
    var id_tblLetras = $("#id_tblLetras");
    var id_tblCuota = $("#id_tblCuota");

    //Variables para botones
    var btnVer = ' <button type="button" title="Ver" class="icon-eye btnEye btnDetalle" data-toggle="modal"  data-target="#modalCuenta" ></button>';
    var btnVer2 = ' <button type="button" title="Ver" class="icon-eye btnEye btnDetalle2" data-toggle="modal"  data-target="#modalCuenta" ></button>';
    // Variables adicionales
    var id_div_modulo = $("#id_div_modulo");
    var label_fehc_cuota = $("#label_fehc_cuota");
    var label_plazo = $("#label_plazo");
    var id_div_cuota = $("#id_div_cuota");
    var id_div_retraso = $("#id_div_retraso");
    

    //Convertimos la tabla preventa en dataTable y le pasamos parametros
    id_tblCuentas.DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        order: [[0, "desc"]],
        columnDefs: [
            {
                "width": "10%",
                "targets": [0]
            },
            {
                "width": "15%",
                "targets": [2,3,4,5,6,7,8]
            },
            {
                "width": "35%",
                "targets": [1]
            },
            {
                "targets": [9, 10, 11, 12, 13, 14],
                "visible": false,
                "searchable": false
            },
            {
                "className": "text-right", "targets": [5]
            },
            {
                "className": "text-center", "targets": [0,4,6,7,8]
            }
        ],
        rowCallback: function (row, data, index) {
            switch (data[6]) {
                case 0:
                    if (data[7] === 0) {
                        $(row).find('td').css({ 'color': 'orange', 'font-weight': 'bolder' });
                    } else {
                        $(row).find('td').css({ 'color': 'red', 'font-weight': 'bolder' });
                    }
                    break;
            }
        }
    });
    //creo variables despues de que el DataTable este creado en el DOM.
    var tabla = id_tblCuentas.dataTable(); //funcion jquery
    var table = id_tblCuentas.DataTable(); //funcion DataTable-libreria
    table.columns.adjust().draw();

    function limpiarModal() {
        //Limpiar campos de la modal ver
        MVvendedor.val("");
        MVcliente.val("");
        MVruta.val("");
        MVentrega.val("");
        MVventa.val("");
        MVimporte.val("");
        MVdocumento.val("");
        MVmoneda.val("");
        MVfechaReg.val("");
        MVfechaPag.val("");
        MVplazo.val("");
        MVretraso.val("");
        MVmodulo.val("");
        MVprefijo.val("");
    }

    function listarCuentas() {
        //DESCRIPCION: Obtener la lista de cuentas por cobrar pendientes de pago
        $.ajax({
            type: "POST",
            url: "frmMantCuentasCobrar.aspx/ListarCuentasCobrar",
            data: "{ codCliente : 0 } ",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table.clear().draw();
                llenarTablaCuentas(data.d);
            }
        });
    }
    listarCuentas();

    function llenarTablaCuentas(data) {
        //DECRIPCION: Llenar la tabla de cuentas pendientes
        let plazo = 0;
        let retraso = 0;
        let fechaActual = new Date();
        let fechaActual2 = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), fechaActual.getDate());
        for (let i = 0; i < data.length; i++) {
            let fechaPag = $.datepicker.parseDate('dd/mm/yy', data[i].fechaCobro);
            let dif = fechaPag - fechaActual2;
            let dias = Math.floor(dif / (1000 * 60 * 60 * 24));
            if (dias >= 0) {
                plazo = dias + " dias";
                retraso = 0;
            } else {
                plazo = 0;
                retraso = (-1)*dias + " dias";
            }

            tabla.fnAddData([
                data[i].codOperacion,    //0
                data[i].nombCli,
                data[i].desModulo,
                data[i].desPrefijo,
                data[i].fechaCobro,
                data[i].importe.toFixed(2),  //5
                plazo,
                retraso,
                btnVer,
                data[i].nombVend,
                data[i].nomRuta,    //10
                data[i].direccion,
                data[i].desDocum,  
                data[i].desMoneda,
                data[i].fechaTransaccion
            ]);
        }
    }
    $('body').on('click', '.btnDetalle', function () {
        limpiarModal();
        id_div_modulo.show();
        id_div_cuota.hide();
        id_div_retraso.show();
        label_fehc_cuota.html("Fecha Pago");
        label_plazo.html("Plazo");
        let tr = $(this).parent().parent();
        MVvendedor.val(table.row(tr).data()[9]);
        MVcliente.val(table.row(tr).data()[1]);
        MVruta.val(table.row(tr).data()[10]);
        MVentrega.val(table.row(tr).data()[11]);
        MVventa.val(table.row(tr).data()[0]);
        MVimporte.val(table.row(tr).data()[5]);
        MVdocumento.val(table.row(tr).data()[12]);
        MVmoneda.val(table.row(tr).data()[13]);
        MVfechaReg.val(table.row(tr).data()[14]);
        MVfechaPag.val(table.row(tr).data()[4]);
        MVplazo.val(table.row(tr).data()[6]);
        MVretraso.val(table.row(tr).data()[7]);
        MVmodulo.val(table.row(tr).data()[2]);
        MVprefijo.val(table.row(tr).data()[3]);
    });

    //Convertimos la tabla preventa en dataTable y le pasamos parametros
    id_tblLetras.DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        order: [[0, "desc"]],
        columnDefs: [
            {
                "width": "10%",
                "targets": [0]
            },
            {
                "width": "15%",
                "targets": [2, 3, 4, 5]
            },
            {
                "width": "35%",
                "targets": [1]
            },
            {
                "targets": [6,7, 8, 9, 10, 11,12,13],
                "visible": false,
                "searchable": false
            },
            {
                "className": "text-right", "targets": [3]
            },
            {
                "className": "text-center", "targets": [0,2,4,5]
            }
        ],
        rowCallback: function (row, data, index) {
            switch (data[13]) {
                case 0:
                    //if (data[7] === 0) {
                        $(row).find('td').css({ 'color': 'red', 'font-weight': 'bolder' });
                    //} else {
                    //    $(row).find('td').css({ 'color': 'red', 'font-weight': 'bolder' });
                    //}
                    break;
            }
        }
    });
    //creo variables despues de que el DataTable este creado en el DOM.
    var tabla2 = id_tblLetras.dataTable(); //funcion jquery
    var table2 = id_tblLetras.DataTable(); //funcion DataTable-libreria
    table2.columns.adjust().draw();

    function listarLetras() {
        //DESCRIPCION: Obtener la lista de letras por cobrar pendientes de pago
        $.ajax({
            type: "POST",
            url: "frmMantCuentasCobrar.aspx/ListarLetrasCobrar",
            data: "{ codCliente : 0 }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table2.clear().draw();
                llenarTablaLetras(data.d);
            }
        });
    }
    listarLetras();

    function llenarTablaLetras(data) {
        //DECRIPCION: Llenar la tabla de letras pendientes
        let fechaActual = new Date();
        let fechaActual2 = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), fechaActual.getDate());
        //console.log("1");
        for (let i = 0; i < data.length; i++) {
            let cont = 0;
            let codPrestamo = data[i].codPrestamo;

            const p = new Promise((resolve, reject) => {
                $.ajax({
                    type: "POST",
                    url: "frmMantCuentasCobrar.aspx/ListarCuotas",
                    data: "{ codPrestamo : " + codPrestamo + "}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        let datos = data.d;
                        for (let i = 0; i < datos.length; i++) {
                            let fechaPag = $.datepicker.parseDate('dd/mm/yy', datos[i].fechaPago);
                            let dif = fechaPag - fechaActual2;
                            let dias = Math.floor(dif / (1000 * 60 * 60 * 24));
                            if (dias >= 0) {
                                cont = cont + 1;
                            }
                        }
                        //console.log(codPrestamo+ " : "+ cont + " ---3");
                        resolve(cont);
                    }
                });
            });
            p.then(res => {
                //console.log("--------" + res);
                //console.log(codPrestamo + " : " + cont + " ---4");

                tabla2.fnAddData([
                    data[i].codOperacion,    //0
                    data[i].nombCli,
                    data[i].fechaTransaccion,
                    data[i].importe.toFixed(2),
                    data[i].plazo,
                    btnVer2,        //5
                    data[i].nombVend,
                    data[i].nomRuta,
                    data[i].direccion,
                    data[i].desDocum,
                    data[i].desMoneda,  //10
                    data[i].nroCuotas,
                    data[i].codPrestamo,
                    cont
                ]);
                //console.log("final");
            });
        }
    }
    $('body').on('click', '.btnDetalle2', function () {
        limpiarModal();
        id_div_modulo.hide();
        id_div_cuota.show();
        id_div_retraso.hide();
        label_fehc_cuota.html("Cuotas");
        label_plazo.html("N° Prestamo");
        
        let tr2 = $(this).parent().parent();
        let codPrestamo = table2.row(tr2).data()[12];

        MVvendedor.val(table2.row(tr2).data()[6]);
        MVcliente.val(table2.row(tr2).data()[1]);
        MVruta.val(table2.row(tr2).data()[7]);
        MVentrega.val(table2.row(tr2).data()[8]);
        MVventa.val(table2.row(tr2).data()[0]);
        MVimporte.val(table2.row(tr2).data()[3]);
        MVdocumento.val(table2.row(tr2).data()[9]);
        MVmoneda.val(table2.row(tr2).data()[10]);
        MVfechaReg.val(table2.row(tr2).data()[2]);
        MVplazo.val(table2.row(tr2).data()[12]);     //Aca se mostrara el numero de prestamo
        MVfechaPag.val(table2.row(tr2).data()[11]); //Aca se mostrara el numero de cuotas
        ////MVretraso
        obtenerCuotas(codPrestamo);

    });

    //Convertimos la tabla preventa en dataTable y le pasamos parametros
    id_tblCuota.DataTable({
        bFilter: false,
        bInfo: false,
        bPaginate:false,
        order: [[0, "asc"]],
        columnDefs: [
            {
                "className": "text-right", "targets": [2]
            },
            {
                "className": "text-center", "targets": [0, 1, 3, 4]
            }
        ]
    });
    //creo variables despues de que el DataTable este creado en el DOM.
    var tabla3 = id_tblCuota.dataTable(); //funcion jquery
    var table3 = id_tblCuota.DataTable(); //funcion DataTable-libreria
    table3.columns.adjust().draw();

    function obtenerCuotas(codPrestamo) {
        //DESCRIPCION: Obtener la lista de cuotas de un prestamo
        $.ajax({
            type: "POST",
            url: "frmMantCuentasCobrar.aspx/ListarCuotas",
            data: "{ codPrestamo : " + codPrestamo + "}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table3.clear().draw();
                llenarTablaCuota(data.d);
            }
        });
    }

    function llenarTablaCuota(data) {
        //DECRIPCION: Llenar la tabla de cuotas del prestamo
        let plazo = 0;
        let retraso = 0;
        
        let fechaActual = new Date();
        let fechaActual2 = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), fechaActual.getDate());

        for (let i = 0; i < data.length; i++) {
            let fechaPag = $.datepicker.parseDate('dd/mm/yy', data[i].fechaPago);
            let dif = fechaPag - fechaActual2;
            let dias = Math.floor(dif / (1000 * 60 * 60 * 24));
            if (dias >= 0) {
                plazo = dias + " dias";
                retraso = 0;
            } else {
                plazo = 0;
                retraso = (-1) * dias + " dias";
            }

            tabla3.fnAddData([
                data[i].nroCuota,
                data[i].fechaPago,
                data[i].importe.toFixed(2),
                plazo,
                retraso,
                data[i].estCuota
            ]);
        }
    }
    

});