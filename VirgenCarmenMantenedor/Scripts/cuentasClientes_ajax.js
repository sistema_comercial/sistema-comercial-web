﻿$(document).ready(function () {
    //Lista de Variables para filtros
    var clienteInput = $("#id_cliente");
    var clienteCod = $("#id_codCliente");
    //Lista de Variables para modal ver
    var MVvendedor = $("#MVvendedor");
    var MVcliente = $("#MVcliente");
    var MVruta = $("#MVruta");
    var MVentrega = $("#MVentrega");
    var MVventa = $("#MVventa");
    var MVimporte = $("#MVimporte");
    var MVdocumento = $("#MVdocumento");
    var MVmoneda = $("#MVmoneda");
    var MVfechaReg = $("#MVfechaReg");
    var MVfechaPag = $("#MVfechaPag");
    var MVplazo = $("#MVplazo");
    var MVretraso = $("#MVretraso");
    var MVmodulo = $("#MVmodulo");
    var MVprefijo = $("#MVprefijo");
    //Variables para tablas
    var id_tblCuentas = $("#id_tblCuentas");
    var id_tblCuota = $("#id_tblCuota");

    //Variables para botones
    var btnBuscar = $("#btnBuscar");
    var btnVer = ' <button type="button" title="Ver" class="icon-eye btnEye btnDetalle" data-toggle="modal"  data-target="#modalCuenta" ></button>';
    var btnVer2 = ' <button type="button" title="Ver" class="icon-eye btnEye btnDetalle2" data-toggle="modal"  data-target="#modalCuenta" ></button>';
    // Variables adicionales
    var id_div_modulo = $("#id_div_modulo");
    var label_fehc_cuota = $("#label_fehc_cuota");
    var label_plazo = $("#label_plazo");
    var id_div_cuota = $("#id_div_cuota");
    var id_div_retraso = $("#id_div_retraso");

    var objCuenta = {};
    var objLetra = {};

    clienteCod.val('0');


    //Convertimos la tabla preventa en dataTable y le pasamos parametros
    id_tblCuentas.DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        order: [[0, "desc"]],
        columnDefs: [
            {
                "width": "10%",
                "targets": [0]
            },
            {
                "width": "15%",
                "targets": [2, 3, 4, 5, 6]
            },
            {
                "width": "35%",
                "targets": [1]
            },
            {
                "targets": [7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
                "visible": false,
                "searchable": false
            },
            {
                "className": "text-right", "targets": [5]
            },
            {
                "className": "text-center", "targets": [0, 4, 6]
            }
        ]
    });
    //creo variables despues de que el DataTable este creado en el DOM.
    var tabla = id_tblCuentas.dataTable(); //funcion jquery
    var table = id_tblCuentas.DataTable(); //funcion DataTable-libreria
    table.columns.adjust().draw();

    function limpiarModal() {
        //Limpiar campos de la modal ver
        MVvendedor.val("");
        MVcliente.val("");
        MVruta.val("");
        MVentrega.val("");
        MVventa.val("");
        MVimporte.val("");
        MVdocumento.val("");
        MVmoneda.val("");
        MVfechaReg.val("");
        MVfechaPag.val("");
        MVplazo.val("");
        MVretraso.val("");
        MVmodulo.val("");
        MVprefijo.val("");
    }

    function obtenerCliente() {
        btnBuscar.click(function () {
            let codCliente = clienteCod.val();
            if (codCliente === '0') {
                swal("", "Ingrese el Nombre o Documento del Cliente", "error");
                //table.clear().draw();
            } else {
                listarCuentas(codCliente);
            }
        });
    }
    obtenerCliente();

    function listarCuentas(codCliente) {
        //DESCRIPCION: Obtener la lista de cuentas por cobrar pendientes de pago
        $.ajax({
            type: "POST",
            url: "frmMantCuentasCobrar.aspx/ListarCuentasCobrar",
            data: "{ codCliente : " + codCliente + " } ",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table.clear().draw();
                llenarTablaCuentas(data.d, codCliente);
            }
        });
    }

    function llenarTablaCuentas(data, codCliente) {
        //DECRIPCION: Llenar la tabla de cuentas pendientes
        objCuenta = data;
        console.log(objCuenta);
        let plazo = 0;
        let retraso = 0;
        let fechaActual = new Date();
        let fechaActual2 = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), fechaActual.getDate());

        for (let i = 0; i < data.length; i++) {
            let fechaPag = $.datepicker.parseDate('dd/mm/yy', data[i].fechaCobro);
            let dif = fechaPag - fechaActual2;
            let dias = Math.floor(dif / (1000 * 60 * 60 * 24));
            if (dias >= 0) {
                plazo = dias + " dias";
                retraso = 0;
            } else {
                plazo = 0;
                retraso = (-1) * dias + " dias";
            }

            tabla.fnAddData([
                data[i].codOperacion,    //0
                data[i].nombVend,
                data[i].desModulo,
                data[i].desPrefijo,
                data[i].fechaTransaccion,
                data[i].importe.toFixed(2),  //5
                btnVer,
                plazo,
                retraso,
                data[i].nombCli,
                data[i].nomRuta,   //10
                data[i].direccion,
                data[i].desDocum,
                data[i].desMoneda,
                data[i].fechaCobro,
                data[i].nroCuotas,
                data[i].codPrestamo,
            ]);
        }
        listarLetras(codCliente);
    }
    $('body').on('click', '.btnDetalle', function () {
        limpiarModal();
        id_div_modulo.show();
        id_div_cuota.hide();
        id_div_retraso.show();
        label_fehc_cuota.html("Fecha Pago");
        label_plazo.html("Plazo Rest");
        let tr = $(this).parent().parent();
        MVvendedor.val(table.row(tr).data()[1]);
        MVcliente.val(table.row(tr).data()[9]);
        MVruta.val(table.row(tr).data()[10]);
        MVentrega.val(table.row(tr).data()[11]);
        MVventa.val(table.row(tr).data()[0]);
        MVimporte.val(table.row(tr).data()[5]);
        MVdocumento.val(table.row(tr).data()[12]);
        MVmoneda.val(table.row(tr).data()[13]);
        MVfechaReg.val(table.row(tr).data()[4]);
        MVfechaPag.val(table.row(tr).data()[14]);
        MVplazo.val(table.row(tr).data()[7]);
        MVretraso.val(table.row(tr).data()[8]);
        MVmodulo.val(table.row(tr).data()[2]);
        MVprefijo.val(table.row(tr).data()[3]);
    });

    function listarLetras(codCliente) {
        //DESCRIPCION: Obtener la lista de letras por cobrar pendientes de pago
        $.ajax({
            type: "POST",
            url: "frmMantCuentasCobrar.aspx/ListarLetrasCobrar",
            data: "{ codCliente : " + codCliente + " } ",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                llenarTablaCuentasLetras(data.d);
            }
        });
    }

    function llenarTablaCuentasLetras(data) {
        //DECRIPCION: Agregar a la tabla de letras pendientes
        objLetra = data;
        console.log(objLetra);
        console.log(typeof objLetra);
        let retraso = 0;
        for (let i = 0; i < data.length; i++) {
            tabla.fnAddData([
                data[i].codOperacion,    //0
                data[i].nombVend,
                data[i].desModulo,
                data[i].desPrefijo,
                data[i].fechaTransaccion,
                data[i].importe.toFixed(2),     //5
                btnVer2,
                data[i].plazo,
                retraso,
                data[i].nombCli,
                data[i].nomRuta,    //10
                data[i].direccion,
                data[i].desDocum,
                data[i].desMoneda,
                data[i].fechaCobro,
                data[i].nroCuotas,  //15
                data[i].codPrestamo
            ]);
        }
    }
    $('body').on('click', '.btnDetalle2', function () {
        limpiarModal();
        id_div_modulo.hide();
        id_div_cuota.show();
        id_div_retraso.hide();
        label_fehc_cuota.html("Cuotas");
        label_plazo.html("N° Prestamo");

        let tr2 = $(this).parent().parent();
        let codPrestamo = table.row(tr2).data()[16];

        MVvendedor.val(table.row(tr2).data()[1]);
        MVcliente.val(table.row(tr2).data()[9]);
        MVruta.val(table.row(tr2).data()[10]);
        MVentrega.val(table.row(tr2).data()[11]);
        MVventa.val(table.row(tr2).data()[0]);
        MVimporte.val(table.row(tr2).data()[5]);
        MVdocumento.val(table.row(tr2).data()[12]);
        MVmoneda.val(table.row(tr2).data()[13]);
        MVfechaReg.val(table.row(tr2).data()[4]);
        MVplazo.val(table.row(tr2).data()[16]);     //Aca se mostrara el numero de prestamo
        MVfechaPag.val(table.row(tr2).data()[15]); //Aca se mostrara el numero de cuotas
        ////MVretraso
        obtenerCuotas(codPrestamo);
    });

    //Convertimos la tabla preventa en dataTable y le pasamos parametros
    id_tblCuota.DataTable({
        bFilter: false,
        bInfo: false,
        bPaginate: false,
        order: [[0, "asc"]],
        columnDefs: [
            {
                "className": "text-right", "targets": [2]
            },
            {
                "className": "text-center", "targets": [0, 1, 3, 4]
            }
        ]
    });
    //creo variables despues de que el DataTable este creado en el DOM.
    var tabla3 = id_tblCuota.dataTable(); //funcion jquery
    var table3 = id_tblCuota.DataTable(); //funcion DataTable-libreria
    table3.columns.adjust().draw();

    function obtenerCuotas(codPrestamo) {
        //DESCRIPCION: Obtener la lista de cuotas de un prestamo
        $.ajax({
            type: "POST",
            url: "frmMantCuentasCobrar.aspx/ListarCuotas",
            data: "{ codPrestamo : " + codPrestamo + "}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table3.clear().draw();
                llenarTablaCuota(data.d);
            }
        });
    }

    function llenarTablaCuota(data) {
        //DECRIPCION: Llenar la tabla de cuotas del prestamo
        let plazo = 0;
        let retraso = 0;
        let fechaActual = new Date();
        let fechaActual2 = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), fechaActual.getDate());

        for (let i = 0; i < data.length; i++) {
            let fechaPag = $.datepicker.parseDate('dd/mm/yy', data[i].fechaPago);
            let dif = fechaPag - fechaActual2;
            let dias = Math.floor(dif / (1000 * 60 * 60 * 24));
            if (dias >= 0) {
                plazo = dias + " dias";
                retraso = 0;
            } else {
                plazo = 0;
                retraso = (-1) * dias + " dias";
            }

            tabla3.fnAddData([
                data[i].nroCuota,
                data[i].fechaPago,
                data[i].importe.toFixed(2),
                plazo,
                retraso,
                data[i].estCuota
            ]);
        }
    }

    //LISTAR CAMPOS -----------------------------------------------------------------------------------------------------------------------
    clienteInput.keyup(function () {
        var cad = $(this).val();
        if (cad.length === 0) {
            clienteCod.val("0");
        } else {
            buscarClienteNombre(cad);
        }
    });
    function buscarClienteNombre(cadena) {
        //DESCRIPCION: Listas los cientes con autocomplete
        clienteInput.autocomplete({
            minLength: 2,
            source:
                function (request, response) {
                    $.ajax({
                        type: "POST",
                        url: "frmMantPreventa.aspx/buscarCliente",
                        data: JSON.stringify({ cadena: cadena }),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        error: function (xhr, ajaxOtions, thrownError) {
                            console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                        },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                var obj = new Object();
                                obj.label = item.nombres;
                                obj.value = item.nombres;
                                obj.codCliente = item.codCliente;
                                return obj;
                            }));
                        }
                    });
                },
            select: function (event, ui) {
                clienteInput.val(ui.item.label);
                clienteCod.val(ui.item.codCliente);
            }
            , change: function (event, ui) {
                if (!ui.item) {
                    clienteInput.val('');
                    clienteCod.val('0');

                }
            }
        });
    }


});