﻿$(document).ready(function () {
    var marca = $("#idMarca");                      //ID del Input de Marca
    var modelo = $("#idModelo");                    //ID del Input de Modelo
    var placa = $("#idPlaca");                      //ID del Input de Placa
    var peso = $("#idPeso");                        //ID del Input de Peso
    var fabricacion = $("#idFabricacion");          //ID de Fabricacion
    var tipoVehiculo = $("#idTipoVehiculo");        //ID del Input de Tipo de Vehiculo
    var Mmensajem = $("#Mmensajem");                //ID del DIV para mostrar mensaje en la ventana principal
    var Mpmensajem = $("#Mpmensajem");              //ID de etiqueta "P" para mostrar mensaje en la ventana principal

    var Mmarca = $("#MidMarca");                      //ID del Input de Marca
    var Mmodelo = $("#MidModelo");                    //ID del Input de Modelo
    var Mplaca = $("#MidPlaca");                      //ID del Input de Placa
    var Mpeso = $("#MidPeso");                        //ID del Input de Peso
    var Mfabricacion = $("#MidFabricacion");          //ID de Fabricacion
    var MtipoVehiculo = $("#MidTipoVehiculo");        //ID del Input de Tipo de Vehiculo
    var Mmensaje = $("#Mensaje");                     //ID del DIV para mostrar mensaje en la ventana principal
    var Mpmensaje = $("#Mpmensaje");                  //ID de etiqueta "P" para mostrar mensaje en la ventana principal

    var tmpidvehiculox;                           //variable para editar un vehiculo
    var tmpplacax;                               //Variable para validar la placa
    var tmpmarcax;                               //Variable para validar la marca
    var tmpmodelox;                              //Variable para validar la modelo
    var tmppesox;                                //Variable para validar la peso
    var tmpfabricacionx;                         //Variable para validar la fabricacion
    var tmptipovehiculox;                        //Variable para validar la tipo vehiculo

    var Mvehiculo = $("#Mvehiculo");          //Input para mostrar vehiculo al que se le agregara conductor
    var MmensajeS = $("#MensajeS");                     //ID del DIV para mostrar mensaje en la ventana principal
    var MpmensajeS = $("#MpmensajeS");                  //ID de etiqueta "P" para mostrar mensaje en la ventana principal

    var Tvehiculo = $("#Tvehiculo");          //Input para mostrar vehiculo al que se le agregara conductor
    var MensajeT = $("#MensajeT");                     //ID del DIV para mostrar mensaje en la ventana principal
    var MpmensajeT = $("#MpmensajeT");                  //ID de etiqueta "P" para mostrar mensaje en la ventana principal

    var hoy = new Date();
    var yyyy = hoy.getFullYear();

    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar btnEditarX" data-toggle="modal" ></button>';
    var btnEliminar = ' <button type="button" title="Eliminar" class="icon-bin btnDelete btnDeleteX" data-toggle="modal" ></button>';
    var btnAdd = ' <button type="button" title="Agregar Conductor" class="icon-plus btnPlus btnPlusX" data-toggle="modal" ></button>';
    var btnVer = ' <button type="button" title="Ver Conductor" class="icon-eye btnEye btnEyeX" data-toggle="modal" ></button>';

    var btnEliminarConductor = ' <button type="button" title="Eliminar" class="icon-bin btnDelete btnDeleteC" data-toggle="modal" ></button>';
    var btnAddConductor = ' <button type="button" title="Agregar Conductor" class="icon-plus btnPlus btnPlusC" data-toggle="modal" ></button>';

    $('#MSTablePrincipal').DataTable({
        //DESCRIPCION : Funcion que determina tamaño de columnas de tabla.
        paging: true,
        ordering: true,
        info: true,
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda rapida: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            {
                "width": "2%",
                "targets": [0],
                "class": "text-center"
            },
            {
                "width": "15%",
                "targets": [1, 2, 3, 6]
            },
            {
                "width": "10%",
                "targets": [4, 5],
                "class": "text-right"
            },
            {
                "targets": [7],
                "visible": false,
                "searchable": false
            },
            {
                "width": "15%",
                "targets": [8],
                "class": "text-center"
            }
        ]
    });

    var tabla = $("#MSTablePrincipal").dataTable();
    var table = $("#MSTablePrincipal").DataTable();

    $('#TableConductor').DataTable({
        //DESCRIPCION : Funcion que determina tamaño de columnas de tabla.
        paging: true,
        ordering: true,
        info: true,
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            {
                "width": "5%",
                "targets": [0],
                "class": "text-center"
            },
            {
                "width": "90%",
                "targets": [1]
            },
            {
                "targets": [2],
                "visible": false,
                "searchable": false
            },
            {
                "width": "5%",
                "targets": [3],
                "class": "text-center"
            }
        ]
    });

    var chofera = $("#TableConductor").dataTable();
    var chofere = $("#TableConductor").DataTable();

    $('#TableLista').DataTable({
        //DESCRIPCION : Funcion que determina tamaño de columnas de tabla.
        paging: true,
        ordering: true,
        info: true,
        language: {
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No hay registros",
            info: "Mostrando la página _PAGE_ de _PAGES_",
            infoEmpty: "No hay registros disponibles.",
            infoFiltered: "(filtered from _MAX_ total records)",
            search: "Busqueda: ",
            oPaginate: {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        columnDefs: [
            {
                "width": "5%",
                "targets": [0],
                "class": "text-center"
            },
            {
                "width": "90%",
                "targets": [1]
            },
            {
                "targets": [2],
                "visible": false,
                "searchable": false
            },
            {
                "width": "5%",
                "targets": [3],
                "class": "text-center"
            }
        ]
    });

    var lista = $("#TableLista").dataTable();
    var liste = $("#TableLista").DataTable();

    $("#btnAgre").click(function () {
        Mmensajem.css("display", "none");
        $('#Mactualizar').hide();
        $('#Mguardar').show();
        document.getElementById("MidPlaca").disabled = false;
        limpiar();
    });

    function Eventos() {
        //DESCRIPCION : Funcion para validar seleccion en los input
        Mplaca.change(function () {
            Mmensaje.css("display", "none");
        });

        Mmarca.change(function () {
            Mmensaje.css("display", "none");
        });

        Mmodelo.change(function () {
            Mmensaje.css("display", "none");
        });

        Mfabricacion.change(function () {
            Mmensaje.css("display", "none");
        });

        MtipoVehiculo.change(function () {
            Mmensaje.css("display", "none");
        });
    }

    Eventos();

    function LetrasNumeros(e) {
        var key = e.keyCode || e.which;
        var tecla = String.fromCharCode(key).toLowerCase();
        var letras = "abcdefghijklmnñopqrstuvwxyz0123456789 -";
        
        if (letras.indexOf(tecla) == -1) {
            e.preventDefault();
        }
    }
    document.getElementById('MidPlaca').addEventListener('keypress', LetrasNumeros);
    document.getElementById('MidMarca').addEventListener('keypress', LetrasNumeros);
    document.getElementById('MidModelo').addEventListener('keypress', LetrasNumeros);
    document.getElementById('MidTipoVehiculo').addEventListener('keypress', LetrasNumeros);

    function Numeros(e) {
        var key = e.keyCode || e.which;
        var tecla = String.fromCharCode(key).toLowerCase();
        var letras = "0123456789";

        if (letras.indexOf(tecla) == -1) {
            e.preventDefault();
        }
    }
    document.getElementById('MidPeso').addEventListener('keypress', Numeros);
    document.getElementById('MidFabricacion').addEventListener('keypress', Numeros);

    $("#Mguardar").click(function () {
        //DESCRIPCION : Guardar archivo excel en BD
        Mmensaje.css("display", "none");
        var tmpplaca;                               //Variable para validar la placa
        var tmpmarca;                               //Variable para validar la marca
        var tmpmodelo;                              //Variable para validar la modelo
        var tmppeso;                                //Variable para validar la peso
        var tmpfabricacion;                         //Variable para validar la fabricacion
        var tmptipovehiculo;                        //Variable para validar la tipo vehiculo
        var flag;


        if (Mplaca.val().trim() != "") {
            flag = true;
            console.log('Entro');
            tmpplaca = Mplaca.val().trim();

            if (Mmarca.val().trim() != "") {
                tmpmarca = Mmarca.val().trim();
            }
            else {
                tmpmarca = "";
            }

            if (Mmodelo.val().trim() != "") {
                tmpmodelo = Mmodelo.val().trim();
            }
            else {
                tmpmodelo = "";
            }

            if (Mpeso.val().trim() != "" && Mpeso.val().trim() >= 0) {
                tmppeso = Mpeso.val().trim();
            }
            else {
                tmppeso = "0";
            }

            if (Mfabricacion.val().trim() != "") {
                if (Mfabricacion.val().trim() > '1950' && Mfabricacion.val().trim() <= yyyy) {
                    tmpfabricacion = Mfabricacion.val().trim();
                }
                else {
                    flag = false;
                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("INGRESE FECHA MAYOR AL AÑO 1950 Y MENOR A LA ACTUAL");
                    Mmensaje.css("display", "block");
                }
            }
            else {
                tmpfabricacion = "0";
            }
            
            if (MtipoVehiculo.val().trim() != "") {
                tmptipovehiculo = MtipoVehiculo.val().trim();
            }
            else {
                tmptipovehiculo = "";
            }
            if (flag == true) {
                var json = JSON.stringify({ Jplaca: tmpplaca, Jmarca: tmpmarca, Jmodelo: tmpmodelo, Jpeso: tmppeso, Jfabricacion: tmpfabricacion, Jtipovehiculo: tmptipovehiculo });
                RegistrarVehiculo(json);
            }
        }
        else {
            Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
            Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                .css("border-color", "rgb(203, 46, 46)");
            Mpmensaje.html("DEBE REGISTRAR PLACA");
            Mmensaje.css("display", "block");
        }
    });

    function RegistrarVehiculo(data) {
        //Descricion : Registro de Vehiculos
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/RegistrarVehiculo",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {
                if (response.d == 0) {
                    limpiar();
                    Mpmensaje.html("Se registro correctamente");
                    Mpmensaje.css("color", "#ffffff");
                    Mmensaje.css("background-color", "#337ab7")
                        .css("border-color", "#2e6da4");
                    Mmensaje.css("display", "block");
                } else {
                    if (response.d == 1) {
                        Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                        Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                            .css("border-color", "rgb(203, 46, 46)");
                        Mpmensaje.html("ERROR AL REALIZAR REGISTRO");
                        Mmensaje.css("display", "block");
                    }
                    else {
                        if (response.d == 2) {
                            Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                            Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                                .css("border-color", "rgb(203, 46, 46)");
                            Mpmensaje.html("PLACA DUPLICADA");
                            Mmensaje.css("display", "block");
                        }
                    }
                }
            }
        });
    }

    function limpiar() {
        //DESCRIPCION : Funcion para limpiar campos.
        Mplaca.val("");
        Mmarca.val("");
        Mmodelo.val("");
        Mpeso.val("");
        Mfabricacion.val("");
        MtipoVehiculo.val("");
        Mmensaje.css("display", "none");
    }

    $("#idBuscar").click(function () {
        table.clear().draw();

        Mmensajem.css("display", "none");

        if (placa.val() != "") {
            tmpplacax = placa.val();
        }
        else {
            tmpplacax = "";
        }

        if (marca.val() != "") {
            tmpmarcax = marca.val();
        }
        else {
            tmpmarcax = "";
        }

        if (modelo.val() != "") {
            tmpmodelox = modelo.val();
        }
        else {
            tmpmodelox = "";
        }

        if (peso.val() != "") {
            tmppesox = peso.val();
        }
        else {
            tmppesox = 0;
        }

        if (fabricacion.val() != "") {
            tmpfabricacionx = fabricacion.val();
        }
        else {
            tmpfabricacionx = 0;
        }

        if (tipoVehiculo.val() != "") {
            tmptipovehiculox = tipoVehiculo.val();
        }
        else {
            tmptipovehiculox = "";
        }

        var json = JSON.stringify({ Jplaca: tmpplacax, Jmarca: tmpmarcax, Jmodelo: tmpmodelox, Jpeso: tmppesox, Jfabricacion: tmpfabricacionx, Jtipovehiculo: tmptipovehiculox });
        ObtenerVehiculo(json);
    });

    function ObtenerVehiculo(data) {
        //Descricion : Registro de Vehiculos
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/ListarVehiculo",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table.clear().draw();
                CargarTabla(data.d);
            }
        });
    }

    function CargarTabla(data) {
        //DESCRIPCION : Funcion que inserta el listado de Stock en el body del DataTable
        for (var i = 0; i < data.length; i++) {
            tabla.fnAddData([
                i + 1,
                data[i].idPlaca,
                data[i].idMarca,
                data[i].idModelo,
                data[i].idPeso,
                data[i].idFabricacion,
                data[i].idTipoVehiculo,
                data[i].idVehiculo,
                btnEditar + btnEliminar + btnAdd + btnVer
            ]);
        }
        
        $("body").on('click', '.btnEditarX', function () {
            //DESCRIPCION : Funcion que me trae el Stock a Visualizar.
            Mmensajem.css("display", "none");
            limpiar();
            var tr = $(this).parent().parent();
            tmpidvehiculox = table.row(tr).data()[7];
            Mplaca.val(table.row(tr).data()[1]);
            Mmarca.val(table.row(tr).data()[2]);
            Mmodelo.val(table.row(tr).data()[3]);
            Mpeso.val(table.row(tr).data()[4]);
            if (table.row(tr).data()[5] == 0) {
                Mfabricacion.val('');
            }
            else {
                Mfabricacion.val(table.row(tr).data()[5]);
            }

            MtipoVehiculo.val(table.row(tr).data()[6]);
            $("#Mguardar").hide();
            $('#Mactualizar').show();
            document.getElementById("MidPlaca").disabled = true;
            $('#modalPrincipal').modal('show');
        });

        $("body").on('click', '.btnDeleteX', function () {
            //DESCRIPCION : Funcion que me trae el Codigo del Stock a Eliminar.
            Mmensajem.css("display", "none");
            var tr = $(this).parent().parent();
            var cod = table.row(tr).data()[7];
            swal({
                title: "Se eliminara el registro",
                text: "¿Esta seguro que desea eliminar el registro?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        EliminarVehiculo(cod)
                    } else {
                        swal("Se Canceló la eliminación");

                    }
                });
        });

        $("body").on('click', '.btnPlusX', function () {
            //DESCRIPCION : Funcion para agregar conductores.
            Mmensajem.css("display", "none");
            MmensajeS.css("display", "none");
            tmpidvehiculox = '';
            Mvehiculo.val('');
            var tr = $(this).parent().parent();
            tmpidvehiculox = table.row(tr).data()[7];
            Mvehiculo.val(table.row(tr).data()[1]);
            liste.clear().draw();
            $('#modalSecundaria').modal('show');
        });

        $("body").on('click', '.btnEyeX', function () {
            //DESCRIPCION : Funcion que me trae el Stock a Visualizar.
            Mmensajem.css("display", "none");
            MensajeT.css("display", "none");
            tmpidvehiculox = '';
            Tvehiculo.val('');
            var tr = $(this).parent().parent();
            tmpidvehiculox = table.row(tr).data()[7];
            Tvehiculo.val(table.row(tr).data()[1]);
            chofere.clear().draw();
            var json = JSON.stringify({ Jvehiculo: tmpidvehiculox });
            ObtenerAsignados(json);
            $('#modalTercera').modal('show');
        });
    }

    $("#Mbuscar").click(function () {
        liste.clear().draw();

        MmensajeS.css("display", "none");
        
        var json = JSON.stringify({ Jvehiculo: tmpidvehiculox });
        ObtenerPendientes(json);
    });

    function ObtenerPendientes(data) {
        //Descricion : Lista de Conductores disponibles
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/ListarConductores",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                liste.clear().draw();
                CargarConductores(data.d);
            }
        });
    }

    function ObtenerAsignados(data) {
        //Descricion : Lista de Conductores disponibles
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/ListarConductoresAsignados",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                chofere.clear().draw();
                CargarConductoresAsignados(data.d);
            }
        });
    }

    function CargarConductores(data) {
        //DESCRIPCION : Funcion que inserta el listado de conductores pendientes de asignar
        for (var i = 0; i < data.length; i++) {
            lista.fnAddData([
                i + 1,
                data[i].idNombre,
                data[i].id,
                btnAddConductor
            ]);
        }

        $("body").on('click', '.btnPlusC', function () {
            //DESCRIPCION : Funcion para agregar conductores.
            MmensajeS.css("display", "none");
            var tr = $(this).parent().parent();

            //Mandar a registrar en la nueva tabla con el Cod de Persona y Vehiculo. Actualizar la tabla
            var idpers = liste.row(tr).data()[2];
            var json = JSON.stringify({ Jconductor: idpers, Jvehiculo: tmpidvehiculox });
            AgregarNuevo(json);
        });
    }

    function CargarConductoresAsignados(data) {
        //DESCRIPCION : Funcion que inserta el listado de conductores pendientes de asignar
        for (var i = 0; i < data.length; i++) {
            chofera.fnAddData([
                i + 1,
                data[i].idNombre,
                data[i].id,
                btnEliminarConductor
            ]);
        }

        $("body").on('click', '.btnDeleteC', function () {
            //DESCRIPCION : Funcion para agregar conductores.
            MensajeT.css("display", "none");
            var tr = $(this).parent().parent();

            var idTransporte = chofere.row(tr).data()[2];
            EliminarNuevo(idTransporte);
        });
    }

    function AgregarNuevo(data) {
        //Descricion : Registro de Vehiculos
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/RegistrarConductores",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {
                if (response.d == 0) {
                    liste.clear().draw();
                    var json = JSON.stringify({ Jvehiculo: tmpidvehiculox });
                    ObtenerPendientes(json);

                    MpmensajeS.html("Se agregó correctamente");
                    MpmensajeS.css("color", "#ffffff");
                    MmensajeS.css("background-color", "#337ab7")
                        .css("border-color", "#2e6da4");
                    MmensajeS.css("display", "block");
                } else {
                    if (response.d == 1) {
                        MpmensajeS.css("color", "rgba(179, 10, 10, 0.69)");
                        MmensajeS.css("background-color", "rgba(255, 0, 0, 0.51)")
                            .css("border-color", "rgb(203, 46, 46)");
                        MpmensajeS.html("Error al agregar conductor");
                        MmensajeS.css("display", "block");
                    }
                }
            }
        });
    }

    function EliminarNuevo(idTransporte) {
        //DESCRIPCION : Funcion que elimina los productos del stock
        var json = JSON.stringify({ CODIGO: idTransporte });
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/EliminarRegistro",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {
                //Recargar
                if (response.d == 0) {
                    var jsom = JSON.stringify({ Jvehiculo: tmpidvehiculox });
                    ObtenerAsignados(jsom);

                    MensajeT.css("display", "none");
                    swal("Se eliminó el registro", {
                        icon: "success",
                    });
                }
                else {
                    swal("Error en la eliminación");
                }
            }
        })
    };

    function EliminarVehiculo(cod) {
        //DESCRIPCION : Funcion que elimina los productos del stock
        var json = JSON.stringify({ CODIGO: cod });
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/EliminarVehiculo",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {
                if (response.d == 0) {
                    var jsom = JSON.stringify({ Jplaca: tmpplacax, Jmarca: tmpmarcax, Jmodelo: tmpmodelox, Jpeso: tmppesox, Jfabricacion: tmpfabricacionx, Jtipovehiculo: tmptipovehiculox });
                    ObtenerVehiculo(jsom);
                    Mmensajem.css("display", "none");
                    swal("Se eliminó el registro", {
                        icon: "success",
                    });
                }
                else {
                    swal("Error en la eliminación");
                }
            }
        })
    };

    $("#Mactualizar").click(function () {
        //DESCRIPCION : Guardar archivo excel en BD
        Mmensaje.css("display", "none");
        
        var tmpplaca;                               //Variable para validar la placa
        var tmpmarca;                               //Variable para validar la marca
        var tmpmodelo;                              //Variable para validar la modelo
        var tmppeso;                                //Variable para validar la peso
        var tmpfabricacion;                         //Variable para validar la fabricacion
        var tmptipovehiculo;                        //Variable para validar la tipo vehiculo

        tmpplaca = Mplaca.val();

        if (tmpplaca != "") {
            if (Mmarca.val() != "") {
                tmpmarca = Mmarca.val();
            }
            else {
                tmpmarca = "";
            }

            if (Mmodelo.val() != "") {
                tmpmodelo = Mmodelo.val();
            }
            else {
                tmpmodelo = "";
            }

            if (Mpeso.val() != "") {
                if (Mpeso.val() < 0) {
                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("EL PESO NO PUEDE SER NEGATIVO");
                    Mmensaje.css("display", "block");
                }
                else {
                    tmppeso = Mpeso.val();
                }
            }
            else {
                tmppeso = "0";
            }

            if (Mfabricacion.val() != "") {
                if (Mfabricacion.val() > '1950') {
                    tmpfabricacion = Mfabricacion.val();
                }
                else {
                    Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
                    Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                        .css("border-color", "rgb(203, 46, 46)");
                    Mpmensaje.html("INGRESE FECHA CORRECTA MAYOR AL AÑO 1950");
                    Mmensaje.css("display", "block");
                }
            }
            else {
                tmpfabricacion = "0";
            }

            if (MtipoVehiculo.val() != "") {
                tmptipovehiculo = MtipoVehiculo.val();
            }
            else {
                tmptipovehiculo = "";
            }

            var json = JSON.stringify({ Jid: tmpidvehiculox,Jplaca: tmpplaca, Jmarca: tmpmarca, Jmodelo: tmpmodelo, Jpeso: tmppeso, Jfabricacion: tmpfabricacion, Jtipovehiculo: tmptipovehiculo });
            ModificarVehiculo(json);
        }
        else {
            Mpmensaje.css("color", "rgba(179, 10, 10, 0.69)");
            Mmensaje.css("background-color", "rgba(255, 0, 0, 0.51)")
                .css("border-color", "rgb(203, 46, 46)");
            Mpmensaje.html("DEBE REGISTRAR PLACA");
            Mmensaje.css("display", "block");
        }
    });

    function ModificarVehiculo(data) {
        //Descricion : Registro de Vehiculos
        $.ajax({
            type: "POST",
            url: "frmMantVehiculo.aspx/ModificarVehiculo",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {
                if (response.d == 0) {
                    limpiar();
                    tmpidvehiculox = '';
                    $('#modalPrincipal').modal('hide');
                    var json = JSON.stringify({ Jplaca: tmpplacax, Jmarca: tmpmarcax, Jmodelo: tmpmodelox, Jpeso: tmppesox, Jfabricacion: tmpfabricacionx, Jtipovehiculo: tmptipovehiculox });
                    ObtenerVehiculo(json);

                    Mpmensajem.html("Se actualizó correctamente");
                    Mpmensajem.css("color", "#ffffff");
                    Mmensajem.css("background-color", "#337ab7")
                        .css("border-color", "#2e6da4");
                    Mmensajem.css("display", "block");
                } else {
                    if (response.d == 1) {
                        tmpidvehiculox = '';
                        $('#modalPrincipal').modal('hide');

                        Mpmensajem.css("color", "rgba(179, 10, 10, 0.69)");
                        Mmensajem.css("background-color", "rgba(255, 0, 0, 0.51)")
                            .css("border-color", "rgb(203, 46, 46)");
                        Mpmensajem.html("ERROR AL ACTUALIZAR REGISTRO");
                        Mmensajem.css("display", "block");
                    }
                    else {
                        if (response.d == 2) {
                            tmpidvehiculox = '';
                            $('#modalPrincipal').modal('hide');

                            Mpmensajem.css("color", "rgba(179, 10, 10, 0.69)");
                            Mmensajem.css("background-color", "rgba(255, 0, 0, 0.51)")
                                .css("border-color", "rgb(203, 46, 46)");
                            Mpmensajem.html("PLACA NO EXISTE");
                            Mmensajem.css("display", "block");
                        }
                    }
                }
            }
        });
    }
});