﻿
$(document).ready(function () {

    var motivo = document.querySelector("#motivosIngreso_id");
    var motivoCompras = document.querySelector("#motivoCompras");
    var otroMotivo = document.querySelector("#otroMotivo");

    //varibales de ingreso

    
    var cantidad = document.querySelector("#cantidad");
    var fechaVencimiento = document.querySelector("#fechaVencimiento");
    var fechaProceso = document.querySelector("#fechaProceso");
    var almacen_text = $('select[id="almacen_id"] option:selected').text();
    var almacen = document.querySelector("#almacen_id");
    var cabeceraPedido = [];
    var detalleIngreso = [];
    

    const OPERACION_INSERTAR = 1;
    const OPERACION_ELIMINAR = 0;
    const OPERACION_NEUTRO = 2;
    var id_ingreso = document.querySelector("#id_ingreso");

    var btnEliminar = ' <button type="button" title="Anular" class="icon-bin btnDelete" data-toggle="modal" data-target="#modalAnular"></button>';
    var btnEditar = ' <button type="button" title="Editar" class="icon-pencil btnEditar" data-toggle="modal" data-target=".bd-example-modal-lg" ></button>';
    var btndescargar = ' <button type="button" title="Descargar" class="icon-file-pdf btnPdf btndescargar"></button>';
    var ip;
    var user;
    var lote;

    $(".select2").select2();

    $.getJSON('http://ip-api.com/json?callback=?', function (data) {
        ip = data.query;
    });

    function requestDatosSession() {
        $.ajax({
            type: "POST",
            url: "login.aspx/getSession",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                user = data.d[0].perfil;
            }
        });

    }

    requestDatosSession();


    function init() {
        otroMotivo.classList.add('d-none');
        motivoCompras.classList.add('d-none');

        var f = new Date();
        document.querySelector("#fechaProceso").value = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
        document.querySelector("#fechaVencimiento").value = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear() ;
    }

    init();


    function obtenerLote() {
        $.ajax({
            type: "POST",
            url: "frmMantIngresos.aspx/obtenerLote",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                lote = data.d+1;
            }
        });

    }

    obtenerLote();

    motivo.addEventListener('change', function () {
        changeSelect();
    });

    function changeSelect() {
        motivo.value == '1048' ? motivoCompras.classList.replace('d-none', 'd-block') : motivoCompras.classList.replace('d-block', 'd-none');
        motivo.value == '1049' ? otroMotivo.classList.replace('d-none', 'd-block') : otroMotivo.classList.replace('d-block', 'd-none');
        motivo.value == '1047' ? init() : '';
    }

    $("#tablaIngresos").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "20%", "targets": [3] },
            { "width": "30%", "targets": [4] },
            { "width": "10%", "targets": [5] },
            { "width": "10%", "targets": [6] },
            { "width": "10%", "targets": [7] },
            { "visible": false, "targets": [5,0] }
        ]
    });

    var tblingresos = $("#tablaIngresos").DataTable();
    tblingresos.columns.adjust().draw();


    $("#tblListado_ingresos").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [0] },
            { "width": "15%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "25%", "targets": [4] },
            { "width": "25%", "targets": [6] },
            { "width": "25%", "targets": [7] },
            { "visible": false, "targets": [3,5] }
        ]
    });

    var tblListado_ingresos = $("#tblListado_ingresos").DataTable();
    tblListado_ingresos.columns.adjust().draw();


    /******* boton para agregar productos a la taba de detalle *****/

    document.querySelector("#btn_agregar").addEventListener('click', function () {

        var producto_id = document.querySelector("#producto_id");

        var producto_text = $('select[id="producto_id"] option:selected').text();

        let item = tblingresos.rows().count();

        item = item + 1;

        if (tblingresos.rows().count() > 0) {

            let contador = 0;

            for (let i = 0; i < tblingresos.rows().count(); i++) {
                if (tblingresos.rows().data()[i][2] == producto_id.value) {
                    contador++;
                }

            }

            if (contador == 0) {
                tblingresos.row.add([
                    '',
                    item,
                    producto_id.value,
                    producto_text,
                    cantidad.value,
                    almacen.value,
                    almacen_text,
                    fechaVencimiento.value,
                    btnEliminar
                ]).draw();
            } else {
                swal({
                    icon: "info",
                    title: "El producto ya se encuentra agregado en la lista"
                })
            }

        } else {
            tblingresos.row.add([
                '',
                item,
                producto_id.value,
                producto_text,
                cantidad.value,
                almacen.value,
                almacen_text, 
                fechaVencimiento.value,
                btnEliminar
            ]).draw();
        }

        tblingresos.search('').draw();

    });

    /******* boton para eliminar un producto de la taba de detalle *****/

    $('#tablaIngresos tbody').on('click', 'tr .btnDelete', function () {
        let tr = $(this).parent().parent();
        if (tblingresos.row(tr).data()[0] == "") {

            tblingresos.row(tr).remove().draw()

        } else {
            
            let data = tblingresos.row(tr).data()

                let detalle = {
                    'codProducto': data[2],
                    'cantidadProducto': data[4],
                    'fechaVencimiento': data[7],
                    'codAlmacen': data[5],
                    'flagOperacion': OPERACION_ELIMINAR,
                    'ip': ip,
                    'usuario': user,
                    'mac': ip,
                    'codIngresoMercaderia': data[0]
                }
                detalleIngreso.push(detalle)
                tblingresos.row(tr).remove().draw()


        }

    });



    document.querySelector('#nuevoIngreso').addEventListener('click', () => {
        limpiarFormulario();
    });

/****** LÓGICA PARA LISTAR *******/

    function list(fecha, ingreso, proveedor, motivo) {

        $.ajax({
            type: "POST",
            url: "frmMantIngresos.aspx/listaIngresos",
            data: JSON.stringify({ fechaIngreso: fecha, ingreso: ingreso, proveedor: proveedor, motivo: motivo}),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantIngresos.aspx/listaIngresos :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                tblListado_ingresos.clear().draw();
                for (let i = 0; i < data.d.length;i++) {
                    tblListado_ingresos.row.add([
                        data.d[i].codIngreso,
                        data.d[i].fechaIngreso,
                        data.d[i].lote,
                        data.d[i].codProveedor,
                        data.d[i].descProveedor,
                        data.d[i].motivo,
                        data.d[i].descMotivo,
                        btnEditar + btnEliminar + btndescargar
                    ]).draw();
                }
                tblListado_ingresos.search('').draw();
            }
        }); 
    }

    list(null,0,0,1047);

   /******* LÓGICA PARA GUARDAR UN INGRESO ******/

    document.querySelector("#btn_guardar").addEventListener('click',function () {

        document.querySelector("#mensaje").classList.replace('d-none', 'd-block');

       
        
        if (tblingresos.rows().count() == 0) {
            swal({
                icon: "warning",
                title: "DEBE AGREGAR LA LISTA DE PRODUCTOS QUE DESEA REGISTRAR"
            })
        } else if (fechaProceso.value == "") {
            swal({
                icon: "warning",
                title: "INGRESE LA FECHA DE INGRESO"
            })
        } else if (proveedor_id.value == 0) {
            swal({
                icon: "warning",
                title: "SELECCIONE EL PROVEEDOR"
            })
        }  else {

            if (id_ingreso.value != "") {
                cabecera = {
                    'fechaIngreso': fechaProceso.value,
                    'codProveedor': proveedor_id.value,
                    'motivo': motivo.value,
                    'codIngreso': id_ingreso.value
                }

                cabeceraPedido.push(cabecera);

                for (let i = 0; i < tblingresos.rows().count(); i++) {

                    let detalle = {
                        'codProducto': tblingresos.rows().data()[i][2],
                        'cantidadProducto': tblingresos.rows().data()[i][4],
                        'fechaVencimiento': tblingresos.rows().data()[i][7],
                        'codAlmacen': tblingresos.rows().data()[i][5],
                        'flagOperacion': tblingresos.rows().data()[i][0] == "" ? OPERACION_INSERTAR : OPERACION_NEUTRO,
                        'codIngresoMercaderia': id_ingreso.value,
                        'ip': ip,
                        'usuario': user,
                        'mac': ip
                    }
                    detalleIngreso.push(detalle)
                }

                //editar ingreso

                $.ajax({
                    type: "POST",
                    url: "frmMantIngresos.aspx/editarIngreso",
                    data: JSON.stringify({ cabecera: cabeceraPedido, detalle: detalleIngreso }),
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        if (data.d.split('|')[1] == 0) {
                            swal({
                                icon: "success",
                                title: data.d.split('|')[0]
                            });

                            limpiarFormulario();
                            detalleIngreso.length = 0;
                            list(null, 0, 0, 1047);
                        } else {
                            swal({
                                icon: "error",
                                title: "SE PRESENTO UN ERROR AL INTENTAR HACER EL REGISTRO"
                            })
                        }
                    },
                    complete: function (status) {
                        document.querySelector("#mensaje").classList.replace('d-block', 'd-none');
                    }
                });
            } else {

                cabecera = {
                    'fechaProceso': fechaProceso.value,
                    'codProveedor': proveedor_id.value,
                    'motivo': motivo.value,
                    'lote': lote,
                    'ip': ip,
                    'usuario': user,
                    'mac': ip
                }

                cabeceraPedido.push(cabecera);

                for (let i = 0; i < tblingresos.rows().count(); i++) {

                    let detalle = {
                        'codProducto': tblingresos.rows().data()[i][2],
                        'cantidadProducto': tblingresos.rows().data()[i][4],
                        'fechaVencimiento': tblingresos.rows().data()[i][7],
                        'codAlmacen': tblingresos.rows().data()[i][5],
                        'ip': ip,
                        'usuario': user,
                        'mac': ip
                    }
                    detalleIngreso.push(detalle)
                }

                $.ajax({
                    type: "POST",
                    url: "frmMantIngresos.aspx/guardarIngreso",
                    data: JSON.stringify({ cabecera: cabeceraPedido, detalle: detalleIngreso }),
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log("Error en frmMantProm.aspx/ListarPromociones :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                    },
                    success: function (data) {
                        if (data.d.split('|')[1] == 0) {
                            swal({
                                icon: "success",
                                title: data.d.split('|')[0]
                            });


                            limpiarFormulario();
                            detalleIngreso.length = 0;
                            list(null, 0, 0, 1047);
                        } else if (data.d.split('|')[1] == 1) {
                            swal({
                                icon: "error",
                                title: data.d.split('|')[0]
                            })
                        } else {
                            swal({
                                icon: "error",
                                title: "SE PRESENTO UN PROBLEMA AL INTENTAR ACTUALIZAR EL REGISTRO"
                            })
                        }
                    },
                    complete: function (status) {
                        document.querySelector("#mensaje").classList.replace('d-block', 'd-none');
                    }
                });
            }  

        }

            
    });


    /***** ELIMINAR INGRESO *****/

    $('#tblListado_ingresos tbody').on('click', 'tr .btnDelete', function () {
        let tr = $(this).parent().parent();
        let id = tblListado_ingresos.row(tr).data()[0];

        $.ajax({
            type: "POST",
            url: "frmMantIngresos.aspx/eliminarPedido",
            data: JSON.stringify({ ingreso: id }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantProm.aspx/eliminarPedido :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                if (data.d.split('|')[1] == 0) {
                    swal({
                        icon: "success",
                        title: data.d.split('|')[0]
                    })

                    tblListado_ingresos.row(tr).remove().draw();
                } else {
                    swal({
                        icon: "error",
                        title: "No se pudo eliminar el ingreso seleccionado"
                    })
                }
            },
            complete: function (status) {
                document.querySelector("#mensaje").classList.replace('d-block', 'd-none');
            }
        });

    });


    /***** EDITAR INGRESO *****/

    $('#tblListado_ingresos tbody').on('click', 'tr .btnEditar', function () {
        let tr = $(this).parent().parent();
        let id = tblListado_ingresos.row(tr).data()[0];
        limpiarFormulario();
        $.ajax({
            type: "POST",
            url: "frmMantIngresos.aspx/listaIngresoDetalle",
            data: JSON.stringify({ codigo: id }),
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error en frmMantIngresos.aspx/listaIngresoDetalle :" + xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                document.querySelector("#id_ingreso").value = id;

                let motivoTabla = tblListado_ingresos.row(tr).data()[5];
                motivo.value = motivoTabla;

                let y = tblListado_ingresos.row(tr).data()[1].split('/')[2];
                let m = tblListado_ingresos.row(tr).data()[1].split('/')[1];
                let d = tblListado_ingresos.row(tr).data()[1].split('/')[0];

                fechaProceso.value = y + "-" + m + "-" + d;
                $('#proveedor_id').val(tblListado_ingresos.row(tr).data()[3]);
                $('#proveedor_id').select2({
                    text: tblListado_ingresos.row(tr).data()[4]
                });
                changeSelect();
                llenarFormularioEditar(data.d)
            }
        });

    });

    function llenarFormularioEditar(detalle) {
        

        for (let i = 0; i < detalle.length;i++) {
            tblingresos.row.add([
                detalle[i].codIngresoMercaderia,
                i,
                detalle[i].codProducto,
                detalle[i].descProducto,
                detalle[i].cantidadProducto,
                detalle[i].codAlmacen,
                detalle[i].descAlmacen,
                detalle[i].fechaVencimiento,
                btnEliminar
            ]).draw();
        }

        tblingresos.search('').draw();
    }

    /*********** LÓGICA PARA BUSQUEDA ***********/
    document.querySelector("#buscarIngreso").addEventListener('click', function () {

        document.querySelector("#form-busqueda").addEventListener('submit', e => e.preventDefault());

        let motivoBusqueda = document.querySelector("#motivoBusqueda").value;
        let codigoIngresoBusqueda = document.querySelector("#codigoIngresoBusqueda").value == "" ? 0 : document.querySelector("#codigoIngresoBusqueda").value;
        let fechaIngreso = document.querySelector("#fechaIngreso").value=="" ? null : document.querySelector("#fechaIngreso").value;
        let proveedorBusqueda = document.querySelector("#proveedorBusqueda").value;

        if (motivoBusqueda == 1047 && codigoIngresoBusqueda == 0 && fechaIngreso == null && proveedorBusqueda == 0) {
            swal({
                icon: "error",
                title: "Para iniciar una  busqueda debe ingresar un parámetro"
            })
        } else {            
            list(fechaIngreso, codigoIngresoBusqueda, proveedorBusqueda, motivoBusqueda);
        }


    });


    /**** LÓGICA PARA GENERAR PDF ***/


    $('#tblListado_ingresos tbody').on('click', 'tr .btnPdf', function () {
        let tr = $(this).parent().parent();
        let id = tblListado_ingresos.row(tr).data()[0];
        var nomPdf = "INGRESO_" + id + ".pdf";
        $.ajax({
            type: "POST",
            url: "frmMantIngresos.aspx/generarPdf",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ codigoIngreso: id }),
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {

                var bytes = new Uint8Array(data.d);
                enviarpdf(nomPdf, bytes);
            }
        });


    })

    function enviarpdf(nomPdf, datos) {
        var blob = new Blob([datos]);
        var link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        var fileName = nomPdf;
        link.download = fileName;
        link.click();
    }


    /**** LÓGICA PARA LIMPIEZA DE  FORMULARIO DE REISTRO Y DE EDICION ****/

    function limpiarFormulario() {
         cantidad.value = "";
         fechaVencimiento .value= "";
        fechaProceso.value = "";

        $('#almacen_id').val(0);
        $('#almacen_id').select2({
            text: "SELECCIONE ALMACEN"
        });

        $('#proveedor_id').val(0);
        $('#proveedor_id').select2({
            text: "SELECCIONE PROVEEDOR"
        });

        $('#producto_id').val(0);
        $('#producto_id').select2({
            text: "SELECCIONE PRODUCTO"
        });

        tblingresos.clear().draw();
    }

})