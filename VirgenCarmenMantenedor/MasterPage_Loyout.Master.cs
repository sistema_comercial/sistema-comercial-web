﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool codUser = string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["codUser"]));
            bool codPersona = string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["codPersona"]));
            bool sucursal = string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["sucursal"]));
            bool perfil = string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["perfil"]));
            bool nombres = string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["nombres"]));
            
            if (codUser || codPersona || sucursal || perfil || nombres)
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                Cargar_Modulos(Convert.ToInt32(Session["codUser"]));
            }
        }

        public void Cargar_Modulos(int codigoUsuario)
        {
            string cadena = "";
            List<CENModulo> listModulos = new CLNMenu().cargarModulos(codigoUsuario);
            foreach (var modulo in listModulos)
            {
                cadena = cadena + "<li class='barra_item' id='" + modulo.descripcion + "'>";
                cadena = cadena + "<a class='barra_link' id='a_" + modulo.descripcion + "'>" + modulo.descripcion + "</a>";
                cadena = cadena + "<ul class='submenu' id='ul" + modulo.descripcion + "'>";
                List<CENMenu> listMenu = new CLNMenu().cargarMantenedoresPorPerfil(codigoUsuario, modulo.codModulo);
                foreach (var menu in listMenu)
                {
                    cadena = cadena + "<li class='submenu_item'>";
                    cadena = cadena + "<a class='submenu_link' href='" + menu.rutaSubMenu + "'>" + menu.nomSubMenu + "</a>";
                    cadena = cadena + "</li>";
                }
                cadena = cadena + "</ul>";
                cadena = cadena + "</li>";
            }
            menuVertical.InnerHtml = cadena;
        }
    }
}