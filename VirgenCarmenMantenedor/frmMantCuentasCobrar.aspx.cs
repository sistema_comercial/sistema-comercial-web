﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantCuentasCobrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<CENCuentas> ListarCuentasCobrar(int codCliente)
        {
            List<CENCuentas> listaCuentas = null;
            CLNCuentaCobro objCLNCuentaCobro = null;
            try
            {
                objCLNCuentaCobro = new CLNCuentaCobro();
                listaCuentas = objCLNCuentaCobro.ListarCuentasCobrar(codCliente);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return listaCuentas;
        }

        [WebMethod]
        public static List<CENCuentas> ListarLetrasCobrar(int codCliente)
        {
            List<CENCuentas> listaLetras = null;
            CLNCuentaCobro objCLNCuentaCobro = null;
            try
            {
                objCLNCuentaCobro = new CLNCuentaCobro();
                listaLetras = objCLNCuentaCobro.ListarLetrasCobrar(codCliente);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return listaLetras;
        }

        [WebMethod]
        public static List<CENCuotas> ListarCuotas(int codPrestamo)
        {
            List<CENCuotas> listaCuotas = null;
            CLNCuentaCobro objCLNCuentaCobro = null;
            try
            {
                objCLNCuentaCobro = new CLNCuentaCobro();
                listaCuotas = objCLNCuentaCobro.ListarCuotas(codPrestamo);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return listaCuotas;
        }
    }
}