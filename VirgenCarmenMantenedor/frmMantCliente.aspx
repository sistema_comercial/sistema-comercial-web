﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantCliente.aspx.cs" Inherits="VirgenCarmenMantenedor.MantenimientoCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>MANTENIMIENTO CLIENTES</title>
    
    <!-- Libreria Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>

    <!-- Libreria DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    
    <%-- Libreria GoMap --%>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBOknK-sxAODK3eN-KU9GKwusA_aq6LHLo&sensor=false"></script>
    <script src="Scripts/jquery.gomap-1.3.3.min.js"></script>
    <script src="Scripts/GoogleMapsClient.js"></script>

    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="css/mant_cliente.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="icon/style.css" />
    <link rel="stylesheet" href="./CSS/alertify.min.css" />
    <script type="text/javascript" src="./Scripts/alertify.min.js"></script>
    <link rel="stylesheet" href="./CSS/default.min.css" />
    <script type="text/javascript" src="./Scripts/JavaScriptMantCliente.js"></script>
    <script src="Scripts/mant_cliente.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div class="container mt-2">
        <form id="formBusqueda">
          <div class="box-header"  runat="server" style="cursor: pointer">
            <h3 class="box-title">Mantenedor de clientes</h3>
        </div>
             <div class="form-group col-xs-12">
            <div class="div_bloque">
            <div class="title_bloque">Filtros de Busquedad</div>
            <br />
            <label class="label_bloque">Buscar por</label>
            <label for="rbNumDocumento" class="radio_bloque">
                N° Documento 
               <input type="radio" name="opcion" id="rb_tdoc" value="1" />               
            </label>
            <label for="rbNombres" class="radio_bloque">
                <input type="radio" name="opcion" id="rb_nom" value="2" />
                Nombres
            </label>
            <br />
            <label for="cbTipoDocumento" class="label_bloque">Tipo Doc.</label>
                <select class="cbo_bloque" ID="select_tipo_doc" disabled runat="server" ClientIDMode="Static" > </select>
            <br />
            <label for="lblNumDocRuc" class="label_bloque">Nº Doc. / Ruc</label>
                <input type="text" class="input_bloque_mediun" id="numdoc" maxlength="15" disabled />
            <br />
            <label for="lbNombres" class="label_bloque">Nombres</label>
            <input type="text" class="input_bloque_mediun" id="nom" maxlength="70" disabled />
            <br />
                <button type="submit" id="buscarCliente" class="btn_bloque">Buscar</button>
        </div>
        </div>
        </form>
        <button class="btn btn-primary" data-toggle="modal" id="btn_mostrar_modal" data-target="#modalRegistrar" >Registrar nuevo cliente</button>
       </div>

    <!-- Modal REGISTRAR / EDITAR -->
    
    <div class="modal fade " id="modalRegistrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registrar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
          
                        <form id="pnlRegMod" class="form-group col-xs-12">                        
                                <div class="">
                                    <div class="jumbotron p-2 mb-2 ">Registrar Cliente</div>
                                    <br />
                                    <div class="form-row py-2">
                                        <div class="col-sm-12 col-lg-6">
                                            <label>Tipo Persona</label>
                                            <select class="form-control w-100" name="select_tipo_per"  id="select_tipo_per_reg" ClientIDMode="Static" runat="server"></select>
                                        </div>
                                        <div class="col-sm-12 col-lg-6 ">
                                            <label class="">Tipo Documento</label>
                                            <select class="form-control w-100" name="select_tipo_doc_reg" id="select_tipo_doc_reg" runat="server" ClientIDMode="Static"></select>
                                        </div>
                                    </div>
                                    <div class="form-row py-2">
                                         <div class="col-sm-12 col-lg-6">
                                            <label class="w-100" id="labelMrmNumDocumento" >Número de documento</label>
                                            <input type="text" class="form-control w-100" id="txtMrmNumDocumento" maxLength="8">
                                            <input type="text"  id="CodPersona" hidden>                                            
                                             <div class="d-none" id="busquedaReniec">
                                                 <div class="d-flex align-items-center p-3">
                                                    <span class="sr-only text-dark">Buscando datos reniec...</span>
                                                    <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                                                </div>
                                             </div>

                                        </div>
                                        <div class="col-sm-12 col-lg-6 d-none">
                                            <label>Perfil Cliente</label>
                                            <select class="form-control w-100" name="select_perfil_clinte" id="select_perfil_clinte" ClientIDMode="Static" runat="server" hidden></select>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <label >Clasificacion</label>
                                            <select class="form-control w-100" name="select_clasificacion" id="select_clasificacion" ClientIDMode="Static" runat="server"></select>
                                        </div>
                                    </div>                            
                                    <div class="form-row py-2">
                                        <div class="col-sm-12 col-lg-6 d-none">
                                            <label  hidden>Frecuencia</label>
                                            <select class="form-control w-100" name="cbMrmFrecuencia" id="cbMrmFrecuencia" runat="server" ClientIDMode="Static" ></select>
                                         </div>
                                        <div class="col-sm-12 col-lg-6">
                                             <label class="">Ruta</label>
                                             <select class="form-control w-100" name="select_ruta" id="select_ruta" ClientIDMode="Static" runat="server"></select>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <label >Lista Precio</label>
                                            <select class="form-control w-100" ClientIDMode="Static" name="select_lista_precio" id="select_lista_precio" runat="server"></select>
                                        </div>
                                    </div>
                                    <div class="form-row py-2">
                                        
                                        <div class="col-sm-12 col-lg-6 d-none">
                                            <label class="">Orden Atencion</label>
                                            <input type="text" class="form-control w-100" id="txtMrmOrdenAtencion" maxLength="15" hidden>
                                        </div>
                                    </div>
                                    <div class="form-row py-2">
                                       
                                        <div class="col-sm-12 col-lg-6">
                                            <label  for="txtMrmRUC" >RUC</label>
                                            <input type="text" class="form-control w-100" id="txtMrmRUC" maxLength="11">
                                            <div class="d-none" id="busquedaRuc">
                                                 <div class="d-flex align-items-center p-3">
                                                    <span class="sr-only text-dark">Buscando datos ruc...</span>
                                                    <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                                                </div>
                                             </div>
                                        </div>
                                         <div class="col-sm-12 col-lg-6">
                                            <label for="txtMrmRazonSocial" >Razon Social</label>
                                            <input type="text" class="form-control w-100" id="txtMrmRazonSocial" maxLength="50">
                                            <div class="d-none" id="cvMrmRazonSocial"  ></div>
                                        </div>
                                    </div>
                                    <div class="form-row py-2">
                                       
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="" for="txtMrmNombres" >Nombres</label>
                                            <input type="text" class="form-control w-100" id="txtMrmNombres"  maxLength="30"> 
                                            <input type="text" class="form-control w-100" id="txtCodPersona"  hidden> 
                                        </div> 
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="" for="txtMrmApellPaterno" >Apellido Paterno</label>
                                            <input type="text" class="form-control w-100" id="txtMrmApellPaterno" maxLength="20">
                                        </div>

                                    </div>
                                    <div class="form-row">
                                       
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="" for="txtMrmApellMaterno" >Apellido Materno</label>
                                            <input type="text" class="form-control w-100" id="txtMrmApellMaterno"  MaxLength="20">           
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <label  for="txtMrmCorreo">Correo</label>
                                            <input type="text" class="form-control w-100" id="txtMrmCorreo" >
                                        </div>
                                    </div>                            
                                                           
                                    <div class="form-row my-2">                                
                                        
                                        <div class="col-sm-12 col-lg-6">
                                            <label for="txtMrmTelefono"> Telefono</label>
                                            <input type="text" class="form-control w-100" id="txtMrmTelefono" >
                                        </div>
                                         <div class="col-sm-12 col-lg-6">
                                            <label   for="txtMrmCelular" >Celular</label>
                                            <input type="text" class="form-control w-100" id="txtMrmCelular">
                                        </div>    
                                    </div>
                                 
                            
                                </div>
                            <hr />
                                <br />
                                  <div class="">
                                    <div class="jumbotron p-2 mb-2 ">Domicilio Fiscal</div>
                                    <br />
                                    <div class="form-row my-2">                                
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="">Departamento</label>
                                            <select class="form-control w-100" name="cbMrmDepartamento" id="cbMrmDepartamento" ClientIDMode="Static" runat="server" ></select>
                                        </div>   
                                        <div class="col-sm-12 col-lg-6">
                                            <label >Provincia</label>
                                            <select class="form-control w-100" name="cbMrmProvincia" ClientIDMode="Static" id="cbMrmProvincia"  runat="server" ></select>
                                        </div>   
                                    </div>
                                    <div class="form-row my-2">                                
                                        <div class="col-sm-12">
                                             <label >Distrito</label>
                                             <select class="form-control w-100" id="cbMrmDistrito" name="cbMrmDistrito" ClientIDMode="Static" runat="server"></select>
                                        </div>   
                                
                                    </div>
                                        <div class="form-row p-0 py-1">
                                                <div class="col-sm-10">
                                                   <label class="" for="txtMrmDireccion" >Direccion</label>
                                                    <input type="text" class="form-control w-100" id="txtMrmDireccion" maxLength="100">                                 
                                                </div>
                                                <div class="col-sm-2 d-flex align-items-end">
                                                   <button type="button" class="btn btn-primary " id="buttonModalsDireccion" data-toggle="modal" data-target="#modalMapClient" >Mapa</button>
                                                 </div>
                                    </div>     
                                    <div class="form-row my-2">                                
                                        <div class="col-sm-12 col-lg-6">
                                           <label class="" for="txtLatitud" >Latitud</label>
                                           <input type="text" class="form-control w-100" id="txtLatitudXa" maxLength="100" disabled>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="" for="txtLongitud" >Longitud</label>
                                            <input type="text" class="form-control w-100" id="txtLongitudYa" maxLength="100" disabled>
                                        </div>
                                    </div>  
                           
                                    <br />
                                </div>
                                  <div id="punto_entrega_form">
                                            <div class="jumbotron p-2 mb-2 ">ADICIONAR PUNTOS DE ENTREGA</div>
                                            <br>
                                            <div class="form-row p-0 py-1">
                                                                          
                                                <div class="col-sm-12 col-lg-6">
                                                    <label class="label_bloque">DEPARTAMENTO</label>
                                                    <select class="form-control w-100" name="departamento_pe" runat="server" id="select_departamento_pe" ClientIDMode="static"></select>
                                                </div>
                                                  <div class="col-sm-12 col-lg-6">
                                                    <label class="label_bloque">PROVINCIA</label>
                                                <select class="form-control w-100" name="departamento_pe" runat="server" id="select_provincia_pe" ClientIDMode="static"></select>
                                    
                                              </div>  
                                            </div>
                                            <div class="form-row p-0 py-1">
                                            
                                              <div class="col-12">
                                                <label class="label_bloque">DISTRITO</label>
                                                <select class="form-control w-100" runat="server" id="select_distrito_pe" ClientIDMode="static"></select>
                                              </div>                                    
                                            </div>   
                                
                                              <div class="form-row p-0 py-1">
                                                <div class="col-sm-10">
                                                    <label class="">DIRECCION</label>
                                                    <input type="text" class="form-control w-100" id="txt_direccion_pe" >
                                                    <input type="text" class="form-control" id="longitud_pe" hidden >
                                                    <input type="text" class="form-control" id="latitud_pe" hidden >
                                                </div>
                                                <div class="col-sm-2 d-flex align-items-end">
                                                     <button type="button" class="btn btn-primary " id="buttonModalpe" data-toggle="modal" data-target="#modalMapClient" >Mapa</button>
                                                </div>
                                        
                                              </div>                                     
                                              <div class="col-12 p-0 pt-1 pb-2">
                                                 <label class="label_bloque">REFERENCIA</label>
                                                 <input type="text" class="form-control w-100" id="txt_referencia_pe" >
                                              </div>  

                                                <Button type="button" class="btn btn-primary float-right" id="btnAgregarPE">Agregar</Button>
                                 
                                    
                                        </div>
                                <br />
                        
                                 <hr />
                         
                                 <div class="row" >                            
                           
                            <div class="col-sm-12">
                                
                                    <div class="table-responsive">
                                            <table id="tbl_puntos_entrega" class="table table-striped table-bordered" style="width: 100%;">
                                                <thead>
                                                    <tr>                  
                                                        <th>DEPARTAMENTO</th> <!-- 0-->                    
                                                        <th>PROVINCIA</th> <!-- 1 -->
                                                        <th>DISTRITO</th> <!-- 2-->
                                                        <th>DIRECCION</th><!-- 3-->
                                                        <th>REFERENCIA</th><!--4-->                        
                                                        <th>CORRDENADA X</th><!--5-->                        
                                                        <th>CORRDENADA Y</th><!--6-->                        
                                                        <th>UBIGEO</th><!--7-->                        
                                                        <th>CODIGO</th><!--8-->                        
                                                        <th>FLAG</th><!--9-->                        
                                                        <th>OPERACIONES</th><!-- 10-->                        
                                                    </tr>
                                                </thead>                                    
                                                <tbody id="tbl_puntes_entrega_body">
                                        
                                                </tbody>
                                        </table>
                                    </div>
                            </div>
                                </div>
                               <br />
                    
                           </form>
                  </div>
       
      
              <div class="modal-footer p-0 mt-0">
                    <input type="button" class="btn btn-default" id="btnCancelarMdRegMod"  value="REGRESAR"  />
                    <input type="button" class="btn btn-primary" id="btnAceptarMdRegMod"  value="REGISTRAR CLIENTE"  />

              </div>
             <div class="d-none" id="preload">
                 <div class="d-flex align-items-center p-3">
                    <strong>Loading...</strong>
                    <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                </div>
             </div>
            </div>
          </div>
        </div>
   

    <div class="container p-1">
        <div class="table-responsive">
              <table id="tbl_clientes" class="table table-striped table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                                      
                        <th>CODIGO PERSONA</th> <!-- 0-->                    
                                                <th>CODIGO TIPO PERSONA</th> <!-- 1 -->
                        <th>TIPO PERSONA</th><!-- 2-->
                                                <th>COD TIPO DOC</th> <!-- 3-->
                        <th>TIPO DOCUMENTO</th><!-- 4-->
                        <th>NUM. DOC</th><!-- 5-->
                        <th>RUC</th><!-- 6-->
                        <th>RAZÓN SOCIAL</th><!-- 7-->
                         <th>NOMBRES Y APELLIDOS</th><!-- 8-->
                        <th>NOMBRES</th><!-- 9-->
                        <th>APP. PATERNO</th><!--10-->                        
                        <th>APP. MATERNO</th><!-- 11-->
                        <th>DIRECCION</th><!--12-->
                        <th>CORREO</th><!--13-->                         
                        <th>TELEFONO</th><!--14-->
                        <th>CELULAR</th><!-- 15-->
                        <th>OPERACIONES</th><!--16-->
                        <th>COD PERFIL</th><!-- 17-->
                        <th>COD CLASIFICACION</th><!-- 18-->                                                
                        <th>COD LISTA PRECIO</th><!-- 19-->       
                        <th>O. ATENCIÓN</th><!-- 20-->
                        <th>CORDENADA X</th><!--21-->
                        <th>CORDENADA Y</th><!--22-->
                        <th>UBIGEO</th><!--23-->   
                        <th>FRECUENCIA</th><!--24-->
                        <th>RUTA</th><!--25-->   
                    </tr>
                </thead>
                
                <tbody id="tbl_body_table">
                    
                </tbody>
            </table>
        </div>
    </div>


    <%--modal del mapa--%>

    <div class="modal fade" id="modalMapClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
           
           
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Asignación de direcció</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="Mmensaje">
                        <p id="Mpmensaje"></p>
                    </div>
                </div>
                <div class="modal-body map-modal">
                    <div class="row">
                        <div class="col-md-12 modal_body_map">
                            <div class="location-map" id="location-map">
                                <div style="width: 100%; height: 400px;" id="map"></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer map-modal">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelarModal">Cancelar</button>
                        <button type="button" class="btn btn-primary d-none" data-dismiss="modal" id="btnGuardarMap">Guardar</button>
                        <button type="button" class="btn btn-primary d-none" data-dismiss="modal" id="btnGuardarMapPE">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     

</asp:Content>

