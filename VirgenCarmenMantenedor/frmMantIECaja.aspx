﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantIECaja.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantIECaja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="Scripts/DataTable/datatables.min.css" />    
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.min.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.structure.min.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.theme.min.css" />
    <link rel="stylesheet" href="CSS/modal_style.css" />
    <link rel="stylesheet" href="CSS/DataTable.css" />
    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="css/mant_ie_cajas.css" type="text/css" media="screen, projection" />

    <link rel="stylesheet" href="icon/style.css" />
    <script src="Scripts/DataTable/datatables.min.js"></script>
    <script src="Scripts/sweetalert.min.js"></script>
    <script src="Scripts/moment.min.js"></script>
    <script type="text/javascript" src="Scripts/jqueryui/jquery-ui.min.js"></script>
    <script src="Scripts/mant_ie_cajas.js"></script>
    <title>Mantenedor de Ingresos/Egresos Caja</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <h3 class="box-title" id="tituloID">Mantenedor de Ingresos/Egresos Caja</h3>
                </div>

                <!-- ESTE SERA EL SUB TITULO GENERAL DE LO QUE SE QUIERE HACER -->
                <div class="form-group sub-titulo">
                    <h4 class="box-filtros">Filtros de B&uacute;squeda</h4>
                </div>

                <div class="form-group col-xs-12">
                    <div class="form-group col-xs-6">
                        <label for="" class="col-sm-3 label label-default horizontal">Tipo de Transacci&oacute;n</label>
                        <select id="selectTipoTrans" class="form-control horizontal">
                            <option value="0">SELECCIONE UN TIPO DE TRANSACCIÓN</option>

                        </select>
                    </div>     
                    
                    <div class="form-group col-xs-6">
                        <label for="" class="col-sm-3 label label-default horizontal">Modo de Pago</label>
                        <select id="selectModoPago" class="form-control horizontal">
                            <option value="0">SELECCIONE UN MODO DE PAGO</option>

                        </select>
                    </div>  
                </div>

                <div class="form-group col-xs-12">
                    <div class="form-group col-xs-6">
                        <label for="" class="col-sm-3 label label-default horizontal">Tipo de Registro</label>
                        <select id="selectTipoRegistro" class="form-control horizontal">
                            <option value="0">SELECCIONE UN TIPO DE REGISTRO</option>

                        </select>
                    </div> 

                    <div class="form-group col-xs-6">
                        <label for="" class="col-sm-3 label label-default horizontal">Tipo de Moneda</label>
                        <select id="selectTipoMoneda" class="form-control horizontal">
                            <option value="0">SELECCIONE UN TIPO DE MONEDA</option>

                        </select>
                    </div>
                    
                    <div class="form-group col-xs-12">
                        <button type="submit" id="btnBuscar" class="btn btn-primary">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="form-group sub-titulo">
                <h4 class="box-filtros">Valores´para listar tabla</h4>
            </div>

            <div class="form-group col-xs-12">
                <label for="" id="fecha" class="col-sm-1 label label-default horizontal">Fecha</label>
                <div class='col-sm-4'>
                    <input type="text" name="date_begin" id="ape-date" class="form-control date-range-filter"
                        placeholder="De: dd-mm-yyyy" />
                </div>

                <label for="" class="col-sm-1 label label-default horizontal">Caja</label>
                <div class='col-sm-5'>
                    <select id="selectCaja" class="form-control horizontal">
                        <option value="0">SELECCIONE UNA CAJA</option>

                    </select>
                </div>
            </div>

             <div class="form-group col-xs-8" id="divDetalleApertura">
            </div>

            <div class="form-group col-xs-12" id="MmensajeCaja">
                    <p id="MpmensajeCaja"></p>
                </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                    <h4 class="box-filtros">Registrar</h4>
                    <button type="button" class="btn btn-primary" id="btnRegistrarMovManual" data-toggle="modal" data-target="#modalAccion">
                        Registrar Movimiento Manual
                    </button>
                </div>
        </div>
        <div class="row">
            <div class="box-body table-responsive">
                <table id="tb_movs_caja" class="table table-bordered table-hover">

                    <thead>
                        <tr>
                            <th scope="col">N° TRANSACCION MOVIMIENTO</th>
                            <th scope="col">FECHA</th>
                            <th scope="col">TIPO DE REGISTRO</th>
                            <th scope="col">COD. TIPO DE MOVIMIENTO</th>
                            <th scope="col">TIPO DE MOVIMIENTO</th>
                            <th scope="col">IMPORTE</th>
                            <th scope="col">COD. TIPO DE MONEDA</th>
                            <th scope="col">TIPO DE MONEDA</th>
                            <th scope="col">MODO DE PAGO</th>
                            <th scope="col">TIPO DE TRANSACCIÓN</th>
                            <th scope="col">ACCION</th>

                        </tr>
                    </thead>
                    <tbody id="tbl_body_table_movs_caja" class="ui-sortable">
                    </tbody>
                    <!-- aqui traeremos la data por medio de ajax -->
                    <tfoot>
                        <tr>
                            <th scope="col">N° TRANSACCION MOVIMIENTO</th>
                            <th scope="col">FECHA</th>
                            <th scope="col">TIPO DE REGISTRO</th>
                            <th scope="col">COD. TIPO DE MOVIMIENTO</th>
                            <th scope="col">TIPO DE MOVIMIENTO</th>
                            <th scope="col">IMPORTE</th>
                            <th scope="col">COD. TIPO DE MONEDA</th>
                            <th scope="col">TIPO DE MONEDA</th>
                            <th scope="col">MODO DE PAGO</th>
                            <th scope="col">TIPO DE TRANSACCIÓN</th>
                            <th scope="col">ACCION</th>

                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-6">
                <label  class="label label-default horizontal col-sm-4" for="inputName">Monto Total Transferencia</label>
                <div class="col-xs-3">
                    <input type="text" class="form-control" id="txtMST" readonly=""/>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" id="txtMDT" readonly=""/>
                </div>
            </div>
            <div class="form-group col-xs-6">
                <label  class="label label-default horizontal col-sm-4" for="inputName">Monto Total Efectivo</label>
                <div class="col-xs-3">
                    <input type="text" class="form-control" id="txtMSE" readonly=""/>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" id="txtMDE" readonly=""/>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAccion" tabindex="-1" role="dialog" aria-labelledby="modalAccionLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lblTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group" id="Mmensaje">
                        <p id="Mpmensaje"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group">

                            <div class="form-group col-xs-12">
                                <input type="hidden" class="form-control" id="txtNtraTransaccionCaja" readonly=""/>
                            </div>

                            <div class="form-group col-xs-12">
                                <label for="" id="dtpFecha" class="col-sm-3 label label-default">Fecha</label>
                                <div class='col-sm-9'>
                                    <input type="text" id="date-mov" class="form-control"
                                        placeholder="dd-mm-yyyy" readonly=""/>
                                </div>
                            </div>

                            <div class="form-group col-xs-12">
                                <label for="lblTipoMov">Tipo de Movimiento</label>
                                <select class="form-control" id="selectTipoMov_Modal">
                                    <option value="0">SELECCIONE UN TIPO DE MOVIMIENTO</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12">
                          
                                <label  class="label label-default col-xs-4" for="inputName">Modo de Pago</label>
                                <div class="col-xs-8">
                                        <input type="text" class="form-control" id="txtModoPago" value="EFECTIVO" readonly=""/>
                                </div>
                            </div>

                            <div class="form-group col-xs-12">
                                <label for="lblTipoMoneda">Tipo de Moneda</label>
                                <select class="form-control" id="selectTipoMoneda_Modal">
                                    <option value="0">SELECCIONE UN TIPO DE MONEDA</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12">
                          
                                <label  class="label label-default col-xs-4" for="inputName">Importe</label>
                                <div class="col-xs-8">
                                        <input type="text" class="form-control decimales" id="txtImporte"/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
                            <button type="submit" class="btn btn-primary" id="btnRegistrar">Registrar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
