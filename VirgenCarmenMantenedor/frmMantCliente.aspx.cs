﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class MantenimientoCliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) //ingresa solo la primera vez que se carga el formulario
            {

                //Inicio la variable que deseo mantener en caso refresco la pagina

                CENPuntoEntrega objListaPuntosEntrega = new CENPuntoEntrega();
                //ViewState["objListaPuntosEntrega"] = objListaPuntosEntrega;
                CENPuntoEntrega objLpeEliminados = new CENPuntoEntrega();
                //ViewState["objLpeEliminados"] = objLpeEliminados;
                CENPuntoEntrega objLpeAgregados = new CENPuntoEntrega();
                //ViewState["objLpeAgregados"] = objLpeAgregados;

                CENConcepto objListaConcepto = new CENConcepto();
                //ViewState["objListaConcepto"] = objListaConcepto;

                CENCliente objListaCliente = new CENCliente();
                //ViewState["objListaCliente"] = objListaCliente;

                CENGlobales objGlobal = new CENGlobales();
                //ViewState["objGlobal"] = objGlobal;


                //inizalizacion
                CLNConcepto concepto = new CLNConcepto();
                CLNDepartamento departamento = new CLNDepartamento();

                //llamado de métodos
                List<CENConcepto> listTipoDoc = concepto.ListarConceptos(2);//tipo de documento
                List<CENConcepto> listTipoPer = concepto.ListarConceptos(11);//Tipo de persona
                List<CENConcepto> listPerfil = concepto.ListarConceptos(12);//PERFIL DEL CLIENTE
                List<CENConcepto> listClasificacion = concepto.ListarConceptos(13);//CLASIFICACION
                List<CENConcepto> listPrec = concepto.ListarConceptos(15);//Lista de precios
                List<CENConcepto> listRutas = concepto.ListarConceptos(16);//Lista de rutas
                List<CENDepartamento> listDept = departamento.ListarDepartamentos(7);

                //departamntos

                select_departamento_pe.Items.Clear();
                cbMrmDepartamento.Items.Clear();
                foreach (var value in listDept)
                {
                    string item = value.codDepartamento.ToString();
                    string descripcion = value.nombre;
                    select_departamento_pe.Items.Add(new ListItem(descripcion, item));
                    cbMrmDepartamento.Items.Add(new ListItem(descripcion, item));
                }


                //tipo documento
                select_tipo_doc.Items.Clear();
                select_tipo_doc.Items.Add(new ListItem("TIPO DE DOCUMENTO", "0"));

                select_tipo_doc_reg.Items.Clear();
                select_tipo_doc_reg.Items.Add(new ListItem("TIPO DE DOCUMENTO", "0"));
                foreach (var value in listTipoDoc)
                {
                    string item = value.correlativo.ToString();
                    string descripcion = value.descripcion;
                    select_tipo_doc.Items.Add(new ListItem(descripcion, item));
                    select_tipo_doc_reg.Items.Add(new ListItem(descripcion, item));
                }

                //tipo de persona
                select_tipo_per_reg.Items.Clear();
                select_tipo_per_reg.Items.Add(new ListItem("TIPO CLIENTE", "0"));
                foreach (var value in listTipoPer)
                {
                    string item = value.correlativo.ToString();
                    string descripcion = value.descripcion;
                    select_tipo_per_reg.Items.Add(new ListItem(descripcion, item));
                }




                //CLASIFICACION
                select_clasificacion.Items.Clear();
                select_clasificacion.Items.Add(new ListItem("CLASFIFICACION DEL CLIENTE", "0"));
                foreach (var value in listClasificacion)
                {
                    string item = value.correlativo.ToString();
                    string descripcion = value.descripcion;
                    select_clasificacion.Items.Add(new ListItem(descripcion, item));
                }

                //lista de precios
                select_lista_precio.Items.Clear();
                select_lista_precio.Items.Add(new ListItem("TIPO DE PRECIO", "0"));
                foreach (var value in listPrec)
                {
                    string item = value.correlativo.ToString();
                    string descripcion = value.descripcion;
                    select_lista_precio.Items.Add(new ListItem(descripcion, item));
                }

                //lista de rutas
                select_ruta.Items.Clear();
                select_ruta.Items.Add(new ListItem("RUTA", "0"));
                foreach (var value in listRutas)
                {
                    string item = value.correlativo.ToString();
                    string descripcion = value.descripcion;
                    select_ruta.Items.Add(new ListItem(descripcion, item));
                }
            }
        }





        [WebMethod]
        public static int EliminarPuntoEntrega(CENPuntoEntrega puntoEntrega)
        {

            CLNPuntoEntrega cln = new CLNPuntoEntrega();
            try
            {

                return cln.EliminarPuntoEntrega(2, puntoEntrega);
            }
            catch (Exception ex)
            {
                throw ex;
            }




        }


        [WebMethod]
        public static CEN_RespuestaWSReniec BuscarClienteReniec(string numDocumento)
        {
            CEN_RespuestaWSReniec respuesta = new CEN_RespuestaWSReniec();
            CEN_RequestReniec request = new CEN_RequestReniec();
            CLNConexionServicio cLNConexion = new CLNConexionServicio();
            try
            {
                request.numDocumento = numDocumento;
                respuesta = cLNConexion.ConsultaClienteReniec(request);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return respuesta;
        }

        [WebMethod]
        public static CEN_RespuestaWSSunat BuscarClienteSunat(string numDocumento)
        {
            CEN_RespuestaWSSunat respuesta = new CEN_RespuestaWSSunat();
            CEN_RequestSunat request = new CEN_RequestSunat();
            CLNConexionServicio cLNConexion = new CLNConexionServicio();
            try
            {
                request.numDocumento = numDocumento;
                respuesta = cLNConexion.ConsultaClienteSunat(request);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return respuesta;
        }


        [WebMethod]
        public static bool ValidarExisteCliente(string numDocumento, int tipoDoc, int tipoPer)
        {
            CLNCliente cliente = new CLNCliente();
            bool estadoClient = true;
            int estadoruc = CENConstante.g_const_0;
            try
            {
                Byte sw = 0;
                if (tipoDoc == CENConstante.g_const_1 && tipoPer == CENConstante.g_const_1 && numDocumento.Length == 8)
                {
                    sw = cliente.buscarDniCliente(Int32.Parse(numDocumento));
                    if (sw == 0)
                        estadoClient = true;
                    else
                        estadoClient = false;
                }

                if (tipoDoc == CENConstante.g_const_3 || tipoPer == CENConstante.g_const_2 || numDocumento.Length == 11)
                {
                    estadoruc = cliente.buscarRUCCliente(numDocumento);
                    if (estadoruc == 0)
                        estadoClient = true;
                    else
                        estadoClient = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return estadoClient;
        }

        [WebMethod]
        public static int registrarCliente(List<CENCliente> cabecera, List<CENPuntoEntrega> puntoEntrega = null)
        {

            try
            {
                //    Console.WriteLine(cliente);
                CLNCliente objcli = new CLNCliente();
                CENCliente objCliente = new CENCliente();
                CENPuntoEntrega objPunto = new CENPuntoEntrega();
                objCliente.lcli = cabecera;
                objPunto.listPuntoEntrega = puntoEntrega;
                return objcli.registrarCliente(objCliente, objPunto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static int editarCliente(List<CENCliente> cabecera, List<CENPuntoEntrega> puntoEntrega = null)
        {

            try
            {
                //    Console.WriteLine(cliente);
                CLNCliente objcli = new CLNCliente();
                CENCliente objCliente = new CENCliente();
                CENPuntoEntrega objPunto = new CENPuntoEntrega();
                objCliente.listCliente = cabecera;
                objPunto.listPuntoEntrega = puntoEntrega;
                return objcli.modificarCliente(objCliente, objPunto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [WebMethod]
        public static int eliminarCliente(int codCliente)
        {

            try
            {
                CLNCliente objcli = new CLNCliente();
                return objcli.eliminarCliente(codCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static List<CENDepartamento> listDepartamento()
        {

            List<CENDepartamento> listDept = null;
            try
            {
                CLNDepartamento departamento = new CLNDepartamento();

                listDept = departamento.ListarDepartamentos(7);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDept;
        }

        [WebMethod]
        public static List<CENProvincia> listProvincia()
        {
            List<CENProvincia> listProv = null;
            try
            {
                CLNProvincia provincia = new CLNProvincia();

                listProv = provincia.ListarProvincias(8);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listProv;
        }

        [WebMethod]
        public static List<CENDistrito> listDistrito()
        {
            List<CENDistrito> listDist = null;
            try
            {
                CLNDistrito distrito = new CLNDistrito();

                listDist = distrito.ListarDistritos(9);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDist;
        }


        [WebMethod]
        public static List<CENCliente> BusquedaCliente(int auxtipoDocumento, string auxNumDocumento, string auxNombres)
        {
            CLNCliente cliente = new CLNCliente();
            List<CENCliente> listCliente = null;

            try
            {
                listCliente = cliente.ListarClientes(auxtipoDocumento, auxNumDocumento, auxNombres);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listCliente;
        }

        [WebMethod]
        public static List<CENPuntoEntrega> BuscarPuntosEntrega(int codPersona)
        {
            CLNCliente cliente = new CLNCliente();
            List<CENPuntoEntrega> GroupuntosEntrega = new List<CENPuntoEntrega>();
            try
            {
                return GroupuntosEntrega = cliente.ListarPuntosEntrega(codPersona);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}