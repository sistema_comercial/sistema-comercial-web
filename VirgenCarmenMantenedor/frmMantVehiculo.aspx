﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantVehiculo.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantVehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="Scripts/DataTable/datatables.min.css" />
    <link rel="stylesheet" href="css/web_general.css" type="text/css" media="screen, projection" />
    
    <link rel="stylesheet" href="icon/style.css" />
    <link rel="stylesheet" href="Scripts/jqueryui/jquery-ui.min.css" />

    <script src="Scripts/DataTable/datatables.min.js"></script>

    <link rel="stylesheet" href="CSS/modal_style.css" />
    <link rel="stylesheet" href="CSS/DataTable.css" />
    <script type="text/javascript" src="Scripts/alertify.min.js"></script> 
    <script type="text/javascript" src="Scripts/sweetalert.min.js"></script>
      
    <script src="Scripts/frmMantVehiculo.js"></script>

    <title></title>

    <style>
        #anchover{
          width: 50% !important;
        }
        #anchover1{
          width: 50% !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                    <h3 class="box-title">MANTENEDOR DE VEHICULOS</h3>
                </div>
                <div class="form-group">
                        <h4 class="box-filtros">Filtros</h4>
                </div>
                <div class="form-group row">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Placa </label>
                        <div class='col-sm-8'>
                            <input type="text" id="idPlaca" class="form-control" maxlength="10"/>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Marca </label>
                        <div class='col-sm-8'>
                            <input type="text" id="idMarca" class="form-control" maxlength="30"/>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Modelo </label>
                        <div class='col-sm-8'>
                            <input type="text" id="idModelo" class="form-control" maxlength="30"/>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Peso Máximo </label>
                        <div class='col-sm-8'>
                            <input type="text" id="idPeso" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="5"/>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Fecha de Fabricación </label>
                        <div class='col-sm-8'>
                            <input type="text" id="idFabricacion" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="4"/>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 label label-default">Tipo de Vehículo </label>
                        <div class='col-sm-8'>
                            <input type="text" id="idTipoVehiculo" class="form-control" maxlength="30"/>
                        </div>
                    </div>
                </div>
                <div class="form-group"  style="text-align:right">
                    <button type="button" class="btn btn-primary" id="idBuscar">Buscar</button>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="form-group" id="Mmensajem">
                <p id="Mpmensajem"></p>
            </div>
            <div class="box-body table-responsive">
                <table id="MSTablePrincipal" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th scope="col" style="text-align: center;vertical-align: middle;">ITEM</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">PLACA</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">MARCA</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">MODELO</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">PESO</th>                            
                            <th scope="col" style="text-align: center;vertical-align: middle;">FABRICACION</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">TIPO VEHICULO</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">ID</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">OPCIONES</th>
                        </tr>
                    </thead>
                    <tbody id="Mtbl_body_table">
                    </tbody>
                    <tfoot>
                        <tr>
                            <th scope="col" style="text-align: center;vertical-align: middle;">ITEM</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">PLACA</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">MARCA</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">MODELO</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">PESO</th>                            
                            <th scope="col" style="text-align: center;vertical-align: middle;">FABRICACION</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">TIPO VEHICULO</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">ID</th>
                            <th scope="col" style="text-align: center;vertical-align: middle;">OPCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal"  data-target="#modalPrincipal" id="btnAgre">Agregar</button>
        </div>
    </div>
    <!--MODAL REGISTRO-->
    <div class="modal fade" id="modalPrincipal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: scroll;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="box-title">REGISTRO - VEHICULOS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form role="form" onsubmit="return false;">
                        
                        <div class="form-group">
                            <h4 class="box-filtros">Ingresar</h4>
                        </div>
                        <div class="form-group row">
                            <div class="form-group" id="Mensaje">
                                <p id="Mpmensaje"></p>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Placa </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="MidPlaca" class="form-control" required="required" maxlength="10"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Marca </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="MidMarca" class="form-control" maxlength="30"/>
                                </div>
                            </div>

                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Modelo </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="MidModelo" class="form-control" maxlength="30"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Peso Máximo </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="MidPeso" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="5"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default"  for="idSelect">Fecha Fabricación </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="MidFabricacion" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="4"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Tipo Vehículo </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="MidTipoVehiculo" class="form-control" maxlength="30"/>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div style="text-align:right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="Mactualizar">Actualizar</button>
                        <button type="button" class="btn btn-primary" id="Mguardar">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL REGISTRO CHOFER-->
    <div class="modal fade" id="modalSecundaria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: scroll;">
        <div class="modal-dialog" id="anchover">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="box-title">ASIGNAR - CONDUCTORES</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form role="form" onsubmit="return false;">

                        <div class="form-group row">
                            <div class="form-group" id="MensajeS">
                                <p id="MpmensajeS"></p>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Vehiculo </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="Mvehiculo" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="form-group"  style="text-align:right">
                                <button type="button" class="btn btn-primary" id="Mbuscar">Buscar</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <table id="TableLista" class="table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th scope="col" style="text-align:center">ITEM</th>
                                        <th scope="col" style="text-align:center">NOMBRE</th>
                                        <th scope="col" style="text-align:center">ID</th>
                                        <th scope="col" style="text-align:center">OPCION</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_mostrar_lista">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col" style="text-align:center">ITEM</th>
                                        <th scope="col" style="text-align:center">NOMBRE</th>
                                        <th scope="col" style="text-align:center">ID</th>
                                        <th scope="col" style="text-align:center">OPCION</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </form>
                    <div style="text-align:right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL VER CHOFER ASIGNADO-->
    <div class="modal fade" id="modalTercera" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: scroll;">
        <div class="modal-dialog" id="anchover1">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="box-title">CONDUCTORES ASIGNADOS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form role="form" onsubmit="return false;">
                        
                        <div class="form-group row">
                            <div class="form-group" id="MensajeT">
                                <p id="MpmensajeT"></p>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 label label-default">Vehiculo </label>
                                <div class='col-sm-8'>
                                    <input type="text" id="Tvehiculo" class="form-control" disabled/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <table id="TableConductor" class="table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th scope="col" style="text-align:center">ITEM</th>
                                        <th scope="col" style="text-align:center">NOMBRE</th>
                                        <th scope="col" style="text-align:center">ID</th>
                                        <th scope="col" style="text-align:center">OPCION</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_mostrar_conductor">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col" style="text-align:center">ITEM</th>
                                        <th scope="col" style="text-align:center">NOMBRE</th>
                                        <th scope="col" style="text-align:center">ID</th>
                                        <th scope="col" style="text-align:center">OPCION</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </form>
                    <div style="text-align:right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
