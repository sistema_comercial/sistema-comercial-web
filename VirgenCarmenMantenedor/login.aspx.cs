﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            Cargar_Sucursales();
            
        }

        [WebMethod]
        public static List<CENUsuario> getSession()
        {
            List<CENUsuario> DataUser;
            CENUsuario user = new CENUsuario();
            try
            {
                DataUser = new List<CENUsuario>();
                user.perfil = System.Web.HttpContext.Current.Session["perfil"].ToString();
                user.nombre = System.Web.HttpContext.Current.Session["nombres"].ToString();
                user.ntraUsuario = Convert.ToInt32(System.Web.HttpContext.Current.Session["codUser"].ToString());
                user.sucursal = System.Web.HttpContext.Current.Session["sucursal"].ToString();
                
                DataUser.Add(user);

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                DataUser = null;
            }
            return DataUser;
        }

        [WebMethod]
        public static Boolean InsertSession(string perfil, string nombre, string sucursal, string codUser, string codPersona, string usuario)
        {
            try
            {
                System.Web.HttpContext.Current.Session["perfil"] = perfil;
                System.Web.HttpContext.Current.Session["nombres"] = nombre;
                System.Web.HttpContext.Current.Session["sucursal"] = sucursal;
                System.Web.HttpContext.Current.Session["codUser"] = codUser;
                System.Web.HttpContext.Current.Session["codPersona"] = codPersona;
                System.Web.HttpContext.Current.Session["usuario"] = usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        [WebMethod]
        public static List<CENUsuario> consultarDatos(string user, string pass, int intentos, int sucursal)
        {
            CLNUsuario objUser = new CLNUsuario();
            List<CENUsuario> datos;
            try
            {
                datos = objUser.credencialesUsuario(user, pass, intentos, sucursal);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return datos;
        }
                     
        [WebMethod]
        public static int CerrarSesionInactividad()
        {
            int respuesta = 2000;
            try
            {
                System.Web.HttpContext.Current.Session["perfil"] = null;
                System.Web.HttpContext.Current.Session["nombres"] = null;
                System.Web.HttpContext.Current.Session["sucursal"] = null;
                System.Web.HttpContext.Current.Session["codUser"] = null;
                System.Web.HttpContext.Current.Session["codPersona"] = null;
                System.Web.HttpContext.Current.Session["usuario"] = null;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static Boolean validarDatos(string user, string pass, int codSucursal)
        {
            CLNUsuario objLogicUsuario = new CLNUsuario();
            CENUsuario objUsuario = new CENUsuario();

            try
            {
                int intentos = 0;

                objUsuario = (CENUsuario)objLogicUsuario.credencialesUsuario(user, pass, intentos, codSucursal)[0];

                if (objUsuario.respuesta == 505)
                {
                    System.Web.HttpContext.Current.Session["perfil"] = objUsuario.perfil;
                    System.Web.HttpContext.Current.Session["nombres"] = objUsuario.nombre;
                    System.Web.HttpContext.Current.Session["sucursal"] = objUsuario.sucursal;
                    System.Web.HttpContext.Current.Session["codUser"] = objUsuario.ntraUsuario;
                    System.Web.HttpContext.Current.Session["codPersona"] = objUsuario.fkcodPersona;
                    System.Web.HttpContext.Current.Session["usuario"] = user;
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return false;
            }
        }

        private void Cargar_Sucursales()
        {
            try
            {
                List<CENSucursalVIEW> listSucursal = new CLNUsuario().cargarSucursal(18);
                sucursal.Items.Clear();
                sucursal.Items.Add(new ListItem("SELECCIONE SUCURSAL", "0"));
                foreach (var itemSucursal in listSucursal)
                {
                    string item = itemSucursal.ntraSucursal.ToString();
                    var descripcion = itemSucursal.descripcion;
                    sucursal.Items.Add(new ListItem(descripcion, item));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }
}