﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNPanelControl
    {

        public List<CENPanelControl> listadoCrecimiento(int flag)
        //DESCRIPCION:Retorna el crecimiento de las vetnas por dia,mes,año
        {
            CADPanelControl cad = new CADPanelControl();
            try
            {
                return cad.listadoCrecimiento(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
