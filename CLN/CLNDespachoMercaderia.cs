﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;
using CAD;

namespace CLN
{
    public class CLNDespachoMercaderia
    {
        public List<CENConceptos> ListarConcepto(int flag)
        {
            CADDespachoMercaderia objCADDespacho = null;

            try
            {
                objCADDespacho = new CADDespachoMercaderia();
                return objCADDespacho.ListarConcepto(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CENDespachoCliente> BuscarCliente(string cliente)
        {
            CADDespachoMercaderia objCADCliente = null;
            try
            {
                objCADCliente = new CADDespachoMercaderia();
                return objCADCliente.BuscarCliente(cliente);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<CENDespachoMercaderia> ListarDatosPorFiltro(CENDespachoDatos datos)
        {
            //DESCRIPCIÓN: función de datos por filtros
            CADDespachoMercaderia objCADDespacho = null;
            try
            {
                objCADDespacho = new CADDespachoMercaderia();
                return objCADDespacho.ListarVentaDespacho(datos);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    
        public List<CENDespachoDetVenta> ListaDetVenta(int codVenta)
        {
            //DESCRIPCIÓN: función para listar detalle de la venta para despacho
            CADDespachoMercaderia objCLNDet = null;          
                     
            try
            {
                objCLNDet = new CADDespachoMercaderia();
                return objCLNDet.ListarDetalleVentaD(codVenta);              
               
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int RegistroDespachoMercaderia(CENRegistroDespachoM objDespacioM)
        {
            CADDespachoMercaderia objCLNRegistroDespachoM = null;
            try
            {
                objCLNRegistroDespachoM = new CADDespachoMercaderia();
                return objCLNRegistroDespachoM.insertarMercaderia(objDespacioM);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
