﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLNCuentaCobro
    {
        public CENCuentaCobro BuscarCuentaPorCobrar(int codVenta, int flag)
        {
            CADCuentaCobro objCADCxC = null;
            try
            {
                objCADCxC = new CADCuentaCobro();
                return objCADCxC.BuscarCuentaPorCobrar(codVenta, flag);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //--------------------------------------------------------------------
        public List<CENCuentas> ListarCuentasCobrar(int codCliente)
        {
            CADCuentaCobro objCADCxC = null;
            try
            {
                objCADCxC = new CADCuentaCobro();
                return objCADCxC.ListarCuentasCobrar(codCliente);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CENCuentas> ListarLetrasCobrar(int codCliente)
        {
            CADCuentaCobro objCADCxC = null;
            try
            {
                objCADCxC = new CADCuentaCobro();
                return objCADCxC.ListarLetrasCobrar(codCliente);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CENCuotas> ListarCuotas(int codPrestamo)
        {
            CADCuentaCobro objCADCxC = null;
            try
            {
                objCADCxC = new CADCuentaCobro();
                return objCADCxC.ListarCuotas(codPrestamo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
