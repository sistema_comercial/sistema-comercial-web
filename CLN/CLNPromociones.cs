﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLNPromociones
    {

        public List<CENPromociones> ListarEstadoPrmocion(int flag)
        {
            CADPromociones objPromociones = null;
            try
            {
                objPromociones = new CADPromociones();

                return objPromociones.ListarEstadoPrmocion(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENPromocionesLista> ListarPromociones(CENPromociones datos)
        {
            CADPromociones objCADPromociones = null;

            try
            {
                objCADPromociones = new CADPromociones();
                return objCADPromociones.ListarPromociones(datos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENVendedor> ListarVendedores(int sucursal)
        {
            try
            {
                return new CADPersona().listarVendedor(sucursal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ElimiarPromocion(CENPromocionesLista objtPromocionesAD)
        {

            CADPromociones objCLNPromociones = null;
            try
            {
                objCLNPromociones = new CADPromociones();
                return objCLNPromociones.ElimiarPromocion(objtPromocionesAD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<CENDetallePromocion> ListarDetalle(int codPromocion)
        {
            CADPromociones objCADPreventa = null;
            try
            {
                objCADPreventa = new CADPromociones();
                return objCADPreventa.ListarDetalle(codPromocion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string InsertarPromociones(CENPromocionesInsert objProduct, CEN_Detalle_Flag_Promocion objDetalle)
        {
            CADPromociones objCLNProduct = null;

            try
            {
                objCLNProduct = new CADPromociones();
                return objCLNProduct.InsertarPromociones(objProduct, objDetalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string actualizarPromocion(CENPromocionesInsert objProduct, CEN_Detalle_Flag_Promocion objDetalle)
        {
            CADPromociones objCLNProduct = null;

            try
            {
                objCLNProduct = new CADPromociones();
                return objCLNProduct.actualizarPromocion(objProduct, objDetalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        


        public int EliminarProductoActualizarPromocion(int codPromo, string codProd)
        {
            CADPromociones objCLNEeliminarDetalle = null;
            try
            {
                objCLNEeliminarDetalle = new CADPromociones();
                return objCLNEeliminarDetalle.EliminarProductoActualizarPromocion(codPromo, codProd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CENProductolista> ListarProductosPromocion(string cadena)
        {
            CADPromociones objCADPromocion = null;

            try
            {
                objCADPromocion = new CADPromociones();
                return objCADPromocion.ListarProductosPromocion(cadena);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<CENConceptos> listarTipoVenta(int flag) {

            CADPreventa cdPrev = new CADPreventa();
            List<CENConceptos> listaTipoVenta = null;

            try
            {
                listaTipoVenta = cdPrev.ListarConcepto(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return listaTipoVenta;
        }
    
 
}
}
