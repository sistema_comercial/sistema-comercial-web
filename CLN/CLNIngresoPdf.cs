﻿using CAD;
using CEN;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CLN
{
    public class CLNIngresoPdf
    {
        public byte[] IngresoPdf(int codIngreso)
        {
            
            CLNIngresoMercaderia ingresoMercaderia = new CLNIngresoMercaderia();
            List<CENIngresoMercaderia> data = null;
            List<CENDetalleIngreso> datadetalle= new List<CENDetalleIngreso>();
            CLNFormatos objCLNFormatos = new CLNFormatos();
            CENConstante cons = new CENConstante();
            MemoryStream ms = null;
            byte[] l_pdfbytes = null;

            try
            {
                data = ingresoMercaderia.listaIngresos(null, codIngreso, CENConstante.g_const_0, CENConstante.g_const_1047);
                datadetalle = ingresoMercaderia.listaIngresoDetalle(codIngreso);

                //listaDetalle = objCADPreventa.ListarDetalle(codventa);


                ms = new MemoryStream();



                Document doc = new Document(PageSize.A4, CENConstante.g_const_30, CENConstante.g_const_30, CENConstante.g_const_30, CENConstante.g_const_30);
                PdfWriter pw = PdfWriter.GetInstance(doc, ms);
                doc.Open();

                //Color de fuentes
                var CPrincipal = new BaseColor(CENConstante.g_const_1, CENConstante.g_const_108, CENConstante.g_const_179);//color tema principal tonalidad azul
                var CSecundario = new BaseColor(CENConstante.g_const_128, CENConstante.g_const_128, CENConstante.g_const_128);//color gris
                var CGenerico = new BaseColor(CENConstante.g_const_0, CENConstante.g_const_0, CENConstante.g_const_0);//color negro
                var CBlanco = new BaseColor(CENConstante.g_const_255, CENConstante.g_const_255, CENConstante.g_const_255);//color blanco
                //fuentes
                Font ftitulo1 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_14, Font.BOLD, CPrincipal);
                Font ftitulo2 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_11, Font.BOLD, CPrincipal);
                Font flabel = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_11, Font.BOLD, CSecundario);
                Font flabe2 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_11, Font.BOLD, CGenerico);
                Font flabe3 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_11, Font.NORMAL, CPrincipal);
                Font fsec10n = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_10, Font.BOLD, CSecundario);
                Font fsec10 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_10, Font.NORMAL, CSecundario);
                Font fcampo1 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_11, Font.NORMAL, CSecundario);
                Font fcampo2 = new Font(Font.FontFamily.HELVETICA, CENConstante.g_const_11, Font.NORMAL, CGenerico);

                Paragraph nuevaLinea = new Paragraph(Chunk.NEWLINE);

                //TITULO
                Paragraph titulo = new Paragraph();
                titulo.Add(new Phrase(CENConstante.g_campo_nombEmpresa, ftitulo1));
                titulo.Alignment = Element.ALIGN_CENTER;
                doc.Add(titulo);

                string RutaBase = HttpContext.Current.Server.MapPath(CENConstante.g_const_tilde) + CENConstante.g_const_pathImg + CENConstante.g_const_pathLogo;
                Image ImagenPDF = Image.GetInstance(RutaBase);
                ImagenPDF.ScalePercent(20f);
                //ImagenPDF.ScaleToFit(125f, 60F);
                ImagenPDF.SetAbsolutePosition(50, 710);
                doc.Add(ImagenPDF);

                //DATOS DE LA DISTRIBUIDORA
                PdfPTable tabla1 = new PdfPTable(CENConstante.g_const_4);
                tabla1.WidthPercentage = CENConstante.g_cons100f;
                //tabla1.TotalWidth = 600f;
                float[] widths_cabecera = new float[] { CENConstante.g_cons25f, CENConstante.g_cons25f, CENConstante.g_cons20f, CENConstante.g_cons30f };
                tabla1.SetWidths(widths_cabecera);
                tabla1.DefaultCell.Border = Rectangle.NO_BORDER;
                tabla1.SpacingBefore = CENConstante.g_cons20f;
                tabla1.SpacingAfter = CENConstante.g_cons20f;

                PdfPCell clFila10 = new PdfPCell(new Phrase(CENConstante.g_const_vacio, ftitulo2));
                clFila10.Border = CENConstante.g_const_0;
                PdfPCell clFila11 = new PdfPCell(new Phrase(CENConstante.g_campo_avenida, ftitulo2));
                //clFila11.BorderWidth = 0.75f;
                //clFila11.FixedHeight = 10f;
                clFila11.Border = CENConstante.g_const_0;
                clFila11.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //PdfPCell clFila12 = new PdfPCell(new Phrase(CENConstante.g_campo_sucursal, ftitulo2));
                PdfPCell clFila12 = new PdfPCell(new Phrase("", ftitulo2));
                //clFila12.BorderWidth = 0.75f;
                //clFila12.FixedHeight = 0f;
                clFila12.Border = CENConstante.g_const_0;
                clFila12.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //PdfPCell clFila13 = new PdfPCell(new Phrase(datosPre.sucursal, ftitulo2));
                PdfPCell clFila13 = new PdfPCell(new Phrase("", ftitulo2));
                //clFila13.BorderWidth = 0.75f;
                //clFila13.FixedHeight = 0f;
                clFila13.Border = CENConstante.g_const_0;
                clFila13.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                PdfPCell clFila20 = new PdfPCell(new Phrase(CENConstante.g_const_vacio, ftitulo2));
                clFila20.Border = CENConstante.g_const_0;
                PdfPCell clFila21 = new PdfPCell(new Phrase(CENConstante.g_campo_urbanizacion, ftitulo2));
                clFila21.Border = CENConstante.g_const_0;
                clFila21.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //PdfPCell clFila22 = new PdfPCell(new Phrase(CENConstante.g_campo_ruc, ftitulo2));
                PdfPCell clFila22 = new PdfPCell(new Phrase("", ftitulo2));
                clFila22.Border = CENConstante.g_const_0;
                clFila22.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                PdfPCell clFila23 = new PdfPCell(new Phrase("10773880579", ftitulo2));
                clFila23.Border = CENConstante.g_const_0;
                clFila23.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                PdfPCell clFila30 = new PdfPCell(new Phrase(CENConstante.g_const_vacio, ftitulo2));
                clFila30.Border = CENConstante.g_const_0;
                PdfPCell clFila31 = new PdfPCell(new Phrase(CENConstante.g_campo_chiclayo, ftitulo2));
                clFila31.Border = CENConstante.g_const_0;
                clFila31.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                PdfPCell clFila32 = new PdfPCell(new Phrase("", ftitulo2));
                //dataVenta.tDoc
                clFila32.Border = CENConstante.g_const_0;
                clFila32.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                PdfPCell clFila33 = new PdfPCell(new Phrase("INGRESO", ftitulo2));
                clFila33.Border = CENConstante.g_const_0;
                clFila33.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                PdfPCell clFila40 = new PdfPCell(new Phrase(CENConstante.g_const_vacio, ftitulo2));
                clFila40.Border = CENConstante.g_const_0;
                PdfPCell clFila41 = new PdfPCell(new Phrase("", ftitulo2));
                clFila41.Border = CENConstante.g_const_0;
                clFila41.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                PdfPCell clFila42 = new PdfPCell(new Phrase("", ftitulo2));
                clFila42.Border = CENConstante.g_const_0;
                clFila42.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                PdfPCell clFila43 = new PdfPCell(new Phrase(""+codIngreso, ftitulo2));
                clFila43.Border = CENConstante.g_const_0;
                clFila43.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                tabla1.AddCell(clFila10);
                tabla1.AddCell(clFila11);
                tabla1.AddCell(clFila12);
                tabla1.AddCell(clFila13);
                tabla1.AddCell(clFila20);
                tabla1.AddCell(clFila21);
                tabla1.AddCell(clFila22);
                tabla1.AddCell(clFila23);
                tabla1.AddCell(clFila30);
                tabla1.AddCell(clFila31);
                tabla1.AddCell(clFila32);
                tabla1.AddCell(clFila33);
                tabla1.AddCell(clFila40);
                tabla1.AddCell(clFila41);
                tabla1.AddCell(clFila42);
                tabla1.AddCell(clFila43);
                doc.Add(tabla1);


                //Datos generales
                //double subtotalPre = (dataVenta.importeTotal - dataVenta.IGV);// - dataVenta.importeRecargo;
                Paragraph titulo2 = new Paragraph(CENConstante.g_campo_datosGen, ftitulo2);
                PdfPTable tabla2 = new PdfPTable(CENConstante.g_const_2);
                tabla2.WidthPercentage = CENConstante.g_cons100f;
                float[] widths_dato1 = new float[] { CENConstante.g_cons25f, CENConstante.g_cons75f };
                tabla2.SetWidths(widths_dato1);
                tabla2.DefaultCell.Border = Rectangle.NO_BORDER;
                tabla2.SpacingBefore = CENConstante.g_cons10f;

                PdfPCell cl2Fila11 = new PdfPCell(new Phrase(CENConstante.g_campo_fechaIngreso, fsec10n));
                cl2Fila11.Border = CENConstante.g_const_0;
                cl2Fila11.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                PdfPCell cl2Fila12 = new PdfPCell(new Phrase(data[0].fechaIngreso, fsec10));
                cl2Fila12.Border = CENConstante.g_const_0;
                cl2Fila12.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                PdfPCell cl2Fila13 = new PdfPCell(new Phrase(CENConstante.g_campo_motivoIngreso, fsec10n));
                cl2Fila13.Border = CENConstante.g_const_0;
                cl2Fila13.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                PdfPCell cl2Fila14 = new PdfPCell(new Phrase(data[0].descMotivo, fsec10));
                cl2Fila14.Border = CENConstante.g_const_0;
                cl2Fila14.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                PdfPCell cl2Fila21 = new PdfPCell(new Phrase(CENConstante.g_campo_proveedor, fsec10n));
                cl2Fila21.Border = CENConstante.g_const_0;
                cl2Fila21.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;

                PdfPCell cl2Fila22 = new PdfPCell(new Phrase(data[0].descProveedor, fsec10));
                cl2Fila22.Border = CENConstante.g_const_0;
                cl2Fila22.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                tabla2.AddCell(cl2Fila11);
                tabla2.AddCell(cl2Fila12);
                tabla2.AddCell(cl2Fila13);
                tabla2.AddCell(cl2Fila14);
                tabla2.AddCell(cl2Fila21);
                tabla2.AddCell(cl2Fila22);
                doc.Add(titulo2);
                doc.Add(tabla2);



                //Detalle de productos del pedido
                Paragraph titulo4 = new Paragraph(CENConstante.g_campo_detaProd, ftitulo2);
                PdfPTable tabla4 = new PdfPTable(CENConstante.g_const_6);
                tabla4.HeaderRows = CENConstante.g_const_1;
                tabla4.WidthPercentage = CENConstante.g_cons100f;
                float[] widths_productos = new float[] {
                    CENConstante.g_cons5f, CENConstante.g_cons10f,
                    CENConstante.g_cons15f, CENConstante.g_cons10f,
                    CENConstante.g_cons10f, CENConstante.g_cons10f };
                tabla4.SetWidths(widths_productos);
                tabla4.DefaultCell.Border = Rectangle.NO_BORDER;
                tabla4.SpacingBefore = CENConstante.g_cons10f;
                tabla4.SpacingAfter = CENConstante.g_cons20f;
                PdfPCell cl4Fila11 = new PdfPCell();
                cl4Fila11 = objCLNFormatos.FormatearCeldaTexto(CENConstante.g_campo_tblnumeral, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons35f, CPrincipal, CBlanco, CPrincipal, CPrincipal, CPrincipal, CBlanco, CENConstante.g_const_1);
                PdfPCell cl4Fila12 = new PdfPCell();
                cl4Fila12 = objCLNFormatos.FormatearCeldaTexto(CENConstante.g_campo_tblsku, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons35f, CPrincipal, CBlanco, CPrincipal, CPrincipal, CPrincipal, CBlanco, CENConstante.g_const_1);
                PdfPCell cl4Fila13 = new PdfPCell();
                cl4Fila13 = objCLNFormatos.FormatearCeldaTexto(CENConstante.g_campo_tbldescrip, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons35f, CPrincipal, CBlanco, CPrincipal, CPrincipal, CPrincipal, CBlanco, CENConstante.g_const_1);
                PdfPCell cl4Fila14 = new PdfPCell();
                cl4Fila14 = objCLNFormatos.FormatearCeldaTexto(CENConstante.g_campo_tblcant, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons35f, CPrincipal, CBlanco, CPrincipal, CPrincipal, CPrincipal, CBlanco, CENConstante.g_const_1);
                PdfPCell cl4Fila15 = new PdfPCell();
                cl4Fila15 = objCLNFormatos.FormatearCeldaTexto(CENConstante.g_campo_fechaVencimiento, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons35f, CPrincipal, CBlanco, CPrincipal, CPrincipal, CPrincipal, CBlanco, CENConstante.g_const_1);
                PdfPCell cl4Fila16 = new PdfPCell();
                cl4Fila16 = objCLNFormatos.FormatearCeldaTexto(CENConstante.g_campo_tblalmacen_destino, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons35f, CPrincipal, CBlanco, CPrincipal, CPrincipal, CPrincipal, CBlanco, CENConstante.g_const_1);
                tabla4.AddCell(cl4Fila11);
                tabla4.AddCell(cl4Fila12);
                tabla4.AddCell(cl4Fila13);
                tabla4.AddCell(cl4Fila14);
                tabla4.AddCell(cl4Fila15);
                tabla4.AddCell(cl4Fila16);
                //for de los productos, promociones y descuentos
                int posicion = CENConstante.g_const_0;
                foreach (CENDetalleIngreso detalle in datadetalle)
                {
                    posicion = posicion + CENConstante.g_const_1;
                    cl4Fila11 = objCLNFormatos.FormatearCeldaTexto(posicion.ToString(), CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons20f, CBlanco, CSecundario, CBlanco, CBlanco, CBlanco, CBlanco, CENConstante.g_const_1);
                    cl4Fila12 = objCLNFormatos.FormatearCeldaTexto(detalle.codProducto, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons20f, CBlanco, CSecundario, CBlanco, CBlanco, CBlanco, CBlanco, CENConstante.g_const_1);
                    cl4Fila13 = objCLNFormatos.FormatearCeldaTexto(detalle.descProducto, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons20f, CBlanco, CSecundario, CBlanco, CBlanco, CBlanco, CBlanco, CENConstante.g_const_1);
                    cl4Fila14 = objCLNFormatos.FormatearCeldaTexto(detalle.cantidadProducto.ToString(), CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons20f, CBlanco, CSecundario, CBlanco, CBlanco, CBlanco, CBlanco, CENConstante.g_const_1);
                    cl4Fila15 = objCLNFormatos.FormatearCeldaTexto(detalle.fechaVencimiento, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons20f, CBlanco, CSecundario, CBlanco, CBlanco, CBlanco, CBlanco, CENConstante.g_const_1);
                    cl4Fila16 = objCLNFormatos.FormatearCeldaTexto(detalle.descAlmacen, CENConstante.consARIAL, CENConstante.g_const_8, CENConstante.g_const_1, CENConstante.g_cons075f, CENConstante.g_cons20f, CBlanco, CSecundario, CBlanco, CBlanco, CBlanco, CBlanco, CENConstante.g_const_1);

                    tabla4.AddCell(cl4Fila11);
                    tabla4.AddCell(cl4Fila12);
                    tabla4.AddCell(cl4Fila13);
                    tabla4.AddCell(cl4Fila14);
                    tabla4.AddCell(cl4Fila15);
                    tabla4.AddCell(cl4Fila16);
                }
                doc.Add(titulo4);
                doc.Add(tabla4);


                doc.Close();

                l_pdfbytes = ms.ToArray();
                ms.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return l_pdfbytes;
        }


        public string FormatoDecimal(double monto)
        {
            return string.Format(CENConstante.g_const_formredini + Math.Abs(CENConstante.g_const_2) + CENConstante.g_const_formredfin, monto);

        }
    }
}
