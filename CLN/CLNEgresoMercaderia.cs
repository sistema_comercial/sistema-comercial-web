﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNEgresoMercaderia
    {

        public string guardarEgreso(CENEgresoMercaderia cabecera, CENDetalleEgreso detalle)
        {
            try
            {
                CADEgresoMercaderia cad = new CADEgresoMercaderia();

                return cad.registrarEgreso(cabecera, detalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string editarEgreso(CENEgresoMercaderia cabecera, CENDetalleEgreso detalle)
        {
            try
            {
                CADEgresoMercaderia cad = new CADEgresoMercaderia();

                return cad.editarEgreso(cabecera, detalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENEgresoMercaderia> listaEgresos(string fechaEgreso, int codEgreso, int motivo)
        {
            try
            {
                CADEgresoMercaderia cad = new CADEgresoMercaderia();
                return cad.listadoEgresoMercaderia(fechaEgreso, codEgreso, motivo);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENDetalleEgreso> listaEgresoDetalle(int codigo)
        {
            try
            {
                CADEgresoMercaderia cad = new CADEgresoMercaderia();
                return cad.listEgresoDetalle(codigo);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string eliminarEgreso(int codigoEgreso)
        {
            CADEgresoMercaderia cad = new CADEgresoMercaderia();
            try
            {
                return cad.eliminarEgreso(codigoEgreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] generarPdf(int codEgreso)
        {
            try
            {
                CLNEgresoMercaderiaPDF pdf = new CLNEgresoMercaderiaPDF();
                return pdf.EgresoPdf(codEgreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
