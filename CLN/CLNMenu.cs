﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNMenu
    {
        public List<CENModulo> cargarModulos(int codUsuario)
        //DESCRIPCION:Lista todos los modulos registrados del perfil asociado al usuario
        {
            List<CENModulo> datos = new List<CENModulo>();
            CADMenu cad = new CADMenu();
            try
            {
                return datos = cad.cargarModulos(codUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENMenu> cargarMantenedoresPorPerfil(int codUsuario, string nomModulo)
        //DESCRIPCION: lista los mantenedores asociados a cada perfil
        {
            List<CENMenu> datos = new List<CENMenu>();
            CADMenu cad = new CADMenu();
            try
            {
                return datos = cad.cargarMantenedoresPorPerfil(codUsuario, nomModulo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENMenu> cargarMantenedoresPorPerfil(int codUsuario, int codModulo)
        //DESCRIPCION: lista los mantenedores asociados a cada perfil
        {
            List<CENMenu> datos = new List<CENMenu>();
            CADMenu cad = new CADMenu();
            try
            {
                return datos = cad.cargarMantenedoresPorPerfil(codUsuario, codModulo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
