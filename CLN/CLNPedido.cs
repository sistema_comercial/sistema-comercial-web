﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNPedido
    {


        public string guadarPedido(CENCabeceraPedido cabeceraPedidos, CENDetallePedido detallePedidos) {
            string mensaje = null;
            try {
                CADPedidos cad = new CADPedidos();

                mensaje = cad.guardarPedido(cabeceraPedidos, detallePedidos);
            }
            catch (Exception ex) {
                throw ex;
            }

            return mensaje;
        }

        public List<CENProductosInsert> listProductos(int flag) {
            List<CENProductosInsert> lista = null;
            try {
                CADProducto cad = new CADProducto();
                return lista = cad.listaProductoFlag(flag);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public List<CENDetalleAlmacen> listAlmacenes(int flag) {
            List<CENDetalleAlmacen> lista;
            CADPedidos cad = new CADPedidos();
            try {
                return lista = cad.listAlmacenes(flag);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public List<CENCabeceraPedido> listaPedido(string fechaPedido, string fechaEntrega, string codProducto, int codProvedor,int comprobante) {
            List<CENCabeceraPedido> listaPedidos = new List<CENCabeceraPedido>();
            CADPedidos CAD = new CADPedidos();
            try {
                return listaPedidos = CAD.listaPedido(fechaPedido,fechaEntrega,codProducto,codProvedor,comprobante);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public string eliminarPedido(int codigoPedido) {
            CADPedidos cad = new CADPedidos();
            try {
                return cad.eliminarPedido(codigoPedido);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        public List<CENDetallePedido> buscarDetallePedido(int codigoPedido)
        {
            CADPedidos cad = new CADPedidos();
            List<CENDetallePedido> lista;
            try
            {
                return lista= cad.listaDetalle(codigoPedido);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string editarPedido(CENCabeceraPedido cabeceraPedidos, CENDetallePedido detallePedidos) {
            CADPedidos cd = new CADPedidos();
            try {
                return cd.editarPedido(cabeceraPedidos, detallePedidos);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public  byte[] generarPdf(int codigoPedido) {
            try {
                CLNPedidosPDF pdf = new CLNPedidosPDF();
                return pdf.PedidosPdf(codigoPedido);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
