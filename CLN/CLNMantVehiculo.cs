﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLNMantVehiculo
    {
        public int RegistrarVehiculo(CENMantVehiculo objtSOP)
        {
            CADMantVehiculo objCAD = null;
            try
            {
                objCAD = new CADMantVehiculo();
                return objCAD.RegistrarVehiculo(objtSOP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENMantVehiculo> ObtenerVehiculo(CENMantVehiculo objtSOP)
        {
            CADMantVehiculo objCLNSO = null;
            try
            {
                objCLNSO = new CADMantVehiculo();
                return objCLNSO.ObtenerVehiculo(objtSOP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int EliminarVehiculo(string CODIGO)
        {

            CADMantVehiculo objCAD = null;
            try
            {
                objCAD = new CADMantVehiculo();
                return objCAD.EliminarVehiculo(CODIGO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ModificarVehiculo(CENMantVehiculo objtSOP)
        {
            CADMantVehiculo objCAD = null;
            try
            {
                objCAD = new CADMantVehiculo();
                return objCAD.ModificarVehiculo(objtSOP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENMantConductorVehiculo> ListarConductores(int Jvehiculo)
        {
            CADMantVehiculo objCLNSO = null;
            try
            {
                objCLNSO = new CADMantVehiculo();
                return objCLNSO.ListarConductores(Jvehiculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RegistrarConductores(CENMantConductorVehiculo objtSOP)
        {
            CADMantVehiculo objCAD = null;
            try
            {
                objCAD = new CADMantVehiculo();
                return objCAD.RegistrarConductores(objtSOP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENMantConductorVehiculo> ListarConductoresAsignados(int Jvehiculo)
        {
            CADMantVehiculo objCLNSO = null;
            try
            {
                objCLNSO = new CADMantVehiculo();
                return objCLNSO.ListarConductoresAsignados(Jvehiculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int EliminarRegistro(string CODIGO)
        {

            CADMantVehiculo objCAD = null;
            try
            {
                objCAD = new CADMantVehiculo();
                return objCAD.EliminarRegistro(CODIGO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
