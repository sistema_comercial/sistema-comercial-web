﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNIngresoMercaderia
    {


        public string guardarIngreso(CENIngresoMercaderia cabecera , CENDetalleIngreso detalle) {
            try {
                CADIngresoMercaderia cad = new CADIngresoMercaderia();

                return cad.guardarPedido(cabecera,detalle);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public string editarIngreso(CENIngresoMercaderia cabecera, CENDetalleIngreso detalle)
        {
            try
            {
                CADIngresoMercaderia cad = new CADIngresoMercaderia();

                return cad.editarIngreso(cabecera, detalle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENIngresoMercaderia> listaIngresos(string fechaIngreso, int ingreso,int proveedor,int motivo) {
            try
            {
                CADIngresoMercaderia cad = new CADIngresoMercaderia();
                return cad.listaIngresos(fechaIngreso,ingreso,proveedor,motivo);

            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public List<CENDetalleIngreso> listaIngresoDetalle(int codigo)
        {
            try
            {
                CADIngresoMercaderia cad = new CADIngresoMercaderia();
                return cad.listaIngresoDetalle(codigo);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string eliminarIngreso(int codigoIngreso)
        {
            CADIngresoMercaderia cad = new CADIngresoMercaderia();
            try
            {
                return cad.eliminarIngreso(codigoIngreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] generarPdf(int codigoIngreso)
        {
            try
            {
                CLNIngresoPdf pdf = new CLNIngresoPdf();
                return pdf.IngresoPdf(codigoIngreso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
