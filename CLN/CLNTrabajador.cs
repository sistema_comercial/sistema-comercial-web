﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;
using CAD;

namespace CLN
{
    public class CLNTrabajador
    {
        public int registrarTrabajador(CENTrabajador data)
        {
            try
            {
                int codPersona;

                CADTrabajador consulta = new CADTrabajador();
                codPersona = consulta.registrarTrabajador(data);
                return codPersona;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENTrabajadorVIEW> ListarTrabajador(int codPersona, int codEstado, int codCargo)
        {
            try
            {
                List<CENTrabajadorVIEW> salida = new List<CENTrabajadorVIEW>();
                CADTrabajador consulta = new CADTrabajador();

                salida = consulta.ListarTrabajador(codPersona,codEstado,codCargo);
                return salida;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
